-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2013 at 12:56 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `inst_annual_plan`
--

CREATE TABLE IF NOT EXISTS `inst_annual_plan` (
  `iap_id` int(11) NOT NULL AUTO_INCREMENT,
  `iip_currentlevel` varchar(255) DEFAULT NULL,
  `iip_po_entrepreneurcreated` int(11) DEFAULT NULL,
  `iip_po_entrepreneursupported` int(11) DEFAULT NULL,
  `iip_po_jobscreated` int(11) DEFAULT NULL,
  `iip_po_other` varchar(255) DEFAULT NULL,
  `iip_dkici_humanresources` varchar(255) DEFAULT NULL,
  `iip_dkici_infrastructureplatforms` varchar(255) DEFAULT NULL,
  `iip_dkici_programsnactivities` varchar(255) DEFAULT NULL,
  `iip_dkici_other` varchar(255) DEFAULT NULL,
  `gs_targeted_level` varchar(255) DEFAULT NULL,
  `gs_po_entrpnrcreated` int(11) DEFAULT NULL,
  `gs_po_entrpnrsupported` int(11) DEFAULT NULL,
  `gs_po_jobscreated` int(11) DEFAULT NULL,
  `gs_po_other` varchar(255) DEFAULT NULL,
  `gs_oicg_humanresources` varchar(255) DEFAULT NULL,
  `gs_oicg_infrastructureplatforms` varchar(255) DEFAULT NULL,
  `gs_oicg_programsnactivities` varchar(255) DEFAULT NULL,
  `gs_oicg_other` varchar(255) DEFAULT NULL,
  `trainingplan` varchar(255) DEFAULT NULL,
  `tc_course` varchar(255) DEFAULT NULL,
  `tc_startdate` datetime DEFAULT NULL,
  `tc_targetedparticipants` int(11) DEFAULT NULL,
  `tc_trainer1` varchar(255) DEFAULT NULL,
  `tc_trainer2` varchar(255) DEFAULT NULL,
  `inst_annual_plancol` varchar(45) DEFAULT NULL,
  `tc_noofparticipants` int(11) DEFAULT NULL,
  `tc_result` varchar(255) DEFAULT NULL,
  `attractingexternalexperts` varchar(255) DEFAULT NULL,
  `eep_list` varchar(255) DEFAULT NULL,
  `eep_noofsessions` int(11) DEFAULT NULL,
  `infrastructuremilestones` varchar(255) DEFAULT NULL,
  `ecellmilestones_dropdown` varchar(255) DEFAULT NULL,
  `ecellmilestones_date` datetime DEFAULT NULL,
  `ecellmilestones_otheractioncomments` varchar(255) DEFAULT NULL,
  `edcmilestones_dropdown` varchar(255) DEFAULT NULL,
  `edcmilestones_date` datetime DEFAULT NULL,
  `edcmilestones_otheractioncomments` varchar(255) DEFAULT NULL,
  `idecesc_dropdown` varchar(255) DEFAULT NULL,
  `idecesc_date` datetime DEFAULT NULL,
  `idecesc_otheractioncomments` varchar(255) DEFAULT NULL,
  `svmu_dropdown` varchar(255) DEFAULT NULL,
  `svmu_date` datetime DEFAULT NULL,
  `svmu_otheractioncomments` varchar(255) DEFAULT NULL,
  `incubator_dropdown` varchar(255) DEFAULT NULL,
  `incubator_date` datetime DEFAULT NULL,
  `incubator_otheractioncomments` varchar(255) DEFAULT NULL,
  `im_other_date` datetime DEFAULT NULL,
  `im_other_text` varchar(255) DEFAULT NULL,
  `im_other_otheractioncomments` varchar(255) DEFAULT NULL,
  `activityplan` varchar(255) DEFAULT NULL,
  `ac_dropdown` varchar(255) DEFAULT NULL,
  `ac_noofinstances` int(11) DEFAULT NULL,
  `ac_name` varchar(255) DEFAULT NULL,
  `ac_planneddate` datetime DEFAULT NULL,
  `ac_audience` varchar(255) DEFAULT NULL,
  `ac_otheractioncomments` varchar(255) DEFAULT NULL,
  `leadfacultysignoff_yesno` int(11) DEFAULT NULL,
  `leadfacultysignoff_name` varchar(255) DEFAULT NULL,
  `leadfacultysignoff_date` datetime DEFAULT NULL COMMENT '		',
  `nensignoff_yesno` int(11) DEFAULT NULL,
  `nensignoff_name` varchar(255) DEFAULT NULL,
  `nensignoff_date` datetime DEFAULT NULL,
  `instituteheadsignoff_yesno` int(11) DEFAULT NULL,
  `instituteheadsignoff_name` varchar(255) DEFAULT NULL,
  `instituteheadsignoff_date` datetime DEFAULT NULL,
  PRIMARY KEY (`iap_id`),
  UNIQUE KEY `eep_list_UNIQUE` (`eep_list`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for institute annual planning exercise' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
