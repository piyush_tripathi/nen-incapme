<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
                        var selectedid=$('#inst').val();
                        var param={id:selectedid};
                         $.getJSON("script.php",param,function(data,status,xhr)//getJSON(url,data,callback function)
             {
                                        $.each(data,function(i,data){
                                                $("#facdrpdwn").append("<option>"+data.first_name+"&nbsp;"+data.last_name+"</option>");         
                                        }); 
             });
                        $("#inst").change(function(){
                                
                                        var crntid=$('#inst').val();
                                        //alert(crntid);
                                        var prm={id:crntid};
                                        $.getJSON("script.php",prm,function(data,status,xhr)
                                         { 
										 console.log(data);
                                                 $("#facdrpdwn").replaceWith("<select id='facdrpdwn' name='faculty'></select>");
                                                        $.each(data,function(i,data){
                                                                        $("#facdrpdwn").append("<option value='"+data.id+"'>"+data.first_name+"&nbsp;"+data.last_name+"</option>"); 
                                                        }); 
                                         });
                                
                                });
        });
</script>

<?php

require "DatabaseFunctions.php";

//Given institute info
$instituteId = '2';
$instituteName = '';

function getFaculties($instituteid) {
     global $db;
     $q = "Select f.first_name, f.last_name from faculty f join institute i on (f.institute_id=i.institute_id) where i.institute_id=$instituteid";
        $data=array();
     $result=mysql_query($q);
     while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
          $data[]=$row;               
     }
     return $data;                      
}


function getInstitutes(){
        global $db;// to access $db inside getInstitutes function
        $query="select id, name from institute";// query to select id, name from institute table
        $result=mysql_query($query);//executing the query
		$data=array();//declaring the data array
     while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
          $data[]=$row;               
     }
     ?>
	 <form name='FactultyForm' method='post'>
	 <?php
     $htm="<select id='inst' name='inst'>";//<select id='inst'>
	 $htm.="<option value=''>Select an Institute</option>";
     $selectedValue='';     
     foreach($data as $item) {
        $selected = ($item['id'] == $selectedValue) ? ' selected ' :''; //if(id==$selectedvalue) $selected='selected'; else $selected=''
                     $htm.="<option  value = \"".$item['id']."\" $selected>".$item['name']."</option>";//<option value='id' selected/''>name</option>   
     }
     $htm.= "</select>";
         return $htm;
}

        


$institutes= getInstitutes();

echo $institutes;//<select><option></option></select>


$html="<select id='facdrpdwn' name='faculty'></select>";
echo $html;
?>
<input type='submit' name='submit' value='View Report'/>
</form>
<?php
if(isset($_POST['submit'])){//CODE AFTER SUBMISSION OF THE FORM
$institute_id=$_POST['inst'];
$faculty_id=$_POST['faculty'];
//QUERY TO GET THE PERFORMANCE DETAILS WE ARE HERE GRABBING THE TYPE OF ACTIVITY PARTICIPANT(category), ACTIVITY TYPE, START TIME OF THE ACTIVITY BASED ON THE PARTICIPANT TYPE = student
$sel="SELECT x.category,y.activity_id id,y.activityinfo_type type,y.activityplacetime_date_start date
FROM activity_participants x  
LEFT JOIN activity y on x.activity_id  = y.activity_id  
WHERE x.activity_id IN (SELECT activity_id FROM activity WHERE activity_id IN (SELECT activity_id FROM activity_contributor_corodinator WHERE human_id = $faculty_id) )
AND x.category = 'student'";
//echo $sel;
$records=fetchRecords($sel);
date_default_timezone_set('Asia/Calcutta');
$current_year=date('Y',time());
$count_events=count($records);

?>
<table border='0' cellspacing='2px' cellpadding='5px'>
<tr  class='head'><th class='mainhead'>Performance Report</th><th>Total Events</th></tr>
<?php
if($count_events){// IF THERE ARE ANY RECORDS BASED ON THE QUERY
$activity_id = $types = array();
$entrepreneurship_courses=0;
$student_courses=0;
$students_supported= $count_events; // STUDENTS SUPPORTED WOULD BE THE NUMBER OF STUDENTS ATTENDING THE ACTIVITY
foreach($records as $record){ 
$date=$record['date']; //  THIS IS TO CHECK WHETHER THE ACTIVITY TOOK PLACE IN THIS YEAR OR NOT.
$arr=explode("-",$date);
$year=$arr[0];// GETTING YEAR HERE
if($year == $current_year){
$activity_id[$record['id']] = $record['type'];// DUMPING ALL THE ACTIVITIES INTO activity_id ARRAY 
if($record['type']=='Course')// IF THE ACTIVITY TYPE = COURSE, INCREASE THE STUDENTS ATTENDING THE COURSE ACTIVITY.
$student_courses++;
}
}

if(in_array('Course',$activity_id)){ // IF THERE IS AN ACTIVITY COURSE, TAKE A COUNT OF ALL OF THEM HAPPENING WITH A PARTICULAR ACTIVITY ID
$types = array_count_values($activity_id);
$entrepreneurship_courses = $types['Course'];
}

?>
<tr class='data'><th class='subhead'>Activity Engagement</th>
			<td align='center'><?php echo count($activity_id);?></td>
			
</tr>
<tr class='data'><th class='subhead'>Students Supported</th>
			<td align='center'><?php echo $students_supported;?></td>
			
</tr>
<tr class='nodata'><th>Student-Entrepreneurs Mentored via SVMU</th>
			<td> </td>
						
</tr>
<tr class='nodata'><th>Entrepreneurs Mentored via SVMU</th>
			<td> </td>
</tr>
<tr class='data'><th class='subhead'>Entrepreneurship Courses taught</th>
			<td align='center'><?php echo $entrepreneurship_courses;?></td>
			
</tr>
<tr class='data'><th class='subhead'>Students attending courses</th>
			<td align='center'><?php echo $student_courses;?></td>
			
</tr>


<?php
//echo $institute_id,"::",$faculty_id;
}
else//IF NO DATA IS PRESENT SHOW THIS TABLE
{
?>
<tr class='data'><th class='subhead'>Activity Engagement</th>
				<th colspan='14' rowspan='6' align=''>This Faculty Has No Updated Information On Any Activities</th>
</tr>
<tr class='data'><th class='subhead'>Students Supported</th>
				
</tr>
<tr class='nodata'><th>Student-Entrepreneurs Mentored via SVMU</th>
				
</tr>
<tr class='nodata'><th>Entrepreneurs Mentored via SVMU</th>
				
</tr>
<tr class='data'><th class='subhead'>Entrepreneurship Courses taught</th>
				
</tr>
<tr class='data'><th class='subhead'>Students attending courses</th>
				
</tr>

<?php
}
echo "</table>";

?>
<table border='0' cellspacing='2px' cellpadding='5px' width='725'>
<tr  class='mainhead'><th colspan='2'>Leadership Report</th></tr>
<tr class="head"><th colspan='2'>Organisational Responsibilities List</th></tr>
<?php 
//QUERY TO SHOW THE ADDITIONAL RESPONSIBILITIES THE FACULTY IS TAKING UP

$sql = "select caf.human_id caf,cc.ccteam_fac cctf, a.over_lead ecellol, b.over_second ecellsl, c.edcteam_lf
 edclf, d.edcteam_colead edccolead, e.td_leadfacname esclf,f.td_coleadfacname esccolead, h.td_leadfacname
 iedclf, i.td_coleadfacname iedccolead, j.ov_leadfaculty svmulf from human_resource hr
    left join campus_additional_faculty caf on hr.id = caf.human_id
	left join  campus_company cc on hr.id = cc.ccteam_fac
    left join ecell a on hr.id = a.over_lead
    left join ecell b on hr.id = b.over_second
    left join edc c on hr.id = c.edcteam_lf
    left join edc d on hr.id = d.edcteam_colead
    left join esc e on hr.id = e.td_leadfacname
	left join esc f on hr.id = f.td_coleadfacname
    left join iedc h on hr.id = h.td_leadfacname
	left join iedc i on hr.id = i.td_coleadfacname
	left join svmu j on hr.id = j.ov_leadfaculty
	where hr.id = $faculty_id";
$records = fetchRecords($sql);

foreach($records as $record){
	if($record['caf'])
	$responsibilities['CampusCompany'] = 'Lead';
	if($record['cctf'])
	$responsibilities['CampusCompany'] = 'Second Lead';
	if($record['ecellol'])
	$responsibilities['Ecell'] = 'Lead';
	if($record['ecellsl'])
	$responsibilities['Ecell'] = 'Second Lead';
	if($record['edclf'])
	$responsibilities['EDC'] = 'Lead';
	if($record['edccolead'])
	$responsibilities['EDC'] = 'Co Lead';
	if($record['esclf'])
	$responsibilities['ESC'] = 'Lead';
	if($record['esccolead'])
	$responsibilities['ESC'] = 'Co Lead';
	if($record['iedclf'])
	$responsibilities['IEDC'] = 'Lead';
	if($record['iedccolead'])
	$responsibilities['IEDC'] = 'Co Lead';
	if($record['svmulf'])
	$responsibilities['SVMU'] = 'Lead';
}
?>
<?php 
if(isset($responsibilities)){
// STORING RESPONSIBILITIES IN A SESSION SO THAT WE CAN USE IT IN ANOTHER REPORT WHICH IS DEPENDENT ON THIS FIELD.
$_SESSION['Responsibilities'] = $responsibilities;
foreach($responsibilities as $K => $V){?>
<tr class="data"><th colspan='2'><?php echo $V; ?> at <?php echo $K; ?></th></tr>
<?php } } else {?>
<tr><th>No Additional Responsibilities</th></tr>
<?php } ?>
</table>
<?php } ?>
<style type='text/css'>
.head{background:#6495ED;}
.data{background:#F5F5DC}
.nodata{background:#C0C0C0;color:red}
.mainhead{background:pink;}
.subhead{background:#FFA07A;color:sienna}
</style>
<title>Factulty Activity Report</title>