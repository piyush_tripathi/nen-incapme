<?php 
//include the database functions file
require "DatabaseFunctions.php";
?>
<form name='Institute Report' method='post'>
<select name='InstituteDetails'>
	<option value=''>Select An Institute</option>
	<?php $sel="SELECT id,name FROM institute";
	$records=fetchRecords($sel);
	foreach($records as $record){?>
	<option value='<?php echo $record['id'].",".$record['name']; ?>'><?php echo $record['name']; ?></option>
	<?php }?>
</select>
<input type='submit' name='submit' value='View Report'/>
</form>
<?php
// Code after the view report Button is Hit
if(isset($_POST['submit'])){
$institute_details=$_POST['InstituteDetails'];
$arr=explode(',',$institute_details);
$institute_id=$arr[0];
$institute_name=$arr[1];
echo "<h2 align='center' style='color:Green'>Institute Report for '$institute_name':</h2>";//Institute Report Heading
//*************************************************************************************************************************
//**************Institute Report: Number of Entrepreneurship Faculty ; E-Leaders ; E-Club Members**************************
//*************************************************************************************************************************
$ent=$eleaders=$eclub=0;
$sel_entrepreneur_faculty=" SELECT COUNT(*) total FROM human_resource h JOIN human_institute hi ON (h.id=hi.human_id) WHERE hi.institute_id=$institute_id AND h.Entrepreneur=1";
$records=fetchRecords($sel_entrepreneur_faculty);
$ent=$records[0]['total'];
$sel_eleaders="SELECT COUNT(*) total FROM `ecell_over_mem` m join ecell e on (m.ecell_id = e.id) WHERE e.institute_id = $institute_id AND m.is_e_leader=1";
$records=fetchRecords($sel_eleaders);

$eleaders=$records[0]['total'];

?>
<table width='700' cellpadding='5' cellspacing='2' border='0' align='center'>
<tr class='mainhead'><th rowspan='13' valign='top' align='center' >People & Activities:</th></tr>
<tr class='head'><th >People</th><th>Number</th></tr>
<tr class='data'><td >Entrepreneur Faculty</td><td align='center'><?php if($ent) echo $ent; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >E-Leaders</td><td align='center'><?php if($eleaders) echo $eleaders; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >E-Club Members</td><td align='center'><?php if($eclub) echo $eclub; else echo "<span class='nodata'>0</span>";?></td></tr>
<?php
//*************************************************************************************************************************
//**************************Institute Report: Number of Participants in Activity*******************************************
//*************************************************************************************************************************
$num1=$num2=$num3=0;
$sel_activity_participants="SELECT a.activityinfo_type Activity,a.activitybeneficiaries_number Participants,a.activity_id Id,i.name FROM activity a JOIN institute i ON a.institute_id=i.id WHERE institute_id=$institute_id";
$records=fetchRecords($sel_activity_participants);
?>
<tr class='head'><th>Activities</th><th>Number Of Participants</th></tr>
<?php
//$id1=$id2=$id3=$activity_name1=$activity_name2=$activity_name3='';
foreach($records as $record){
if($record['Activity']=="Lecture/ Talk" || $record['Activity']=="Panel Discussion"){
//$activity_name1.=$record['Activity'].",";
$num1+=$record['Participants'];
//$id1.=$record['Id'].",";
}
if($record['Activity']=="Course" || $record['Activity']=="Workshop"){
//$activity_name2.=$record['Activity'].",";
$num2+=$record['Participants'];
//$id2.=$record['Id'].",";
}
if($record['Activity']=="Conference" || $record['Activity']=="Competition" || $record['Activity']=="Outreach"){
//$activity_name3.=$record['Activity'].",";
$num3+=$record['Participants'];
//$id3.=$record['Id'].",";
}
}
?>
<tr class='data'>
	<td>Lectures, Talks & Panel Discussions</td>
	<td align='center'><?php if($num1) echo $num1; else echo "<span class='nodata'>0</span>";?></td>
</tr>
<tr class='data'>
	<td>Courses & Workshops</td>
	<td align='center'><?php if($num2) echo $num2; else echo "<span class='nodata'>0</span>";?></td>
</tr>
<tr class='data'>
	<td>Competitions,Conferences & Outreach</td>
	<td align='center'><?php if($num3) echo $num3; else echo "<span class='nodata'>0</span>";?></td>
</tr>
<?php
//***************************************************************************************************************************************************
//*************Institute Report: Networking (Angel and VCs,Experts and professionals,Entrepreneurs) and Number of Participants**************
//***************************************************************************************************************************************************
?>
<tr class='head'><th >Networking</th><th>Number</th></tr>
<tr class='nodata'><td >Angels & VCs</td><td>---</td></tr>
<tr class='nodata'><td >Experts & Professionals</td><td>---</td></tr>
<tr class='nodata'><td >Entrepreneurs</td><td>---</td></tr>
<?php
//******************************************************************************************************************************************
//*************Institute Report: Courses (FDP Trainings,University Prescribed,Self-Developed) and Number of Participants**************
//*******************************************************************************************************************************************
$fdp_trainings = $univ_prescribed = $self_developed = 0;
$training_sql = "SELECT COUNT(*) Total FROM fdp WHERE institute_id = $institute_id";
$records = fetchRecords($training_sql);
if(count($records))
$fdp_trainings = $records[0]['Total'];
$univ_sql = "SELECT COUNT(*) Total FROM fdp_to_course ftc JOIN fdp ON fdp.fdp_id = ftc.fdp_id WHERE fdp.institute_id = $institute_id AND ftc.course_provider = 'University Prescribed'";
$records = fetchRecords($univ_sql);
if(count($records))
$univ_prescribed = $records[0]['Total'];
$self_sql = "SELECT COUNT(*) Total FROM fdp_to_course ftc JOIN fdp ON fdp.fdp_id = ftc.fdp_id WHERE fdp.institute_id = $institute_id AND ftc.course_provider = 'Developed by Faculty'";

$records = fetchRecords($self_sql);
if(count($records))
$self_developed = $records[0]['Total'];
?>
<tr class='mainhead'><th align='center' rowspan='23' valign='top' >Capacity:</th></tr>
<tr class='head'><th >Courses</th><th>Number</th></tr>
<tr class='latestdata'><td >FDP Trainings</td><td><?php echo $fdp_trainings; ?></td></tr>
<tr class='latestdata'><td >University Prescribed</td><td><?php echo $univ_prescribed; ?></td></tr>
<tr class='latestdata'><td >Self-Developed</td><td><?php echo $self_developed; ?></td></tr>
<?php
//**************************************************************************************************************************************
//*************Institute Report: Campus Company (Number of Campus Cos,Students involved,Total Revenue) and Number**************
//**************************************************************************************************************************************
$CC=$students=$revenue=0;
$sel_number_of_CC="SELECT COUNT(*) total FROM campus_company WHERE institute_id=$institute_id";
$records=fetchRecords($sel_number_of_CC);
$CC=$records[0]['total'];
$sel_students_involved="SELECT COUNT(*) total FROM student_members sm JOIN campus_company cc ON sm.cc_id=cc.cc_id WHERE cc.institute_id=$institute_id";
$records=fetchRecords($sel_number_of_CC);
$students=$records[0]['total'];
$sel_total_revenue="SELECT SUM(cchealth_revenue) total FROM campus_company WHERE institute_id=$institute_id";
$records=fetchRecords($sel_total_revenue);
$revenue=$records[0]['total'];
?>
<tr class='head'><th >Campus Company</th><th>Number</th></tr>
<tr class='data'><td >Campus Companies</td><td><?php if($CC) echo $CC; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >Students Involved</td><td><?php if($students) echo $students; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >Total Revenue</td><td><?php if($revenue) echo $revenue; else echo "<span class='nodata'>0</span>";?></td></tr>
<?php
//*******************************************************************************************************************************************
//**********Institute Report: Student Venture Monitoring Unit :: External Mentors, Mentoring Sessions, Long-Term Relationships**************
//*******************************************************************************************************************************************
$ext_mentors = $mentoring_sessions = 0;
$ext_sql ="SELECT COUNT(m.instororg) ExtMentors FROM svmu_prgm_mentors m JOIN svmu s ON s.svmu_id=m.svmu_id JOIN institute i ON i.id=s.institute_id WHERE s.institute_id=$institute_id AND m.instororg != i.name";
$records = fetchRecords($ext_sql);
if(count($records))
$ext_mentors = $records[0]['ExtMentors'];
$sessions_sql ="SELECT COUNT(vt.totmntrngmeets) Sessions FROM `svmu_vt_details` vt JOIN svmu s ON vt.svmu_id=s.svmu_id WHERE institute_id=$institute_id AND vt.totmntrngmeets > 0";
$records = fetchRecords($sessions_sql);
if(count($records))
$mentoring_sessions = $records[0]['Sessions'];
?>
<tr class='head'><th >Student Venture Monitoring Unit</th><th>Number</th></tr>
<tr class='latestdata'><td >External mentors</td><td><?php echo $ext_mentors; ?></td></tr>
<tr class='latestdata'><td >Mentoring Sessions</td><td><?php echo $mentoring_sessions; ?></td></tr>
<tr class='nodata'><td >Long-Term Relationships</td><td>---</td></tr>
<?php
//*****************************************************************************************************************************************
//**************Institute Report: IEDC (Number of Projects,Funding received till date) and Number*********************************
//*****************************************************************************************************************************************
$iedc_projects=$iedc_funding = 0;
$proj_sql = "SELECT COUNT(pd.id) TotProjects FROM iedc_project_data pd JOIN iedc ie ON ie.iedc_id=pd.iedc_id JOIN institute i ON i.id=ie.institute_id WHERE ie.institute_id=$institute_id";
$records = fetchRecords($proj_sql);
if(count($records))
$iedc_projects = $records[0]['TotProjects'];
$funding_sql = "SELECT ie.fund_received Funds FROM iedc ie JOIN institute i ON i.id=ie.institute_id WHERE ie.institute_id=$institute_id";
$records = fetchRecords($funding_sql);
if(count($records))
$iedc_funding = $records[0]['Funds'];
if(!$iedc_funding) $iedc_funding = 0;
?>
<tr class='head'><th >IEDC</th><th>Number</th></tr>
<tr class='latestdata'><td >Projects</td><td><?php echo $iedc_projects; ?></td></tr>
<tr class='latestdata'><td >Funding Recieved Till Date</td><td><?php echo "Rs ",$iedc_funding," /-"; ?></td></tr>
<?php
//*******************************************************************************************************************************************
//Institute Report: Innovation Centre (No of Projects,No of Patents,Private funding received,No of students,No of alumni,No of faculty)
//*******************************************************************************************************************************************
?>
<tr class='head'><th >Innovation Centre</th><th>Number</th></tr>
<tr class='nodata'><td >Projects</td><td>---</td></tr>
<tr class='nodata'><td >Patents</td><td>---</td></tr>
<tr class='nodata'><td >Private Funding Recieved</td><td>---</td></tr>
<tr class='nodata'><td >Students</td><td>---</td></tr>
<tr class='nodata'><td >Alumni</td><td>---</td></tr>
<tr class='nodata'><td >Faculty</td><td>---</td></tr>
<?php
//*******************************************************************************************************************************************
//Institute Report: Outcomes (Student start-ups,Students joining start-ups,Graduate start-ups,Alumni start-ups,Entrepreneurs supported,Jobs created)
//*******************************************************************************************************************************************
$start_ups=$join_start_ups=$graduate_start_ups=$alumni_start_ups=$ent_supported=$jobs_created=0;
$sel_ent_supported="SELECT SUM(activitybeneficiaries_type) total FROM activity WHERE institute_id=$institute_id";
$records=fetchRecords($sel_ent_supported);
$ent_supported=$records[0]['total'];
?>
<tr class='mainhead'><th align='center' rowspan='17' valign='top' >Outcomes:</th></tr>
<tr class='head'><th >Outcomes</th><th>Number</th></tr>
<tr class='data'><td >Student Start-Ups</td><td><?php if($start_ups) echo $start_ups; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >Students Joining Start-ups</td><td><?php if($join_start_ups) echo $join_start_ups; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >Graduate Start-ups</td><td><?php if($graduate_start_ups) echo $graduate_start_ups; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >Alumni Start-ups</td><td><?php if($alumni_start_ups) echo $alumni_start_ups; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >Entrepreneur Supported</td><td><?php if($ent_supported) echo $ent_supported; else echo "<span class='nodata'>0</span>";?></td></tr>
<tr class='data'><td >Jobs Created</td><td><?php if($jobs_created) echo $jobs_created; else echo "<span class='nodata'>0</span>";?></td></tr>
<?php
//***************************************************************************************************************
//*************Institute Report: Revenues (Fees,Private Sponsorships,Government funding) **************
//***************************************************************************************************************
$fees=$pri_sponsors=$inst_fund=0;
$sel_fees="SELECT SUM(activitybeneficiaries_fees) total FROM activity WHERE institute_id=$institute_id";
$records=fetchRecords($sel_fees);
$fees=$records[0]['total'];
$sel_pri_sponsors="SELECT activitycontributor_sponsors sponsors FROM activity WHERE institute_id=$institute_id";

$records=fetchRecords($sel_pri_sponsors);
if($records)
	foreach($records as $record){
	$sponsor_record=$record['sponsors'];

	$each_record=explode(',',$sponsor_record);

	$count_sponsors=count($each_record);
		for($pqr=0;$pqr<$count_sponsors;$pqr++){
		$each_record[$pqr]=str_replace("(","",$each_record[$pqr]);
		$pri_sponsors += intval($each_record[$pqr]);
		
		}
	}
$sel_inst_fund="SELECT SUM(edcfund_sanction) total FROM edc WHERE institute_id=$institute_id";
$records=fetchRecords($sel_inst_fund);
$inst_fund=$records[0]['total'];
$sel_inst_fund="SELECT SUM(fund_received) total FROM iedc WHERE institute_id=$institute_id";
$records = fetchRecords($sel_inst_fund);
$inst_fund += $records[0]['total'];
?>
<tr class='head'>
				<th >Revenues</th><th>Number</th>
</tr>
<tr class='data'>
				<td >Fees</td>
				<td><?php if($fees) echo "Rs ".$fees."/-"; else echo "<span class='nodata'>0</span>";?></td>
</tr>
<tr class='data'>
				<td >Private Sponsorships</td>
				<td><?php if($pri_sponsors) echo "Rs ".$pri_sponsors."/-"; else echo "<span class='nodata'>0</span>";?></td>
</tr>
<tr class='data'>
				<td >Institutional Funding</td>
				<td><?php if($inst_fund) echo "Rs ".$inst_fund."/-"; else echo "<span class='nodata'>0</span>";?></td>
</tr>
<?php
//*******************************************************************************************************************************************
//*************Institute Report: Branding (National & International Prizes,Mainstream Media mentions,Social Media engagement,Prizes Won)**
//*******************************************************************************************************************************************
$sel_prizes_won="SELECT count(*) total FROM activity WHERE activityoutcome_institution=$institute_id";
$records=fetchRecords($sel_prizes_won);
$prizes_won=$records[0]['total'];
?>
<tr class='head'><th >Branding</th><th>Number</th></tr>
<tr class='nodata'><td >National & International Prizes</td><td>---</td></tr>
<tr class='nodata'><td >Mainstram Media Mentions</td><td>---</td></tr>
<tr class='nodata'><td >Social Media Engagement</td><td>---</td></tr>
<tr class='data'><td >Prizes Won</td><td><?php if($prizes_won) echo $prizes_won; else echo "<span class='nodata'>0</span>";?></td></tr>

<?php

echo "</table>";
}
?>
<style type='text/css'>
.head{background:#6495ED;}
.data{background:#F5F5DC}
.nodata{background:#F5F5DC;color:red}
.mainhead{background:pink;}
.latestdata{background:#F5F5DC;color:rgb(201, 216, 30);}
</style>
<title>Institute Report</title>


