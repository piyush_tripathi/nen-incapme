<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
                        var selectedid=$('#inst').val();
                        var param={id:selectedid};
                         $.getJSON("script.php",param,function(data,status,xhr)//getJSON(url,data,callback function)
             {
                                        $.each(data,function(i,data){
                                                $("#facdrpdwn").append("<option>"+data.first_name+"&nbsp;"+data.last_name+"</option>");         
                                        }); 
             });
                        $("#inst").change(function(){
                                
                                        var crntid=$('#inst').val();
                                        //alert(crntid);
                                        var prm={id:crntid};
                                        $.getJSON("script.php",prm,function(data,status,xhr)
                                         { 
										 console.log(data);
                                                 $("#facdrpdwn").replaceWith("<select id='facdrpdwn' name='faculty'></select>");
                                                        $.each(data,function(i,data){
                                                                        $("#facdrpdwn").append("<option value='"+data.id+"'>"+data.first_name+"&nbsp;"+data.last_name+"</option>"); 
                                                        }); 
                                         });
                                
                                });
        });
</script>

<?php

require "DatabaseFunctions.php";

//Given institute info
$instituteId = '2';
$instituteName = '';

function getFaculties($instituteid) {
     global $db;
     $q = "Select f.first_name, f.last_name from faculty f join institute i on (f.institute_id=i.institute_id) where i.institute_id=$instituteid";
        $data=array();
     $result=mysql_query($q);
     while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
          $data[]=$row;               
     }
     return $data;                      
}


function getInstitutes(){
        global $db;// to access $db inside getInstitutes function
        $query="select id, name from institute";// query to select id, name from institute table
        $result=mysql_query($query);//executing the query
		$data=array();//declaring the data array
     while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
          $data[]=$row;               
     }
     ?>
	 <form name='FactultyForm' method='post'>
	 <?php
     $htm="<select id='inst' name='inst'>";//<select id='inst'>
	 $htm.="<option value=''>Select an Institute</option>";
     $selectedValue='';     
     foreach($data as $item) {
        $selected = ($item['id'] == $selectedValue) ? ' selected ' :''; //if(id==$selectedvalue) $selected='selected'; else $selected=''
                     $htm.="<option  value = \"".$item['id']."\" $selected>".$item['name']."</option>";//<option value='id' selected/''>name</option>   
     }
     $htm.= "</select>";
         return $htm;
}

        


$institutes= getInstitutes();

echo $institutes;//<select><option></option></select>


$html="<select id='facdrpdwn' name='faculty'></select>";
echo $html;
?>
<input type='submit' name='submit' value='View Report'/>
</form>
<?php if(isset($_POST['submit'])){//ONCE THE FORM IS SUBMITTED
$institute_id=$_POST['inst'];
$faculty_id=$_POST['faculty'];
//CHECKBOX LEVEL ARRAY CHECKS THE APPROPRIATE REASON FOR THE FACULTY TO BE AT THAT LEVEL
$checkbox['STARTER'][] = $checkbox['PROGRESSIVE'][] = $checkbox['SENIOR'][] = array();
//REASONS LEVEL ARRAY IS TO SHOW THE REASONS WHY A FACULTY IS AT THAT PARTICULAR LEVEL
$reasons['STARTER'][]="Signed up as NEN Faculty Leader and declared self commitment";
$reasons['PROGRESSIVE'][]="Supporting the E Cell as Faculty Advisor";
$reasons['PROGRESSIVE'][]="Running programs that impart  key concepts, develop a skill or develop the ability to use an entrepreneurial tool.";
$reasons['SENIOR'][]="Teaching course(s) with original content or adapting existing content";
$reasons['SENIOR'][]="Teaching/leading a series of workshops for students either using original content or adapting existing content";
$reasons['SENIOR'][]="Leading programs that provide  intense real world experience for the students";

//EVERY FACULTY IS AT STARTER LEVEL IF HE SIGNS UP WITH NEN
$level = "STARTER";
$checkbox[$level][0] = "checked = 'true'";

$sql = "SELECT ecell.startup_board_faculty Advisor FROM ecell WHERE startup_board_faculty = $faculty_id";
$records = fetchRecords($sql);


if(count($records)){
if($records[0]['Advisor'] == $faculty_id){
//A FACULTY IS AT LEVEL PROGRESSIVE WHEN HE IS ON THE ADVISOR BOARD OF ECELL 'OR'
$level = "PROGRESSIVE";
$checkbox[$level][0] = "checked = 'true'";
}
}

$sql = "SELECT a.activityinfo_type FROM activity_contributor_corodinator acc JOIN activity a ON a.activity_id = acc.activity_id WHERE acc.human_id = $faculty_id AND a.activityinfo_type IN ('Lecture/ Talk','Panel Discussion','Excersice/Games')";
$records = fetchRecords($sql);
if(count($records)){
//A FACULTY IS AT LEVEL PROGRESSIVE WHEN HE IS A CONTRIBUTOR TO ANY OF THE ABOVE ACTIVITY
$level = "PROGRESSIVE";
$checkbox[$level][1] = "checked = 'true'";
}

$sql = "SELECT a.activityinfo_type Type FROM activity_contributor_corodinator acc JOIN activity a ON a.activity_id = acc.activity_id WHERE acc.human_id = $faculty_id AND a.activityinfo_type IN ('Course','Workshop')";
$records = fetchRecords($sql);

if(count($records)){
if($records[0]['Type'] == 'Course'){
//A FACULTY IS AT LEVEL SENIOR WHEN HE IS A CONTRIBUTOR TO COURSE 'OR'
$level = "SENIOR";
$checkbox[$level][0] = "checked = 'true'";
}else{
//A FACULTY IS AT LEVEL SENIOR WHEN HE IS A CONTRIBUTOR TO WORKSHOP 'OR'
$level = "SENIOR";
$checkbox[$level][1] = "checked = 'true'";
}
}

if(isset($_SESSION['Responsibilities']) && in_array('LEAD',$_SESSION['Responsibilities'])){
//I HAVE SAVED THE RESPONSIBILITIES FROM FACULTY ACTIVITY REPORT IN A SESSION AND CHECKING IF HE IS A LEAD OR NOT
//IF THE FACULTY IS A LEAD THEN HE IS AT LEVEL SENIOR 'OR'
$level = "SENIOR";
$checkbox[$level][2] = "checked = 'true'";
}
?>
<?php $count = count($reasons[$level]); $i = 1;?>
<table border='0' cellspacing='2px' cellpadding='5px'>
<tr  class='mainhead'><th colspan='2'>Faculty Level Report</th></tr>
<tr class='head'><th>Level</th><th>Reason</th></tr>
<tr class='data'>
	<th rowspan='<?php echo $count;?>'><?php echo $level?></th><td><input type='checkbox' <?php echo $checkbox[$level][0]; ?> disabled='disabled'/><?php echo $reasons[$level][0]; ?></td>
</tr><?php while($i < $count){ ?>
<tr class='data'>
	<td><input type='checkbox' <?php if(isset($checkbox[$level][$i])) echo $checkbox[$level][$i]; ?> disabled='disabled'/><?php echo $reasons[$level][$i]; ?></td>

</tr><?php ++$i; } ?>
</table>
<?php } ?>

<style type='text/css'>
.head{background:#6495ED;}
.data{background:#F5F5DC}
.nodata{background:#C0C0C0;color:red}
.mainhead{background:pink;}
.subhead{background:#FFA07A;color:sienna}
</style>
<title>Factulty Level Report</title>
	
