jQuery(document).ready(function(){
        
        $('.delActivity').click(function(){
            var id = $(this).attr('value');
            var name = $(this).closest('tr').find('a[href^="/activity/edit"]').text();
            console.log(name);
            $('#delActivity #activity_id').attr('value',id);
            $('#delActivity #activityName').text(name); 
        });
        $('#submitactivitycontributor').click(function(){
          
             var AoA = $('#test tr').map(function(){
                return [
                    $('td',this).map(function(){
                        return $(this).text();
                    }).get()
                ];
                  }).get();
            var json = JSON.stringify(AoA);
            
                       
        }); 
        
        $('#submitactivityparticipants').click(function(){
             var arr = $('#gridparticipants tr').map(function(){
                return [
                    $('td[class!="number"]',this).map(function(){
                        return $(this).text();
                    }).get()
                ];
                  }).get();

            
            var json = JSON.stringify(arr);
            // // console.log(arr)
            $('#activity_participants').attr('value',json);
            $('#insertactivityparticipants').submit();
            
            
        })
        
        // changing the venue names after changing the city in main activity edit page 
        $('select[id="activity_city"]').change(function() {
            
            var city =  $('select[id="activity_city"]').val();
          
            
            $.ajax({
                url:"/index.php/activity/getvenue?city="+city,
                type: 'GET',
                dataType: 'json',
                success:function(data) {
                    var htm = '';

                   for(i=0;i<data.length;i++){
                       if(i==0){
                           htm +='<option selected value="'+data[i]['id']+'">'+data[i]['name']+'</option>';
                          
                       }else{
                           htm +='<option value="'+data[i]['id']+'">'+data[i]['name']+'</option>';
                       }
                         
                    }

                    
                   $('select[id="activityplacetime_venue"]').html(htm);
                  
                   
                }
                
             });
            
            
            
        });
        
        //end function 
        // changing the faculty names after changing the institute in main activity edit page 
        $('select[id="leadSpeakerInstitute"]').change(function() {
            
            var id =  $('select[id="leadSpeakerInstitute"]').val();
            //alert(inst);
            $.ajax({
                url:"/index.php/activity/getfacultylist?instituteid="+id,
                type: 'GET',
                dataType: 'json',
                success:function(data) {
                    var htm = '';
                   for(i=0;i<data.length;i++){
                       if(i==0){
                           htm +='<option selected value="'+data[i]['id']+'">'+data[i]['first_name']+data[i]['last_name']+'</option>';
                       }else{
                           htm +='<option value="'+data[i]['id']+'">'+data[i]['first_name']+data[i]['last_name']+'</option>';
                       }
                         
                    }
                    
                   $('select[id="lead-speaker"]').html(htm); 
                }
                
             });
            
            
            
        });
        
        
        
        //end function 
    });
    
    
