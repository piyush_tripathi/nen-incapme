
var overview;
$(document).ready(function(){
	
    ecell = {
		
        showBlock: function(block) {
            var fields = ecell.groups[block];
            var data = ecell.data;
			
            for(f in fields) {
                var itm = '#lbl_'+ f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1 || data[f]=="1") {
                        //$(itm).attr('checked', );
                        $(itm).prop('checked', true);
                    //console.log($(itm));
                    } else {
                        $(itm).removeAttr('checked');
                    }
                } else if(type == 'select' ) {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).html(data[f].caption);
                        } else {
                            $(itm).html(data[f]);
                        }
                    }
                } else if (type == 'text') {
                    $(itm).html(data[f]);
                //console.log(data[f]);
                } else if(type == 'grid') {
                    var dfname = fields[f].field;
                    if(f == 'over_members') {
                        ecell.showOverviewMembers(dfname);
                    }
                    else if(f == 'over_eleaders') {
                        ecell.showOverviewMembers(dfname, true);
                    }
					
                //console.log(data[gname]);
                } else if (f == 'multipledd') {
					
                } else if (type == 'multiple_sponsors') {
                    ecell.showMultipleSponsors(f);
                }
            }
        },
		
        editBlock: function(block) {
            var fields = ecell.groups[block];
            var data = ecell.data;
		
            for(f in fields) {
                var itm =  '#'+f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1 || data[f]=="1") {
                        $(itm).prop('checked', true);
                    } else {
                        $(itm).prop('checked', false);
                    }
                } else if(type == 'select' ) {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).val(data[f].value);
                            try {
                                /*hack for custom select for select2js */
                                $('#s2id_'+f+ ' a span').text(data[f].caption);
                            } catch(e) {

                            }
                        } else {
                            $(itm).val(data[f]);
                            $('#s2id_'+f+ ' a span').text(data[f]);
                        }   
                                        

                    }
                } else if (type == 'text') {
                    $(itm).val(data[f]);
                //console.log(data[f]);
                } else if(type == 'grid') {
                    var dfname = fields[f].field;
                    if(f == 'over_members') {
                        ecell.editOverviewMembers(dfname);
                    }
                    else if(f == 'over_eleaders') {
                        ecell.editOverviewMembers(dfname); //2nd parameters for eleaders
                    }

                //console.log(data[gname]);
                } else if (type == 'multipledd') {
					
                } else if (type == 'multiple_sponsors') {
                    ecell.editMultipleSponsors(f);
                }
            }
        },
	
		
        showOverviewMembers: function(field) {
            var m = ecell.data[field];
            //console.log(m);
            var r='';
            if(m instanceof Array) {
                for(i=0; i< m.length; i++) {
                    var r = r + '<tr><td>'+m[i].name +'</td><td>'+ m[i].organisation + '</td><td>'+m[i].designation +'</td><td>'+ m[i].email + '</td><td>' + m[i].tel + '</td></tr>';
				
                }
                $('#lbl_'+field).html('<table width="90%" style="border: 1px solid #ccc;">'+r+'</table>');
            } else {
                $('#lbl_'+field).html('');
            }
        },
		
        editOverviewMembers: function(field) {
            var m = ecell.data[field];
            //console.log(m);
            var r='';
            if(m instanceof Array) {
                for(i=0; i< m.length; i++) {
                    var r = r + '<tr><td>'+m[i].name +'</td><td>'+ m[i].organisation + '</td><td>'+m[i].designation +'</td><td>'+ m[i].email + '</td><td>' + m[i].tel + '</td></tr>';
                //console.log(m[i].organisation);
                }
                $('#'+field).html('<table width="90%" style="border: 1px solid #ccc;">'+r+'</table>');
            } else {
                $('#lbl_'+field).html('');
            }
        },
		
        editMultipleSponsors: function(field) {
            var sponsors = ecell.data[field];
            ecell.tempSponsosrs = ecell.data[field];
            console.log(ecell.data[field]);
            var htm = '';
            if(sponsors instanceof Array) {
                for(i=0; i< sponsors.length; i++) {
                    htm += '<div class="individual-sponsor"><input class="cls-estab-amount" type="text" id="estab_amount' + '_' + i + '" value="'+sponsors[i].estab_amount+'">';
                    htm += '<input type="text" class="cls-estab-sponsor" id="estab_sponsor' + '_' + i + '" value="'+sponsors[i].estab_sponsor+'"><a href="#" class="remove-sponsor" value="'+i+'">X</a><p></div>	';
                }
                jQuery('#'+field).html(htm);
            }
        },
		
        showMultipleSponsors: function(field) {
            var sponsors = ecell.data[field];
			
            var htm = '';
            if(sponsors instanceof Array) {
                for(i=0; i< sponsors.length; i++) {
                    htm += '<label>&nbsp;</label> Amount: '+sponsors[i].estab_amount+' | Sponsor: '+sponsors[i].estab_sponsor+'<p>';
                }
            }
            jQuery('#lbl_'+field).html(htm);
        },
		
		
		
		
		
		
        show: function(block) {
            //if(!block) return;
            //console.log(block)
            $('#'+block+ '-container .view-block').show();
            $('#'+block+ '-container .edit-block').hide();
			
        },

        edit: function(block) {
            if(!block) return;
            //.log(block)
            $('#'+block+ '-container  .view-block').hide();
            $('#'+block+ '-container  .edit-block').show();
        //console.log($('#'+block+ '-container  .edit-block'))
        },
		
        initShowBlock: function(block) {
            if(!block) return;
            ecell.showBlock(block);
            ecell.editBlock(block);
            ecell.show(block);
        },
		
        updateEditBlock: function(block) {
            if(!block) return;
            ecell.showBlock(block);
            ecell.editBlock(block);
            ecell.edit(block);
        },
		
        save: function(block) {
            jQuery.ajax({
                type: "POST", 
                url: "/ajax/",
                data: {
                    'method': 'ecell.update', 
                    'data': ecell.data, 
                    'block': block, 
                    'ecellid': 1
                },
                dataType: 'json'
            }).success(function(response) {
                //console.log(response);
                });
			
        },
		
		
        loadData: function() {
            jQuery.ajax({
                url: '/ajax/',
                data: {
                    'method': 'ecell.get'
                },
                type: 'post',
                dataType: 'json',
                cache: false,
                success: function(responseJSON) {
                    ecell.data=responseJSON;
                    //ecell.showOverview();
                    //ecell.editOverview();
                    ecell.showBlock('overview');
                    ecell.editBlock('overview');
						
                    ecell.showBlock('startup');
                    ecell.editBlock('startup');
                    ecell.showBlock('establish');
                    ecell.editBlock('establish');
                    ecell.showBlock('hiperformance');
                    ecell.editBlock('hiperformance');
						
                    ecell.show('overview');
                    ecell.show('startup');
                    ecell.show('establish');
                    ecell.show('hiperformance');
						
						
                },
     
                complete: function() {
				             
                }
            });
        }
    };
	
    //ecell.data =  eval("("+jsonStr+")");
    ecell.xgroups = {
        'overview': ['over_date', 'over_name', 'over_lead', 'over_second', 'over_strength', 'over_num', 'over_eleadnum', 'over_pointofcontact1', 'over_pointofcontact2'],
        'startup' : ['startup_roles', 'startup_facadv', 'startup_board_faculty', 'startup_success', 'startup_calen'],
        'establish': ['estab_prof', 'estab_market', 'estab_visible', 'estab_money', 'estab_report', 'estab_uploadrep'],
        'hiperformance': ['hiperf_news', 'hiperf_latestnews', 'hiperf_web', 'hiperf_link', 'hiperf_support', 'hiperf_supportevidence', 'hiperf_sign', 'hiperf_signtitle', 'hiperf_signdate']
    };
    ecell.groups = 
    {
        'overview': {
            over_date:{
                "type":"text"
            },
            over_name:{
                "type":"text"
            },
            over_lead:{
                "type":"select"
            },
            over_second:{
                "type":"select"
            },
            over_strength:{
                "type":"text"
            },
            over_num:{
                "type":"text"
            },
            over_eleadnum:{
                "type":"text"
            },
            over_pointofcontact1:{
                "type":"select"
            },
            over_pointofcontact2:{
                "type":"select"
            },
            over_members: {
                "type":"grid",
                "field": "over_members"
            },
            over_eleaders: {
                "type":"grid",
                "field": "over_eleaders"
            }
        },
        'startup' : {
            startup_roles:{
                "type":"checkbox"
            },
            startup_facadv:{
                "type":"checkbox"
            },
            startup_board_faculty:{
                "type":"select"
            },
            startup_success:{
                "type":"checkbox"
            },
            startup_calen:{
                "type":"file"
            }
        },
        'establish' : {
            estab_prof:{
                "type":"checkbox"
            },
            estab_market:{
                "type":"checkbox"
            },
            estab_visible:{
                "type":"checkbox"
            },
            estab_money:{
                "type":"checkbox"
            },
            estab_sponsors: {
                "type": "multiple_sponsors",
                "callback": "somecallback"
            },
            estab_report:{
                "type":"checkbox"
            },
            estab_uploadrep:{
                "type":"file"
            }
        },
		
        'hiperformance': {
			
            hiperf_news:{
                "type":"checkbox"
            },
            hiperf_latestnews:{
                "type":"file"
            },
            hiperf_web:{
                "type":"checkbox"
            },
            hiperf_link:{
                "type":"text"
            },
            hiperf_support:{
                "type":"checkbox"
            },
            hiperf_supportevidence:{
                "type":"text"
            },
            hiperf_sign:{
                "type":"checkbox"
            },
            hiperf_signtitle:{
                "type":"text"
            },
            hiperf_signdate:{
                "type":"text"
            }
        }
    };
	
	
	
    ecell.loadData();
	
    $('.lnk-show-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('show-link-'.length);
        ecell.show(blk);
    //.log(blk);
		
    });
	
    $('.lnk-edit-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('edit-link-'.length);
		
        //console.log('--'+blk);
        ecell.edit(blk);
		
    });
	
	
    /** save overview **/
	
    $('.cls-save-overview').click(function(){
        var fields = ecell.groups.overview;
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type == 'select' ) {
                var facid = jQuery(item).val();
                var facname = $(item + ' :selected').text();
                var fac = {
                    'value': facid, 
                    caption: facname
                };
                ecell.data[f] = fac;

            } else if(fields[f].type == 'grid') {
                            
            } else {
                ecell.data[f] = jQuery(item).val();
            }
        }
        ecell.initShowBlock('overview');
        ecell.save('overview');
		
    });
	
    $('.cls-cancel-overview').click(function(){		
        ecell.initShowBlock('overview');
    });
	
    /** save startup **/
    $('.cls-save-startup').click(function(){
        var fields = ecell.groups.startup;
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type == 'checkbox') {
                if(typeof jQuery(item).attr('checked') == 'undefined')  {
                    ecell.data[f] = "0";
                }else {
                    ecell.data[f] = "1";
                }
            //console.log(jQuery(item).attr('checked'));
            } else {
                ecell.data[f] = jQuery(item).val();
            }
        }
        ecell.initShowBlock('startup');
        ecell.save('startup');
		
    });
	
    $('.cls-cancel-startup').click(function(){		
        ecell.initShowBlock('startup');
    });
	
    /** save establish **/
    $('.cls-save-establish').click(function(){
        var fields = ecell.groups.establish;
        for(f in fields) {
            var item = '#'+ f;		
            if((fields[f].type != 'grid') || (fields[f].type != 'multiple_sponsors')) {
                if(fields[f].type == 'checkbox') {
                    if(typeof jQuery(item).attr('checked') == 'undefined')  {
                        ecell.data[f] = "0";
                    }else {
                        ecell.data[f] = "1";
                    }
                //console.log(jQuery(item).attr('checked'));
                } else {
                    ecell.data[f] = jQuery(item).val();
                }
            }
			
            //handle sponsors
            if(fields[f].type != 'multiple_sponsors') {
                var pos=0;
                var amt=[];
                var spr=[];

                $('.individual-sponsor .cls-estab-amount').each(function( index ) {
                    amt[pos]=$(this).val();
                    pos++
                });
                pos=0;
                $('.individual-sponsor .cls-estab-sponsor').each(function( index ) {
                    spr[pos]=$(this).val();
                    pos++;		
                });

				
                var s=[];
                for(var i=0; i< pos; i++) {
                    s[i] = {
                        'estab_amount': amt[i], 
                        'estab_sponsor': spr[i]
                        }
                }
                ecell.data.estab_sponsors = s;
            //ecell.reinitSponsors(true);
            }
        }
        ecell.save('establish');
        ecell.initShowBlock('establish');
		
    });
	
    $('.cls-cancel-establish').click(function(){		
		
        ecell.initShowBlock('establish');
    });
	
    /** save hiperformance **/
    $('.cls-save-hiperformance').click(function(){
        var fields = ecell.groups.hiperformance;
        for(f in fields) {
            var item = '#'+ f;		
			
            if(fields[f].type == 'checkbox') {
                if(typeof jQuery(item).attr('checked') == 'undefined')  {
                    ecell.data[f] = "0";
                }else {
                    ecell.data[f] = "1";
                }
			
            } else {
                ecell.data[f] = jQuery(item).val();
            }
			
        }
        ecell.save('hiperformance');
        ecell.initShowBlock('hiperformance');
		
    });
	
    $('.cls-cancel-hiperformance').click(function(){		
        ecell.initShowBlock('hiperformance');
    });
	
    $('#estab_sponsor_add').click(function(){
        var htm = '';
        var i = ecell.tempSponsosrs.length;
        if(i > 5) {
            alert('enough');
            return false;
        }
        //ecell.tempSponsosrs[i]={'estab_amount':'', 'estab_sponsor': ''}
        htm += '<div class="individual-sponsor"><input class="cls-estab-amount" type="text" id="estab_amount' + '_' + i + '" value="">';
        htm += '<input type="text" class="cls-estab-sponsor" id="estab_sponsor' + '_' + i + '" value=""><a href="#" class="remove-sponsor" value="'+i+'">X</a><p></div>	'; 
        $('#estab_sponsors').append(htm);
        console.log(ecell.data.estab_sponsors);
    });
	
    $('.remove-sponsor').live('click', function(event){
        event.preventDefault();
        $(this).parent().remove();
        //var pos = $(this).attr('value');
        //ecell.reinitSponsors();
		
        console.log(ecell.data.estab_sponsors)
    });
	
	
	
	
    jQuery("input:file").unbind('change').bind('change', function(e) {
        var frm = 'form_'+$(this).attr('id');
        console.log($(this));
        document.forms[frm].submit();
    });
	
})

function uploaEcellMembers() {
    var m = jQuery('#target_overview_members');
    var ret = m.contents().find('body').html();
    var members = eval("("+ret+")");
    //console.log(members);
    ecell.data.over_members = members.over_members;
    ecell.data.over_eleaders = members.over_eleaders;
    ecell.data.over_num = members.over_num;
    ecell.data.over_eleadnum = members.over_eleadnum;
    m =  members.over_eleaders;
    var htmOptions = '';
    for(i=0; i< m.length; i++) {
        htmOptions += '<option>'+m[i].name+'</option>';
    }
    $('#over_pointofcontact1').html(htmOptions);
    $('#over_pointofcontact2').html(htmOptions);
	
	
    ecell.showBlock('overview');
    ecell.editBlock('overview');
//console.log(members)
}

function uploadFiles(name) {
    var m = jQuery('#target_'+name);
    var ret = m.contents().find('body').html();
    var res = eval("("+ret+")");
    console.log(res);
    if(name == '') {
        link = '';
    }
	
	
//ecell.showBlock('overview');
//ecell.editBlock('overview');
//console.log(members)
}	

