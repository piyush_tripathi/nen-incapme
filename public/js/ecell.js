//mark
$(document).ready(function(){
	   jQuery("input:file[class=fileParticipant]").unbind('change').bind('change', function(e) {            
//            jQuery('#form_upload_participants').submit();
var form = $(this).closest('form');
form.submit();
        });
        update_ecell_options();
    ecell = {
		
        showBlock: function(block) {
            var fields = ecell.groups[block];
            var data = ecell.data;
			console.log(data);
            for(f in fields) {
                var itm = '#lbl_'+ f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1 || data[f]=="1") {
                        $(itm).prop('checked', true); 
                    } else {
                        $(itm).removeAttr('checked'); 
                    }
                } else if(type == 'select' ) {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).html(data[f].caption);
                        } else {
                            $(itm).html(data[f]);
                        }
                    }
                } else if (type == 'text') {
                    $(itm).html(data[f]);
                }else if (type == 'hidden') {
                   //alert(data[f]);
                } 
                else if(type == 'grid') {
                    if(f == 'over_members') {
                        ecell.showOverviewMembers();
                    }
                    if(f == 'estab_professionals') {
                        ecell.showEstabProfessionals();
                    }
                    
                } else if (f == 'hidden') {
					
                } else if (type == 'multiple_sponsors') {
                    ecell.showMultipleSponsors(f);
                }
            }
            
            if(block == 'hiperformance') {
                
            }
            
            
        },
		
        editBlock: function(block) {
            var fields = ecell.groups[block];
            var data = ecell.data;
		
            for(f in fields) {
                var itm =  '#'+f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1 || data[f]=="1") {
                        $(itm).prop('checked', true);
                    } else {
                        $(itm).prop('checked', false);
                    }
                } else if(type == 'select' ) {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).val(data[f].value);
                            try {
                                /*hack for custom select for select2js */
                                $('#s2id_'+f+ ' a span').text(data[f].caption);
                            } catch(e) {

                            }
                        } else {
                            $(itm).val(data[f]);
                            $('#s2id_'+f+ ' a span').text(data[f]);
                        }   
                                        

                    }
                } else if (type == 'text') {
                    $(itm).val(data[f]);
                //console.log(data[f]);
                } else if(type == 'grid') {
                    if(f == 'over_members') {
                        ecell.editOverviewMembers();
                    }
                    if(f == 'estab_professionals') {
                        ecell.editEstabProfessionals();
                    }
                } else if (type == 'multipledd') {
					
                } else if (type == 'multiple_sponsors') {
                    ecell.editMultipleSponsors(f);
                }
            }
        },
	
        
        showOverviewMembers: function() {
            var d = ecell.data.over_members;
            ecell.viewOverviewMemberGrid.processData(d, "view_over_members");
            var data;
            data = ecell.data.over_members.data;
           // console.log(data);
            if ( data[0] ==0) {
                 $('#div_over_members').hide();
                
            }
            else{
                $('#div_over_members').show();               
            }
            if(data instanceof Array) {
                var r='';
                for(i = 0; i < data.length; i++) {
                    try {
                        if(data[i].values['is_e_leader'] == 1)
                            var r = r + '<tr><td>'+data[i].values.name +'</td><td>'+ data[i].values.email + '</td><td>'+ data[i].values.contact_no +'</td><td>'+ data[i].values.year_grad + '</td></tr>';
                    } catch(e){}
                }
            }
            //var tableHead = '<table class=" table table-bordered table-striped" ><thead><tr><th>Name</th><th>Email</th><th>Contact No</th><th>Year Of Graduation</th></tr></thead><tbody>';
            //$('#view_over_eleaders').html(tableHead + r + '</tbody></table>');
            
        },
		
        editOverviewMembers: function() {
            var d = ecell.data.over_members;
            ecell.overviewMemberGrid.processData(d, "over_members");
        },
        
        
        editMultipleSponsors: function(field) {
            var sponsors = ecell.data[field];
            ecell.tempSponsosrs = ecell.data[field];
            //console.log(ecell.data[field]);
            var htm = '';
            if(sponsors instanceof Array) {
                for(i=0; i< sponsors.length; i++) {
                    
                    
                    if(i === 0) {
                        htmDelete = '';
                        
                         htm += '<div class="individual-sponsor"><input type="text" placeholder="Sponsor name" class="cls-estab-sponsor" id="estab_sponsor' + '_' + i + '" value="'+sponsors[i].estab_sponsor+'">';
                        htm += '<input placeholder="Rs.XXX" class="cls-estab-amount" type="text" id="estab_amount' + '_' + i + '" value="'+sponsors[i].estab_amount+'">'+htmDelete+'</div>';
                        
                    }
                    if(i > 0) {
                        
                        var htmDelete ='<a href="#" class="remove-sponsor btn" value="'+i+'"><i class="icon-minus"></i></a>';
                        
                        htm += '<div class="individual-sponsor"><input type="text" placeholder="Sponsor name" class="cls-estab-sponsor" id="estab_sponsor' + '_' + i + '" value="'+sponsors[i].estab_sponsor+'">';
                        htm += '<input placeholder="Rs.XXX" class="cls-estab-amount" type="text" id="estab_amount' + '_' + i + '" value="'+sponsors[i].estab_amount+'">'+htmDelete+'</div>';

                    }
                    
                    
                    
                }
                jQuery('#estab_sponsors').html(htm);
                
            // when cancel
            } else {
                jQuery('#estab_sponsors').html('');
            }
        },
		
        showMultipleSponsors: function(field) {
            var sponsors = ecell.data[field];
			
            var htm = '';
            if(sponsors instanceof Array) {
                for(i=0; i< sponsors.length; i++) {
                    htm += '<li><span class="sponsor-amt"><b>Institution : </b>'+sponsors[i].estab_sponsor+'</span><b>Amount : </b>' +sponsors[i].estab_amount+' </li>';
                }
            }
            jQuery('#lbl_'+field).html('<ol>'+htm+'</ol>');
        },
        
        editEstabProfessionals: function() {
            var d = ecell.data.estab_professionals;
            ecell.estabProfessionalsGrid.processData(d, "establish_professionals");
        },
        showEstabProfessionals: function() {
            var d = ecell.data.estab_professionals;
            ecell.viewEstabProfessionalsGrid.processData(d, "view_establish_professionals");
        },
		
		
        show: function(block) {
            //if(!block) return;
            //console.log(block)
            $('#'+block+ '-container .view-block').show();
            $('#'+block+ '-container .edit-block').hide();
			
        },

        edit: function(block) {
            if(!block) return;
            //.log(block)
            $('#'+block+ '-container  .view-block').hide();
            $('#'+block+ '-container  .edit-block').show();
        //console.log($('#'+block+ '-container  .edit-block'))
        },
		
        initShowBlock: function(block) {
            if(!block) return;
            $('#edit-link-'+block).removeClass('hide-block');
            ecell.showBlock(block);
            ecell.editBlock(block);
            ecell.show(block);
            initDependency(block);
        },
		
        updateEditBlock: function(block) {
            if(!block) return;
            ecell.showBlock(block);
            ecell.editBlock(block);
            ecell.edit(block);
        },
		
        save: function(block) {
             $('a.ajax-loader').trigger('click');
             jQuery.ajax({
                type: "POST", 
                url: "/ajax/",
                data: {
                    'method': 'ecell.update', 
                    'data': ecell.tdata, 
                    'block': block, 
                    'ecellid': ecellid
                },
                dataType: 'json',
                success: function(response) {
                    
                    if(response == true) {
                        var blockfields = ecell.groups[block]; 
                        for(f in blockfields) {
                            ecell.data[f] = ecell.tdata[f];
                        }
                        ecell.initShowBlock(block);
                        notify('#noti-ecell-'+block, 'success', 'Record updated...');
                        
                      //  var nextCollapseId = saveContinueGetNextId('ecell', block); //step by step form Submittion
                      //  console.log('Open : ' + nextCollapseId);
                        
                    } else {
                        notify('#noti-ecell-'+block, 'error', 'Record not updated...');
                        console.log('Unexpected Error:Data not saved');
                    }
                //console.log(response);
                //console.log(block);
                },
                complete: function() {
                 $('button.ajax-close').trigger('click');
                   checkBoxStatus();
                    hideShow();
                //console.log('here');
                }
            });
        },
			
		
		
        loadData: function(ecellid) {
            ecell.overviewMemberGrid = new EditableGrid("OverviewMemberGrid"); 
            ecell.viewOverviewMemberGrid = new EditableGrid("showOverviewMemberGrid");
            ecell.estabProfessionalsGrid = new EditableGrid("estabProfessionalGrid");
            ecell.viewEstabProfessionalsGrid = new EditableGrid("showEstabProfessionalGrid");
            
            ecell.data=ecellData;
            ecell.tdata = ecellData;

            ecell.showBlock('overview');
            ecell.editBlock('overview');

            ecell.showBlock('startup');
            ecell.editBlock('startup');
            ecell.showBlock('establish');
            ecell.editBlock('establish');
            ecell.showBlock('hiperformance');
            ecell.editBlock('hiperformance');

            ecell.show('overview');
            ecell.show('startup');
            ecell.show('establish');
            ecell.show('hiperformance');
            initDependency('overview');
            initDependency('startup');
            initDependency('establish');
            initDependency('hiperformance');
            
//            jQuery.ajax({
//                url: '/ajax/',
//                data: {
//                    'method': 'ecell.get',
//                    'ecellid': ecellid
//                },
//                type: 'post',
//                dataType: 'json',
//                cache: false,
//                success: function(responseJSON) {
//                    ecell.data=responseJSON;
//                    ecell.tdata = responseJSON;
//
//                    ecell.showBlock('overview');
//                    ecell.editBlock('overview');
//						
//                    ecell.showBlock('startup');
//                    ecell.editBlock('startup');
//                    ecell.showBlock('establish');
//                    ecell.editBlock('establish');
//                    ecell.showBlock('hiperformance');
//                    ecell.editBlock('hiperformance');
//						
//                    ecell.show('overview');
//                    ecell.show('startup');
//                    ecell.show('establish');
//                    ecell.show('hiperformance');
//                    initDependency('overview');
//                    initDependency('startup');
//                    initDependency('establish');
//                    initDependency('hiperformance');
//						
//						
//                },
//     
//                complete: function() {
//				             
//                }
//            });
        }
    };
    
    
    
    ecell.groups = 
    {
        'overview': {
            over_date:{
                "type":"text",
                "validation": ["required"]
            },
            over_name:{
                "type":"text",
                "validation": ["required"]
            },
            over_lead:{
                "type":"select"
            },
            over_second:{
                "type":"select"
            },
            over_strength:{
                "type":"text",
                "validation": ["number"]
            },
            over_num:{
                "type":"text",
                "validation": ["number"]
            },
            over_eleadnum:{
                "type":"text",
                "validation": ["number"]
            },
            over_pointofcontact1:{
                "type":"select"
            },
            over_pointofcontact2:{
                "type":"select"
            },
            over_members: {
                "type":"grid",
                "field": "over_members"
            },
            over_eleaders: {
                "type":"grid",
                "field": "over_eleaders"
            }
        },
        'startup' : {
            startup_roles:{
                "type":"checkbox"
            },
            startup_facadv:{
                "type":"checkbox"
            },
            startup_board_faculty:{
                "type":"select"
            },
            startup_success:{
                "type":"checkbox"
            },
            startup_plan:{
                "type":"checkbox"
            },
            startup_calen:{
                "type":"hidden"
            }
        },
        'establish' : {
            estab_prof:{
                "type":"checkbox"
            },
            estab_professionals: {
                "type": "grid"
            },
            estab_market:{
                "type":"checkbox"
            },
            estab_visible:{
                "type":"checkbox"
            },
            estab_money:{
                "type":"checkbox"
            },
            estab_sponsors: {
                "type": "multiple_sponsors",
                "callback": "somecallback"
            },
            estab_report:{
                "type":"checkbox"
            },
            estab_uploadrep:{
                "type":"hidden"
            }
        },
		
        'hiperformance': {
			
            hiperf_news:{
                "type":"checkbox"
            },
            hiperf_latestnews:{
                "type":"file"
            },
            hiperf_web:{
                "type":"checkbox"
            },
            hiperf_link:{
                "type":"text"
            },
            hiperf_support:{
                "type":"checkbox"
            },
            hiperf_supportevidence:{
                "type":"text"
            },
            hiperf_sign:{
                "type":"checkbox"
            },
            hiperf_signtitle:{
                "type":"text"
            },
            hiperf_signdate:{
                "type":"text"
            }
        }
    };
	
	
    ecell.loadData(ecellid);
    
	
    $('.lnk-show-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('show-link-'.length);
        ecell.show(blk);
		
    });
	
    $('.lnk-edit-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('edit-link-'.length);
        ecell.edit(blk);
        $(this).addClass('hide-block');
		
    });
	
	
    /** save overview **/
	
    $('.cls-save-overview').click(function(){
        var fields = ecell.groups.overview;
        var isValid = true;
        isValid = dovalidate(fields, 'noti-ecell-overview');
        if(! isValid) {
            return;
        }
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type == 'select' ) {
                var facid = jQuery(item).val();
                var facname = $(item + ' :selected').text();
                var fac = {
                    'value': facid, 
                    caption: facname
                };
                ecell.tdata[f] = fac;

            } else if(fields[f].type == 'grid') {
                continue;
                            
            } else {
                ecell.tdata[f] = jQuery(item).val();
            }
        }
        
       
        var d = ecell.tdata.over_members.metadata;
        var noCols = d.length;
        var nd = ecell.overviewMemberGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                
                
                var val = nd[i].columns[j];
                o[col] =  val;
                if(col == 'is_e_leader'){
                    if(val) {
                        val = 1;
                    } else {
                        val = 0;
                    }
                    o[col] =  val;
                }
                

            }
            newdata[i] = {
                'id': i+1, 
                'values' : o
            };
        }
        ecell.tdata.over_members.data = newdata;
        ecell.save('overview');
        
         update_ecell_options();
        
        
        
		
    });
	
    $('.cls-cancel-overview').click(function(){		
        ecell.initShowBlock('overview');
         update_ecell_options()
    });
	
    /** save startup **/
    $('.cls-save-startup').click(function(){
        var fields = ecell.groups.startup;
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type == 'checkbox') {
                if(typeof jQuery(item).attr('checked') == 'undefined')  {
                    ecell.tdata[f] = "0";
                }else {
                    ecell.tdata[f] = "1";
                }
            //console.log(jQuery(item).attr('checked'));
            } else if(fields[f].type == 'select' ) {
                var facid = jQuery(item).val();
                var facname = $(item + ' :selected').text();
                var fac = {
                    'value': facid, 
                    caption: facname
                };
                ecell.tdata[f] = fac;

            } else {
                ecell.tdata[f] = jQuery(item).val();
            }
        }
        //ecell.initShowBlock('startup');
        ecell.save('startup');
         update_ecell_options()
		
    });
	
    $('.cls-cancel-startup').click(function(){		
        ecell.initShowBlock('startup');
         update_ecell_options()
    });
	
    /** save establish **/
    $('.cls-save-establish').click(function(){
        var fields = ecell.groups.establish;
        var isValid = true;
        isValid = dovalidate(fields, 'noti-ecell-establish');
        if(! isValid) {
            return;
        }
        for(f in fields) {
            var item = '#'+ f;		
            if((fields[f].type != 'grid') && (fields[f].type != 'multiple_sponsors')) {
                if(fields[f].type == 'checkbox') {
                    if(typeof jQuery(item).attr('checked') == 'undefined')  {
                        ecell.tdata[f] = "0";
                    }else {
                        ecell.tdata[f] = "1";
                    }
                } else {
                    ecell.tdata[f] = jQuery(item).val();
                }
            }
			
            //handle sponsors
            if(fields[f].type == 'multiple_sponsors') {
                var pos=0;
                var amt=[];
                var spr=[];
                
                if(typeof $('#estab_money').attr('checked') != 'undefined') {

                    $('.individual-sponsor .cls-estab-amount').each(function( index ) {
                        amt[pos]=$(this).val();
                        pos++
                    });
                    pos=0;
                    $('.individual-sponsor .cls-estab-sponsor').each(function( index ) {
                        spr[pos]=$(this).val();
                        pos++;		
                    });


                    var s=[];

                    for(var i=0; i< pos; i++) {
                        if(isAmount(amt[i], true) !== true  ) {
                            console.log(amt[i]);
                            $('#estab_amount_' + i).focus();
                            $('#estab_amount_' + i).addClass('error-box');
                            
                            notify('#noti-ecell-establish', 'error', '', 'Please enter positive number');
                            return;
                        } else {
                            $('#estab_amount_' + i).removeClass('error-box');
                        }
                        s[i] = {
                            'estab_amount': amt[i], 
                            'estab_sponsor': spr[i]
                        }
                    }
                    ecell.tdata.estab_sponsors = s;
                //ecell.reinitSponsors(true);
                } else {
                    jQuery('#estab_sponsors').html('');
                }
            }
        }
        //establish professionals
        var d = ecell.tdata.estab_professionals.metadata;
        var noCols = d.length;
        var nd = ecell.estabProfessionalsGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                
                
                var val = nd[i].columns[j];
                o[col] =  val;
            }
            newdata[i] = {
                'id': i+1, 
                'values' : o
            };
        }
        ecell.tdata.estab_professionals.data = newdata;
        
        
        ecell.save('establish');
    //ecell.initShowBlock('establish');
     update_ecell_options();
		
    });
	
    $('.cls-cancel-establish').click(function(){		
		
        ecell.initShowBlock('establish');
        update_ecell_options();
    });
	
    /** save hiperformance **/
    $('.cls-save-hiperformance').click(function(){
        var fields = ecell.groups.hiperformance;
        for(f in fields) {
            var item = '#'+ f;		
			
            if(fields[f].type == 'checkbox') {
                if(typeof jQuery(item).attr('checked') == 'undefined')  {
                    ecell.tdata[f] = "0";
                }else {
                    ecell.tdata[f] = "1";
                }
			
            } else {
                ecell.tdata[f] = jQuery(item).val();
            }
			
        }
        ecell.save('hiperformance');
    //ecell.initShowBlock('hiperformance');
		update_ecell_options();
    });
	
    $('.cls-cancel-hiperformance').click(function(){		
        ecell.initShowBlock('hiperformance');
        update_ecell_options();
    });
	
    
    //Sponsors
    
    //Has Sponsors or not
    $('#estab_money').click(function(){
        
        if(typeof jQuery(this).attr('checked') != 'undefined')  {
            if($('.cls-estab-amount').length == 0) {
                var i =0;
                var htm = '';
                htm += '<div class="individual-sponsor"><input type="text" placeholder="Sponsor" class="cls-estab-sponsor" id="estab_sponsor' + '_' + i + '" value="">';
                htm += '<input placeholder="Rs:XXX" class="cls-estab-amount" type="text" id="estab_amount' + '_' + i + '" value=""></div>'; 
                $('#estab_sponsors').append(htm);
                
            }
        }
    });
    
    $('#estab_sponsor_add').click(function(){
        var htm = '';
        if($('.cls-estab-amount').length > 10) {
            alert('You cannot add more sponsors.');
            return false;
        } else {
            var i = $('.cls-estab-amount').length
            var htmDelete = '<a href="#" class="remove-sponsor btn" value="'+i+'"><i class="icon-minus"></i></a><p>';
            if(i==0) {
                htmDelete = '';
            }
         
            //ecell.tempSponsosrs[i]={'estab_amount':'', 'estab_sponsor': ''}
            htm += '<div class="individual-sponsor"><input type="text" placeholder="Sponsor" class="cls-estab-sponsor" id="estab_sponsor' + '_' + i + '" value="">';
            htm += '<input placeholder="Rs: XXX" class="cls-estab-amount" type="text" id="estab_amount' + '_' + i + '" value="">'+htmDelete+'</div>	'; 
            $('#estab_sponsors').append(htm);
        }
    });
	
    $('.remove-sponsor').live('click', function(event){
        event.preventDefault();
        $(this).parent().remove();
    });
	
	
	
	
    jQuery("input[class!='fileParticipant']:file").unbind('change').bind('change', function(e) {
        var frm = 'form_'+$(this).attr('id');
        //console.log($(this));
        document.forms[frm].submit();
    });
    
    
    //memeber checkbox clicked
    jQuery('#over_members td.boolean input:checkbox').live('click', function(event){
        var pos = $(this).parent().parent().attr('id').split('_')[1];
        if(!pos) pos=0
        
        var d = ecell.overviewMemberGrid.data;
        var el=0;
        var htmOptions = '';
        for(i=0; i< d.length; i++) {
            if(d[i].columns[4]==1) { // 4 is for eleader, 0 is for name
                el++ ;
                htmOptions += '<option selected>'+d[i].columns[0]+'</option>';
            }
        }
        //ecell.data.over_num = d.length;
        //ecell.data.over_eleadnum = el;
        $('#over_eleadnum').val(el);
        $('#over_num').val(d.length);
        $('#over_pointofcontact1').html(htmOptions);
        $('#over_pointofcontact2').html(htmOptions);
        $('#over_pointofcontact1').val(ecell.data.over_members.over_elead);
        $('#s2id_over_pointofcontact1 a span').text(ecell.data.over_members.over_elead);
        $('#over_pointofcontact2').val(ecell.data.over_members.over_second);
        $('#s2id_over_pointofcontact2 a span').text(ecell.data.over_members.over_second);
    });
    
    
    
    /** add new item to members grid**/
    $("#add-grid-ecell_members").click(function() {
        var d = ecell.tdata.over_members.metadata;
        var noCols = d.length;
        var nd = ecell.overviewMemberGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {
                'id': i+1, 
                'values' : o
            };
        }
        var _members = {};
    
        _members.data = newdata;
        _members.metadata = ecell.data.over_members.metadata;
        
        //add column to tdata
        var last = _members.data.length;
        
        var newitem = {
            "id" : last+1,
            "values" : {
                "name": "",
                "email": "",
                "contact_no": "",
                "year_grad": "",
                "is_e_leader": ""
            }
        }
        _members.data[last] = newitem;
        ecell.overviewMemberGrid.processData(_members, "over_members");
         update_ecell_grid_count(last);
    });
    function update_ecell_grid_count(count){
//        var count  = $('#over_members tr').length -1 ; 
        $('#over_num').val(count+1);
    }
 
    
    /** add new item to advisor grid **/
    $("#add-grid-ecell_estab_prof").click(function() {
        var d = ecell.tdata.estab_professionals.metadata;
        var noCols = d.length;
        var nd = ecell.estabProfessionalsGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {
                'id': i+1, 
                'values' : o
            };
        }
        var _estab_professionals = {};
    
        _estab_professionals.data = newdata;
        _estab_professionals.metadata = ecell.data.estab_professionals.metadata;
        
        //add column to tdata
        var last = _estab_professionals.data.length;
        
        var newitem = {
            "id" : last+1,
            "values" : {
                "name": "",
                "organization": "",
                "designation": "",
                "email": "",
                "contact_no": ""
            }
        }
        
        _estab_professionals.data[last] = newitem;
        ecell.estabProfessionalsGrid.processData(_estab_professionals, "establish_professionals");
    });
    
    
    /** dependency on checkbox **/
    
    function initDependency(blk) {
        if(!blk) return;
        $('.cls-dependency', $('#'+blk+'-container')).each(function(){
            if(typeof jQuery(this).attr('checked') == 'undefined'){  
                $('.dep-'+$(this).attr('id')).addClass('hide-block')
            } else {
                if($('.dep-'+$(this).attr('id')).hasClass('hide-block')){
                    $('.dep-'+$(this).attr('id')).removeClass('hide-block')
                }
            }
        });
    }
    
    $('.cls-dependency').click(function(){
        var blk = $('.dep-'+$(this).attr('id'));
        if(typeof jQuery(this).attr('checked') == 'undefined')  {
            if($(blk).hasClass('hide-block') == false) {
                $(blk).addClass('hide-block');
            }
        } else {
            if($(blk).hasClass('hide-block') == true) {
                $(blk).removeClass('hide-block');
            }
        }

    });
	
})

function uploaEcellMembers() {
    var m = jQuery('#target_overview_members');
    var ret = m.contents().find('body').html();
    var members = eval("("+ret+")");
    
    $('#over_num').val(members.over_num);
    $('#over_eleadnum').val(members.over_eleadnum);
    
    
    var _over_members = {};
    _over_members.data = members.over_members;
    _over_members.metadata = ecell.data.over_members.metadata;
    ecell.overviewMemberGrid.processData(_over_members, "over_members");
    
    
    // var m = _over_members.data;
    var m = members.over_eleaders;
     
    var htmOptions = '';
    for(i=0; i< m.length; i++) {
        htmOptions += '<option>'+m[i].values.name+'</option>';
    }
    $('#over_pointofcontact1').html(htmOptions);
    $('#over_pointofcontact2').html(htmOptions);
}

function uploaEstabProfessionals() {
    var m = jQuery('#target_estab_professionals');
    var ret = m.contents().find('body').html();
    var professionals = eval("("+ret+")");
    
    var _estab_professionals = {};
    _estab_professionals.data = professionals.estab_professionals;
    _estab_professionals.metadata = ecell.data.estab_professionals.metadata;
    ecell.estabProfessionalsGrid.processData(_estab_professionals, "establish_professionals");
}


function uploadFiles(name) {
    var m = jQuery('#target_'+name);
    var ret = m.contents().find('body').html();
    var res = eval("("+ret+")");
    console.log(res);
    if(res.status == 'success') {
        $('#download_'+name).attr('class', 'label-up');
    } else {
        $('#download_'+name).attr('class', 'label-up hide-block');
    }
//ecell.showBlock('overview');
//ecell.editBlock('overview');
//console.log(members)
}

//Prakash Script

 $(document).ready(function(){
            checkBoxStatus();
            hideShow();
        
            $('#block2,#block3,#block4').click(function(){
                checkBoxStatus();
                hideShow();
            });
           
        });
        
        function checkBoxStatus(){
            $('.view-less,.view-less1,.view-less2,.view-less3,.view-less4,.view-less5').hide();
            $('.hidden-block,.hidden-block1, .hidden-block2 , .hidden-block3, .hidden-block4,.hidden-block5').hide();
            $(':checkbox').each(function() {
                var id = 'status_'+ $(this).attr('id');
                var status = $(this).attr('checked');
                var yes_list = "#yes_"+ id;
                var label_id="#"+id;
                var label_class="."+id;
                if( status == 'checked'){
                    $(label_id).show().text('Yes');
                    $(yes_list).show();
                    var sponserList= $('#lbl_estab_sponsors ol li').size();
                    $('#sponsorlist').text(sponserList +'  sponsor');
                    
                    var yespart = $('#view_establish_professionals tr').size();
                    if(yespart=="1"){
                        $('.view-more3').hide();
                    }else{
                        $('.view-more3').show();
                    }
                } 
                else {
                    $(label_id).show().text('No');
                    $(yes_list).hide();
                    $(label_class).hide();
                }   
            });
            $('input[type="text"]').each(function() {
                var id = $(this).attr('id');
                var idv =$(this).val();
                var lbl = '#lbls_'+ $(this).attr('id');
                var lblv = '#lblv_'+ $(this).attr('id');
                if( idv == ''){
                    $(lbl).show().text('No Data');
                    $(lblv).show().text('0');
                } 
                else {
                    $(lbl).hide();
                    $(lblv).hide();
                }   
            });
             
        }
    
        function hideShow(){
            var yes = $('#status_hiperf_sign').text();
            var evntTitle = $('#hiperf_signtitle').val();
            var evntDate = $('#hiperf_signdate').val();
            if(yes=="Yes"){
                if(evntTitle=="" && evntDate =="" ){
                    $('.view-more5').hide();
                }
                else {
                     $('.view-more5').show();
                }
               
            }else{
                $('.view-more5').hide();
            }
            
            
            
            var emember = $('#view_over_members tr').size();
            if(emember=="1"){
                $('.view-more').hide();
            }else{
                $('.view-more').show();
            }
       
        }
      

           

function get_download_link(data){
        if(data){
            var vals  = data.split('/');
            var link1= vals.pop();
            var link2= vals.pop();
            var link3= vals.pop();
            var link = link3+'/'+ link2 +'/' + link1 ;
            return link;
        }
    }
    
    
function update_ecell_options(){
    setTimeout(get_upload_data, 3000);
  
}
function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
    i, val, params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}

function get_upload_data(){
    
    var id = getParameter('instituteid');
    var module = 'ecell';
   var url = '/ajax/get-uploaded-data?module='+module+'&id='+id;
   var link = 
   $.get( url, function( data ) {
            $.each( data[0], function( key, value ) {
          
           var lbl = 'lbl_'+key;
                if(value){
                   
                    var link = '/'+ value.split('/').slice(-3).join('/');
                    $('#'+lbl+' a').attr('href',link);
                    $('#'+lbl).show();
                }else{
                    $('#'+lbl).hide();
                }
          });
          
}, "json" );
}