$(document).ready(function(){
    campus = {
		
        showBlock: function(block) {
            var fields = campus.groups[block];
            var data = campus.data;
			
            for(f in fields) {
                var itm = '#lbl_'+ f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1 || data[f]=="1") {
                        $(itm).attr('checked', 'checked');
                    } else {
                        $(itm).removeAttr('checked');
                    }
                } else if( type == 'text') {
                    $(itm).html(data[f]);
                //console.log(data[f]);
                } else if(type == 'select') {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).html(data[f].caption);
                        } else {
                            $(itm).html(data[f]);
                        }
                    }
                } else if(type == 'grid') {
                    var dfname = fields[f].field;
                    if(f == 'student_members') {
                        campus.showStudentMembers();
                    }
                    else if(f == 'advisors') {
                        campus.showAdvisors();
                    }
					
                //console.log(data[gname]);
                } else if (f == 'ccteam_additional_fac') {
                    //console.log(data[f]);
                    var add_fac = '<ul>';
                    for(i=0; i< data[f].length; i++) {
                        add_fac = add_fac + '<li>'+data[f][i].caption+'</li>';
                    }
                    add_fac = add_fac + '</ul>';
                    $('#lbl_ccteam_additional_fac').html(add_fac);
                
                    //$(s).prepend(opt);
                } else if (type == 'multiple_sponsors') {
                    campus.showMultipleSponsors(f);
                }
                
                
            }
            //if(campus.groups.detail.)
        },
		
        editBlock: function(block) {
            var fields = campus.groups[block];
            var data = campus.data;
		
            for(f in fields) {
                var itm =  '#'+f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1) {
                        $(itm).attr('checked', 'checked');
                    } else {
                        $(itm).removeAttr('checked');
                    }
                } else if(type == 'select' ) {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).val(data[f].value);
                            try {
                                /*hack for custom select for select2js */
                                $('#s2id_'+f+ ' a span').text(data[f].caption);
                            } catch(e) {

                            }
                        } else {
                            $(itm).val(data[f]);
                            $('#s2id_'+f+ ' a span').text(data[f]);
                        }   
                                        

                    }
                } else if (type == 'text') {
                    $(itm).val(data[f]);
                //console.log(data[f]);
                } else if(type == 'grid') {
                    var dfname = fields[f].field;
                    if(f == 'student_members') {
                        campus.editStudentMembers();
                    }
                    else if(f == 'advisors') {
                        campus.editAdvisors(); //2nd parameters for eleaders
                    }

                //console.log(data[gname]);
                } else if (type == 'multiple_sponsors') {
                    campus.editMultipleSponsors(f);
                }
                
                if(f == 'cchealth_closure') {
                    if(campus.tdata.ccdetail_status=='Active') {
                        $('#cchealth_closure').prop('disabled', true);
                    } else {
                        $('#cchealth_closure').prop('disabled', false);
                    }
                }
                
            }
            
            var n='A Gandhi'
            var s = '#s2id_ccteam_additional_fac ul';
            var opt ='<li class="select2-search-choice"><div>A Gandhi</div><a href="#" onclick="return false;" class="select2-search-choice-close" tabindex="-1"></a></li>';
            //$(s).prepend(opt);
        },
	
		
        showStudentMembers: function() {
            var d = campus.data.student_members;
            //console.log(d.data.length);
             try{  if(d.data.length == 0)
            {
               $('#div_stu_members').hide(); 
            }
            else{
                $('#div_stu_members').show();  
                
            }
            }
            catch(e){
                $('#div_stu_members').hide();  
            }
            campus.viewStudentGrid.processData(d, "showccStudentMembers");
        },
		
        editStudentMembers: function() {
            var d = campus.data.student_members;
            campus.studentGrid.processData(d, "ccStudentmembers");
        },
		
        showAdvisors: function() {
            var d = campus.data.advisors;
            //console.log(d.data.length);
//             if (  typeof d.data == 'undefinded') {
//                 $('#div_adv_members').hide();
//               
//                
//            }else if( d.data.length == 0){
//               $('#div_adv_members').hide(); 
//            }
//           
//            else{
//                $('#div_adv_members').show();  
//               
//            }
            //
            try{
               if( d.data.length == 0){
               $('#div_adv_members').hide(); 
            } 
            else{
                $('#div_adv_members').show();  
               
            }
            }
            catch(e){
                $('#div_adv_members').hide();   
            }
            campus.viewAdvisorGrid.processData(d, "showccAdvisors");
        },
		
        editAdvisors: function() {
            var d = campus.data.advisors;
            campus.advisorGrid.processData(d, "ccAdvisors");
        },
        show: function(block) {
            //if(!block) return;
            //console.log(block)
            $('#'+block+ '-container .view-block').show();
            $('#'+block+ '-container .edit-block').hide();
			
        },

        edit: function(block) {
            if(!block) return;
            //.log(block)
            $('#'+block+ '-container  .view-block').hide();
            $('#'+block+ '-container  .edit-block').show();
        //console.log($('#'+block+ '-container  .edit-block'))
        },
		
        initShowBlock: function(block) {
            if(!block) return;
            $('#edit-link-'+block).removeClass('hide-block');
            campus.showBlock(block);
            campus.editBlock(block);
            campus.show(block);
        },
		
        updateEditBlock: function(block) {
            if(!block) return;
            campus.showBlock(block);
            campus.editBlock(block);
            campus.edit(block);
        },
		
        save: function(block) {
            $('a.ajax-loader').trigger('click');
            jQuery.ajax({
                type: "POST", 
                url: "/ajax/",
                data: {
                    'method': 'campus.update',
                    'data': campus.tdata, 
                    'block': block, 
                    'campusid': campusid
                },
                dataType: 'json',
                success: function(response) {
                    if(response == true) {
                        var blockfields = campus.groups[block]; 
                        for(f in blockfields) {
                            campus.data[f] = campus.tdata[f];
                        }
                        campus.initShowBlock(block);
                         checkList();
                         checkStatus();
                        checkSelected();
                        notify('#noti-campus-'+block, 'success', 'Record updated ...');
                        $('h6.adv_hide,#showccAdvisors').removeClass('adv_hide');
                        $('h6.stu_hide,#showccStudentMembers').removeClass('stu_hide');
                    } else {
                       // console.log('Unexpected Error: Data not saved');
                        notify('#noti-ecell-'+block, 'error', 'Record not updated ...');
                    }
                    //console.log(response);
                    //console.log(block);
                },
                complete: function() {
                    $('button.ajax-close').trigger('click');
                    //console.log('here');
                }
            });
			
        },
		
		
        loadData: function(campusid) {
            campus.studentGrid = new EditableGrid("CCStudentGrid"); 
            campus.viewStudentGrid = new EditableGrid("ViewCCStudentGrid");
            campus.advisorGrid = new EditableGrid("CCAdvisorGrid"); 
            campus.viewAdvisorGrid = new EditableGrid("ViewCCAdvisorGrid");
            campus.data=campusData;
            campus.tdata=campusData;
            campus.showBlock('overview');
            campus.editBlock('overview');
            campus.showBlock('detail');
            campus.editBlock('detail');
            campus.showBlock('team');
            campus.editBlock('team');
            campus.showBlock('healthstatus');
            campus.editBlock('healthstatus');
            
            if(defaultEdit) {
                campus.edit('overview');
                $('#edit-link-overview').addClass('hide-block');
            } else {
                campus.show('overview');
            }
            campus.show('detail');
            campus.show('team');
            campus.show('healthstatus');
            
//            jQuery.ajax({
//                url: '/ajax/',
//                data: {
//                    'method': 'campus.get',
//                    'campusid': campusid
//                },
//                type: 'post',
//                dataType: 'json',
//                cache: false,
//                success: function(responseJSON) {
//                    campus.data=responseJSON;
//                    campus.tdata=responseJSON;
//                    campus.showBlock('overview');
//                    campus.editBlock('overview');
//                    campus.showBlock('detail');
//                    campus.editBlock('detail');
//                    campus.showBlock('team');
//                    campus.editBlock('team');
//                    campus.showBlock('healthstatus');
//                    campus.editBlock('healthstatus');
//                    campus.show('overview');
//                    campus.show('detail');
//                    campus.show('team');
//                    campus.show('healthstatus');
//                },
//     
//                complete: function() {
//				             
//                }
//            });
        }
    };
	
	
    campus.groups ={
        'overview': {
            "cc_name":{
                "type":"text",
                "validation": ["required"]
            },
            "cc_date":{
                "type":"text"
                
            },
            "cc_area":{
                "type":"select",
                "validation": ["required"]
            }
        },
        'detail': {
			
            "ccdetail_areadetail":{
                "type":"text"
            },
            "ccdetail_outlay":{
                "type":"text",
                "validation": ["number"]
            },
            "ccdetail_status":{
                "type":"select"
            }
        },
        'team': {
            "ccteam_fac":{
                "type": "select"
            },
            "ccteam_additional_fac":{
                "type": "multiselect"
            },
            "student_members": {
                "type": "grid"
            },
            "advisors": {
                "type": "grid"
            }
        },
        'healthstatus': {
            "cchealth_closure":{
                "type":"text"
            },
            "cchealth_revenue":{
                "type":"text",
                "validation": ["number"]
            },
            "cchealth_review":{
                "type":"text"
            },
            "cchealth_remarks":{
                "type":"text"
            }
        }
    };
	
    campus.loadData(campusid);
	
    $('.lnk-show-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('show-link-'.length);
        campus.show(blk);
    //.log(blk);
		
    });
	
    $('.lnk-edit-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('edit-link-'.length);
		
        //console.log('--'+blk);
        campus.edit(blk);
        $(this).addClass('hide-block');
		
    });
	
        /** save overview **/
	
    $('.cls-save-overview').click(function(){
        var fields = campus.groups.overview;
        var isValid = true;
        isValid = dovalidate(fields, 'noti-campus-overview');
        if(! isValid) {
            return;
        }
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type != 'grid') {
                campus.tdata[f] = jQuery(item).val();
            }
        }
        campus.save('overview');
    });
    
    
	
    $('.cls-cancel-overview').click(function(){		
        campus.initShowBlock('overview');
    });
	
    /** save detail **/
    $('.cls-save-detail').click(function(){
        var fields = campus.groups.detail;
        var isValid = true;
        isValid = dovalidate(fields, 'noti-campus-detail');
        if(! isValid) {
            return;
        }
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type == 'checkbox') {
                if(typeof jQuery(item).attr('checked') == 'undefined')  {
                    campus.tdata[f] = "0";
                }else {
                    campus.tdata[f] = "1";
                }
            //console.log(jQuery(item).attr('checked'));
            } else {
                campus.tdata[f] = jQuery(item).val();
            }
        }
        campus.save('detail');
		
    });
	
    $('.cls-cancel-detail').click(function(){		
        campus.initShowBlock('detail');
    });
    
    
    /** save team **/
    $('.cls-save-team').click(function(){
        var fields = campus.groups.team;
        
        var facs = [];
        var i =0;
        $('#ccteam_additional_fac :selected').each(function(){
                var fac = {
                    'value' : $(this).val(),
                    'caption': $(this).text()
                }
                console.log(fac);
                facs[i] = fac;
                i++;
        });
        campus.tdata.ccteam_additional_fac = facs; 
        console.log(campus.tdata.ccteam_additional_fac);
        
        
        for(f in fields) {
            var item = '#'+ f;	
            
            
            if(fields[f].type == 'select' ) {
                var facid = jQuery(item).val();
                var facname = $(item + ' :selected').text();
                var fac = {
                    'value': facid, 
                    caption: facname
                };
                campus.tdata[f] = fac;

            }
            
            
        }
        
        /** student_members **/
        var d = campus.tdata.student_members.metadata;
        var noCols = d.length;
        var nd = campus.studentGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {'id': i+1, 'values' : o};
        }
        campus.tdata.student_members.data = newdata;
        
        
        /** advisors **/
        var d1 = campus.tdata.advisors.metadata;
        var noCols1 = d1.length;
        var nd1 = campus.advisorGrid.data;
        var newdata1=[];
        for(i=0 ; i< nd1.length; i++ ) {
            var o={};
            for(j=0; j< noCols1; j++) {
                var col = d1[j].name;
                var val = nd1[i].columns[j];
                o[col] =  val;
                
            }
            newdata1[i] = {'id': i+1, 'values' : o};
        }
        campus.tdata.advisors.data = newdata1;
        
        
        campus.save('team');
		
    });
	
    $('.cls-cancel-team').click(function(){		
        campus.initShowBlock('team');
    });
	
    /** save healthstatus **/
    $('.cls-save-healthstatus').click(function(){
        var fields = campus.groups.healthstatus;
        var isValid = true;
        isValid = dovalidate(fields, 'noti-campus-healthstatus');
        if(! isValid) {
            return;
        }
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type != 'grid') {
                campus.tdata[f] = jQuery(item).val();
            }
        }
        campus.save('healthstatus');
		
    });
    
    $('.cls-cancel-healthstatus').click(function(){		
        campus.initShowBlock('healthstatus');
    });
	
    jQuery("input:file").unbind('change').bind('change', function(e) {
        var frm = 'form_'+$(this).attr('id');
        //console.log($(this));
        document.forms[frm].submit();
    });
    
    jQuery('#ccdetail_status').change(function(e){
        if($(this).val()=='Active') {
            $('#cchealth_closure').prop('disabled', true);
        } else {
            $('#cchealth_closure').prop('disabled', false);
        }
    })
    
    /** add new item to members grid**/
    $("#add-grid-campus_members").click(function() {
//        show the view mode data grid 
        
        var d = campus.tdata.student_members.metadata;
        var noCols = d.length;
        var nd = campus.studentGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {'id': i+1, 'values' : o};
        }
        var _members = {};
    
        _members.data = newdata;
        _members.metadata = campus.data.student_members.metadata;
        
        //add column to tdata
        var last = _members.data.length;
        
        var newitem = {
            "id" : last+1,
            "values" : {
                "designation": " ",
                "first_name": " ",
                "last_name": "",
                "dob": "",
                "sex": "M",
                "email": "",
                "contact_no": "",
                "start_date": "",
                "remarks": ""
            }
        }
        _members.data[last] = newitem;
        campus.studentGrid.processData(_members, "ccStudentmembers");
    });
    
    /** add new item to advisor grid **/
    $("#add-grid-campus_advisors").click(function() {
        
        var d = campus.tdata.advisors.metadata;
        var noCols = d.length;
        var nd = campus.advisorGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {
                'id': i+1, 
                'values' : o
            };
        }
        var _advisors = {};
    
        _advisors.data = newdata;
        _advisors.metadata = campus.data.advisors.metadata;
        
        //add column to tdata
        var last = _advisors.data.length;
        
        var newitem = {
            "id" : last+1,
            "values" : {
                "designation": "",
                "institute": "",
                "first_name": "",
                "last_name": "",
                "apellation": "",
                "date_of_joining": "",
                "date_of_leaving": "",
                "remarks": ""
            }
        }
        
        _advisors.data[last] = newitem;
        campus.advisorGrid.processData(_advisors, "ccAdvisors");
    });
    
    //delete campus
 
});

function uploadCampusMembers() {
    var m = jQuery('#target_students_members');
    var ret = m.contents().find('body').html();
    var members = eval("("+ret+")");
    
    var _members = {};
    _members.data = members.student_members;
    _members.metadata = campus.data.student_members.metadata;
    campus.studentGrid.processData(_members, "ccStudentmembers");
    
}

function uploadCampusAdvisors() {
    var m = jQuery('#target_advisors');
    var ret = m.contents().find('body').html();
    var advisors = eval("("+ret+")");
    
    var _advisors = {};
    _advisors.data = advisors.advisors;
    _advisors.metadata = campus.data.advisors.metadata;
    campus.advisorGrid.processData(_advisors, "ccAdvisors");
}

$(document).ready(function(){
        checkList();
        checkStatus();
        checkSelected();
    });
    function checkList(){
        $('.view-less,.view-less1,.view-less2').hide();
        $('.hidden-block,.hidden-block1, .hidden-block2').hide();
        var sponserList= $('#lbl_ccteam_additional_fac ul li').size();
        if(sponserList==0){
            $('#addFaculty').text('No Data');
            $('.view-more').hide();
        }
        else if(sponserList==1){
            $('#addFaculty').text(sponserList +' '+' faculty co-ordinator');
             $('.view-more').show();
        }
        else{
            $('#addFaculty').text(sponserList +' '+' faculty co-ordinators');
             $('.view-more').show();
        }
        var advList=$('#showccAdvisors tr').size();
        if(advList==1){
            $('#advList').text('No Data');
            $('.view-more2').hide();
        }
        else if(advList==2){
            $('#advList').text(advList-1 +' '+' advisior');
             $('.view-more2').show();
        }else {
            $('#advList').text(advList-1 +' '+' advisiors');
             $('.view-more2').show();
        } 
        
        var studList=$('#showccStudentMembers tr').size();
        if(studList==1){
            $('#studList').text('No Data');
            $('.view-more1').hide();
        }
        else if(studList==2){
            $('#studList').text(studList-1 +' '+' student'); 
             $('.view-more1').show();
        }else{
            $('#studList').text(studList-1 +' '+' students'); 
             $('.view-more1').show();
        }
    }
        
    function checkStatus(){
        $('input[type=text]').each(function(){
            var oldId = 'lbl_'+ $(this).attr('id');
            var newId = 'status_'+ $(this).attr('id');
            var idv =$(this).val();
            var oldValue ="#"+oldId;
            var newValue = "#"+newId;
            if(idv==""){
                console.log(newValue);
                $(oldValue).text('No Data');
                $(newValue).hide();
            }
        });
        
    }
    
    function checkSelected() {
        var r=$("#lbl_ccteam_fac").text();
        if(r==""){
            $("#lbl_ccteam_fac").text("No Data");
            //console.log("empty");
        }
        //console.log(r);
    }

    
