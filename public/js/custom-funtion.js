jQuery(document).ready(function(){
    
//        for updatint the activity bar graph in institute dashboard page 
//            for default state
//hide content header name 
           
            var year = new Date().getFullYear();
            var institute_id  = $('#instituteid').val();
            load_dashboard_activities(year,institute_id);
            $('#btn_activityCalender').click(function(){
                var year = '';
                var institute_id = '';
                load_dashboard_activities(year,institute_id);
            });
        //for opening the section 
       // $('#t-open').click();
        var hash = window.location.hash
        switch(hash){
            case '#tab1':
                  // $('#t-open').click();
                   
                   break;
            case '#tab3':
                   $('#t-close1').click();
                   $('#t-open').click();
                   break;
            case '#tab5':
                    $('#t-close2').click();
                    $('#t-open').click();
                   break;
           case '#tab7':
                   $('#t-close3').click();
                   $('#t-open').click();
                   break;
           default:
                   //$('#t-open').click();
                   break;        
        }
        
        // for geting default venues
        
        city_venue();
        // end for valdation purpose
        $('.delActivity').click(function(){
            var id = $(this).attr('value');
            var name = $(this).closest('tr').find('a[href^="/activity/edit"]').text();
            
            name  = $.trim(name);
            $('#delActivity #activity_id').attr('value',id);
            $('#delActivity #activityName').text(name); 
        });
      
        
        $('#submitactivitycontributor').click(function(){
          
             var AoA = $('#test tr').map(function(){
                return [
                    $('td',this).map(function(){
                        return $(this).text();
                    }).get()
                ];
                  }).get();
            var json = JSON.stringify(AoA);
            
                       
        }); 
        

        
        // changing the venue names after changing the city in main activity edit page 
        function city_venue(){
            var city =  $('select[id="activity_city"]').val();
            
            if (typeof(city) == "undefined") {
                return false;
            }
            
            $.ajax({
                url:"/activity/getvenue?city="+city,
                type: 'GET',
                dataType: 'json',
                beforeSend: function ( xhr ) {
                    $('.ajax_load_venue').remove();
                    $('#div_activityplacetime_venue').hide().before('<span class="ajax_load_venue">Please Wait...</span>');
                  }
                ,
                success:function(data) {
                    //console.log(data);
                    var htm = '';
                    if(data['error']){
                       $('.ajax_load_venue').text(data['data']);
                    }else{
                        for(i=0;i<data.length;i++){
                       if(i==0){
                           htm +='<option selected value="'+data[i]['id']+'">'+data[i]['name']+'</option>';
                          
                       }else{
                                htm +='<option value="'+data[i]['id']+'">'+data[i]['name']+'</option>';
                            }

                         }
                         $('select[id="activityplacetime_venue"]').html(htm);
                  
                  $('#div_activityplacetime_venue').show('fast',function(){$('.ajax_load_venue').remove();})
                    }
                   

                    
                   
                }
                
             });
        }
        $('select[id="activity_city"]').change(function() {
            city_venue();
        });
        
        //end function 
        // changing the faculty names after changing the institute in main activity edit page  div_lead-speaker
        $('select[id="leadSpeakerInstitute"]').change(function() {
            
            var id =  $('select[id="leadSpeakerInstitute"]').val();
            //alert(inst);
            $.ajax({
                url:"/activity/getfacultylist",
                type: 'POST',
                dataType: 'json',
                data : {'instituteid':id},
                 beforeSend: function ( xhr ) {
                     $('.ajax_load_city').remove();
                    $('#div_lead-speaker').hide().before('<span class="ajax_load_city">Please Wait...</span>');
                  }
                ,
                success:function(data) {
                    //console.log(data);
                    
                   
                    
                    if(data['error']){  
                       
                         $('.ajax_load_city').text(data['data']);
                    }else{
                                $('#div_lead-speaker').show('fast',function(){$('.ajax_load_city').remove();})
                                var htm = '';
                                var prevHtml = '';
                                $('select[id="lead-speaker"] option:selected').each(function () {
                                    prevHtml += '<option selected value="' + $(this).val() + '">'+$(this).text() + '</option>';
                                  });

                               for(i=0;i<data.length;i++){
                                   if(i==0){
                                       htm +='<option  value="'+data[i]['id']+'">'+data[i]['first_name']+' '+data[i]['last_name']+'</option>';
                                   }else{
                                       htm +='<option value="'+data[i]['id']+'">'+data[i]['first_name']+' '+data[i]['last_name']+'</option>';
                                   }

                                }
                    }
                    
                   $('select[id="lead-speaker"]').html(prevHtml+ htm); 
                    
                },
        error: function(error) {
            $('.ajax_load_city').remove();
        }
               
                
             });
       
            
        });
        

        
 $('#activityoutcome_plan_yes').click(function(){
          $('.follow-updetails').show();
        });
        $('#activityoutcome_plan_no').click(function(){
          $('.follow-updetails').hide();
        });
        
        //end function 
    });
          
    
function load_dashboard_activities(year,institute_id){
    //set msg for empty rows in 
    
    $.ajax({
        type: "POST",
        url: "/dashboard/getinstactivity/",
        data: {'year':year,'instituteid':institute_id},
        timeout: 2000,
        dataType: 'json',
        beforeSend: function() {
           
            //$("div#activityCalender").html('<span class="span_activityCalender">Loading....</span>');
        },
        complete: function() {
             //$(".span_activityCalender").remove();
            
        },        
        cache: false,
        success: function(result) {
            //console.log(result);
            //for showing message 
            //console.log(result);
            if(!jQuery.isEmptyObject(result)){
            $('.activity_graph').show();
            $('.activity_msg').hide();
            }
            else{
                $('.activity_graph').hide();
            $('.activity_msg').show();
            }
            try{
                
                    $.each(result, function(key, value){ 
                        
                        $.each(value, function(k, v){
                            //console.log(v.activity_id);
                            //console.log(institute_id);
                            var link = '/activity/edit?id='+parseInt(v.activity_id, 10)+'&instituteid='+parseInt(institute_id, 10);
                            // console.log(link);
                            var htm = '<p class="activityPara"><a href="'+link+'" rel="tooltip" class="tip-top" title="'+v.activityinfo_title+' ">'+v.activityinfo_title+'</a><span class="activityType">'+v.activityinfo_type+'</span>'+display_date(v.activityplacetime_date_start)+'</p>';
                        $('#td-'+key).append(htm);
                        //console.log('#td-'+key); 
                        
                    });
                    });
         
        }
            
            catch(err){
                $(".span_activityCalender").remove();
             $("div#activityCalender").html('<div class="alert">Oh snap! Change a few things up and try submitting again.</div>');
            }
            
                        
           
        },
        error: function (request, status, error) {
                //$(".span_activityCalender").remove();
              //$("div#activityCalender").html('<div class="alert">Oh snap! Change a few things up and try again.</div>');
    }
    });
    
}
function display_date(date){
        if(!date || date=='0000-00-00 00:00:00' || date==''){
            return '';
        }
       var m_names = new Array("January", "February", "March", 
"April", "May", "June", "July", "August", "September", 
"October", "November", "December");

        var d = new Date(date);
        var curr_date = d.getDate();
        var curr_month = d.getMonth()+1;
        var curr_year = d.getFullYear();
      var result  = (curr_date + "-" + curr_month  + "-" + curr_year);
      
      return result;
       
        
    }