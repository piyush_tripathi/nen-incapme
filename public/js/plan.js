$(document).ready(function() {
    load_plan();
    //update_form();
    $('#create_institute_plan').click(function() {
        var form = $('form');
        var callback = 'result_create_institute_plan';
        var url = form.attr('action');
        var data = form.serialize();

        ajax_call(form, url, data, callback);

    })
    $('.btn_save_form').click(function() {
        var form = $(this).closest('form');
        var callback = 'load_plan';
        var url = form.attr('action');
        var id = $(this).attr('id')
        var data = form.serialize();
        //$(form).submit();
        //// //console.log(url);
        //// //console.log(form);
        ajax_call(form, url, data, callback);
        $('#plan-info-view-hide, #plan-contr-view-hide, #plan-parti-view-hide,#plan-outcome-view-hide').click();
        $('.p1,p2,.p3,.p4').show();
        $('#success_' + id).html('<div class="alert alert-success success-top"> <button class="close" data-dismiss="alert">×</button><i class="icon-back"></i><span class="error-msg1"><strong> Success! </strong> Record updated.</span></div>');
    })

    $('#plan-info-view, #plan-contr-view, #plan-parti-view,#plan-outcome-view').click(function() {
        load_plan();
    });


    $('#btn_publish_plan').click(function() {
        $('#form_publish_plan').submit();
        load_plan();
    })
    $('#plan-info-view').on('click', function() {
        $('.p1').show();

        //         $(this).closest('form').each (function(){
        //            this.reset();
        //          });
    });
    $('#plan-act-info-view').trigger('click');

    $('#edit1').bind('click', function() {
        $('.p1').hide();
    });
    $('#plan-contr-view').on('click', function() {
        $('.p2').show();
        //            $(this).closest('form').each (function(){
        //  this.reset();
        //});
    });
    $('#plan-act-contr-view').trigger('click');

    $('#edit2').bind('click', function() {
        $('.p2').hide();
    });
    $('#plan-parti-view').on('click', function() {
        $('.p3').show();
        //            $(this).closest('form').each (function(){
        //  this.reset();
        //});
    });
    $('#plan-act-parti-view').trigger('click');

    $('#edit3').bind('click', function() {
        $('.p3').hide();
    });
    $('#plan-outcome-view').on('click', function() {
        $('.p4').show();
    });
    $('#btn_save_two').on('click', function() {
        var val = $('#s2id_gs_targeted_level span').text();
        $('#tab3 #lbl_gs_targeted_level').text(val);
        $('.p2').show();
    });





});

// for showing the other option in course block 
$('.select_course_type').live('change', function() {

    if ($(this).val() == 'Other') {

        var id = $(this).parent().parent().attr('id');
        $('#' + id + ' .div_course_other:last').show();

    }
    else {
        var id = $(this).parent().parent().attr('id');
        $('#' + id + ' .div_course_other:last').hide();
        $('#' + id + ' .div_course_other:last .course_other').attr('value', '');
    }
});
function result_create_institute_plan(data) {

    if (data['success']) {
        window.location.href = data['url'];
    }
    else {
        alert('error happpend');
    }
}
function ajax_post_form(form, callback) {



}
function ajax_call(source, url, data, callback) {

    var crnturl = url;
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        timeout: 3000,
        beforeSend: function() {
            //console.log(url);
            //console.log(data);
            //console.log(source);
            $('a.ajax-loader').trigger('click');
        },
        complete: function() {
            $('button.ajax-close').trigger('click');

        },
        cache: false,
        success: function(result) {
            console.log(result);
            $('button.ajax-close').trigger('click');


//                if(callback=='result_create_institute_plan' )
//                {
//                    var crntdata = new Array();
//                    var planid= result.plan_id;
//                    var strurl='http://qa.incapme.com/plan/edit?id='+planid+'&'+crnturl.substring(10);
//                    
//                    crntdata['url'] = strurl;
//                    crntdata['success']= 'success';
//                    
//                    result_create_institute_plan(crntdata);
//                }
//                else{

            var fn = window[callback];
            fn(result);
//                }

        },
        error: function(error) {
            $('button.ajax-close').trigger('click');

            //alert("Some problems have occured. Please try again later: " );
            //console.log(error);
        }
    });


}

function load_plan() {
    var source = $('#tab2');
    var callback = 'load_plan_result';
    var url = '/plan/getplandata/';
    var data = {
        'plan_id': getParameter('id')
    };
    //// //console.log(data);
    var type = $('#plan_type').val();
    if (type != 'create') {
        ajax_call(source, url, data, callback);
    }

}

function load_plan_result(data) {
    if (!data) {
        return false;
    }
    //  // //console.log(data);
    //    $(":text").each(function(){
    //        var id  = $(this).attr('id');
    //       //// //console.log(data[id]);
    //       
    //      $(this).attr('value', data[id]);
    //    });
    //$('#plan-info-view-hide, #plan-contr-view-hide, #plan-parti-view-hide,#plan-outcome-view-hide').click();
    $('input:not([type=hidden]),select,textarea').each(function() {
        //// //console.log(this.name); 

        if (this.name == 'iip_po_other[]' || this.name == 'iip_dkici_other[]' || this.name == 'gs_oicg_other[]' || this.name == 'gs_po_other[]' || this.name == 'eep_list[]') {
            var index = this.name.replace("[", "");
            index = index.replace("]", "");

            var value = data[index];
            var htm = multi_htm(value, this.name);
            //// //console.log(htm);
            var text = multi_lbl(value);
            if (value) {

                $('#div_' + index).html(htm);
                $('#lbl_' + index).html(text);
            }
            else {

                $('#lbl_' + index).html(text);
            }

        }
        else if ($(this).is('input:checkbox')) {
            var val = data[this.name];
            if (!val) {
                val = 0;
            }
            var htm = chk_htm(this.name, val);
            var text = chk_lbl(this.name, val);

        }
        else if ($(this).is('select')) {
            var val = data[this.name];
            if (val) {
                dd_htm(this.name, val);
                dd_lbl(this.name, val);
            }
        }
        else if ($(this).is('textarea')) {
            var val = data[this.name];
            if (!val) {
                val = '';
            }
            $(this).val(val);
            var id = $(this).attr('name');
            $('#lbl_' + id).text(val);
        }
        else if ($(this).attr('name') == 'tc_startdate[]') {  //course block
            var val = data['tc_course'];
            if (val) {
                tc_course_htm(val);
                tc_course_lbl(val);
            }
            else {

                tc_course_lbl(val);
            }
        }
        else if ($(this).attr('name') == 'im_other_date[]') {  //infra others block
            var val = data['im_other'];
            if (val) {
                im_other_htm(val);
            }
        }
        else if ($(this).attr('name') == 'ac_planneddate[]') {  //activity block
            var val = data['activityplan'];
            if (val) {
                activityplan_htm(val);
                activity_lbl(val);
            }
        }
        else if ($(this).attr('class')) {
            var val = data[this.name];
            if ($(this).attr('class').split(' ')[0] == 'datepicker' && val) {
                var val = data[this.name];
                //conver date yyyy-mm-dd to dd-mm-yyyy
                var date = db_to_date(val);
                $('input[name="' + this.name + '"]').attr('value', date);
            }


        }
        else {
            var val = data[this.name];
            if (!val) {
                val = '';
            }

            $(this).attr('value', val);
            if (val.length >= 25) {
                val = val.substring(0, 25) + '...'
            }

            $('#lbl_' + this.name).text(val);

        }
    });

    //    now cheching the form for stages : determine which forms to be shown 

    var stage = (parseInt(data['plan_stage']) + 1) + '';
    // //console.log('stage-'+stage);
    $('.plan_forms').hide();
    $('#view1,#view2,#view3,#view4').hide();   // hide all forms 
    //show forms as per the stage 
    switch (stage) {
        case '0':
            $('.forms').hide();
            break;
        case '1':
            //           alert('stage1');
            $('#view1').show();
            $('#edit1').show();
            break;
        case '2':
            $('#view1,#view2').show();

            break;
        case '3':
            $('#view1,#view2,#view3').show();

            break;
        case '4':
            $('#view1,#view2,#view3,#view4').show();

            break;

        case '5':
            $('#view1,#view2,#view3,#view4').show();
            break;
        case '6':
            $('#view1,#view2,#view3,#view4').show();
            $('#edit1,#edit2,#edit3,#edit4').hide();
            break;
        default:
            $('#view1').show();
            $('#edit1').show();
            break;

    }

    //end switch case 

    //   Now getting the checkbox status for the publish button display 
    if (data['leadfacultysignoff_yesno'] && data['nensignoff_yesno'] && data['instituteheadsignoff_yesno']) {
        $('#btn_publish_plan').show();
    }
    // now upating level values 
    update_label_number();
    var date = data['created'];
    update_plan_dates(date);
    dd_selected_other();
    check_publish();

}
function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
            i, val, params = searchString.split("&");

    for (i = 0; i < params.length; i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}




function multi_htm(data, source) {
    var htm = '';
    var btn = '';
    $(data).each(function(key, val) {
        switch (source) {
            case 'iip_po_other[]':
                btn = '<div class="repeat-box"> <span class="addnew add btn pull-right"><i class="icon-plus"></i></span> <input type="text" name="iip_po_other[]"  class="increment" value="" > </div>';
                htm += '<div class="repeat-box">   <span class="remove btn pull-right"><i class="icon-minus"></i></span>   <input type="text" name="iip_po_other[]"  class="increment" value="' + val + '" >  </div>';
                break;
            case 'iip_dkici_other[]':
                btn = '<div class="repeat-box"> <span class="addprogram add btn pull-right"><i class="icon-plus"></i></span>             <input type="text" name="iip_dkici_other[]"  class="increment" value="" >         </div> ';
                htm += '<div class="repeat-box">  <span class="remove btn pull-right"><i class="icon-minus"></i></span>     <input type="text" name="iip_dkici_other[]"  class="increment" value="' + val + '" >  </div> ';
                break;
            case 'gs_po_other[]':
                btn = '<div class="repeat-box">   <span class="addgoal add btn pull-right"><i class="icon-plus"></i></span>                                     <input type="text" name="gs_po_other[]"  class="increment" value="">                             </div>';
                htm += '<div class="repeat-box">    <span class="remove btn pull-right"><i class="icon-minus"></i></span>   <input type="text" name="gs_po_other[]"  class="increment" value="' + val + '">  </div>';
                break;
            case 'gs_oicg_other[]':
                btn = '<div class="repeat-box">    <span class="addcapacity add btn pull-right"><i class="icon-plus"></i></span>                                     <input type="text" name="gs_oicg_other[]" class="increment" value="">                             </div>'
                htm += ' <div class="repeat-box">    <span class="remove btn pull-right"><i class="icon-minus"></i></span> <input type="text" name="gs_oicg_other[]" class="increment"value="' + val + '">  </div> ';
                break;
            case 'eep_list[]':

                btn = '<div class="eep_list_more"> <span class="addnew add btn pull-right"><i class="icon-plus"></i></span> <input type="text" name="eep_list[]"  class="increment" value="" > </div>';
                htm += '<div class="eep_list_more">   <span class="remove btn pull-right"><i class="icon-minus"></i></span>   <input type="text" name="eep_list[]"  class="increment" value="' + val + '" >  </div>';
                break;
            default:

                break;
        }


    });

    return htm + btn;

}

function multi_lbl(data) {

    if (!data) {
        return '<ol></ol>';
    }
    var htm = '<ol>';
    $(data).each(function(key, val) {
        if (val !== 'null')
            htm += ' <li>' + val + '</li>';
    });
    htm += '</ol>';
    return htm;

}
function dd_htm(source, data) {

    $('#' + source + ' option').each(function(i) {

        if ($(this).val() == data) {

            $(this).attr('selected', true);
            var bt_id = '#s2id_' + source;
            $(bt_id + ' span').text($(this).text());
        }

    });
}

function dd_lbl() {

}

function chk_htm(source, data) {

    if (data == 1) {
        $('#' + source).attr('checked', 'true');
        $('.' + source).show();

    }
    else {
        $('#' + source).removeAttr("checked");
        $('.' + source).hide();
    }
}
function chk_lbl(source, data) {
    if (data == 1) {
        $('#lbl_' + source).text('Yes');
        $('.lbl_' + source).show();
    }
    else {
        $('#lbl_' + source).text('No');
        $('.lbl_' + source).hide();
    }
}



//script for page elements 


$(document).ready(function() {

    $('.remove').live('click', function() {
        $(this).parent().remove();
    });

    $('.addnew').live('click', function() {
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertAfter(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addnew').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
        //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });

    $('.addprogram').live('click', function() {
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertAfter(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addprogram').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
        //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });

    $('.addgoal').live('click', function() {
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertAfter(thisbox);
        newbox.find('.increment').val("");

        $(this).removeClass('addgoal').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
        //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });

    $('.addcapacity').live('click', function() {
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertAfter(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addcapacity').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
        //            newbox.find('in   put.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
    $('.addExpert').live('click', function() {
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertAfter(thisbox);
        newbox.find('input:not(.add)').val("");
        $(this).removeClass('addExpert').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');

    });

    $('.addcourse').live('click', function() {
        var htm = '    <div class="repeat-box3">\n\
                            <span class="remove btn pull-right" style="position:relative; left:15px;"><i class="icon-minus"></i></span>                             \n\
                             <select name="tc_course_type[]" class="chzn-select select_course_type" tabindex="10">                                                                       \n\
                                     <option value="none">Select Course type</option>\n\
                                     <option value="0">Orientation to Entrepreneurship</option>\n\
                                     <option value="1">Kick starting your entrepreneurship campus</option>\n\
                                     <option value="2">Building Technology Ventures</option>\n\
                                     <option value="3">Business Models and Business Plans</option>\n\
                                     <option value="4">Getting to Market</option>\n\
                                     <option value="5">Entrepreneurial Finance</option>\n\
                                     <option value="6">IPR Management</option>\n\
                                     <option value="7">Incubator management</option>\n\
                                     <option value="8">Entrepreneurial Finance</option>\n\
                                     <option value="9">Project to Process</option> \n\
                                     <option value="10">Building Organization</option>\n\
                                     <option value="11">Practical Ethics</option> \n\
                                     <option value="12">High Impact Infrastructure Design Launching intensive program</option>\n\
                                     <option value="13">NEN GS Tools for growth</option>\n\
                                     <option value="14">NEN GS Kick Starting Ventures</option>\n\
                                     <option value="15">Mentoring Skill Enhancement Program Practice Teaching</option>\n\
                                     <option value="16">Mentoring Skill Enhancement Program Practice Mentoring</option>\n\
                                     <option value="17">Mentoring Skills Finishing School</option>\n\
                                    <option value="Other">Other</option>\n\
                                 </select>                                 \n\
                                <span class="classBold"> Start Date : \n\
                                    <input type="text" name="tc_startdate[]" class="datepicker dp"  data-date-format="dd-mm-yyyy"/> \n\
                                </span>\n\
                                <div  class="div_course_other" style="display:none"> <input type="text" class="course_other" name="tc_course_type_other[]" placeholder="Other Course"></div>\n\
                             \n\
                            </div>';
        $(htm).appendTo('#div_tc');

        $('.dp').datepicker({
            autoclose: 'true'
        });
        $('.chzn-select').chosen({
            width: "37.7%"
        });



    });

    $('.im-other-mildstone').live('click', function() {
        console.log("append");
        var htm = '   <div class="double-reapter1"><span class="remove btn pull-right" id="remove-otherinfra"><i class="icon-minus"></i></span>                          \n\
                                 <span class="textfit"><input type="text" name="im_other_text[]"></span>                               \n\
                                <span class="double-date date1"> Date :\n\
                                    <input type="text" name="im_other_date[]" class="datepicker im-other-date"  data-date-format="mm-yyyy"/> \n\
                                <div class="clearfix"></span>\n\
                             <textarea name="im_other_otheractioncomments[]" placeholder="Commentary "></textarea>\n\
                            </div>';
        $(htm).appendTo('#div_im_other');
        //$('#div_im_other span:last').parent().css("margin-bottom", "35px");
        $('.im-other-date').datepicker({
            autoclose: 'true',
            minViewMode: 'months'

        });
    });
    $("#remove-otherinfra").live('click', function() {
        //$('#div_im_other span:last').parent().css("margin-bottom", "35px");
    });

    $('.activityPlan').live('click', function() {
        var htm = '   <div class="activity-reapter" style="position:relative;">\n\
                                <span class="remove actRemove btn pull-right"><i class="icon-minus"></i></span>                          \n\
                                <div class="act-name">Name : <input type="text" name="ac_name[]" value="New activity"> &nbsp;   \n\
                                    Start Date : <input type="text" name="ac_planneddate[]" class="datepicker ac-date"  data-date-format="dd-mm-yyyy">\n\
                                </div><div class="act-name1">   \n\
                            Type :  <select name="ac_type[]" class="chzn-select123" tabindex="10"> \n\
                                            <option value="none">Select Activity type</option>\n\
                                            <option value="0">Lecture or Talk</option>\n\
                                            <option value="1">Exercise & Games</option>\n\
                                            <option value="2">Outreach</option>\n\
                                            <option value="3">Panel Discussion</option>\n\
                                            <option value="4">Competition</option>\n\
                                            <option value="5">Conference</option>\n\
                                            <option value="6">Workshop</option>\n\
                                            <option value="7">Course</option>\n\
                                            <option value="8">Webinar</option>\n\
                                            <option value="9">Review</option>\n\
                                            <option value="10">Mentoring</option>\n\
                                            <option value="11">Startup support programs/showcase</option>\n\
                                            <option value="12">National Platforms</option>\n\
                                        </select> &nbsp;\n\
                                     Audience : <select name="ac_audience[]" class="chzn-select12" tabindex="10">\n\
                                            <option value="none">Select Targeted Audience</option>\n\
                                            <option value="0">Faculty</option>\n\
                                            <option value="1">Students/ Entrepreneur Pipeline</option>\n\
                                            <option value="2">Entrepreneurs</option>\n\
                                            <option value="3">Other-type text</option>\n\
                                        </select>\n\
                            </div></div>';
        $(htm).appendTo('#div_activityplan');
        $('.ac-date').datepicker({
            autoclose: 'true'
        });
        $('.chzn-select123').chosen({
             width: "32.9%"
        });
        $('.chzn-select12').chosen({
              width: "34.6%"
        });
    });

});


function db_to_date(dateStr)
{
    dArr = dateStr.split("-");  // ex input "2010-01-18"
    if (!dArr[2]) {
        return dateStr;
    }
    else {
        return dArr[2] + "-" + dArr[1] + "-" + dArr[0].substring(2); //ex out: "18/01/10"
    }
}
function  tc_course_htm(val) {
    if (!val) {
        return false;
    }

    // var box  = $('#div_tc').html();
    $('#div_tc').html('');
    var htm = '    <div class="repeat-box3">\n\
                                    <span class="remove btn pull-right" style="position:relative; left:15px;"><i class="icon-minus"></i></span>                              \n\
                                   <select name="tc_course_type[]" class="chzn-select123 select_course_type" tabindex="10">                                                                       \n\
                                     <option value="none">Select Course type</option>\n\
                                     <option value="0">Orientation to Entrepreneurship</option>\n\
                                     <option value="1">Kick starting your entrepreneurship campus</option>\n\
                                     <option value="2">Building Technology Ventures</option>\n\
                                     <option value="3">Business Models and Business Plans</option>\n\
                                     <option value="4">Getting to Market</option>\n\
                                     <option value="5">Entrepreneurial Finance</option>\n\
                                     <option value="6">IPR Management</option>\n\
                                     <option value="7">Incubator management</option>\n\
                                     <option value="8">Entrepreneurial Finance</option>\n\
                                     <option value="9">Project to Process</option> \n\
                                     <option value="10">Building Organization</option>\n\
                                     <option value="11">Practical Ethics</option> \n\
                                     <option value="12">High Impact Infrastructure Design Launching intensive program</option>\n\
                                     <option value="13">NEN GS Tools for growth</option>\n\
                                     <option value="14">NEN GS Kick Starting Ventures</option>\n\
                                     <option value="15">Mentoring Skill Enhancement Program Practice Teaching</option>\n\
                                     <option value="16">Mentoring Skill Enhancement Program Practice Mentoring</option>\n\
                                     <option value="17">Mentoring Skills Finishing School</option>\n\
                                     <option value="Other">Other</option>\n\
                                 </select>                                 \n\
                                <span class="classBold"> Start Date : \n\
                                    <input type="text" name="tc_startdate[]" class="datepicker dp"  data-date-format="dd-mm-yyyy"/> \n\
                                </span>\n\
                                <div  class="div_course_other"> <input type="text" class="course_other" name="tc_course_type_other[]" placeholder="Other Course"></div>\n\
                             \n\
                            </div>';
    $.each(val, function(k, v) {
        // if the value stored containes user defined field other and value in form of Other|<value>:date
        if (v[0].substring(0, 5) == 'Other') {

            var dd = v[0].substring(0, 5);
            var other_text = v[0].split('|')[1];
            var selected = 'Other';

        }
        else {
            dd = v[0];
            other_text = '';
        }


        if (v[1]) {
            var date = v[1];
        }
        else {
            var date = '';
        }
        $('#div_tc').append('<span id="tc_course' + k + '">' + htm + '</span>');
        var select = '#tc_course' + k + ' select';
        var text = '#tc_course' + k + ' .course_other';
        var div = '#tc_course' + k + ' .div_course_other';
        var input = $('#tc_course' + k + ' input[name="tc_startdate[]"]');
        $(select + ' option').each(function(i) {
            if ($(this).val() == dd) {

                $(this).attr('selected', true);
                $(text).attr('value', other_text);

            }

        });
        //hide if not other is selected 
        if (selected != 'Other') {
            $(div).hide();
        }

        $(input).attr('value', date);
        $('.dp').datepicker({
            autoclose: 'true'
        });
        $('.chzn-select123').chosen({
            width: "37.7%"
        });
    });
    //$('#div_tc_addmore').append('<span class="addcourse add btn pull-right" style="position:relative;"><i class="icon-plus"></i></span>');

}

function tc_course_lbl(val) {
    // //console.log(val);
    if (typeof val == "undefined") {
        $('#lbl_tc_course_type').html('');
        return null;
    }
    var course_type = {
        "0": "Orientation to Entrepreneurship",
        "1": "Kick starting your entrepreneurship campus",
        "2": "Building Technology Ventures",
        "3": "Business Models and Business Plans",
        "4": "Getting to Market",
        "5": "Entrepreneurial Finance",
        "6": "IPR Management",
        "7": "Incubator management",
        "8": "Entrepreneurial Finance",
        "9": "Project to Process",
        "10": "Building Organization",
        "11": "Practical Ethics",
        "12": "High Impact Infrastructure Design Launching intensive program",
        "13": "NEN GS Tools for growth",
        "14": "NEN GS Kick Starting Ventures",
        "15": "Mentoring Skill Enhancement Program Practice Teaching",
        "16": "Mentoring Skill Enhancement Program Practice Mentoring",
        "17": "Mentoring Skills Finishing School",
        "Other": "Other"
    };
    var txt = '';
    $.each(val, function(k, v) {
        txt += '<li><b>Course : </b>' + course_type[v[0].substring(0, 5)] + '&nbsp;&nbsp;<b>Start Date : </b>' + v[1] + '</li>';
    });
    $('#lbl_tc_course_type').html(txt);


}
function activity_lbl(val) {
    var activity_plan = {
        "none": "Select Activity type",
        "0": "Lecture or Talk",
        "1": "Exercise & Games",
        "2": "Outreach",
        "3": "Panel Discussion",
        "4": "Competition",
        "5": "Conference",
        "6": "Workshop",
        "7": "Course",
        "8": "Webinar",
        "9": "Review",
        "10": "Mentoring",
        "11": "Startup support programs/showcase",
        "12": "National Platforms"
    };
    var activity_audience = {
        "none": "Select Target Audience",
        "0": "Faculty",
        "1": "Students/ Entrepreneur Pipeline",
        "2": "Entrepreneurs",
        "3": "Other-type text"
    }
    var txt = '';
    $.each(val, function(k, v) {
        txt += '<li><b>Activity name : </b>' + v[0] + '&nbsp; &nbsp;<b>Date : </b>' + v[1] + '&nbsp;&nbsp;<b>Type : </b>' + activity_plan[v[2]] + '&nbsp;&nbsp;<b>Audience : </b>' + activity_audience[v[3]] + '</li>';
    });
    $('#lbl_ac_type').html(txt);

}

function dd_text(source, val) {
    source.children('option').each(function(i) {

        if ($(this).val() == val) {
            var chk_text = $(this).text();
            return chk_text;
        }

    });

}
function  im_other_htm(val) {
    if (!val) {
        return false;
    }

    // var box  = $('#div_tc').html();
    //$('#div_im_other').html('<span class="im-other-mildstone add btn pull-right" style="position:relative; left:10px;"><i class="icon-plus"></i></span>');
    $('#div_im_other').html('');
    var htm = '   <div class="double-reapter1"><span class="remove btn pull-right" id="remove-otherinfra" style="position:relative;"><i class="icon-minus"></i></span>                          \n\
                                 <span class="textfit"><input type="text" name="im_other_text[]"></span>                               \n\
                                <span class="double-date date1"> Date :\n\
                                    <input type="text" name="im_other_date[]" class="datepicker im-other-date"  data-date-format="mm-yyyy"/> \n\
                                <div class="clearfix"></span>\n\
                             <textarea name="im_other_otheractioncomments[]" placeholder="Commentary "></textarea>\n\
                            </div>';
    $.each(val, function(k, v) {

        $('#div_im_other').append('<span id="im_other' + k + '">' + htm + '</span>');
        var name = '#im_other' + k + ' input[name="im_other_text[]"]';
        var date = '#im_other' + k + ' input[name="im_other_date[]"]';
        var details = '#im_other' + k + ' textarea[name="im_other_otheractioncomments[]"]';

        $(name).val(v[0]);
        $(details).val(v[2]);
        if (v[1]) {
            var date_text = v[1];
        }
        else {
            var date_text = '';
        }
        $(date).attr('value', date_text);

    });
    //$('#div_im_other span:last').parent().css("margin-bottom", "35px");
    $('.im-other-date').datepicker({
        autoclose: 'true',
        minViewMode: 'months'

    });

}

function  activityplan_htm(val) {
    if (!val) {
        return false;
    }
    $('#div_activityplan').html('');
    // var box  = $('#div_tc').html();
    //$('#div_activityplan').html('<span class="activityPlan add btn pull-right"><i class="icon-plus"></i></span>');
    var htm = '   <div class="activity-reapter">\n\
                                <span class="remove actRemove btn pull-right"><i class="icon-minus"></i></span>                          \n\
                                <div class="act-name">Name : <input type="text" name="ac_name[]"> &nbsp;   \n\
                                    Start Date : <input type="text" name="ac_planneddate[]" class="datepicker ac-date"  data-date-format="dd-mm-yyyy">\n\
                                    \n\
                                </div><div class="act-name1">   \n\
                            Type : <select name="ac_type[]"  class="chzn-select123" tabindex="10"> \n\
                                            <option value="none">Select Activity type</option>\n\
                                            <option value="0">Lecture or Talk</option>\n\
                                            <option value="1">Exercise & Games</option>\n\
                                            <option value="2">Outreach</option>\n\
                                            <option value="3">Panel Discussion</option>\n\
                                            <option value="4">Competition</option>\n\
                                            <option value="5">Conference</option>\n\
                                            <option value="6">Workshop</option>\n\
                                            <option value="7">Course</option>\n\
                                            <option value="8">Webinar</option>\n\
                                            <option value="9">Review</option>\n\
                                            <option value="10">Mentoring</option>\n\
                                            <option value="11">Startup support programs/showcase</option>\n\
                                            <option value="12">National Platforms</option>\n\
                                        </select> &nbsp;\n\
                                     Audience : <select name="ac_audience[]" class="chzn-select12" tabindex="10">\n\
                                            <option value="none">Select Targeted Audience</option>\n\
                                            <option value="0">Faculty</option>\n\
                                            <option value="1">Students/ Entrepreneur Pipeline</option>\n\
                                            <option value="2">Entrepreneurs</option>\n\
                                            <option value="3">Other-type text</option>\n\
                                        </select>\n\
                            </div></div>';
    $.each(val, function(k, v) {

        $('#div_activityplan').append('<span id="activityplan' + k + '">' + htm + '</span>');
        var name = '#activityplan' + k + ' input[name="ac_name[]"]';
        var date = '#activityplan' + k + ' input[name="ac_planneddate[]"]';
        var type = '#activityplan' + k + ' select[name="ac_type[]"]';
        var audi = '#activityplan' + k + ' select[name="ac_audience[]"]';
        $(name).val(v[0]);
        $(date).attr('value', v[1]);
        //for type 
        $(type + ' option').each(function(i) {
            if ($(this).val() == v[2]) {
                $(this).attr('selected', true);
            }
        });
        //for audi 
        $(audi + ' option').each(function(i) {
            if ($(this).val() == v[3]) {
                $(this).attr('selected', true);
            }
        });


        $('.ac-date').datepicker({
            autoclose: 'true'
        });
        $('.chzn-select123').chosen({
             width: "32.9%"
        });
        $('.chzn-select12').chosen({
              width: "34.6%"
        });
    });

}



function update_label_number() {
    $('.view-less,.view-less1,.view-less2,.view-less3,.view-less4,.view-less5,.view-less6,.view-less7,.view-less8, .view-less9').hide();
    $('.hidden-block,.hidden-block1, .hidden-block2,.hidden-block3,.hidden-block4,.hidden-block5,.hidden-block6,.hidden-block7,.hidden-block8, .hidden-block9').hide();
    var iipOther = $('#lbl_iip_dkici_other ol li').size();
    if (iipOther == 0) {
        $('#count_iip_dkici_other').text('No Data');
        $('.view-more1').hide();
    } else {
        $('#count_iip_dkici_other').text(iipOther);
        $('.view-more1').show();
    }

    var iipo_Other = $('#lbl_iip_po_other ol li').size();
    if (iipo_Other == 0) {
        $('#count_iip_po_other').text('No Data');
        $('.view-more').hide();
    } else {
        $('#count_iip_po_other').text(iipo_Other);
        $('.view-more').show();
    }

    var gspo_Other = $('#lbl_gs_po_other ol li').size();
    if (gspo_Other == 0) {
        $('#count_gs_po_other').text('No Data');
        $('.view-more2').hide();
    } else {
        $('#count_gs_po_other').text(gspo_Other);
        $('.view-more2').show();
    }

    var gs_oicg_Other = $('#lbl_gs_oicg_other ol li').size();
    if (gs_oicg_Other == 0) {
        $('#count_gs_oicg_other').text('No Data');
        $('.view-more3').hide();
    } else {
        $('#count_gs_oicg_other').text(gs_oicg_Other);
        $('.view-more3').show();
    }

    var tplan = $('#trainingplan').val();
    if (tplan == '') {
        $('#count_trainingplan').text('No Data').show();
        $('.view-more4').hide();
    } else {
        $('.view-more4').show();
        $('#count_trainingplan').hide();
    }

    var course = $('#lbl_tc_course_type li').size();
    if (course == 0) {
        $('#count_tc_course_type').text('No Data').show();
        $('.view-more5').hide();
    } else {
        $('.view-more5').show();
        $('#count_tc_course_type').text(course);
    }

    var extrExpert = $('#attractingexternalexperts').val();
    if (extrExpert == '') {
        $('#count_attractingexternalexperts').text('No Data').show();
        $('.view-more6').hide();
    } else {
        $('.view-more6').show();
        $('#count_attractingexternalexperts').hide();
    }

    var eeplist = $('#lbl_eep_list ol li').size();
    var eepsession = $('#eep_noofsessions').val();
    if (eeplist == 0 && eepsession == '') {
        $('#count_eep_list').text('No Data').show();
        $('.view-more8').hide();
    } else {
        $('.view-more8').show();
        $('#count_eep_list').hide();
    }

    var actplan = $('#lbl_ac_type li').size();
    if (actplan == '0') {
        $('#count_ac_type').text('No Data').show();
        $('.view-more9').hide();
    } else {
        $('.view-more9').show();
        $('#count_ac_type').text(actplan).show();
    }
    /// update the dropdown label 
    // for tc_course 
    //            for infrstructure milestones ecellmilestones_date

    var ecell = '<li><b>E-CELL : </b>' + $('#ecellmilestones_dropdown option:selected').filter(':not([value="none"])').text() + '<b>&nbsp;&nbsp; Date : </b>' + $('#ecellmilestones_date').val()
            + '&nbsp; &nbsp;<b>Commentary : </b>' + $('textarea[name="ecellmilestones_otheractioncomments"]').val() + '</li>';

    var edc = '<li><b>EDC : </b>' + $('#edcmilestones_dropdown option:selected').filter(':not([value="none"])').text() + '<b>&nbsp;&nbsp; Date : </b>' + $('#edcmilestones_date').val()
            + '&nbsp; &nbsp;<b>Commentary : </b>' + $('textarea[name="edcmilestones_otheractioncomments"]').val() + '</li>';

    var IEDC = '<li><b>IEDC : </b>' + $('#iedcesc_dropdown option:selected').filter(':not([value="none"])').text() + '<b>&nbsp;&nbsp; Date : </b>' + $('#iedcesc_date').val()
            + '&nbsp; &nbsp;<b>Commentary : </b>' + $('textarea[name="iedcesc_otheractioncomments"]').val() + '</li>';
    var mu = '<li><b>MU : </b>' + $('#svmu_dropdown option:selected').filter(':not([value="none"])').text() + '<b>&nbsp;&nbsp; Date : </b>' + $('#svmu_date').val()
            + '&nbsp; &nbsp;<b>Commentary : </b>' + $('textarea[name="svmu_otheractioncomments"]').val() + '</li>';
    var incu = '<li><b>INCUB : </b>' + $('#incubator_dropdown option:selected').filter(':not([value="none"])').text() + '<b>&nbsp;&nbsp; Date : </b>' + $('#incubator_date').val()
            + '&nbsp; &nbsp;<b>Commentary : </b>' + $('textarea[name="incubator_otheractioncomments"]').val() + '</li>';
    //            var edc = '<li>EDC' + $('#edcmilestones_dropdown option:selected').text() +':'+$('#edcmilestones_date').val()
    //                +':' +$('textarea[name="edcmilestones_otheractioncomments"]').val() + '</li>' ;
    var htm = '<ol>' + ecell + edc + IEDC + mu + incu + '</ol>';
    $('#lbl_ecellmilestones_dropdown').html(htm);

    var milestone = $('#lbl_ecellmilestones_dropdown li').size();
    if (milestone == 0) {
        $('#count_ecellmilestones_dropdown').text('No Data').show();
        $('.view-more7').hide();
    } else {
        $('.view-more7').show();
        $('#count_ecellmilestones_dropdown').text(milestone).show();
    }

    $('input[type="text"]').each(function() {
        var id = $(this).attr('id');
        var idv = $(this).val();
        var lbl = '#lbls_' + $(this).attr('id');
        var lblv = '#lblv_' + $(this).attr('id');
        if (idv == '') {
            $(lbl).show().text('No Data');
            $(lblv).show().text('0');
        }
        else {
            $(lbl).hide();
            $(lblv).hide();
        }
    });

}

//js for publish button
$("#instituteheadsignoff_yesno").on("click", function() {
    if ($('#instituteheadsignoff_yesno').attr('checked')) {
        $('.instituteheadsignoff_yesno').show();


    } else {
        $('.instituteheadsignoff_yesno').hide();
        //$('.publish').hide();
    }
});

$("#leadfacultysignoff_yesno").on("click", function() {
    if ($('#leadfacultysignoff_yesno').attr('checked')) {
        $('.leadfacultysignoff_yesno').show();

    } else {
        $('.leadfacultysignoff_yesno').hide();
        //$('.publish').hide();
    }
});

$("#nensignoff_yesno").on("click", function() {
    if ($('#nensignoff_yesno').attr('checked')) {
        $('.nensignoff_yesno').show();


    } else {
        $('.nensignoff_yesno').hide();
        //$('.publish').hide();
    }
});
function update_plan_dates(date) {
    //updates the dates lable for plan input , goal setting and getting there 
    if (!date) {
        return null;
    }
    date = date.substring(0, 4);
    var plan_input = '(' + (parseInt(date) - 1) + '-' + parseInt(date) + ')';
    var plan_goal = '(' + parseInt(date) + '-' + (parseInt(date) + 1) + ')';
    var plan_getting = '(' + parseInt(date) + '-' + (parseInt(date) + 1) + ')';

    $('.date_year_input').text(plan_input);
    $('.date_year_goal').text(plan_goal);
    $('.date_year_getting').text(plan_getting);


}


function dd_selected_other() {
    $('.select-other').each(function() {
        var id = $(this).attr('id');
        var oth = "#oth_" + id;
        var str = $('#' + id + " option:selected").val();

        if (str == "Other")
        {

            $(oth).show();

        } else {
            $(oth).hide();
        }
    });
}

function course_other_select() {
    // for showing the other option in course block 
    $('.select_course_type').live('change', function() {

        if ($(this).val() == 'Other') {

            var id = $(this).parent().parent().attr('id');
            $('#' + id + ' .div_course_other').removeClass('hide');
        }
        else {
            var id = $(this).parent().parent().attr('id');
            $('#' + id + ' .div_course_other').addClass('hide');
        }
    });
}

function check_publish() {
    if ($('#instituteheadsignoff_yesno').attr('checked') && $('#nensignoff_yesno').attr('checked') && $('#leadfacultysignoff_yesno').attr('checked')) {
        $('.publish').show();

    } else {
        $('.publish').hide();
    }
    var val = $('#s2id_gs_targeted_level span').text();
    $('#tab3 #lbl_gs_targeted_level').text(val);

}


