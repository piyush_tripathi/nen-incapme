$(document).ready(function(){
    load_fdp();
    update_breadcrumb();
    //update_form();
    $('#create_institute_fdp').click(function(){
        var form  = $('form');
        var callback = 'result_create_institute_fdp';
        var url  = form.attr( 'action' );
        var data  = form.serialize();
        
        ajax_call(form,url,data,callback);
        
    }) 
    $('#btn_publish_fdp').click(function(){
        var form  = $(this).closest('.div_form').find(':input');
        var callback = 'load_fdp_publish';
        var url  =  '/fdp/publish/';       
        var data  = {
            'fdp_id':$('input[name="fdp_id"]').val()
        };
        //$(form).submit();
        //// //console.log(url);
        //// //console.log(form);
        
        ajax_call(form,url,data,callback);
        load_fdp_publish();
    });
    $('.btn_save_form').click(function(){      
        var form  = $(this).closest('.div_form').find(':input');
        var callback = 'load_fdp';
        var url  =  $(this).closest('.div_form').attr( 'action' );
        var id=$(this).attr('id');
        var data  = form.serialize();
        //$(form).submit();
        //// //console.log(url);
        //// //console.log(form);
        ajax_call(form,url,data,callback);
        $('#fdp-info-view-hide').click();
        $('.p1').show();
        $('#success_'+id).html('<div class="alert alert-success success-top"> <button class="close" data-dismiss="alert">×</button><i class="icon-back"></i><span class="error-msg1"><strong> Success! </strong> Record updated.</span></div>');
    }) 
    
    $('#fdp-info-view').click(function(){
        load_fdp();
    });
    $('#btn_save_one').click(function(){
        $('.p1').show(); 
        $('#fpd-info-view').click(); 
    });
   
//    $('#btn_publish_fdp').click(function(){
//        $('#form_publish_fdp').submit();
//        load_fdp();
//    })
    $('#fpd-info-view').on('click', function() {
        $('.p1').show(); 
        load_fdp();
        
    });
    $('#fdp-act-info-view').trigger('click');
    
    $('#edit1').bind('click',function(){
        $('.p1').hide(); 
    });
   
    //to Check the status of fields scripts 
  
    $('.registered,.trained,.certified').live('click',function(){
        var val =$(this).closest('table').find('.course_id').val();
        // alert(val);
     
        if($('input[name=reg_'+val+']').is(':checked')){
            
            $('input[name=reg_date_'+val+']').attr('disabled',false);
            $('input[name=train_'+val+']').attr('disabled',false);
            
            if($('input[name=train_'+val+']').is(':checked')){
                $('input[name=train_date_'+val+']').attr('disabled',false);
                $('input[name=certi_'+val+']').attr('disabled',false);
                
                
                if($('input[name=certi_'+val+']').is(':checked')){
                    $('input[name=certi_date_'+val+']').attr('disabled',false);
                    $('input[name=remark_'+val+']').attr('disabled',false);
                    
                }else{
                    $('input[name=certi_date_'+val+']').val("").attr('disabled',true);
                    $('input[name=remark_'+val+']').val("").attr('disabled',true);
                }
            }else{
                $('input[name=train_date_'+val+']').val("").attr('disabled',true);
                $('input[name=certi_'+val+']').attr('disabled',true);
                $('input[name=certi_'+val+']').attr('checked',false);
                $('input[name=certi_date_'+val+']').val("").attr('disabled',true);
                $('input[name=remark_'+val+']').val("").attr('disabled',true);
               
            }
        }else{
            $('input[name=reg_date_'+val+']').val("").attr('disabled',true);
            $('input[name=train_'+val+']').attr('checked',false);
            $('input[name=train_'+val+']').attr('disabled',true);
            $('input[name=train_date_'+val+']').val("").attr('disabled',true);
            $('input[name=certi_'+val+']').attr('disabled',true);
            $('input[name=certi_'+val+']').attr('checked',false);
            $('input[name=certi_date_'+val+']').val("").attr('disabled',true);
            $('input[name=remark_'+val+']').val("").attr('disabled',true);
        }
        
    }); //to Check the status of Other fields
  
        $('.registered,.trained,.certified').live('click',function(){
            var val =$(this).closest('table').find('.course_id_value').val();
           //  alert(val);
     
            if($('input[name=oth_reg_'+val+']').is(':checked')){
            
                $('input[name=oth_reg_date_'+val+']').attr('disabled',false);
                $('input[name=oth_train_'+val+']').attr('disabled',false);
            
                if($('input[name=oth_train_'+val+']').is(':checked')){
                    $('input[name=oth_train_date_'+val+']').attr('disabled',false);
                    $('input[name=oth_train_date_'+val+']').val('');
                    $('input[name=oth_certi_'+val+']').attr('disabled',false);
                
                
                    if($('input[name=oth_certi_'+val+']').is(':checked')){
                        $('input[name=oth_certi_date_'+val+']').attr('disabled',false);
                        $('input[name=oth_remark_'+val+']').attr('disabled',false);
                    
                    }else{
                        $('input[name=oth_certi_date_'+val+']').val("").attr('disabled',true);
                        $('input[name=oth_remark_'+val+']').val("").attr('disabled',true);
                    }
                }else{
                    $('input[name=oth_train_date_'+val+']').val("").attr('disabled',true);
                    $('input[name=oth_certi_'+val+']').attr('disabled',true);
                    $('input[name=oth_certi_'+val+']').attr('checked',false);
                    $('input[name=oth_certi_date_'+val+']').val("").attr('disabled',true);
                    $('input[name=oth_remark_'+val+']').val("").attr('disabled',true);
               
                }
            }else{
                $('input[name=oth_reg_date_'+val+']').val("").attr('disabled',true);
                $('input[name=oth_train_'+val+']').attr('checked',false);
                $('input[name=oth_train_'+val+']').attr('disabled',true);
                $('input[name=oth_train_date_'+val+']').val("").attr('disabled',true);
                $('input[name=oth_certi_'+val+']').attr('disabled',true);
                $('input[name=oth_certi_'+val+']').attr('checked',false);
                $('input[name=oth_certi_date_'+val+']').val("").attr('disabled',true);
                $('input[name=oth_remark_'+val+']').val("").attr('disabled',true);
            }
    
    
    
    
        });
        // for date validation - prev and next date 
        
        //to Check the status of fields scripts 
        jQuery('.reg-date').datepicker({autoclose:true})
            .on('changeDate', function(e){
             var val =$(this).parent().parent().parent().children().children('td').eq(0).children('.course_id').val();
            
      var id = '#ff'+(val-1)+'_train_date' ;
      var date1=$(this).val();
      $(id).val("");
      
      $(id).datepicker("remove");
      $(id).datepicker({
            autoclose: true,
            startDate: date1
        });
      
      
            });
//            
              jQuery('.train-date').datepicker({autoclose:true})
            .on('changeDate', function(e){
             var val =$(this).parent().parent().parent().children().children('td').eq(0).children('.course_id').val();
            
      var id = '#ff'+(val-1)+'_certi_date' ;
      var date1=$(this).val();
      $(id).val("");
      
      $(id).datepicker("remove");
      $(id).datepicker({
            autoclose: true,
            startDate: date1
        });
        
        });
//  $('.reg-date').onDatechange(function(){ 
//      var val =$(this).closest('table').find('.course_id_value').val();
//      var id = '#ff_'+(val-1)+'_train_date' ;
//      var date1=$(this).val();
//      $(id).val("");
//      alert(val);
//      $(id).datepicker("remove");
//      $(id).datepicker({
//            autoclose: true,
//            startDate: date1
//        });
//      
//      
//        });

   
                
function updateMeeting(selector){
    var date1=$(selector).val();
    jQuery(selector).datepicker({
        autoclose: true,
        startDate: date1
    });

}
  
//        $('.reg-date,.train-date,.certi-date').live('changeDate',function(){
//            var val =$(this).closest('table').find('.course_id_value').val();
//           //  alert(val);
//     
//            if($('input[name=oth_reg_'+val+']').is(':checked')){
//            
//                $('input[name=oth_reg_date_'+val+']').attr('disabled',false);
//                $('input[name=oth_train_'+val+']').attr('disabled',false);
//            
//                if($('input[name=oth_train_'+val+']').is(':checked')){
//                    $('input[name=oth_train_date_'+val+']').attr('disabled',false);
//                    $('input[name=oth_train_date_'+val+']').val('');
//                    $('input[name=oth_certi_'+val+']').attr('disabled',false);
//                
//                
//                    if($('input[name=oth_certi_'+val+']').is(':checked')){
//                        $('input[name=oth_certi_date_'+val+']').attr('disabled',false);
//                        $('input[name=oth_remark_'+val+']').attr('disabled',false);
//                    
//                    }else{
//                        $('input[name=oth_certi_date_'+val+']').val("").attr('disabled',true);
//                        $('input[name=oth_remark_'+val+']').val("").attr('disabled',true);
//                    }
//                }else{
//                    $('input[name=oth_train_date_'+val+']').val("").attr('disabled',true);
//                    $('input[name=oth_certi_'+val+']').attr('disabled',true);
//                    $('input[name=oth_certi_'+val+']').attr('checked',false);
//                    $('input[name=oth_certi_date_'+val+']').val("").attr('disabled',true);
//                    $('input[name=oth_remark_'+val+']').val("").attr('disabled',true);
//               
//                }
//            }else{
//                $('input[name=oth_reg_date_'+val+']').val("").attr('disabled',true);
//                $('input[name=oth_train_'+val+']').attr('checked',false);
//                $('input[name=oth_train_'+val+']').attr('disabled',true);
//                $('input[name=oth_train_date_'+val+']').val("").attr('disabled',true);
//                $('input[name=oth_certi_'+val+']').attr('disabled',true);
//                $('input[name=oth_certi_'+val+']').attr('checked',false);
//                $('input[name=oth_certi_date_'+val+']').val("").attr('disabled',true);
//                $('input[name=oth_remark_'+val+']').val("").attr('disabled',true);
//            }
//    
//    
//    
//    
//        });


    });
 
    function result_create_institute_fdp(data){
    
        if(data['success']){
            window.location.href = data['url'];
        }
        else{
            alert('error happpend');
        }
    }
    function ajax_post_form(form,callback){
    
    
 
    }
    function ajax_call(source,url,data,callback){

        var crnturl = url;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: data,
            timeout: 3000,
            beforeSend: function() {
                //console.log(url);
                //console.log(data);
                //console.log(source);
                $('a.ajax-loader').trigger('click');
            },
            complete: function() {
                $('button.ajax-close').trigger('click');
          
            },          
            cache: false,
            success: function(result) {
                //console.log(result);
                $('button.ajax-close').trigger('click');             
               
                var fn = window[callback];
                fn(result);
                //                }
                 
            },
            error: function(error) {
                $('button.ajax-close').trigger('click');
          
                //alert("Some problems have occured. Please try again later: " );
                //console.log(error);
            }
        });


    }
    function load_fdp_publish(){
        
        var url = location.href;    // current url
        
        window.location = url;
    }
    function load_fdp(){
        var source  = $('#tab2');
        var callback = 'load_fdp_result';
        var url  = '/fdp/getfdpdata/';
        var data  = {
            'fdp_id':getParameter('id'),
            'institute_id':getParameter('instituteid')
        };
        //// //console.log(data);
        var type = $('#fdp_type').val();
        if(type!='create'){
            ajax_call(source,url,data,callback);
        }
       
    }

    function load_fdp_result(data){
        if(!data){
            return false;
        }
   
        update_elements(data);
        is_lead_faculty  =  data['is_lead_faculty'];
        check_signoff(data['is_lead_faculty']);
        check_publish(data['fdp_details']);
        create_table(data);
    
    }
    function getParameter(paramName) {
        var searchString = window.location.search.substring(1),
        i, val, params = searchString.split("&");

        for (i=0;i<params.length;i++) {
            val = params[i].split("=");
            if (val[0] == paramName) {
                return unescape(val[1]);
            }
        }
        return null;
    }



    //script for page elements 

   
    $(document).ready(function() {
	
        $('.remove-course').live('click',function(){
            $(this).parent().parent().remove();
        });
    
     
        $('.add-new-course').live('click',  function(){
            var count = $('.course_id_value:last').val(); 
            if (typeof(count) == 'undefined') {
                count = 0;
            }
            count++;
            var suffix = '_'+count;
            var htm = '<div class="main-course-block"> \n\
            <div class="c-course"> \n\
            <label class="custom-label">Course Title :</label><input type="text" name="oth_course_title'+suffix+'">\n\
            <span class="remove-course btn pull-right"><i class="icon-remove"></i></span>\n\
            </div>\n\
            <table class="table table-bordered tpd-list1">\n\
            <thead>\n\
            <tr>\n\
            <th>Course Provided by</th><th>Registered</th>\n\
            <th class="calendar-input">Registered Date</th>\n\
            <th>Trained</th>\n\
            <th class="calendar-input">Start Date</th>\n\
            <th>Certified</th>\n\
            <th class="calendar-input">Certified Date</th>\n\
            <th>Remarks</th>\n\
            <th>Lead Faculty Sign-off</th>\n\
            </tr>\n\
            </thead>\n\
            <tbody>\n\
            <tr>\n\
            <td>\n\
            <input type="hidden" class="course_id_value" name="oth_course_id[]" value="'+count+'">\n\
            <input type="text" class="course_provider" name="oth_course_provider'+suffix+'"></td>\n\
            <td><input type="checkbox" name="oth_reg'+suffix+'" class="registered"  value="Yes"></td>\n\
            <td class="calendar-input"><input type="text" name="oth_reg_date'+suffix+'" class="datepicker reg-date" data-date-format="dd-mm-yyyy" disabled></td>\n\
            <td><input type="checkbox" name="oth_train'+suffix+'" class="trained" value="Yes" disabled></td>\n\
            <td class="calendar-input"><input type="text" class="datepicker train-date" name="oth_train_date'+suffix+'" data-date-format="dd-mm-yyyy" disabled></td>\n\
            <td><input type="checkbox" name="oth_certi'+suffix+'" class="certified" value="Yes" disabled></td>\n\
            <td class="calendar-input"><input type="text" name="oth_certi_date'+suffix+'" class="datepicker certi-date" data-date-format="dd-mm-yyyy" disabled></td>\n\
            <td class="remark-input"><input type="text" name="oth_remark'+suffix+'" class="remark" disabled></td>\n\
            <td><input type="checkbox" name="oth_signoff'+suffix+'" class="signoff" value="Yes"></td> \n\
            </tr>\n\
            </tbody>\n\
            </table>\n\
            </div>';
            $(htm).appendTo('#div_oth_course');
            if( $('.signoff:first').attr('disabled') == 'disabled' ) {
                $('input[name="oth_signoff'+suffix+'"]').attr('disabled',true)
            }
        $('.datepicker').datepicker({
             autoclose: true
        });
        //now setting the sign off checkboxes
        check_signoff(is_lead_faculty);
        });

    });

 

    function  update_elements(data){
        if(!data){
            return false;
        }
   
        // for default_course 
        $.each(data['default_courses'],function(k,v){
            set_elements(k,v);
        });    
    
   
        //for other_courses
        $('#div_oth_course').html('');
        $.each(data['other_courses'],function(k,v){
            create_elements(k,v);
            set_elements(k,v,'other');
        });
     
     
        //now update labels
        update_label(data['default_courses']);
        
        //update the breadcrumb        
        $('#bc_name').text(data.fdp_details.faculty_name);
   
    }
 
    function set_elements(k,v,status){
        if(!v){
            return false;
        }
        var id  = v['fdp_courses_id'];
        var suffix = '_'+id; 
        var prefix = '';
        if(status == 'other'){
            var prefix = 'oth_';
        }
        
        var title  = 'input[name="'+prefix+'course_title'+suffix+'"]';
        var provider  = 'input[name="'+prefix+'course_provider'+suffix+'"]';
        var register  = 'input[name="'+prefix+'reg'+suffix+'"]';
        var regDate  = 'input[name="'+prefix+'reg_date'+suffix+'"]';
        var trained  = 'input[name="'+prefix+'train'+suffix+'"]';
        var tranDate  = 'input[name="'+prefix+'train_date'+suffix+'"]';
        var certified  = 'input[name="'+prefix+'certi'+suffix+'"]';
        var certiDate = 'input[name="'+prefix+'certi_date'+suffix+'"]';
        var remark = 'input[name="'+prefix+'remark'+suffix+'"]'; 
        var signoff = 'input[name="'+prefix+'signoff'+suffix+'"]'; 
        
        
        //lead facult sign off 
        if(v['lead_faculty_signoff']=='1'){
            $(signoff).attr('checked',true);
        }
        else{
            $(signoff).attr('checked',false);
        }
        
        if(typeof v['course_name'] != 'undefined'){
            $(title).val(v['course_name']);
        }
       
        $(provider).val(v['course_provider']);       
       
        if(v['registered']=="1"){
            $(register).attr('checked',true);
            $(regDate).val(v['registered_date']);
            if(v['trained']=="1"){
                $(trained).attr('checked',true);
                $(tranDate).val(v['trained_date']);
                if(v['certified']=="1"){
                    $(certified).attr('checked',true);
                    $(certiDate).val(v['certified_date']);
                    $(remark).val(v['remarks']);
                    
                }else{
                     $(certified).attr('checked',false);
                    $(certified).attr('disabled',false);
                    $(certiDate).val('').attr('disabled',true);
                    $(remark).val('').attr('disabled',true);
                }
            }else{
                $(trained).attr('checked',false);  
                $(certified).attr('disabled',true);
                $(certiDate).val('').attr('disabled',true);
                $(tranDate).val('').attr('disabled',true);
                $(remark).val('').attr('disabled',true);
            }
        }else{
            $(trained).attr('checked',false);  
            $(certified).attr('checked',false);
            $(register).attr('checked',false);
            $(regDate).val('').attr('disabled',true);
            $(trained).attr('disabled',true);
            $(tranDate).val('').attr('disabled',true);
            $(certified).attr('disabled',true);
            $(certiDate).val('').attr('disabled',true);
            $(remark).val('').attr('disabled',true);
            
        }
    
    
    }

    function create_elements(k,v){
        var id  = v['fdp_courses_id'];
        var suffix = '_'+id;        
        var htm = '<div class="main-course-block"> \n\
            <div class="c-course"> \n\
            <label class="custom-label">Course Title :</label><input type="text" name="oth_course_title'+suffix+'">\n\
            <span class="remove-course btn pull-right"><i class="icon-remove"></i></span>\n\
            </div>\n\
            <table class="table table-bordered tpd-list1">\n\
            <thead>\n\
            <tr>\n\
            <th>Course Provided by</th><th>Registered</th>\n\
            <th class="calendar-input">Registered Date</th>\n\
            <th>Trained</th>\n\
            <th class="calendar-input">Start Date</th>\n\
            <th>Certified</th>\n\
            <th class="calendar-input">Certified Date</th>\n\
            <th>Remarks</th>\n\
            <th>Lead Faculty Sign-off</th>\n\
            </tr>\n\
            </thead>\n\
            <tbody>\n\
            <tr>\n\
            <td>\n\
             <input type="hidden" class="course_id_value" name="oth_course_id[]" value="'+id+'">\n\
             <input type="text" class="course_provider" name="oth_course_provider'+suffix+'"></td>\n\
            <td><input type="checkbox" name="oth_reg'+suffix+'" class="registered"  value="Yes"></td>\n\
            <td class="calendar-input"><input type="text" name="oth_reg_date'+suffix+'" class="datepicker reg-date" data-date-format="dd-mm-yyyy"></td>\n\
            <td><input type="checkbox" name="oth_train'+suffix+'" class="trained" value="Yes"></td>\n\
            <td class="calendar-input"><input type="text" class="datepicker train-date" name="oth_train_date'+suffix+'" data-date-format="dd-mm-yyyy"></td>\n\
            <td><input type="checkbox" name="oth_certi'+suffix+'" class="certified" value="Yes"></td>\n\
            <td class="calendar-input"><input type="text" name="oth_certi_date'+suffix+'" class="datepicker certi-date" data-date-format="dd-mm-yyyy"></td>\n\
            <td class="remark-input"><input type="text" name="oth_remark'+suffix+'" class="remark"></td>\n\
            <td><input type="checkbox" name="oth_signoff'+suffix+'" class="signoff" value="Yes"></td> \n\
            </tr>\n\
            </tbody>\n\
            </table>\n\
            </div>';
    
        
        $('#div_oth_course').append('<div id="oth_course'+k+'">'+htm+'</div>');
         $('.datepicker').datepicker({
             autoclose: true
        });
    }


    function update_label(data){
        if(!data){
            return false;         
        }
        var lbl_register = $('.lbl_register');
        var lbl_status = $('.lbl_status');
        $.each(data,function(k,v){
            if(v['registered']=='1'){
                lbl_register[k].innerHTML = v['registered_date'];
            }else{
                lbl_register[k].innerHTML = 'Not Registered';
            }
         
            if(v['lead_faculty_signoff']=='1'){
                lbl_status[k].innerHTML = '<span class="certified-icon"></span> Certified';
            }else{
                lbl_status[k].innerHTML = '';
            }
        
        });
        
        
     
     
    }
   
    function check_signoff(is_lead_faculty){
        //remove all signoff texts 
        $('.signoff_text').remove();
        $('.signoff').each(function(){
            if(!is_lead_faculty){               
                $(this).hide();
                
                if($(this).attr('checked')=='checked'){
                    $('<span class="signoff_text">Yes</span>').insertAfter(this);
                }else{
                    $('<span class="signoff_text">No</span>').insertAfter(this);
                }
                $('#btn_publish_fdp').hide();
                $('.add-new-course').show();
                $('.remove-course ').show();
            }
            else{               
                $('#btn_publish_fdp').show();
//                   $('.add-new-course').hide();
                   $('.remove-course ').hide(); 
            }
        });
   
        
    }

    function check_publish(data){
        if(data['fdp_published']=='1'){
            $('#edit1').hide();
            $('.title-overview').append('<span id="fdp_status">PUBLISHED</span>');
        }
        else{
            $('#edit1').show();
        }
    }
    
    function create_table(data){
        var prefix = '<thead><tr><th>Course Code</th><th>Course Title</th><th>Date of Registration</th><th>Status</th></tr></thead><tbody>';
        var suffix = '</tbody>';
        var htm = '';
        $.each(data['other_courses'],function(k,v){
            if(v['lead_faculty_signoff']=='1'){
                var certified  = '<span class="certified-icon"></span> Certified';
            }
            else{
                 var certified = '';
            }
            htm+='<tr><td>'+(k++)+'</td><td>'+v['course_name']+'</td><td><spanid="lbl_fm1_reg_date"class="lbl_register">'+v['registered_date']+'</span></td><td>'+certified+'</td></tr>';
        });
        
        $('#oth_table_courses').html(prefix+htm+suffix);  
    }
    
    
    function update_breadcrumb(){
        
        var htm = '<span id="bc_name"> </span>';
        $('#breadcrumb').append(htm);
        //now we can update the name of faculty in above breadcrumb
    }
    
    function chk_click_result(obj){
        var selector = '#'+obj.id;
       // alert($(selector).attr('checked'));
        if($(selector).attr('checked')!='checked'){
            $(selector).attr('checked','checked');
        }else{
            $(selector).removeAttr('checked');
        }
    }