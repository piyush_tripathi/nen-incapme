$(document).ready(function(){
     jQuery("input:file[class=fileParticipant]").unbind('change').bind('change', function(e) {            
//            jQuery('#form_upload_participants').submit();
var form = $(this).closest('form');
form.submit();
        });
        update_edc_options();
    edc = {
		
        showBlock: function(block) {
            var fields = edc.groups[block];
            var data = edc.data;
			
            for(f in fields) {
                var itm = '#lbl_'+ f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1 || data[f]=="1") {
                        $(itm).attr('checked', 'checked');
                    } else {
                        $(itm).removeAttr('checked');
                    }
                } else if( type == 'text' || type == 'date') {
                    $(itm).html(data[f]);
                //console.log(data[f]);
                } else if(type == 'select') {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).html(data[f].caption);
                        } else {
                            $(itm).html(data[f]);
                        }
                    }
                } else if(type == 'grid') {
                    if(f == 'edc_members') {
                        edc.showStudentMembers();
                    }
                    else if(f == 'edc_advisors') {
                        edc.showAdvisors();
                    }
                } else if (f == 'multipledd') {
					
                } 
            }
        },
		
        editBlock: function(block) {
            var fields = edc.groups[block];
            var data = edc.data;
		
            for(f in fields) {
                var itm =  '#'+f;
                var type = fields[f].type;
                if(type == 'checkbox') {
                    if(data[f]==1) {
                        $(itm).attr('checked', 'checked');
                    } else {
                        $(itm).removeAttr('checked');
                    }
                } else if(type == 'select' ) {
                    if(data[f]) {
                        if(data[f].hasOwnProperty('value')) {
                            $(itm).val(data[f].value);
                            try {
                                /*hack for custom select for select2js */
                                $('#s2id_'+f+ ' a span').text(data[f].caption);
                            } catch(e) {

                            }
                        } else {
                            $(itm).val(data[f]);
                            $('#s2id_'+f+ ' a span').text(data[f]);
                        }   
                                        

                    }
                } else if (type == 'text' || type == 'date') {
                    $(itm).val(data[f]);
                //console.log(data[f]);
                } else if(type == 'grid') {
                    if(f == 'edc_members') {
                        edc.editStudentMembers();
                    }
                    if(f == 'edc_advisors') {
                        edc.editAdvisors(); 
                    }

                } else if (type == 'multipledd') {
					
                } else if (type == 'multiple_sponsors') {
                    edc.editMultipleSponsors(f);
                }
            }
        },
	
		
        showStudentMembers: function() {
            var d = edc.data.edc_members;
            edc.viewStudentGrid.processData(d, "showEdcMembers");
        },
		
        editStudentMembers: function() {
            var d = edc.data.edc_members;
            edc.studentGrid.processData(d, "edcMembers");
        },
		
        showAdvisors: function() {
            var d = edc.data.edc_advisors;
            edc.viewAdvisorGrid.processData(d, "showEdcAdvisors");
        },
		
        editAdvisors: function() {
            var d = edc.data.edc_advisors;
            edc.advisorGrid.processData(d, "edcAdvisors");
        },
        show: function(block) {
            //if(!block) return;
            //console.log(block)
            $('#'+block+ '-container .view-block').show();
            $('#'+block+ '-container .edit-block').hide();
			
        },

        edit: function(block) {
            if(!block) return;
            //.log(block)
            $('#'+block+ '-container  .view-block').hide();
            $('#'+block+ '-container  .edit-block').show();
        //console.log($('#'+block+ '-container  .edit-block'))
        },
		
        initShowBlock: function(block) {
            if(!block) return;
            $('#edit-link-'+block).removeClass('hide-block');
            edc.showBlock(block);
            edc.editBlock(block);
            edc.show(block);
            initDependency(block);
        },
		
        updateEditBlock: function(block) {
            if(!block) return;
            edc.showBlock(block);
            edc.editBlock(block);
            edc.edit(block);
        },
		
        save: function(block) {
            $('a.ajax-loader').trigger('click');
            jQuery.ajax({
                type: "POST", 
                url: "/ajax/",
                data: {
                    'method': 'edc.update',
                    'data': edc.data, 
                    'block': block, 
                    'edcid': edcid
                },
                dataType: 'json',
            success: function(response) {
                //console.log(response);
                    if(response == true) {
                        var blockfields = edc.groups[block]; 
                        for(f in blockfields) {
                            edc.data[f] = edc.tdata[f];
                        }
                        edc.initShowBlock(block);
                        notify('#noti-edc-'+block, 'success', 'Record updated ...');
                      //  var nextCollapseId = saveContinueGetNextId('edc', block);
                      //  console.log('Open : ' + nextCollapseId);
                        
                    } else {
                        console.log('Unexpected Error: Data not saved');
                        notify('#noti-edc-'+block, 'error', 'Record not updated ...');
                    }
            },
            complete: function() {
                 $('button.ajax-close').trigger('click');
                 checkStatus();
                  checkSelected();
                //console.log('here');
                }
            
            });
			
        },
		
		
        loadData: function(edcid) {
            edc.studentGrid = new EditableGrid("EdcStudentGrid"); 
            edc.viewStudentGrid = new EditableGrid("ViewEdcStudentGrid");
            edc.advisorGrid = new EditableGrid("EdcAdvisorGrid"); 
            edc.viewAdvisorGrid = new EditableGrid("ViewEdcAdvisorGrid");
            
            edc.data=edcData;
            edc.tdata=edcData;
            edc.showBlock('team');
            edc.editBlock('team');
            edc.showBlock('program');
            edc.editBlock('program');
            edc.showBlock('funding');
            edc.editBlock('funding');
            edc.showBlock('infrastructure');
            edc.editBlock('infrastructure');
            edc.show('team');
            edc.show('program');
            edc.show('funding');
            edc.show('infrastructure');
            initDependency('infrastructure');
            
//            jQuery.ajax({
//                url: '/ajax/',
//                data: {
//                    'method': 'edc.get',
//                    'edcid': edcid
//                },
//                type: 'post',
//                dataType: 'json',
//                cache: false,
//                success: function(responseJSON) {
//                    edc.data=responseJSON;
//                    edc.tdata=responseJSON;
//                    edc.showBlock('team');
//                    edc.editBlock('team');
//                    edc.showBlock('program');
//                    edc.editBlock('program');
//                    edc.showBlock('funding');
//                    edc.editBlock('funding');
//                    edc.showBlock('infrastructure');
//                    edc.editBlock('infrastructure');
//                    
//                    edc.show('team');
//                    edc.show('program');
//                    edc.show('funding');
//                    edc.show('infrastructure');
//                },
//     
//                complete: function() {
//				             
//                }
//            });
        }
    };

	
    edc.groups ={
        'team': {
            "edcinfo_year":{
                "type":"select",
                "validation": ["required", "number"]
            },
            "edcteam_lf":{
                "type":"select"
            },
            "edcteam_colead":{
                "type":"select"
            },
            "edcteam_stu":{
                "type":"text",
                "validation": ["number"]
            },
            "edc_members":{
                "type":"grid"
            },
            "edc_advisors":{
                "type":"grid"
            },
            "edcadvboard_adv": {
                "type":"text",
                "validation": ["number"]
            },
            "edcadvboard_date1": {
                "type":"date"
            },
            "edcadvboard_date2": {
                "type":"date"
            }
            
        },
        'program': {
			
            "edcprog_disc":{
                "type":"select"
            },
            "edcprog_adopt":{
                "type":"select"
            },
             "edcprog_upload":{
                "type":"hidden"
            }
        },
        
        'funding': {
            "edcfund_sent":{
                "type": "select"
            },
            "edcfund_sanction": {
                "type": "text",
                "validation": ["number"]
            },
            "edcfund_other": {
                "type": "text"
            },
            "edcfund_date": {
                "type": "date"
            }
        },
        'infrastructure': {
            "infra_space" : {
                "type": "checkbox"
            },
            "infra_equipment" : {
                "type": "checkbox"
            },
            "infra_library" : {
                "type": "checkbox"
            },
            "infra_staff" : {
                "type": "checkbox"
            },
            "infra_budget" : {
                "type": "checkbox"
            },
            "infra_other" : {
                "type": "checkbox"
            },
            "infra_other_text" : {
                "type": "text"
            }
        }
    };
	
    edc.loadData(edcid);
	
    $('.lnk-show-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('show-link-'.length);
        edc.show(blk);
		
    });
	
    $('.lnk-edit-block').click(function(event){
        event.preventDefault();
        var blk = $(this).attr('id').substr('edit-link-'.length);
        edc.edit(blk);
        $(this).addClass('hide-block');
		
    });
	
    /** save team **/
    $('.cls-save-team').click(function(){
        /** edc_members **/
        var fields = edc.groups.team;
        var isValid = true;
        isValid = dovalidate(fields, 'noti-edc-team'); 
        if(! isValid) {
            return;
        }
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type != 'grid') {
                if(fields[f].type == 'select' ) {
                        var facid = jQuery(item).val();
                        var facname = $(item + ' :selected').text();
                        var fac = {
                            'value': facid, 
                            caption: facname
                        };
                        edc.tdata[f] = fac;

                } else {
                
                    edc.tdata[f] = jQuery(item).val();
                }
            }
        }
        
        var d = edc.tdata.edc_members.metadata;
        var noCols = d.length;
        var nd = edc.studentGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {'id': i+1, 'values' : o};
        }
        edc.tdata.edc_members.data = newdata;
        
        
        /** advisors **/
        var d1 = edc.tdata.edc_advisors.metadata;
        var noCols1 = d1.length;
        var nd1 = edc.advisorGrid.data;
        var newdata1=[];
        for(i=0 ; i< nd1.length; i++ ) {
            var o={};
            for(j=0; j< noCols1; j++) {
                var col = d1[j].name;
                var val = nd1[i].columns[j];
                o[col] =  val;
                
            }
            newdata1[i] = {'id': i+1, 'values' : o};
        }
        edc.tdata.edc_advisors.data = newdata1;
        
        
        
        //edc.showBlock('team');
        //edc.editBlock('team');
        edc.save('team');
		
    });
    
    /* cancel Team */
    $('.cls-cancel-team').click(function(){		
        edc.initShowBlock('team');
    });
    
    
    /******  Funding ******/
	
    $('.cls-save-funding').click(function(){
        var fields = edc.groups.funding;
        var isValid = true;
        isValid = dovalidate(fields, 'noti-edc-funding');
        if(! isValid) {
            return;
        }
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type != 'grid') {
                edc.tdata[f] = jQuery(item).val();
            }
        }
        //edc.initShowBlock('funding');
        edc.save('funding');
		
    });
    
    
    $('.cls-cancel-funding').click(function(){		
        edc.initShowBlock('funding');
        checkStatus();
    });
	
    
	
    /** save program **/
    $('.cls-save-program').click(function(){
        var fields = edc.groups.program;
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type == 'checkbox') {
                if(typeof jQuery(item).attr('checked') == 'undefined')  {
                    edc.tdata[f] = "0";
                }else {
                    edc.tdata[f] = "1";
                }
            //console.log(jQuery(item).attr('checked'));
            } else {
                edc.tdata[f] = jQuery(item).val();
            }
        }
        //edc.initShowBlock('program');
        edc.save('program');
        update_edc_options();
		
    });
	
    $('.cls-cancel-program').click(function(){		
        edc.initShowBlock('program');
        update_edc_options();
    });
    
    
     /** save infrastructure **/
    $('.cls-save-infrastructure').click(function(){
        var fields = edc.groups.infrastructure;
        for(f in fields) {
            var item = '#'+ f;		
            if(fields[f].type == 'checkbox') {
                if(typeof jQuery(item).attr('checked') == 'undefined')  {
                    edc.tdata[f] = "0";
                }else {
                    edc.tdata[f] = "1";
                }
            } else {
                edc.tdata[f] = jQuery(item).val();
            }
        }
        edc.save('infrastructure');
		
    });
	
    $('.cls-cancel-infrastructure').click(function(){		
        edc.initShowBlock('infrastructure');
    });
    
	
		
    jQuery("input[class!='fileParticipant']:file").unbind('change').bind('change', function(e) {
        var frm = 'form_'+$(this).attr('id');
        document.forms[frm].submit();
    });
    
    
    /** add new item to grid members**/
    $("#add-grid-edc_members").click(function() {
        var d = edc.tdata.edc_members.metadata;
        var noCols = d.length;
        var nd = edc.studentGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {'id': i+1, 'values' : o};
        }
        var _members = {};
    
        _members.data = newdata;
        _members.metadata = edc.data.edc_members.metadata;
        
        //add column to tdata
        var last = _members.data.length;
        
        var newitem = {
            "id" : last+1,
            "values" : {
                "designation": " ",
                "first_name": " ",
                "last_name": "",
                "dob": "",
                "sex": "M",
                "email": "",
                "contact_no": "",
                "start_date": "",
                "remarks": ""
            }
        }
        _members.data[last] = newitem;
        edc.studentGrid.processData(_members, "edcMembers");
    });
    
    /** add new item to advisor grid **/
    $("#add-grid-edc_advisors").click(function() {
        var d = edc.tdata.edc_advisors.metadata;
        var noCols = d.length;
        var nd = edc.advisorGrid.data;
        var newdata=[];
        for(i=0 ; i< nd.length; i++ ) {
            var o={};
            for(j=0; j< noCols; j++) {
                var col = d[j].name;
                var val = nd[i].columns[j];
                o[col] =  val;
                
            }
            newdata[i] = {
                'id': i+1, 
                'values' : o
            };
        }
        var _advisors = {};
    
        _advisors.data = newdata;
        _advisors.metadata = edc.data.edc_advisors.metadata;
        
        //add column to tdata
        var last = _advisors.data.length;
        
        var newitem = {
            "id" : last+1,
            "values" : {
                "designation": "",
                "institute": "",
                "first_name": "",
                "last_name": "",
                "apellation": "",
                "date_of_joining": "",
                "date_of_leaving": "",
                "remarks": ""
            }
        }
        
        _advisors.data[last] = newitem;
        edc.advisorGrid.processData(_advisors, "edcAdvisors");
    });
    
    //dependency
    
    /** dependency on checkbox **/
    
    function initDependency(blk) {
        if(!blk) return;
        $('.cls-dependency', $('#'+blk+'-container')).each(function(){
            if(typeof jQuery(this).attr('checked') == 'undefined'){  
                $('.dep-'+$(this).attr('id')).addClass('hide-block')
            } else {
                if($('.dep-'+$(this).attr('id')).hasClass('hide-block')){
                    $('.dep-'+$(this).attr('id')).removeClass('hide-block')
                }
            }
        });
    }
    
    $('.cls-dependency').click(function(){
        var blk = $('.dep-'+$(this).attr('id'));
        if(typeof jQuery(this).attr('checked') == 'undefined')  {
            if($(blk).hasClass('hide-block') == false) {
                $(blk).addClass('hide-block');
            }
        } else {
            if($(blk).hasClass('hide-block') == true) {
                $(blk).removeClass('hide-block');
            }
        }

    });
    
	
});

function uploadEdcMembers() {
    var m = jQuery('#target_edc_members');
    var ret = m.contents().find('body').html();
    var members = eval("("+ret+")");
    var _members = {};
    
    _members.data = members.edc_members;
    _members.metadata = edc.data.edc_members.metadata;
    edc.studentGrid.processData(_members, "edcMembers");
}

function uploadEdcAdvisors() {
    var m = jQuery('#target_edc_advisors');
    var ret = m.contents().find('body').html();
    var advisors = eval("("+ret+")");
    
    
    var _advisors = {};
    _advisors.data = advisors.edc_advisors;
    _advisors.metadata = edc.data.edc_advisors.metadata;
    edc.advisorGrid.processData(_advisors, "edcAdvisors");
    
}

function uploadFiles(name) {
    var m = jQuery('#target_'+name);
    var ret = m.contents().find('body').html();
    var res = eval("("+ret+")");
    //console.log(res);
    if(res.status == 'error') {
        $('#download_'+name).html('<div class="error">'+res.errmsg+'</div>');
        //$('#download_'+name).html('<div class="error">'+res.errmsg+'</div>');
    } else {
        $('#download_'+name).html('<div class="span4 dwnd-link"><i class="icon-download-alt"></i><a href="#">Download</a></div>');
    }
    if(name == '') {
        link = '';
    }
}
//Date validation
 $(document).ready(function(){
            //date validation
            jQuery('#edcadvboard_date1').datepicker({autoclose:true})
            .on('changeDate', function(e){
                $('#edcadvboard_date2').val("");
                $('#edcadvboard_date2').datepicker("remove");
                updateMeeting();
            });
                
            function updateMeeting(){
                var date1=$('#edcadvboard_date1').val();
                jQuery('#edcadvboard_date2').datepicker({
                    autoclose: true,
                    startDate: date1
                });
	
            }

            checkBoxStatus();
            checkStatus();
            checkSelected();
            
            $('#block1,#block2,#block3, #block4').click(function(){
                checkBoxStatus();
                 checkStatus();
                 $('.view-less,.view-less1,.view-less2,.view-less3,.view-less4,.view-less5,.view-less6,.view-less7,.view-less8, .view-less9').hide();
                $('.hidden-block,.hidden-block1, .hidden-block2,.hidden-block3,.hidden-block4,.hidden-block5,.hidden-block6,.hidden-block7,.hidden-block8, .hidden-block9').hide(); 
            });
    		
        });
        function checkBoxStatus(){
            $(':checkbox').each(function() {
                var id = 'status_'+ $(this).attr('id');
                var status = $(this).attr('checked');
                var label_id="#"+id;
                if( status == 'checked'){
                    $(label_id).show().text('Yes');
                } 
                else {
                    $(label_id).show().text('No');
                }   
            });
            
        }
        function checkStatus(){
            $('input[type=text]').each(function(){
                var oldId = 'lbl_'+ $(this).attr('id');
                var newId = 'status_'+ $(this).attr('id');
                var idv =$(this).val();
                var oldValue ="#"+oldId;
                var newValue = "#"+newId;
                if(idv==""){
                    //console.log(newValue);
                    $(oldValue).text('No Data');
                    $(newValue).hide();
                }
            });
            var advList=$('#showEdcAdvisors tr').size();
            if(advList==1){
                $('#advList').text('No Data');
                $('.view-more1').hide();
            }else if(advList==2){
                $('#advList').text(advList-1 +' '+' advisior');
                $('.view-more1').show();
            }else{
                $('#advList').text(advList-1 +' '+' advisiors');
                $('.view-more1').show();
            }
                
            var studList=$('#showEdcMembers tr').size();
            if(studList==1){
                $('#studList').text('No Data');
                $('.view-more').hide();
            }else if(studList==2){
                $('#studList').text(studList-1 +' '+' student');
                $('.view-more').show();
            }else{
                $('#studList').text(studList-1 +' '+' students');
                $('.view-more').show();
            }
        }
        function checkSelected() {
            var r=$("#lbl_edcteam_lf").text();
            var s=$('#lbl_edcteam_colead').text();
            if(s==""){
                $("#lbl_edcteam_colead").text("No Data");
                //console.log("empty");
            }
            if(r==""){
                $("#lbl_edcteam_lf").text("No Data");
                //console.log("empty");
            }
            //console.log(r);
        }

$("#infra_other").on( "click", function(){
           if(!$('#infra_other').attr('checked')) {
                $('#infra_other_text').val("");
                
           }
        });
        
        
        
function update_edc_options(){
    setTimeout(get_upload_data, 1000);
  
}
function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
    i, val, params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}

function get_upload_data(){
    
    var id = getParameter('instituteid');
    var module = 'edc';
   var url = '/ajax/get-uploaded-data?module='+module+'&id='+id;
   var link = 
   $.get( url, function( data ) {
            $.each( data[0], function( key, value ) {
          
           var lbl = 'lbl_'+key;
                if(value!='' && value){
                   
                    var link = '/'+ value.split('/').slice(-3).join('/');
                    $('#'+lbl+' a').attr('href',link);
                    $('#'+lbl).show();
                }else{
                    $('#'+lbl).hide();
                }
          });
          
}, "json" );
}