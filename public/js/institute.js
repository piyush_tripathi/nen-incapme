$(document).ready(function(){
    load_institute();
    //checking for adding or editing part 
    //checking for adding or editing part 
    try { 
        var action = document.location.href.split('/')[4].substr(0,3);
    }
    catch(err){
        $('.add-user').hide();
        $('.save-user').show();
         $('#user-title').text('(Edit)');
    }  //getting the last part of url such as user/add
    if(action=='add'){
        $('.add-inst').show();
        $('.save-inst').hide();
        $('#inst-title').text('(Add)');
    }else{
         $('.add-inst').hide();
         $('.save-inst').show();
         $('#user-title').text('(Edit)');
    }
    
    //update_form();
    $('#create_institute_institute').click(function(){
        var form  = $('form');
        var callback = 'result_create_institute_institute';
        var url  = form.attr( 'action' );
        var data  = form.serialize();
        var valid = true;
        var name =$('#name').val();
        if( name==""){
               valid = false;
               $('#name').focus().css("border","1px solid red");
               msg+='<li class="list-error"> Institute name is required</li>';
           }
           
        if(valid){
            ajax_call(form,url,data,callback);
        }  
        
        
    }) 
    $('#btn_publish_institute').click(function(){
        var form  = $(this).closest('.div_form').find(':input');
        var callback = 'institute_details';
        var url  =  '/institute/publish/';       
        var data  = {
            'institute_id':$('input[name="institute_id"]').val()
        };
        //$(form).submit();
        //// //console.log(url);
        //// //console.log(form);
        alert(url+':'+data);
        ajax_call(form,url,data,callback);
    });
    $('#save_inst,#add_inst').click(function(){      
        var form  = $(this).closest('.div_form').find(':input');
        var msg  = '';
        if(this.id=='save_inst'){
            var param = getParameter('instituteid');
            var url  =  '/institute/edit?instituteid='+param;
            var callback = 'institute_details';
        }else if(this.id=='add_inst'){
            var url  =  '/institute/add';
            var callback = 'result_create_institute';
        }
        var data  = form.serialize();
        var valid = true;
var name =$('#name').val();
        if( name==""){
               valid = false;
               $('#name').focus().css("border","1px solid red");
               msg+='<li class="list-error"> Institute name is required</li>';
           }
           
           if(valid){
            ajax_call(form,url,data,callback);
        }else{
            $('.alert-success').hide();
            $('#noti_institute').html('<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-error"></i> \n\
                <span class="error-msg">\n\
            <strong>Error!</strong> '+ msg + '</span></div>');
             
        }
         
    }) ;
    //deleting institute   
    $('#delete_inst').click(function(){      
        $('#div_delete_institute').show();
    }) ;
    $('.btn_cancel').click(function(){       
        $('#div_delete_institute').hide();
    }) 
    
    

});
function institute_details(data){
    //update notfication 
    //   $('#noti_user').html('<p>Status:'+ data.status +' Data:'+data.data+'</p>');
    
    if(data){
        
        if(data.status){
            var htm = '<div class="alert alert-success"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-success"></i> <span class="success-msg"> <strong>Success!</strong> '+data.data+'</span></div>' ;
        }
        else{
            var htm = '<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-error"></i> <span class="error-msg"> <strong>Error!</strong> '+data.data+'</span></div>' ;
        }
        $('#noti_institute').html(htm);
    }
} 
function result_create_institute(data){
    
    if(data['success']){
        var htm = '<div class="alert alert-success"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-success"></i> <span class="success-msg"> <strong>Success!</strong> Institute created successfully.</span></div>' ;
       
         $('#noti_institute').html(htm);
         setTimeout(function(){ window.location.href = data['url']},3000);
       
            
    }
    else{
         var htm = '<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-error"></i> <span class="error-msg"> <strong>Error!</strong>Could not create institute,Please try again.</span></div>' ;
          $('#noti_institute').html(htm);
    }
    
}
    
function ajax_call(source,url,data,callback){

    var crnturl = url;
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        timeout: 3000,
        beforeSend: function() {
            //console.log(url);
            //console.log(data);
            //console.log(source);
            $('a.ajax-loader').trigger('click');
        },
        complete: function() {
            $('button.ajax-close').trigger('click');
          
        },          
        cache: false,
        success: function(result) {
            console.log(result);
            $('button.ajax-close').trigger('click');             
               
            var fn = window[callback];
            fn(result);
        //                }
                 
        },
        error: function(error) {
            $('button.ajax-close').trigger('click');
          
        //alert("Some problems have occured. Please try again later: " );
        //console.log(error);
        }
    });


}

function load_institute(){
    var source  = $('#tab2');
    var callback = 'load_institute_result';
    var url  = '/institute/get-institute-data/';
    var data  = {        
        'institute_id':getParameter('instituteid')
           
    };
    //// //console.log(data);
    var type = $('#institute_type').val();
    if(type!='create'){
        ajax_call(source,url,data,callback);
    }
       
}

function load_institute_result(data){
    if(!data){
        return false;
    }
   
    update_elements(data);
       
    
}
function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
    i, val, params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}



//script for page elements 

   
$(document).ready(function() {
	
    $('.remove-course').live('click',function(){
        $(this).parent().parent().remove();
    });
    
     
        
});

 

function  update_elements(result){
     
    var data = result['institute_data'] ;  
    
    if(!result ){
        return false;
    }
   //create dropdowwn for the page 
   if(result['cities'].length > 0 ){
       dd_city(result['cities'],data['city_id']);
   }
    if(!data ){
        return false;
    }
    //now prepoulate the institute details form 
        $("input:[name='abbreviation']").val(data['abbreviation']);
(data['campus_company_exists']=='1') ? $("input:[name='campus_company_exists']").attr('checked','checked') : $("input:[name='campus_company_exists']").removeAttr('checked'); 
        $("input:[name='category']").val(data['category']);    
        $("input:[name='consultant_in_charge']").val(data['consultant_in_charge']);
        (data['e_cell_exists']=='1') ? $("input:[name='e_cell_exists']").attr('checked','checked') : $("input:[name='e_cell_exists']").removeAttr('checked'); 
        
        (data['e_cell_level']=='1') ? $("input:[name='e_cell_level']").attr('checked','checked') : $("input:[name='e_cell_level']").removeAttr('checked'); 
(data['edc_exists']=='1') ? $("input:[name='edc_exists']").attr('checked','checked') : $("input:[name='edc_exists']").removeAttr('checked');
        $("input:[name='geographic_region']").val(data['geographic_region']);
        $("input:[name='head_of_institute']").val(data['head_of_institute']);
        (data['iedc_exists']=='1') ? $("input:[name='iedc_exists']").attr('checked','checked') : $("input:[name='iedc_exists']").removeAttr('checked');
(data['incubator_exists']=='1') ? $("input:[name='incubator_exists']").attr('checked','checked') : $("input:[name='incubator_exists']").removeAttr('checked');
        $("input:[name='institute_level']").val(data['institute_level']);
        $("input:[name='jurisdiction']").val(data['jurisdiction']);
        $("input:[name='mailing_address_report']").val(data['mailing_address_report']);
        $("input:[name='management']").val(data['management']);
        $("input:[name='member_status']").val(data['member_status']);
(data['mentoring_unit_exists']=='1') ? $("input:[name='mentoring_unit_exists']").attr('checked','checked') : $("input:[name='mentoring_unit_exists']").removeAttr('checked');
        $("input:[name='name']").val(data['name']);
        $("input:[name='nen_administrative_classification']").val(data['nen_administrative_classification']);
        $("input:[name='nen_category']").val(data['nen_category']);
        $("input:[name='nen_joining_date']").val(data['nen_joining_date']);
        $("input:[name='nen_territory_grouping']").val(data['nen_territory_grouping']);
        $("input:[name='number_of_students']").val(data['number_of_students']);
        $("input:[name='patent_office_exists']").val(data['patent_office_exists']);
        $("textarea:[name='primary_address_1']").val(data['primary_address_1']);
        $("textarea:[name='primary_address_2']").val(data['primary_address_2']);
        $("input:[name='primary_city']").val(data['primary_city']);
        $("input:[name='primary_state']").val(data['primary_state']);
        $("input:[name='primary_zip_code']").val(data['primary_zip_code']);
        $("input:[name='profile']").val(data['profile']);
        $("select:[name='sustainability']").val(data['sustainability']);
        $("select:[name='type']").val(data['type']);

   
    //show institute list only for the admin
   
        
    $("select:[name='type'],select:[name='city_id'],select:[name='sustainability']").select2({
      placeholder: "Select Institute"
        });
    return true;
}
 
 
function dd_city(cities,selected){
    if(!cities){
        return ''; 
    }
    if(!selected){
       selected = '0'; 
    }
     
    var htm = '<option value="0"> Select City </option>';
    $.each(cities,function(key,val){
        var flag  = val['city_id']== selected ? 'selected' :'' ;
        htm += '<option value="'+val['city_id']+'"  '+flag+' >'+val['name']+' </option>';  
    })
    
    $('#city_id').html(htm);
    
    return true;
}
function con_institute_list(data,selected){
    if(!data){
        return ''; 
    }
    if(!selected){
        selected = '';
    }  
      
    var htm = ' <select name="con_institute[]" id="con_institute" class="insti_dropdown" multiple>';
    var flag  =  '' ;
    var selected_insti = new Array();
    //get selected institutes in an array 
    $.each(selected,function(k,v){
        selected_insti.push(v['id']);
    });
        
    $.each(data,function(key,val){
        if($.inArray(val['id'], selected_insti) != -1 ){
            flag  =  'selected="selected"' ;
        }else{
            flag = '';
        }
        htm += '<option value="'+val['id']+'"  '+flag+' >'+val['name']+' </option>';  
    })
    htm+=' </select>';
        
    $('#div_con_institute').html(htm);
        
    //        $('.insti_dropdown').hide();
    $('#div_con_institute').show();
       
    return true;
}
    
function dd_htm(source,data){
    
    $('#'+source+' option').each(function(i){
        
        if( $(this).val()==data){
           
            $(this).attr('selected', true);
            var bt_id  = '#s2id_'+ source ;
            $(bt_id+ ' span' ).text($(this).text());
        }
      
    });
}
    