/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function init_dashboard(data){
       create_pie_chart(data);
       set_elements(data);
       set_activites(data['activities']);
}

   
function create_pie_chart(data){
        var ecell  = get_pie_data(data.ecell.score);
        var edc  = get_pie_data(data.edc.score);
        var iedc  = get_pie_data(data.iedc.score);
        var esc  = get_pie_data(data.esc.score);
        var svmu  = get_pie_data(data.svmu.score);
        var cc  = get_pie_data(data.cc.score);

    var myPie = new Chart(document.getElementById("canvas_ecell").getContext("2d")).Pie(ecell,{animation:false});
    var myPie1 = new Chart(document.getElementById("canvas_edc").getContext("2d")).Pie(edc,{animation:false});
    var myPie2 = new Chart(document.getElementById("canvas_esc").getContext("2d")).Pie(esc,{animation:false});
    var myPie3 = new Chart(document.getElementById("canvas_iedc").getContext("2d")).Pie(iedc,{animation:false});
    var myPie4 = new Chart(document.getElementById("canvas_cc").getContext("2d")).Pie(cc,{animation:false});
    var myPie5 = new Chart(document.getElementById("canvas_svmu").getContext("2d")).Pie(svmu,{animation:false});
}  

function get_pie_data(complete){
    var incomplete  = 100-complete;
    var pie = new Array();
    pie = [{ value: incomplete,color:"#E64c3c"    },
           {value : complete,color : "#2ecc71"}   
    ];
   
    
    return pie;

}

function set_elements(data){
    
    var types = new Array('ecell','edc','iedc','svmu','cc','esc');
    var today = new Date();
    
    $.each(types,function(k,type){
        var id  = '#card_'+type;
        $(id+ ' .percentage').html('<span>'+data[type].score+'</span>% Complete');
        $(id+ ' .incomplete').html('<span>'+(100-data[type].score)+'</span>% Incomplete');
        // console.log(data[type].last_changed);
        //for all browser support
        if(data[type].last_changed){
             var date = data[type].last_changed.split(' ')[0];  
              var diff = Math.floor(( Date.parse(today ) - Date.parse(date ) ) / 86400000);
        }else{
             var diff = 100;
        }
     
      
       var updated = data[type].last_changed_text;
       
       var diff = parseInt(diff);
       if(diff <= 7){
           var flag = 'green';          
       }
       else if(7 < diff && diff <= 30){
            var flag = 'yellow';
       }
       else if(diff > 30){
           var flag = 'red';
       }
       else{
            var flag = 'red';
            updated = 'Not Updated';
       }
       
       if(updated==""){
           var flag = 'red';
            updated = 'Not Updated';
       }
       
        $(id+' .last-update').html('Last Updated : <span class="'+flag+'">'+updated+'</span>');        
        
    });
    
   
    
}

function set_activites(data){
    var htm =  new Array();
    var prefix = '<thead><tr><th>Sno.</th><th>Activity Name</th><th>Start Date</th><th>Duration</th><th>Option</th></tr></thead><tbody>';
    var suffix = '</tbody>';
    var time = new Array('week','month','qtr');
    $.each(time,function(k,v){
        htm['advance'] = create_act_table(data[v]['advance']);
        htm['upcoming'] = create_act_table(data[v]['upcoming']);
        htm['training'] = create_act_table(data[v]['training']);
     
        $('#'+v+'_adv').html(prefix+ htm['advance'] + suffix);
        $('#'+v+'_act').html(prefix+ htm['upcoming'] + suffix);        
        $('#'+v+'_train').html(prefix+ htm['training'] + suffix);
        
        
        
       
    });


}

function create_act_table(data){
    
    if(!data){
        return false;
    }
    var htm= '';
    $.each(data,function(k,v){
        var ins_id = getParameter('instituteid');
        htm+='<tr> <td>  '+(k+1)+'     </td> <td> '+v['activityinfo_title']+'  </td> <td>   '+display_date(v['activityplacetime_date_start'])+'    </td><td>  '+v['activityplacetime_duration']+'    </td><td><a href="/activity/edit?id='+v['activity_id']+'&instituteid='+ins_id+'" class="label label-info">View</a></td> </tr>';
    });
    
    return htm;
  
    
}


function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
    i, val, params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}


    
