// Script for Datepicker....
jQuery('#over_date').datepicker({
    autoclose: true,
    minViewMode:'months'
});
jQuery('#bi_foundationyear').datepicker({
    autoclose: true,
    minViewMode:'years'
});
jQuery('#ecellmilestones_date,#edcmilestones_date,#svmu_date,#incubator_date,#other_infra,#iedcesc_date,#date_user_select').datepicker({
    autoclose: true,
    minViewMode:'months'
});
jQuery('.datepicker').datepicker({
    autoclose: true
});

//Script multiple files for sponsers & partners 
$(document).ready(function() {
	
    $('.remove').live('click', function(){
        $(this).parent().remove();
    });
    //for resetting the file upload field 
    $('.file_upload_input').change(function(){        
        $(this).clearInputs();
        
    });
    $('.addsponser').live('click', function(){
        var thiscontributor_sponser = $(this).parent();
        newcontributor_sponser = thiscontributor_sponser.clone(true).insertAfter(thiscontributor_sponser);
        newcontributor_sponser.find('input:not(.add)').val("");
	
        // console.log($(this));
        $(this).removeClass('addsponser').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
        newcontributor_sponser.find('input.increment').val(parseInt(thiscontributor_sponser.find('input.increment').val())+1);
    });
    //			
    //    $('.remove').live('click', function(){
    //        $(this).parent().parent().remove();
    //    });
		
		
    $('.remove_partner').live('click', function(){
        $(this).parent().remove();
    });
				
    $('.addpartner').live('click', function(){
        var thiscontributor_partner = $(this).parent();
        newcontributor_partner = thiscontributor_partner.clone(true).insertAfter(thiscontributor_partner);
        newcontributor_partner.find('input:not(.add)').val("");
			
        $(this).removeClass('addpartner').addClass('remove_partner');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
        newcontributor_partner.find('input.increment').val(parseInt(thiscontributor_partner.find('input.increment').val())+1);
    });
			
    $('.remove_partner').live('click', function(){
        $(this).parent().parent().remove();
    });
	
});


// Multiple sponsers Script Ends Here.........

// Login and Reset Pasword  form Validation Starts Here.......	-->		
                
function validate(){
 
    if( document.loginForm.username.value == "" )
    {
        alert( "Please provide user name!" );
        document.loginForm.username.focus() ;
        return false;
    }
    if( document.loginForm.user-pwd.value == "" )
    {
        alert( "Please provide password" );
        document.loginForm.user-pwd.focus() ;
        return false;
    }

    return (true);
}
           
function emailvalidate(){
    var x=document.forms["forgotpassword"]["forgotpwd"].value;
    var atpos=x.indexOf("@");
    var dotpos=x.lastIndexOf(".");
    if( document.forgotpassword.forgotpwd.value == "" )
    {
        alert( "Please Provide Email Id!" );
        document.forgotpassword.forgotpwd.focus() ;
        return false;
    }
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
    {
        alert("Not a valid e-mail address");
        document.forgotpassword.forgotpwd.focus() ;
        return false;
    }
    alert("Successfully recovered the Password")
    return( true );
}
			
// Form Validation Ends Here.   -->
// Select option for Activity info form Page.  starts here....
		
$("select").change(function () {
    var str = "";
    str= jQuery("#activity-type select option:selected").text();
				  
    switch(str) {
        case "Conference": case "Competition": case "National Platform": case "Webinar": case "Review": case "Outreach":  case "Course": case "Workshop": case "Mentoring":case "Start-up Support":
            $("#course-credit").show();
            $("#activity-duration").show();
            break;
						 
        default:
            $("#course-credit").hide();
            $("#activity-duration").hide();
						
    }	
				
})

.change();


// End of Activity info Select Option.....		
//function for lead speaker select options and adding new Lead Speaker	

$(document).ready(function(){
	
    $('#exist-speaker').click (function(){
        var fname = $('#lead-speaker').val();
        add_speaker('','',fname);
		
    })
	
    $('#addspeaker').click (function(){
		
        var fname = $('#leadfname').val();
        var lead_lname = $('#leadlname').val();
        var lead_email = $('#leademail').val();
        add_speaker(lead_email,lead_lname,fname);
	
    });
	
    $('.deletelead').live('click',function(){
        var fname = $(this).attr('value');
        delete_lead(fname);
		
    });
	
    var leadspeaker = [];
	
    var add_speaker = function(lead_email,lead_lname,fname){
		
        if(leadspeaker.length){
            var exist = false;
            for(i=0; i< leadspeaker.length;i++)
            {
                if(leadspeaker[i].name == fname){
                    exist=true;
                    break;
                }
            }
            if(exist == true) {
            //  console.log('exist'+ fname);
            }
            else{
                newpos = leadspeaker.length;
                leadspeaker[newpos] ={
                    name:fname, 
                    lname:lead_lname, 
                    email:lead_email
                };
                leadlist();
            }
        }else {
            leadspeaker[0]={
                name:fname, 
                lname:lead_lname, 
                email:lead_email
            };
            leadlist();
		
        }
	
    } 
	 
	
    var delete_lead = function(fname){
	
        for(i=0; i<leadspeaker.length; i++)
        {
            if(leadspeaker[i].name == fname){
                removeItem = i;
                break;
            }
        }
        leadspeaker.splice(i,1);
        leadlist();
	
    }
	
    var leadlist=function() {
        var llist = $('<ol class="faculty"/>');
        for(i = 0; i< leadspeaker.length; i++) {
            var l = $('<li class="facultylist">'+leadspeaker[i].name+ ', ' + leadspeaker[i].lname + ', ' + leadspeaker[i].email +' <span class="deletelead" value="'+leadspeaker[i].name+'"><input type="button" value="X" class="remove"></span></li>');
            llist.append(l);
            
        //  console.log('----' + leadspeaker[i].email);
        }
        $('#leadlist').html(llist);
        
    }
    // Lead Speaker Option Exisr here.....
    // Add Faculty Function Starts Here.....
    $('#addexist').click(function(){
        var v = $('#dd').val();
        add('','',v);
    })
    
    $('#addEntry').click(function(){
        var t = $('#newpersonemail').val();
        var u = $('#newpersonphone').val();
        var v = $('#newperson').val();
        add(t,u,v);
		
    });
    
    $('.del').live('click',function(){
        var v = $(this).attr('value');
        del(v);
    });
    
    
    var bucket = [];
    
    var add = function(u,t,v) {
        if(bucket.length) {
            var exist = false;
            for(i = 0; i< bucket.length; i++) {
                if(bucket[i].name == v) {
                    exist = true;
                    break;
                }
            //alert(bucket[i].name)
            }
            if(exist == true) {
            //    console.log('exist '+ v);
            } else {
                newpos = bucket.length;
                bucket[newpos] = {
                    name: v,  
                    email: t, 
                    phone: u
                };
                list();
            }
        } else {
            bucket[0]= {
                name: v, 
                email: t, 
                phone: u
            };
            list();
        }
        
    }
    
    var del = function(v) {
        for(i = 0; i< bucket.length; i++) {
            if(bucket[i].name == v) {
                removeItem = i;
                break;
            }
        }
        
        bucket.splice(i,1);
        list();
    }
    
    var list=function() {
        var clist = $('<ol class="faculty"/>');
        for(i = 0; i< bucket.length; i++) {
            var l = $('<li class="facultylist">'+bucket[i].name+ ' ' + bucket[i].phone + ', ' + bucket[i].email +' <span class="del" value="'+bucket[i].name+'"><input type="button" value="X" class="remove"></span></li>');
            clist.append(l);
            
        //  console.log('----' + bucket[i].name);
        }
        $('#list').html(clist);
        
    }
});
$("#submitactivitycontributor").on('click',function(){
    var newFlag=1;
    $.each( $("input[name*='activitycontributor_sponsors_money']"), function(k, v){
      var flag;
     
      if(v.value >= 0 || v.value == '') {
          flag = 1;
          $(this).removeClass('custom-error');
      } else {
          flag = 0;
          $(this).addClass('custom-error');
      }
      newFlag *= flag;
      //console.log(newFlag);
    });
    if(newFlag == 1) {
        $("#insertactivitycontributor").submit();
        //console.log("submit");
    } else {
        $("#contributor-error").html('<div class="alert success-top alert-error"><button class="close" data-dismiss="alert">x</button><i class="icon-back-error"></i><span class="error-msg"><strong> Error! </strong>Please enter positive number</span></div>');
        return false;
        //console.log("return false");
    }
});

function notify(id, type, message, specificError) {
    //console.log("message",specificError);
    var clsType;
    switch(type) {
        case 'error':
            clsType = 'alert-error';
            if(!specificError)
                specificError = 'Invalid data and/or requiered field(s) missing..';
            message = '<i class="icon-back-error"></i><span class="error-msg"><strong> Error! </strong>'+  specificError +'</span>';
            break;
        case 'success':
            clsType = 'alert-success';
            message = '<i class="icon-back"></i><span class="error-msg1"><strong> Success! </strong>' + message +'</span>';
            break;
        case 'info':
            clsType = 'alert-info';
            message = '<strong>Info!! </strong>' + message;
            break;
    }
    var m = '<div class="alert success-top '+ clsType +'"><button class="close" data-dismiss="alert">x</button>'+ message+'</div>';
    $(id).html(m);
//console.log(id);
}



function requiered(e) {
    if(!$(e).val()){
        //  //console.log('value is requiered');
        $(e).focus();
        try {
            var d = $(e).parent().parent();
            d.addClass("error");
        } catch(e){}
        return 'Please fill requiered field(s)';
        
    } else {
        try {
            var d = $(e).parent().parent();
            d.removeClass("error");
        } catch(e){}
    }
    return true;
}

function isNumber(e) {
    if($(e).val() < 0 || isNaN($(e).val()) && $(e).val()){
        try {
            var d = $(e).parent().parent();
            d.addClass("error");
        } catch(e){}
        return "Please enter positive number";
    } else if($(e).val() >= 0){
        try {
            var d = $(e).parent().parent();
            d.removeClass("error");
        } catch(e){}
        return true;
    }
}

function isAvgRating(e) {
    if($(e).val() < 0 || $(e).val() > 5 ){ //console.log("in here");
        try {
            var d = $(e).parent().parent();
            d.addClass("error");
        } catch(e){}
        return "Invalid average rating";
    } else if($(e).val() >= 0){//console.log("down here", $(e).val());
        try {
            var d = $(e).parent().parent();
            d.removeClass("error");
        } catch(e){}
        return true;
    }
}



function isInt(e) {
    if(!$(e).val()) {
        try {
            var d = $(e).parent().parent();
            d.removeClass("error");
        } catch(e){}
        return true;
    }
    if(isNaN($(e).val()) && $(e).val()){
        try {
            var d = $(e).parent().parent();
            d.addClass("error");
        } catch(e){}
        //   //console.log('Not a numbner is requiered');
        return "Please enter number";
    } else {
        if (Math.floor($(e).val()) == $(e).val()) {
            try {
                var d = $(e).parent().parent();
                d.removeClass("error");
            } catch(e){}
            return true;// value is an integer, do something based on that
        } else {
            try {
                var d = $(e).parent().parent();
                d.addClass("error");
            } catch(e){}
            return "Please enter integer";
        }
    }
    return true;
}

function isAmount(v, requiered) {
    if(requiered == true) {
        if(!v)
            return false;
    }
    if(isNaN(v)){
        return false;
    }
    if(v<0){
       return false; 
    }
    return true;
}

function dovalidate(fields, blk) {
    
    var err =[];
    var j=0;
    for(f in fields) {
        
        var item = '#'+ f;
        
        try {
            if(fields[f].validation &&  fields[f].validation instanceof Array) { 
                for(i=0; i< fields[f].validation.length ; i++) {
                    ////console.log(fields[f].validation[i]);
                    if(fields[f].validation[i] == 'required' ) {
                        var o = requiered(item);
                        if(o !== true) {
                            //    //console.log(o);
                            err[err.length] = o;
                        }
                    } 
                    if(fields[f].validation[i] == 'number' ) {
                        var o = isNumber(item);
                        if(o !== true) {
                              //console.log(o);
                            err[err.length] = o;
                        }
                    }
                    if(fields[f].validation[i] == 'integer' ) {
                        var o = isInt(item);
                        if(o !== true) {
                            //   //console.log(o);
                            err[err.length] = o;
                        } 
                    }
                    if(fields[f].validation[i] == 'avgrating' ) { 
                        //console.log($(item).val());
                        var o = isNumber(item);
                        //console.log("validation average block", o);
                        if(o !== true) { //alert("in the block");
                            err[err.length] = o;
                        } else { //alert("avg");
                            var o = isAvgRating(item);
                            if(o !== true) {
                                //    //console.log(o);
                                err[err.length] = o;
                            }
                             
                        }
                    }
                    
                }
            }
            
        } catch(e){}
        j++;
    }
    ////console.log(err);
    
    for(i=0; i< err.length; i++) {
        if(err[i] != true) {
            notify('#'+blk, 'error', 'Invalid data', err[i]); 
            return false;
        }
    }
    return true;
}

function saveContinueGetNextId(page, block) {     //function for step by step form submittion
    var collapse = {
        'ecell' : [
        {
            'key' : 'overview' , 
            'value' : 't-open'
        },

        {
            'key' : 'startup' , 
            'value': 't-close1'
        },

        {
            'key': 'establish', 
            'value': 't-close2'
        },

        {
            'key': 'hiperformance' , 
            'value': 't-close3'
        }
        ],
        'edc' : [
        {
            'key' : 'overview' , 
            'value' : 't-open'
        },

        {
            'key' : 'programme' , 
            'value': 't-close1'
        },

        {
            'key': 'funding', 
            'value': 't-close2'
        },

        {
            'key': 'infrastructure' , 
            'value': 't-close3'
        }
        ]
    }
    
    var blocks = collapse[page];
    for(i=0 ; i< blocks.length; i++) {
        if(blocks[i].key == block) {
            var currentBlock = blocks[i].value;
            $('#'+currentBlock).click(function(){
                $(this).siblings('span').show();
            });
            $('#'+currentBlock).trigger('click'); //close current block
            if(i < (blocks.length -1)) {
                var nextBlock =  blocks[i+1].value;
                $('#'+nextBlock).trigger('click'); //open next block
            }
            break;
        }
    }
}
    
//Add Facuty Function Ends Here......     		
/*
			 
jQuery(document).ready(function() {
    var myData =  jsonData = {
        "metadata": [
        {
            "name": "sore",
            "label": "STUDENT / ENTREPRENEUR",
            "datatype": "string",
            "editable": true
        },
        {
            "name": "fname",
            "label": "FIRST NAME",
            "datatype": "string",
            "editable": true
        },
        {
            "name": "lname",
            "label": "LAST NAME",
            "datatype": "string",
            "editable": true
        },
        {
            "name": "email",
            "label": "EMAIL ID",
            "datatype": "email",
            "editable": true
        },
        {
            "name": "mobileno",
            "label": "MOBILE NO",
            "datatype": "string",
            "editable": true
        },
        {
            "name": "feespaid",
            "label": "FEES PAID",
            "datatype": "string",
            "editable": true
        }
						
						
        ],
        "data": [
        {
            "id": 1,
            "values": {
                "sore": "Student",
                "fname": "Duke",
                "lname": "Palov",
                "email": "dukepalov@gmail.com",
                "mobileno": 3333344444,
                "feespaid": 123,
								
            }
        },
        {
            "id": 2,
            "values": {
                "sore": "Entrepreneur",
                "fname": "Robin",
                "lname": "Issac",
                "email": "robinissac@gmail.com",
                "mobileno": 7777799999,
                "feespaid": 007,
								
            }
        }
        ]
    };
			
    //console.log(myData);
    editableGrid = new EditableGrid("DemoGridJSON"); 
    editableGrid.tableLoaded  = function() {
        this.renderGrid("tablecontent", "testgrid");
    };
    editableGrid.processData(myData);
				
    var read = function (user){
        //var last = editableGrid.data.length;
        ////console.log(user.name);
						
        var presentdata = Array();
        var flag=0;
        var table = $('.testgrid');
        table.find('tr').each(function(index, row)
        {
            var allCells = $(row).find('td');
            if(allCells.length > 0)
            {
                allCells.each(function(index, td)
                {
                    //alert(td.innerHTML);
                    presentdata[flag] = td.innerHTML;
                    flag++;
                });
            }
        });
						
        arrlen=presentdata.length;
        var v=0;
        for(var p=0;p<(arrlen/6);p++)
        {
            var item = {
                "id": p+1,
                "values": {
                    "sore": presentdata[v],
                    "fname": presentdata[v+1],
                    "lname": presentdata[v+2],
                    "email": presentdata[v+3],
                    "mobileno": presentdata[v+4],
                    "feespaid": presentdata[v+5] 
                }
            }
            myData.data[p]= item;
            v=v+6;
        }
						
						
        //console.log(myData);
        if((user.fname!='') && (user.lname!='') && (user.sore!='') && (user.email!='') && (user.mobileno!='') && (user.feespaid!='')){
            var dtlen = myData.length;
            var newitem = {
                "id": dtlen,
                "values": {
                    "sore": user.sore,
                    "fname": user.fname,
                    "lname": user.lname,
                    "email": user.email,
                    "mobileno": user.mobileno,
                    "feespaid": user.feespaid 	
                }
            }
						
            var l = myData.data.length;
            myData.data[l] = newitem;
        }	
        //console.log(myData);            
					
        editableGrid = new EditableGrid("DemoGridJSON"); 
        editableGrid.tableLoaded  = function() {
            this.renderGrid("tablecontent", "testgrid");
        };
        editableGrid.processData(myData);
    }
				
    $("#addNew").click(function() {
        var sore = $('#sore').val();
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var email = $("#email").val();
        var mobileno = $("#mobileno").val();
        var feespaid = $("#feespaid").val();
        var user = {
            "sore": sore, 
            "fname":fname, 
            "lname":lname, 
            "email":email, 
            "mobileno":mobileno,  
            "feespaid":feespaid
        };
        read(user);
    });
});*/
jQuery(document).ready(function(){
   
    jQuery('#activityoutcome_plan_yes').click(function(){
        $('.follow-updetails').show();
    });
    jQuery('#activityoutcome_plan_no').click(function(){
        $('.follow-updetails').hide();
    });
     
    $('#info-view').on('click', function() {
        $('.p1').show(); 
        
        $(this).closest('form').each (function(){
            this.reset();
        });
    });
    $('#act-info-view').trigger('click');
    
    $('#edit1').bind('click',function(){
        $('.p1').hide(); 
    });
    $('#contr-view').on('click', function() {
        $('.p2').show();
        $(this).closest('form').each (function(){
            this.reset();
        });
    });
    $('#act-contr-view').trigger('click');
    
    $('#edit2').bind('click',function(){
        $('.p2').hide(); 
    });
    $('#parti-view').on('click', function() {
        $('.p3').show();
        $('#insertactivityparticipants').each (function(){
            this.reset();
        });

    });
    $('#act-parti-view').trigger('click');
    
    $('#edit3').bind('click',function(){
        $('.p3').hide(); 
    });
    $('#outcome-view').on('click', function() {
        $('.p4').show();
        $(this).closest('form').each (function(){
            this.reset();
        });
    });
    $('#act-outcome-view').trigger('click');
    
    $('#edit4').bind('click',function(){
        $('.p4').hide(); 
    });
    $('#edit5').bind('click',function(){
        $('.p5').hide(); 
    });
    $('#edit6').bind('click',function(){
        $('.p6').hide(); 
    });
    $('#edit7').bind('click',function(){
        $('.p7').hide(); 
    });
    $('#edit8').bind('click',function(){
        $('.p8').hide(); 
    });
    $(".widget-title-ftd").on ('click', function () {
      $(this).next().stop().slideToggle('slow');
      $(this).find(".icon-plus").stop().toggleClass("icon-minus");
    });
   
    
   
    
    //custom script  for ecell
    $('.view-more').on('click', function() {
        $('.hidden-block').slideDown("slow");
        $('.view-more').hide();
        $('.view-less').show();
    });
    $('.view-less').on('click', function() {
        $('.hidden-block').slideUp("slow");
        $('.view-less').hide();
        $('.view-more').show();
    });
    $('.view-more1').on('click', function() {
        $('.hidden-block1').slideDown("slow");
        $('.view-more1').hide();
        $('.view-less1').show();
    });
    $('.view-less1').on('click', function() {
        $('.hidden-block1').slideUp("slow");
        $('.view-more1').show();
        $('.view-less1').hide();
    });
    $('.view-more2').on('click', function() {
        $('.hidden-block2').slideDown("slow");
        $('.view-more2').hide();
        $('.view-less2').show();
    });
    $('.view-less2').on('click', function() {
        $('.hidden-block2').slideUp("slow");
        $('.view-more2').show();
        $('.view-less2').hide();
    });
    $('.view-more3').on('click', function() {
        $('.hidden-block3').slideDown("slow");
        $('.view-more3').hide();
        $('.view-less3').show();
    });
    $('.view-less3').on('click', function() {
        $('.hidden-block3').slideUp("slow");
        $('.view-more3').show();
        $('.view-less3').hide();
    });
    $('.view-more4').on('click', function() {
        $('.hidden-block4').slideDown("slow");
        $('.view-more4').hide();
        $('.view-less4').show();
    });
    $('.view-less4').on('click', function() {
        $('.hidden-block4').slideUp("slow");
        $('.view-more4').show();
        $('.view-less4').hide();
    });
    $('.view-more5').on('click', function() {
        $('.hidden-block5').slideDown("slow");
        $('.view-more5').hide();
        $('.view-less5').show();
    });
    $('.view-less5').on('click', function() {
        $('.hidden-block5').slideUp("slow");
        $('.view-more5').show();
        $('.view-less5').hide();
    });
    $('.view-more6').on('click', function() {
        $('.hidden-block6').slideDown("slow");
        $('.view-more6').hide();
        $('.view-less6').show();
    });
    $('.view-less6').on('click', function() {
        $('.hidden-block6').slideUp("slow");
        $('.view-more6').show();
        $('.view-less6').hide();
    });
    $('.view-more7').on('click', function() {
        $('.hidden-block7').slideDown("slow");
        $('.view-more7').hide();
        $('.view-less7').show();
    });
    $('.view-less7').on('click', function() {
        $('.hidden-block7').slideUp("slow");
        $('.view-more7').show();
        $('.view-less7').hide();
    });
    $('.view-more8').on('click', function() {
        $('.hidden-block8').slideDown("slow");
        $('.view-more8').hide();
        $('.view-less8').show();
    });
    $('.view-less8').on('click', function() {
        $('.hidden-block8').slideUp("slow");
        $('.view-more8').show();
        $('.view-less8').hide();
    });
    $('.view-more9').on('click', function() {
        $('.hidden-block9').slideDown("slow");
        $('.view-more9').hide();
        $('.view-less9').show();
    });
    $('.view-less9').on('click', function() {
        $('.hidden-block9').slideUp("slow");
        $('.view-more9').show();
        $('.view-less9').hide();
    });

});		
/**
 * Clears the selected form elements.
 */
$.fn.clearFields = $.fn.clearInputs = function(includeHidden) {
    var re = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i; // 'hidden' is not in this list
    return this.each(function() {
        var t = this.type, tag = this.tagName.toLowerCase();
        if (re.test(t) || tag == 'textarea') {
            this.value = '';
        }
        else if (t == 'checkbox' || t == 'radio') {
            this.checked = false;
        }
        else if (tag == 'select') {
            this.selectedIndex = -1;
        }
        else if (t == "file") {
            if (/MSIE/.test(navigator.userAgent)) {
                $(this).replaceWith($(this).clone(true));
            } else {
                $(this).val('');
            }
        }
        else if (includeHidden) {
            // includeHidden can be the value true, or it can be a selector string
            // indicating a special test; for example:
            //  $('#myForm').clearForm('.special:hidden')
            // the above would clean hidden inputs that have the class of 'special'
            if ( (includeHidden === true && /hidden/.test(t)) ||
                (typeof includeHidden == 'string' && $(this).is(includeHidden)) )
                this.value = '';
        }
    });
};

function validate_range(e,lower,upper) {
    if( (!isNaN(parseFloat($(e).val())) && isFinite($(e).val())) || ($(e).val())=="" ){            
         
    }
    else{
        try {
            var d = $(e).parent().parent();
            d.addClass("error");
        } catch(e){}
        return false;
    }
    if($(e).val() < lower || $(e).val() > upper){
        try {
            var d = $(e).parent().parent();
            d.addClass("error");
        } catch(e){}
        return false;
    } else {
        try {
            var d = $(e).parent().parent();
            d.removeClass("error");
        } catch(e){}
        return true;
    }
}

$(window).load(function(){
   $("#facdrpdwn").select2();

});
