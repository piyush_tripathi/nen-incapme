var newitem = {
    "id" : ecel.tdata,
    "values" : {"name": "",
        "email": "",
        "contact_no": "",
        "year_grad": "",
        "is_e_leader": ""
    }
}

var last = jsondata.data.length;
jsondata.data[last] = newitem;

var newitem = {"id" : "",
    "values" : {"name": "",
        "organization": "",
        "designation": "",
        "email": "",
        "contact_no": ""
    }
}

var last = edc.tdata.edc_members.data.length;
var newitem = {"id" : last,
    "values" : {
        "designation": "",
        "first_name": "",
        "last_name": "",
        "dob": "",
        "sex": "",
        "email": "",
        "contact_no": "",
        "start_date": "",
        "remarks": ""
    }
}
edc.tdata.edc_members.data[last] = newitem;


var newitem = {"id" : "",
    "values" : {
        "designation": "",
        "institute": "",
        "first_name": "",
        "last_name": "",
        "apellation": "",
        "date_of_joining": "",
        "date_of_leaving": "",
        "remarks": ""
    }
}


$(".add-grid-item").click(function() {
    var gridName = $(this).attr('id');
    switch(gridName) {
        case "grid_edc_members": 
            var last = edc.tdata.edc_members.data.length;
            var newitem = {
                "id" : last,
                "values" : {
                    "designation": "",
                    "first_name": "",
                    "last_name": "",
                    "dob": "",
                    "sex": "",
                    "email": "",
                    "contact_no": "",
                    "start_date": "",
                    "remarks": ""
                }
            }
            edc.tdata.edc_members.data[last] = newitem;
            edc.studentGrid.processData(edc.tdata.edc_members, "edcMembers");
            
            break;
            
        case "grid_campus_members":
        
        case "grid_edc_advisors": 
         
        case "grid_campus_advisors":
            $("#course-credit").show();
            $("#activity-duration").show();
            break;
						 
        default:
            $("#course-credit").hide();
            $("#activity-duration").hide();
						
    }
    
       
});