$(document).ready(function(){
    load_iedc();
    
    //update_form();
    $('#create_institute_iedc').click(function(){
        var form  = $(this).closest('form');
        var callback = 'result_create_institute_iedc';
        var url  = form.attr( 'action' );
        var data  = form.serialize();
        ajax_call(form,url,data,callback);
        
    }) 
    $('.btn_save_form').click(function(){      
        var form  = $(this).closest('.div_form').find(':input');
        var callback = 'load_iedc';
        var url  =  $(this).closest('.div_form').attr( 'action' );
        var id=$(this).attr('id');
        var valid = true;
        var msg = '';
        switch(id){
            case 'btn_save_four':
                $('#pp_updateprojectlist').val(JSON.stringify(grid['grid_project'].data) );
                $('#pp_updatepanelistlist').val(JSON.stringify(grid['grid_panel'].data) );
                valid =  validate_range('#pp_ongoingproject',0,25) && validate_range('#pp_noofprojectsidentified',0,99);  
                if(!validate_range('#pp_ongoingproject',0,25)) {
                    msg+= '<li class="list-error"> Number must be between 0 and 25</li>';
                }
                if(!validate_range('#pp_noofprojectsidentified',0,99)){
                    msg+= '<li class="list-error"> Number must be between 0 and 99</li>';
                }
                break;
            case 'btn_save_two':
                valid =  validate_range('#td_noofactivestudents',0,999); 
                if(!validate_range('#td_noofactivestudents',0,999)) {
                    msg+= '<li class="list-error">Number must be between 0 and 999</li>';
                }
               
                break;             
            case 'btn_save_five':                              
                $('#iedc_team_details').val(JSON.stringify(grid['grid_attendee'].data) );
                break;
            case 'btn_save_eight':
                valid =  validate_range('#fund_received',0,999999) ;  
                if( !validate_range('#fund_received',0,999999)){
                    msg+= '<span> Number must be between 0 and 999999</span>';
                }
                break;
//            default:
//                break;
        }
        
        var data  = form.serialize();
        //$(form).submit();
        ////console.log(url);
        
        if(valid){
            ajax_call(form,url,data,callback);
            $('#iedc-info-view-hide, #iedc-contr-view-hide, #iedc-parti-view-hide,#iedc-outcome-view-hide,#iedc-proj-select-view,#iedc-pitch-fund-view,#iedc-funding-view').click();
           $('.p1,.p2,.p3,.p4,.p5,.p6,.p7').show();
            $('.alert-error,.view-less,.hidden-block').hide();
           $('#success_'+id).html('<div class="alert alert-success success-top"> <button class="close" data-dismiss="alert">×</button><i class="icon-back"></i><span class="error-msg1"><strong> Success! </strong> Record updated.</span></div>');
        }
        else{
            
          $('.alert-success').hide();
            $('#noti_'+id).html('<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-error"></i><span class="error-msg"> <strong>Error!</strong> '+ msg + '</span></div>');
             
        }
        
    }) 
    
    $('#iedc-info-view, #iedc-contr-view, #iedc-parti-view,#iedc-outcome-view,#iedc-proj-view,#iedc-proj-fund-view,#iedc-funding-view').click(function(){
        load_iedc();
    });
    
   
    $('#btn_publish_iedc').click(function(){
        $('#form_publish_iedc').submit();
        load_iedc();
    })
    $('#iedc-info-view').on('click', function() {
        $('.p1').show(); 
        
    //         $(this).closest('form').each (function(){
    //            this.reset();
    //          });
    });
    $('#iedc-act-info-view').trigger('click');
    
    $('#edit1').bind('click',function(){
        $('.p1').hide(); 
    });
    $('#iedc-contr-view').on('click', function() {
        $('.p2').show();
    //            $(this).closest('form').each (function(){
    //  this.reset();
    //});
    });
    $('#iedc-act-contr-view').trigger('click');
    
    $('#edit2').bind('click',function(){
        $('.p2').hide(); 
    });
    $('#iedc-parti-view').on('click', function() {
        $('.p3').show();
    //            $(this).closest('form').each (function(){
    //  this.reset();
    //});
    });
    $('#iedc-act-parti-view').trigger('click');
    
    $('#edit3').bind('click',function(){
        $('.p3').hide(); 
    });
    $('#iedc-outcome-view').on('click', function() {
        $('.p4').show();
    }); 
    $('#submitactivitycontributor').on('click', function() {
        var val = $('#s2id_gs_targeted_level span').text();
        $('#tab3 .title-overview').text(val);
        $('.p2').show();
    }); 
    
    // for review fourth form 
    $('#vt_menteefeedreviewed_n,#vt_menteefeedreviewed_y').click(function(){
            if(this.id =='vt_menteefeedreviewed_n'){
               
                $('.mrevirw').hide();
            }
            else{

                 $('.mrevirw').show();
            }
    });
    
    //for clearing the upload file option in esc form 
//    var selector = '#bi_prncplhodbriefed_n,#bi_planofactiondiscussed_na,#bi_planofactiondiscussed_n,#bi_planofactiondiscussed_na';
//    $(selector).click(function(){
//        reset_form_element(this)  
//    }) 
//   
   
      
});
function result_create_institute_iedc(data){
    
    if(data['success']){
        window.location.href = data['url'];
    }
    else{
       // alert('error happpend');
    }
}

function ajax_call(source,url,data,callback){
    
    var crnturl = url;
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        timeout: 3000,
        beforeSend: function() {
            $('a.ajax-loader').trigger('click');
        //source.append('<div class="ajax_loader">Loading........... </div>');
        },
        complete: function() {
            $('button.ajax-close').trigger('click');
          
        },          
        cache: false,
        success: function(result) {
            $('button.ajax-close').trigger('click');
                console.log(result);
                if(callback=='result_create_institute_iedc')
                {
                    var crntdata = new Array();
                    var iedcid= result.iedc_id;
                    var strurl='http://qa.incapme.com/esc/edit?id='+iedcid+'&'+crnturl.substring(10);
                    
                    crntdata['url'] = strurl;
                    crntdata['success']= 'success';
                    
                    result_create_institute_iedc(crntdata);
                }
                else{
               
                var fn = window[callback];
                fn(result);
                } 
        },
        error: function(error) {
            $('button.ajax-close').trigger('click');
          
        //alert("Some problems have occured. Please try again later: " );
        ////console.log(error.responseText);
        }
    });


}

function load_iedc(){
    var source  = $('#tab2');
    var callback = 'load_iedc_result';
    var url  = '/esc/getescdata/';
    var data  = {
        'esc_id':getParameter('id')
        };
    //////console.log(data);
    var type = $('#iedc_type').val();
    if(type!='create'){
        ajax_call(source,url,data,callback);
    }
       
}

function load_iedc_result(result){
    if(!result){
        return false;
    }
   
    var data = result['data_esc'];
    var data_grid = result['data_grid'];
    ////console.log(result);
    
    $('input[type!="hidden"],select,textarea').each(function() {
        //////console.log(this.name); 
    
        if(this.name=='bi_availableinfra[]'){
            var index = this.name.replace("[","");
            index = index.replace("]","");
            
            var value = data[index];        
            if(value){
                var htm = multi_htm(value,this.name);           
                var text = multi_lbl(value);
                $('.view-more6').show();
            }
            else{               
                $('input[name="bi_availableinfra[]"]').each(function(){

                           $(this).removeAttr('checked');
                    
                   });
                   $('#bi_availableinfra_other_value').val('');
                   $('input[name="bi_availableinfra_other"]').removeAttr('checked');
            }
            
            
            
               
            
        }
        else if(this.name=='bi_availableinfra_other'){
            // do not change this checkbox value by default
            
        }
        
        
        else if(this.name=='vt_menteefeedreviewed'){
            if(data[this.name] =='0'){
                $("#vt_menteefeedreviewed_n").parent().addClass('checked');
                  $("#vt_menteefeedreviewed_n").attr('checked',true);
                $('.mrevirw').hide();
            }
            else{
                 $("#vt_menteefeedreviewed_y").parent().addClass('checked');
                 $("#vt_menteefeedreviewed_y").attr('checked',true);
                 $('.mrevirw').show();
            }
        }
        else if($(this).attr('type')=='button'){
            //alert('button');
        }
//        else  if($(this).is('input:checkbox')){
//            var val = data[this.name];
//            if(!val){
//                val = 0;
//            }
//            var htm = chk_htm(this.name,val);
//            var text = chk_lbl(this.name,val);
//            
//        }
        else  if($(this).is('input:radio')){
            var val = data[this.name];
            var htm = radio_htm(this.name,val);
//            if(val && val!=''){
//                var htm = radio_htm(this.name,val);
//               
//                
//            }
            
           radio_lbl(this.name,val);
            
        }
        else if($(this).is('select')){
            var val = data[this.name];
            if(val){
                dd_htm(this.name,val);
                dd_lbl(this.name,val);
            }
            else{
                 dd_lbl(this.name,val);
            }
        }
        else if($(this).is('textarea')){
            var val = data[this.name];
            if(!val){
                val = '';
                $(this).val(val);
                var id = $(this).attr('name');
                $('#count_'+id).text('No Data');
                $('.view-more7').hide();
                
            }
            else{
                $(this).val(val);
                var id = $(this).attr('name');
                $('#lbl_'+id).text(val);
                $('.view-more7').show();
                $('#count_'+id).text('');
            }
            
        }
        
        else{            
            var val = data[this.name] ;            
            if(val){
                 $(this).attr('value',val) ; 
                 $('#lbl_'+this.name).text(val) ;
            }else{
                $(this).attr('value','')
                $('#lbl_'+this.name).text('No Data') ;
            }
                  
            
        }
    });
 
   
   
    //   NOW UPDATING DATA GRIDS 
        //var participantData;          
        //grid  = new Array();
        //showMydataGrid(data_grid['grid_project'],'grid_project');
        //showMydataGrid(data_grid['grid_panel'],'grid_panel');
       // showMydataGrid(data_grid['grid_attendee'],'grid_attendee');
//        showMydataGrid(data_grid['grid_venture'],'grid_venture');
        
        update_label_values(result);
        
    // now updating conditional element visibility 
    iedccheckStatus();
    //now updating the notification status 
    

}
function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
    i, val, params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}


function update_label_values(result){
    //for apprived infra
    
    var data = result['data_esc'];   
    
    var htm='';

if(data['bi_availableinfra']){
    var label = data['bi_availableinfra'].split(';');    
    var lbl_str = '<ol><li>'+label.join('</li><li>').replace('|',' : ')+'</li></ol>';
    $('#lbl_bi_availableinfra').html(lbl_str);
    $('.view-more').show();
    $('#count_bi_availableinfra').html($('#lbl_bi_availableinfra li').size() );
}
else{
    
    $('#count_bi_availableinfra').html('No Data');
    $('.view-more').hide();
};
   
  if(label){
         var otr_vals = label.pop().split('|');
    if(otr_vals[0] == 'Other'){
        $('input[name="bi_availableinfra_other"]').attr('checked','checked');
        $('input[name="bi_availableinfra_other"]').parent().addClass('checked');
        $('input[name="bi_availableinfra_other_value"]').val( otr_vals[1] );
   }   
   } 
  
  if(data['fund_source']!=null){
       var fnd_vals = data['fund_source'].split('|');
   if(fnd_vals[0] == 'Other'){
        $('input[name="fund_source"]').attr('checked','checked');
        $('#fund_source_na').parent().addClass('checked');
        $('input[name="fund_source_other_value"]').val( fnd_vals[1] );
   } 
  }
  if(data['pff_submittedtoentity']!=null){
      var ent_vals = data['pff_submittedtoentity'].split(';').pop().split('|');
   if(ent_vals[0] == 'Other'){
        $('input[name="pff_submittedtoentity"]').attr('checked','checked');
         $('#pff_submittedtoentity_na').parent().addClass('checked');
        $('input[name="pff_submittedtoentity_other_value"]').val( ent_vals[1] );
   }
  }
   
   
   //for download option
   if(data['bi_uploadplanorMinutes']){
       $('#lbl_bi_uploadplanorMinutes').html('<a class="leftBorder" href="/'+get_download_link(data['bi_uploadplanorMinutes'])+'"> <i class="icon-download-alt"></i> Download </a>');
   }
   else{
        $('#lbl_bi_uploadplanorMinutes').html('');
   };
    
   if(data['bi_planofactionadopted']){
       $('#lbl_bi_planofactionadopted').html(data['bi_planofactionadopted']);
   }
   else{
        $('#lbl_bi_planofactionadopted').html('No Data');
   }
   ;
   
   //for lead faculty detais if no esc selected
   if(data['bi_planofactionadopted']){
       $('#lbl_bi_planofactionadopted').html(data['bi_planofactionadopted']);
   }
   else{
        $('#lbl_bi_planofactionadopted').html('No Data');
   }
   
      
      
     
}

function multi_htm(data,source){
    if(data==null){
        return true;
    }
   var vals  = data.split(';'); 
  
   $('input[name="bi_availableinfra[]"]').each(function(){
       
       if($.inArray($(this).attr('value'),vals)!= -1){
           $(this).attr('checked','checked');
       }
       else{
           $(this).removeAttr('checked');
       }
   });
   
   
}

function multi_lbl(data){
    
    if(!data){
        return '<ol></ol>';
    }
    var htm = '<ol>';
    var vals  = data.split(',');
    $(vals).each(function(key,val){
        if(val!=='null')
        htm+=' <li>'+val+'</li>';
    });
    htm+= '</ol>';
    return htm;
    
}
function dd_htm(source,data){
    
    $('#'+source+' option').each(function(i){
        
        if( $(this).val()==data){
           
            $(this).attr('selected', true);
            var bt_id  = '#s2id_'+ source ;
            $(bt_id+ ' span' ).text($(this).text());
        }
      
    });
}

function dd_lbl(name,val){
    
    var val = $('#'+name+' option:selected').text();
    
        if(val=='undefined' || val==''){
            $('#lbl_'+name).html( 'No Data' );
        }
        else{
            $('#lbl_'+name).html( val );
        }
    
}


function radio_htm(name,val){
    
   $('input:radio[name="'+name+'"]').each(function(){
       $(this).removeAttr('checked');
       $(this).parent().removeClass('checked');
        
   });
 // //console.log('name'+name+'  value:       input:radio[value="'+val+'"]');
//if($('input:radio[value="'+val+'"]').attr('checked')!='checked'){
//    $('input:radio[value="'+val+'"]').attr('checked','checked'); 
//     $('input:radio[value="'+val+'"]').parent().addClass('checked');
//}
if(val=='No' && name=='hasesc' ){
        $('#esc-no').attr('checked','checked');
        $('#esc-no').parent().addClass('checked');
        $('#lbl_'+name).html('No');
}
else if(val=='ESC' && name=='hasesc' ){
        $('#esc-esc').attr('checked','checked');
        $('#esc-esc').parent().addClass('checked');
        $('#lbl_'+name).html('Yes');
       
}
else if(val=='Needs-improvement' ){
        $('#'+name+'_y').attr('checked','checked');
        $('#'+name+'_y').parent().addClass('checked');
        $('#lbl_'+name).html('Needs Improvement');
}
else if(val=='Satisfactory' ){
        $('#'+name+'_n').attr('checked','checked');
        $('#'+name+'_n').parent().addClass('checked');
        $('#lbl_'+name).html('Satisfactory');
}
else if(val=='Not-evaluated' ){
        $('#'+name+'_na').attr('checked','checked');
        $('#'+name+'_na').parent().addClass('checked');
        $('#lbl_'+name).html('Not Evaluated');
}
else if(val=='Planned' ){
        $('#'+name+'_y').attr('checked','checked');
        $('#'+name+'_y').parent().addClass('checked');
        $('#lbl_'+name).html('Planned');
}
else if(val=='Completed' ){
        $('#'+name+'_n').attr('checked','checked');
        $('#'+name+'_n').parent().addClass('checked');
        $('#lbl_'+name).html('Completed');
}
else if(val=='NA' && name=='pp_iicompeteplan' ){
        $('#'+name+'_na').attr('checked','checked');
        $('#'+name+'_na').parent().addClass('checked');
        $('#lbl_'+name).html(val);
}
else if(val=='DST' && name=='pff_submittedtoentity'){
        $('#'+name+'_y').attr('checked','checked');
        $('#'+name+'_y').parent().addClass('checked');
        $('#lbl_'+name).html(val);
}
else if(val=='AICTE' && name=='pff_submittedtoentity'){
        $('#'+name+'_n').attr('checked','checked');
        $('#'+name+'_n').parent().addClass('checked');
        $('#lbl_'+name).html(val);
}
else if(val=='Other' && name=='pff_submittedtoentity' ){
        $('#'+name+'_na').attr('checked','checked');
        $('#'+name+'_na').parent().addClass('checked');
        $('#lbl_'+name).html('Other' + $('#'+name+'_other').val() );
}
else if(val=='DST' && name=='fund_source'){
        $('#'+name+'_y').attr('checked','checked');
        $('#'+name+'_y').parent().addClass('checked');
        $('#lbl_'+name).html('DST');
}
else if(val=='AICTE'&& name=='fund_source' ){
        $('#'+name+'_n').attr('checked','checked');
        $('#'+name+'_n').parent().addClass('checked');
        $('#lbl_'+name).html('AICTE');
}
else if(val=='Other' && name=='fund_source' ){
        $('#'+name+'_na').attr('checked','checked');
        $('#'+name+'_na').parent().addClass('checked');
        $('#lbl_'+name).html('Other' + $('#'+name+'_other').val() );
}
else if(val=='Yes'){
    $('#'+name+'_y').attr('checked','checked');
    $('#'+name+'_y').parent().addClass('checked');
    $('#lbl_'+name).html(val);
}
else if(val=='No'){
        $('#'+name+'_n').attr('checked','checked');
        $('#'+name+'_n').parent().addClass('checked');
        $('#lbl_'+name).html(val);
}
else if(val=='NA'){
        $('#'+name+'_na').attr('checked','checked');
        $('#'+name+'_na').parent().addClass('checked');
        $('#lbl_'+name).html(val);
}
else if(val=='EDC' ){
        $('#esc-esc').attr('checked','checked');
        $('#esc-esc').parent().addClass('checked');
        $('#lbl_'+name).html('EDC');
}

else{
      $('input:radio[name="'+name+'"]').each(function(){
       $(this).removeAttr('checked');
       $(this).parent().removeClass('checked');
        
   });
   iedccheckStatus();
}

   
}
function radio_lbl(name){
   var val = $('input:checked[name="'+name+'"]').val();
        if(val=='undefined'){
            $('#lbl_'+name).html( 'No Selection' );
        }
        else{
            $('#lbl_'+name).html( val );
        }
   
}
function chk_lbl(source,data){
    if(data == 1 ){
        $('#lbl_'+source).text('Yes');
        $('.lbl_'+ source).show();
    }
    else{
        $('#lbl_'+source).text('No');
        $('.lbl_'+ source).hide();
    }
}



//script for page elements 

   
$(document).ready(function() {
	
    $('.remove').live('click', function(){
        $(this).parent().remove();
    });
				
    $('.addnew').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addnew').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
                
    $('.addprogram').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addprogram').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
                
    $('.addgoal').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        //            newbox.find('input:not(.add)').val("");

        $(this).removeClass('addgoal').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
                
    $('.addcapacity').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addcapacity').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
        
   
        

        
        
    

});

 
function db_to_date(dateStr)
{
    dArr = dateStr.split("-");  // ex input "2010-01-18"
    if(!dArr[2]){
        return dateStr;
    }
    else{
        return dArr[2]+ "-" +dArr[1]+ "-" +dArr[0].substring(2); //ex out: "18/01/10"
    }
}

 
 
 function update_label_number(){
    $('.view-less,.view-less1,.view-less2,.view-less3,.view-less4,.view-less5,.view-less6,.view-less7,.view-less8, .view-less9').hide();
    $('.hidden-block,.hidden-block1, .hidden-block2,.hidden-block3,.hidden-block4,.hidden-block5,.hidden-block6,.hidden-block7,.hidden-block8, .hidden-block9').hide();   
    var iipOther = $('#lbl_iip_dkici_other ol li').size();
    if(iipOther==0){
        $('#count_iip_dkici_other').text('No Data');
         $('.view-more1').hide();
    }else{
        $('#count_iip_dkici_other').text(iipOther); 
          $('.view-more1').show();
    }
            
    var iipo_Other = $('#lbl_iip_po_other ol li').size();
    if(iipo_Other ==0){
        $('#count_iip_po_other').text('No Data');
        $('.view-more').hide();
    }else{
        $('#count_iip_po_other').text(iipo_Other);
         $('.view-more').show();
    }
            
    var gspo_Other = $('#lbl_gs_po_other ol li').size();
    if(gspo_Other == 0){
        $('#count_gs_po_other').text('No Data');
        $('.view-more2').hide();
    }else{
        $('#count_gs_po_other').text(gspo_Other); 
        $('.view-more2').show();
    }
                
    var gs_oicg_Other = $('#lbl_gs_oicg_other ol li').size();
    if(gs_oicg_Other == 0){
        $('#count_gs_oicg_other').text('No Data');
        $('.view-more3').hide();
    }else{
        $('#count_gs_oicg_other').text(gs_oicg_Other);  
        $('.view-more3').show();
    }
                
    var tplan = $('#trainingplan').val();
    if(tplan=='') {
        $('#count_trainingplan').text('No Data').show();
        $('.view-more4').hide();
    }else{ 
        $('.view-more4').show();
        $('#count_trainingplan').hide();
    }
                 
    var course = $('#lbl_tc_course_type li').size();
    if(course==0) {
        $('#count_tc_course_type').text('No Data').show();
        $('.view-more5').hide();
    }else{ 
        $('.view-more5').show();
        $('#count_tc_course_type').text(course);
    }
                 
    var extrExpert = $('#attractingexternalexperts').val();
    if(extrExpert=='') {
        $('#count_attractingexternalexperts').text('No Data').show();
        $('.view-more6').hide();
    }else{ 
        $('.view-more6').show();
        $('#count_attractingexternalexperts').hide();
    }
                 
    var eeplist = $('#eep_list').val();
    var eepsession = $('#eep_noofsessions').val();
    if(eeplist=='' && eepsession=='') {
        $('#count_eep_list').text('No Data').show();
        $('.view-more8').hide();
    }else{ 
        $('.view-more8').show();
        $('#count_eep_list').hide();
    }
      
    var actplan =$('#lbl_ac_type li').size();
    if(actplan=='0') {
        $('#count_ac_type').text('No Data').show();
        $('.view-more9').hide();
    }else{ 
        $('.view-more9').show();
        $('#count_ac_type').text(actplan).show();
    }
    /// update the dropdown label 
             
    $('input[type="text"]').each(function() {
        var id = $(this).attr('id');
        var idv =$(this).val();
        var lbl = '#lbls_'+ $(this).attr('id');
        var lblv = '#lblv_'+ $(this).attr('id');
        if( idv == ''){
            $(lbl).show().text('No Data');
             $(lblv).show().text('0');
        } 
        else {
            $(lbl).hide();
             $(lblv).hide();
        }   
    });
           
}
 
 


/** add new item to org tem  grid**/
   
    
/***************** FROM MAIN HTML ***************/
    function checkStatus(){
   
           if($('#ov_mai_sa').attr('checked')) {
                $('.ov_mai_sa').show();
            }else{
                $('.ov_mai_sa').hide();
           }
     
           if($('#ov_mai_ta').attr('checked')) {
                $('.ov_mai_ta').show();
            }else{
                $('.ov_mai_ta').hide();
           }
        
         if($('#ov_mai_fra').attr('checked')) {
                $('.ov_mai_fra').show();
            }else{
                $('.ov_mai_fra').hide();
           }
        
   }
   
   $(document).ready(function(){
        checkStatus();
            
        jQuery("input:file[class=fileParticipant]").unbind('change').bind('change', function(e) {            
//            jQuery('#form_upload_participants').submit();
var form = $(this).closest('form');
form.submit();
        });
   
        // get values on reloading of ifrmae 
        $(".target_participants").on("load", function () {
            var id = $(this).attr('id');
            var str  =  $('#'+id).contents().find('.data_upload_result').text();
            var myData  = $.parseJSON(str);
            var grid_id  = $(this).find('.div_grid').attr('id');   
            showMydataGrid(myData,grid_id);
            
        });
        // get values on reloading of ifrmae 
        $(".uplad_target_participants").on("load", function () {
            var id = $(this).closest('').attr('id');
            var str  =  $('#'+id).contents().find('.data_upload_result').text();
            var myData  = $.parseJSON(str);
            
            
        });
        
        
        /** add new item to advisor grid **/
        $(".addNew").click(function() {
            var target = $(this).attr('target');
            var d = grid[target].columns;
            var noCols = d.length;
            var nd = grid[target].data;
            var newdata=[];
            for(i=0 ; i< nd.length; i++ ) {
                var o={};
                for(j=0; j< noCols; j++) {
                    var col = d[j].name;
                    var val = nd[i].columns[j];
                    o[col] =  val;
                
                }
                newdata[i] = {
                    'id': i+1, 
                    'values' : o
                };
            }
            var _participant_grid = {};
    
            _participant_grid.data = newdata;
            _participant_grid.metadata = grid[target].columns;
        
            //add column to tdata
            var last = _participant_grid.data.length;
        
            if(target=='grid_project'){
                var newitem = {
                "id" : last+1,
                "values" : {

                        projecttitle:'',proponentfirstname:'',proponentlastname:'',proponenttelno:'',proponentemail:'',synopsis:'',whethershortlisted:'',finalselection:'',mentorname:'',pitchedtodstaicte:'',funded:''
                }
                 };
            
            }
            else if(target=='grid_panel'){
                var newitem = {
                "id" : last+1,
                "values" : {
                    pp_designation:'',pp_institutionororganisation:'',pp_firstname:'',pp_lastname:'',pp_title:'',pp_areaofexpertise:'',pp_dateofstartingaspanelist:'',pp_noofprojectsreviewed:'',pp_email:'',pp_phone:''

                }
                 };
            }
            else if(target=='grid_attendee'){
                var newitem = {
                "id" : last+1,
                "values" : {
                   td_category:'',td_role:'',td_firstname:'',td_lastname:'',td_areaofexpertise:'',td_dateofjoining:'',td_dateofleaving:'',td_email:'',td_phone:''
                }
                 };
            }
           
        
            _participant_grid.data[last] = newitem;
            grid[target].processData(_participant_grid, target);           
            //var output  = JSON.stringify(_participant_grid)
           
        });
        
		
        
   
    });
   $('#iedc-info-view,#iedc-contr-view,#iedc-parti-view').click(function(){     
       $('.p1,.p2,.p3').show();
    });
    $('#ov_mai_sa, #ov_mai_ta,#ov_mai_fra').click(function(){ 
        checkStatus();
    });
    
    function showMydataGrid(myData1,target){
          
            myData =  myData1;
            grid[target] = new EditableGrid(target); 
            
            grid[target].processData(myData,target );
          
            var temp = new Array();
            for(i=0;i<myData.length;i++){
                temp[i]  = myData[i].values.join(',');
            }
                    
            var output  = JSON.stringify(myData)
           
					
        }   
    
   function updateMydataGrid(myData,target){
        
        var jsonData = myData ;			
        
        grid[target].processData(myData,target );
      
					
    } 
   function radio_status(name){
	var val = $('input:checked[name="'+name+'"]').val();
        if(val=='undefined'){
            return 'No Selection';
        }
        else{
            return val;
        }

    
    }
    
    function check_data(data){
        if(data){
            return data;
        }
        else {
            return '<span> No Data</span>';
        }
    }
  function reset_form_element(e) {
      
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}  
   function get_download_link(data){
        if(data){
            var vals  = data.split('/');
            var link1= vals.pop();
            var link2= vals.pop();
            var link3= vals.pop();
            var link = link3+'/'+ link2 +'/' + link1 ;
            return link;
        }
    }


