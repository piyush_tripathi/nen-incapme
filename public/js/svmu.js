$(document).ready(function(){
    load_svmu();
    //update_form();
    $('#create_institute_svmu').click(function(){
        var form  = $(this).closest('form');
        var callback = 'result_create_institute_svmu';
        var url  = form.attr( 'action' );
        var data  = form.serialize();
        ajax_call(form,url,data,callback);
        
    }) 
    $('.btn_save_form').click(function(){      
        var form  = $(this).closest('.div_form').find(':input');
        var callback = 'load_svmu';
        var url  =  $(this).closest('.div_form').attr( 'action' );
        var id=$(this).attr('id');
        var valid = true;
         var msg = '';
        switch(id){
            case 'btn_save_one':
                $('#svmu_org').val(JSON.stringify(grid['grid_org'].data) );
                break;
            case 'btn_save_two':
               valid = validate_range('#prgm_noofapplicants',0,99) && validate_range('#prgm_totnoofmentors',0,999999) && validate_range('#prgm_totnoofmentees',0,999999);
               
                if( !validate_range('#prgm_noofapplicants',0,99)) {
                    msg+= '<li class="list-error"> Number must be between 0 and 99</li>';
                }
                if( !(validate_range('#prgm_totnoofmentors',0,999999) && validate_range('#prgm_totnoofmentees',0,999999))){
                    msg+= '<li class="list-error">Please enter positive number</li>';
                }
//                 if( !validate_range('#prgm_totnoofmentees',0,999999)){
//                    msg+= '<li class="list-error">Please enter positive number</li>';
//                }
              //  valid =  validate_range('#prgm_noofapplicants',0,99);
              //  valid =  validate_range("#prgm_totnoofmentors",0,99);
                 $('#svmu_mentee').val(JSON.stringify(grid['grid_mentee'].data) );
                 $('#svmu_mentor').val(JSON.stringify(grid['grid_mentor'].data) );
                break;
            case 'btn_save_three':
                valid =  validate_range('#vt_noofventure',0,99); 
                 msg+= '<li class="list-error"> Number must be between 0 and 99</li>';
                $('#svmu_venture').val(JSON.stringify(grid['grid_venture'].data) );
                break;
            default:
                break;
        }
        
        var data  = form.serialize();
        //$(form).submit();
        ////console.log(url);
        
        if(valid){
            ajax_call(form,url,data,callback);
            $('#svmu-info-view-hide, #svmu-contr-view-hide, #svmu-parti-view-hide,#svmu-outcome-view-hide').click();
            $('.p1,.p2,.p3,.p4').show();
             $('.alert-error').hide();
             $('#success_'+id).html('<div class="alert alert-success success-top"> <button class="close" data-dismiss="alert">×</button><i class="icon-back"></i><span class="error-msg1"><strong> Success! </strong> Record updated.</span></div>');
        }
        else{
          //  var message =  ' Number must be between 0 and 99';
             $('.alert-success').hide();
            $('#noti_'+id).html('<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-error"></i><span class="error-msg"> <strong>Error!</strong> '+msg+'</span></div>');
             
        }
        
    }) 
    
    $('#svmu-info-view, #svmu-contr-view, #svmu-parti-view,#svmu-outcome-view').click(function(){
        load_svmu();
    });
    
   
    $('#btn_publish_svmu').click(function(){
        $('#form_publish_svmu').submit();
        load_svmu();
    })
    $('#svmu-info-view').on('click', function() {
        $('.p1').show(); 
     
    });
    $('#svmu-act-info-view').trigger('click');
    
    $('#edit1').bind('click',function(){
        $('.p1').hide(); 
    });
    $('#svmu-contr-view').on('click', function() {
        $('.p2').show();
    //            $(this).closest('form').each (function(){
    //  this.reset();
    //});
    });
    $('#svmu-act-contr-view').trigger('click');
    
    $('#edit2').bind('click',function(){
        $('.p2').hide(); 
    });
    $('#svmu-parti-view').on('click', function() {
        $('.p3').show();
    //            $(this).closest('form').each (function(){
    //  this.reset();
    //});
    });
    $('#svmu-act-parti-view').trigger('click');
    
    $('#edit3').bind('click',function(){
        $('.p3').hide(); 
    });
    $('#svmu-outcome-view').on('click', function() {
        $('.p4').show();
    }); 
    $('#submitactivitycontributor').on('click', function() {
        var val = $('#s2id_gs_targeted_level span').text();
        $('#tab3 .title-overview').text(val);
        $('.p2').show();
    }); 
    
    // for review fourth form 
    $('#vt_menteefeedreviewed_n,#vt_menteefeedreviewed_y').click(function(){
            if(this.id =='vt_menteefeedreviewed_n'){
               
                $('.mrevirw').hide();
            }
            else{

                 $('.mrevirw').show();
            }
    });
    
    
   
   
      
});
function result_create_institute_svmu(data){
    
    if(data['success']){
        window.location.href = data['url'];
    }
    else{
        alert('error happpend');
    }
}

function ajax_call(source,url,data,callback){
    
    var crnturl = url;
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        timeout: 3000,
        beforeSend: function() {
            $('a.ajax-loader').trigger('click');
        //source.append('<div class="ajax_loader">Loading........... </div>');
        },
        complete: function() {
            $('button.ajax-close').trigger('click');
          
        },          
        cache: false,
        success: function(result) {
            $('button.ajax-close').trigger('click');
                console.log(result);
                if(callback=='result_create_institute_svmu')
                {
                    var crntdata = new Array();
                    var svmuid= result.svmu_id;
                    var strurl='/svmu/edit?id='+svmuid+'&'+crnturl.substring(10);
                    
                    crntdata['url'] = strurl;
                    crntdata['success']= 'success';
                    
                    result_create_institute_svmu(crntdata);
                }
                else{
               
                var fn = window[callback];
                fn(result);
                } 
        },
        error: function(error) {
            $('button.ajax-close').trigger('click');
          
        //alert("Some problems have occured. Please try again later: " );
        ////console.log(error.responseText);
        }
    });


}

function load_svmu(){
    var source  = $('#tab2');
    var callback = 'load_svmu_result';
    var url  = '/svmu/getsvmudata/';
    var data  = {
        'svmu_id':getParameter('id')
        };
    //////console.log(data);
    var type = $('#svmu_type').val();
    if(type!='create'){
        ajax_call(source,url,data,callback);
    }
       
}

function load_svmu_result(result){
    if(!result){
        return false;
    }
    
    var data = result['data_svmu'];
    var data_grid = result['data_grid'];
    ////console.log(result);
    
    $('input[type!="hidden"],select,textarea').each(function() {
        //////console.log(this.name); 
    
        if(this.name=='prgmfinalised[]'){
            var index = this.name.replace("[","");
            index = index.replace("]","");
            
            var value = data[index];
           
           
          
            if(value){
             var htm = multi_htm(value,this.name);
            //////console.log(htm);
            var text = multi_lbl(value);
             $('#div_'+index).html(htm);
                $('#lbl_'+index).html(text);
                $('#count_'+index).html($('#lbl_'+index+' li').size());
                $('.view-more6').show();
            }
            else{
                 
                $('#count_'+index).html('No Data');
                $('.view-more6').hide();
            }
            
            
            
               
            
        }
        else if(this.name=='vt_menteefeedreviewed'){
            if(data[this.name] =='0'){
                $("#vt_menteefeedreviewed_n").parent().addClass('checked');
                  $("#vt_menteefeedreviewed_n").attr('checked',true);
                $('.mrevirw').hide();
            }
            else{
                 $("#vt_menteefeedreviewed_y").parent().addClass('checked');
                 $("#vt_menteefeedreviewed_y").attr('checked',true);
                 $('.mrevirw').show();
            }
        }
        else if($(this).attr('type')=='button'){
            //alert('button');
        }
        else  if($(this).is('input:checkbox')){
            var val = data[this.name];
            if(!val){
                val = 0;
            }
            var htm = chk_htm(this.name,val);
            var text = chk_lbl(this.name,val);
            
        }
        else if($(this).is('select')){
            var val = data[this.name];
            if(val){
                dd_htm(this.name,val);
                dd_lbl(this.name,val);
            }
        }
        else if($(this).is('textarea')){
            var val = data[this.name];
            if(!val){
                val = '';
                $(this).val(val);
                var id = $(this).attr('name');
                $('#count_'+id).text('No Data');
                $('.view-more7').hide();
                
            }
            else{
                $(this).val(val);
                var id = $(this).attr('name');
                $('#lbl_'+id).text(val);
                $('.view-more7').show();
                $('#count_'+id).text('');
            }
            
        }
        
        else{            
            var val = data[this.name] ;            
            if(!val){
                val = '';
            }
            $(this).attr('value',val) ;            
            $('#lbl_'+this.name).text(val) ;
        }
    });
  
   
   
    //   NOW UPDATING DATA GRIDS 
        var participantData;          
        grid  = new Array();
        showMydataGrid(data_grid['grid_org'],'grid_org');
        showMydataGrid(data_grid['grid_mentee'],'grid_mentee');
        showMydataGrid(data_grid['grid_mentor'],'grid_mentor');
        showMydataGrid(data_grid['grid_venture'],'grid_venture');
        
        update_label_values(result);
        
    // now updating conditional element visibility 
    svmuCheckStatus();
    //now updating the notification status 
    

}
function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
    i, val, params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}


function update_label_values(result){
    //for apprived infra
    
    var data = result['data_svmu'];   
    var grid = result['data_grid'];
    var htm='';
    var count = 0;
    if($('#ov_mai_sa').attr('checked')=='checked'){
       htm+= '<li>Space Allocated : '+$('#ov_mai_savalue').val()+'</li>';
       count++;
    }
     if($('#ov_mai_ta').attr('checked')=='checked'){
       htm+= '<li>Time Allocated : '+$('#ov_mai_tsvalue').val()+'</li>';
       count++;
    }
     if($('#ov_mai_fra').attr('checked')=='checked'){
        htm+= '<li>Faculty Resource Allocated : '+$('#ov_mai_fravalue').val()+'</li>';
        count++;
    }
    if(count==0){
        $('#count_ov_mai').text('No Data'); 
        $('.view-more').hide();
    }
    else{
         $('#count_ov_mai').text(count);
         $('.view-more').show(); 
    }
   
    $('#lbl_ov_mai').html('<ol>'+htm+'</ol>');
    //guest list 
    if(grid['grid_org']['data'].length ==0 ){ 
         $('#count_svmu_ov_guestlist').text('No Data');
    }
    else{
        $('#count_svmu_ov_guestlist').text(grid['grid_org']['data'].length);
    }
    
    //lead faculty 
    $('#lbl_ov_leadfaculty').html($('#ov_leadfaculty option:selected').text());
    //date of launch
    if($('#ov_dateoflaunch').val()){
        $('#lbl_ov_dateoflaunch').html($('#ov_dateoflaunch').val());
    }
    else {
        $('#lbl_ov_dateoflaunch').html('<span> No Data</span>');
    }
     
     //org team 
     if(data['ov_organisingteam']){
         $('#lbl_ov_organisingteam').html('<a href="/'+get_download_link(data['ov_organisingteam'])+'"> <i class="icon-download-alt"></i> Download </a>');
     }
     else{
         $('#lbl_ov_organisingteam').html('No File');
     }
      
      //schedule
//        htm+=' <li>'+val+':<span id="val_schedule"></span></li>';
//                    break;
//                case 'Marketing-Plan':
//                    htm+=' <li>'+val+':<span id="val_marketing-plan"></span></li>';
//                    break;
//                case 'Application-Process':
//                   htm+=' <li>'+val+':<span id="val_application-process"></span></li>' prgm_bdma;
       if(data['prgm_uploadschedule']){
         $('#val_schedule').html('<a href="/'+get_download_link(data['prgm_uploadschedule'])+'"> <i class="icon-download-alt"></i> Download </a>');
     }
     else{
         $('#val_schedule').html('No File');
     }
     if(data['prgm_bdma']){
         $('#val_marketing-plan').html('<span>'+data['prgm_bdma']+' </span>');
         
     }
     else{
         $('#val_marketing-plan').html('No Data');
     }
      
      //prgm_uploadapplform
       if(data['prgm_uploadapplform']){
         $('#val_application-process').html('<a href="/'+get_download_link(data['prgm_uploadapplform'])+'"> <i class="icon-download-alt"></i> Download </a>');
     }
     else{
         $('#val_application-process').html('No File');
     }
      
      
      //mentee feedback 
      var feedback = $('input:radio[name="vt_menteefeedreviewed"]:checked').val();
      if(feedback=='0'){
          $('#lbl_vt_menteefeedreviewed').html('<span>No</span>');
      }
      else if(feedback==1){
          $('#lbl_vt_menteefeedreviewed').html('<span>Yes</span>');
      }
      else{
           $('#lbl_vt_menteefeedreviewed').html('<span> No Data</span>');
      }
      
      //venture report 
       if(data['vt_uploadreport']){
          $('#lbl_vt_uploadreport').html('<a href="/'+get_download_link(data['vt_uploadreport'])+'"><i class="icon-download-alt"></i> Download </a>');
     }
     else{
          $('#lbl_vt_uploadreport').html('No File');
     }
     // no of applicants
     
      if($('#prgm_noofapplicants').val()){
          $('#lbl_prgm_noofapplicants').html($('#prgm_noofapplicants').val());
      }
      else{
          $('#lbl_prgm_noofapplicants').html('<span> No Data</span>');
      }
      //mentor label 
      if($('#prgm_totnoofmentors').val()){
          $('#lbl_prgm_totnoofmentors').html($('#prgm_totnoofmentors').val());
      }
      else{
          $('#lbl_prgm_totnoofmentors').html('<span> No Data</span>');
      }
      //mentor label 
      if($('#prgm_totnoofmentees').val()){
          $('#lbl_prgm_totnoofmentees').html($('#prgm_totnoofmentees').val());
      }
      else{
          $('#lbl_prgm_totnoofmentees').html('<span> No Data</span>');
      }
       //venture label 
      if($('#vt_noofventure').val()){
          $('#lbl_vt_noofventure').html($('#vt_noofventure').val());
      }
      else{
          $('#lbl_vt_noofventure').html('<span> No Data</span>');
      }
       //label last review
      if($('#vt_dateoflastreview').val()){
          $('#lbl_vt_dateoflastreview').html($('#vt_dateoflastreview').val());
      }
      else{
          $('#lbl_vt_dateoflastreview').html('<span> No Data</span>');
      }
      //label last review
      if($('#vt_dateoflastreport').val()){
          $('#lbl_vt_dateoflastreport').html($('#vt_dateoflastreport').val());
      }
      else{
          $('#lbl_vt_dateoflastreport').html('<span> No Data</span>');
      }
      // for label radio box
      
     
}

function multi_htm(data,source){
    if(data==null){
        return true;
    }
   var vals  = data.split(','); 
  
   $('input[name="prgmfinalised[]"]').each(function(){
       
       if($.inArray($(this).attr('value'),vals)!= -1){
           $(this).attr('checked','checked');
       }
       else{
           $(this).removeAttr('checked');
       }
   })
   
}

function multi_lbl(data){
    
    if(!data){
        return '<ol></ol>';
    }
    var htm = '<ol>';
    var vals  = data.split(',');
    $(vals).each(function(key,val){
        if(val!=='null'){
            switch(val){
                case 'Schedule':                    
                    htm+=' <li>'+val+':<span id="val_schedule"></span></li>';
                    break;
                case 'Marketing-Plan':
                    htm+=' <li>'+val+':<span id="val_marketing-plan"></span></li>';
                    break;
                case 'Application-Process':
                   htm+=' <li>'+val+':<span id="val_application-process"></span></li>';
                   break;
                default:
                    htm+=' <li>'+val+'</li>';
                    break;
            }
        }
        
    });
    htm+= '</ol>';
    return htm;
    
}
function dd_htm(source,data){
    
    $('#'+source+' option').each(function(i){
        
        if( $(this).val()==data){
           
            $(this).attr('selected', true);
            var bt_id  = '#s2id_'+ source ;
            $(bt_id+ ' span' ).text($(this).text());
        }
      
    });
}

function dd_lbl(){
    
}

function chk_htm(source,data){
    
    if(data == 1){
        $('#'+source).attr('checked','true');
         $('.'+source).show();
      
    }
    else{
        $('#'+source).removeAttr("checked");
         $('.'+source).hide();
    }
}
function chk_lbl(source,data){
    if(data == 1 ){
        $('#lbl_'+source).text('Yes');
        $('.lbl_'+ source).show();
    }
    else{
        $('#lbl_'+source).text('No');
        $('.lbl_'+ source).hide();
    }
}



//script for page elements 

   
$(document).ready(function() {
	
    $('.remove').live('click', function(){
        $(this).parent().remove();
    });
				
    $('.addnew').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addnew').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
                
    $('.addprogram').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addprogram').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
                
    $('.addgoal').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        //            newbox.find('input:not(.add)').val("");

        $(this).removeClass('addgoal').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
                
    $('.addcapacity').live('click', function(){
        var thisbox = $(this).parent();
        newbox = thisbox.clone(true).insertBefore(thisbox);
        newbox.find('input:not(.add)').val("");

        $(this).removeClass('addcapacity').addClass('remove');
        $(this).children().addClass('icon-minus').removeClass('icon-plus');
    //            newbox.find('input.increment').val(parseInt(thisbox.find('input.increment').val())+1);
    });
        
   
        

        
        
    

});

 
function db_to_date(dateStr)
{
    dArr = dateStr.split("-");  // ex input "2010-01-18"
    if(!dArr[2]){
        return dateStr;
    }
    else{
        return dArr[2]+ "-" +dArr[1]+ "-" +dArr[0].substring(2); //ex out: "18/01/10"
    }
}

 
 
 function update_label_number(){
    $('.view-less,.view-less1,.view-less2,.view-less3,.view-less4,.view-less5,.view-less6,.view-less7,.view-less8, .view-less9').hide();
    $('.hidden-block,.hidden-block1, .hidden-block2,.hidden-block3,.hidden-block4,.hidden-block5,.hidden-block6,.hidden-block7,.hidden-block8, .hidden-block9').hide();   
    var iipOther = $('#lbl_iip_dkici_other ol li').size();
    if(iipOther==0){
        $('#count_iip_dkici_other').text('No Data');
         $('.view-more1').hide();
    }else{
        $('#count_iip_dkici_other').text(iipOther); 
          $('.view-more1').show();
    }
            
    var iipo_Other = $('#lbl_iip_po_other ol li').size();
    if(iipo_Other ==0){
        $('#count_iip_po_other').text('No Data');
        $('.view-more').hide();
    }else{
        $('#count_iip_po_other').text(iipo_Other);
         $('.view-more').show();
    }
            
    var gspo_Other = $('#lbl_gs_po_other ol li').size();
    if(gspo_Other == 0){
        $('#count_gs_po_other').text('No Data');
        $('.view-more2').hide();
    }else{
        $('#count_gs_po_other').text(gspo_Other); 
        $('.view-more2').show();
    }
                
    var gs_oicg_Other = $('#lbl_gs_oicg_other ol li').size();
    if(gs_oicg_Other == 0){
        $('#count_gs_oicg_other').text('No Data');
        $('.view-more3').hide();
    }else{
        $('#count_gs_oicg_other').text(gs_oicg_Other);  
        $('.view-more3').show();
    }
                
    var tplan = $('#trainingplan').val();
    if(tplan=='') {
        $('#count_trainingplan').text('No Data').show();
        $('.view-more4').hide();
    }else{ 
        $('.view-more4').show();
        $('#count_trainingplan').hide();
    }
                 
    var course = $('#lbl_tc_course_type li').size();
    if(course==0) {
        $('#count_tc_course_type').text('No Data').show();
        $('.view-more5').hide();
    }else{ 
        $('.view-more5').show();
        $('#count_tc_course_type').text(course);
    }
                 
    var extrExpert = $('#attractingexternalexperts').val();
    if(extrExpert=='') {
        $('#count_attractingexternalexperts').text('No Data').show();
        $('.view-more6').hide();
    }else{ 
        $('.view-more6').show();
        $('#count_attractingexternalexperts').hide();
    }
                 
    var eeplist = $('#eep_list').val();
    var eepsession = $('#eep_noofsessions').val();
    if(eeplist=='' && eepsession=='') {
        $('#count_eep_list').text('No Data').show();
        $('.view-more8').hide();
    }else{ 
        $('.view-more8').show();
        $('#count_eep_list').hide();
    }
      
    var actplan =$('#lbl_ac_type li').size();
    if(actplan=='0') {
        $('#count_ac_type').text('No Data').show();
        $('.view-more9').hide();
    }else{ 
        $('.view-more9').show();
        $('#count_ac_type').text(actplan).show();
    }
    /// update the dropdown label 
    // for tc_course 
    //            for infrstructure milestones ecellmilestones_date
    var ecell = '<li><b>E-CELL : </b>' + $('#ecellmilestones_dropdown option:selected').text() +'<b>&nbsp;&nbsp; Date : </b>'+$('#ecellmilestones_date').val()
    +'&nbsp; &nbsp;<b>Commentary : </b>' +$('textarea[name="ecellmilestones_otheractioncomments"]').val() + '</li>' ;
            
    var edc = '<li><b>EDC : </b>' + $('#edcmilestones_dropdown option:selected').text() +'<b>&nbsp;&nbsp; Date : </b>'+$('#edcmilestones_date').val()
    +'&nbsp; &nbsp;<b>Commentary : </b>' +$('textarea[name="edcmilestones_otheractioncomments"]').val() + '</li>' ;
            
    var IEDC = '<li><b>IEDC : </b>' + $('#iedcesc_dropdown option:selected').text() +'<b>&nbsp;&nbsp; Date : </b>'+$('#iedcesc_date').val()
    +'&nbsp; &nbsp;<b>Commentary : </b>' +$('textarea[name="iedcesc_otheractioncomments"]').val() + '</li>' ;
    var mu = '<li><b>MU : </b>' + $('#svmu_dropdown option:selected').text() +'<b>&nbsp;&nbsp; Date : </b>'+$('#svmu_date').val()
    +'&nbsp; &nbsp;<b>Commentary : </b>' +$('textarea[name="svmu_otheractioncomments"]').val() + '</li>' ;
    var incu = '<li><b>INCUB : </b>' + $('#incubator_dropdown option:selected').text() +'<b>&nbsp;&nbsp; Date : </b>'+$('#incubator_date').val()
    +'&nbsp; &nbsp;<b>Commentary : </b>' +$('textarea[name="incubator_otheractioncomments"]').val() + '</li>' ;
    //            var edc = '<li>EDC' + $('#edcmilestones_dropdown option:selected').text() +':'+$('#edcmilestones_date').val()
    //                +':' +$('textarea[name="edcmilestones_otheractioncomments"]').val() + '</li>' ;
    var htm  = '<ol>'+ecell+edc+IEDC+mu+incu+'</ol>';
    $('#lbl_ecellmilestones_dropdown').html(htm);
           
    var milestone =$('#lbl_ecellmilestones_dropdown li').size();
    if(milestone==0) {
        $('#count_ecellmilestones_dropdown').text('No Data').show();
        $('.view-more7').hide();
    }else{ 
        $('.view-more7').show();
        $('#count_ecellmilestones_dropdown').text(milestone).show(); 
    }
                 
    $('input[type="text"]').each(function() {
        var id = $(this).attr('id');
        var idv =$(this).val();
        var lbl = '#lbls_'+ $(this).attr('id');
        var lblv = '#lblv_'+ $(this).attr('id');
        if( idv == ''){
            $(lbl).show().text('No Data');
             $(lblv).show().text('0');
        } 
        else {
            $(lbl).hide();
             $(lblv).hide();
        }   
    });
           
}
 
 


/** add new item to org tem  grid**/
   
    
/***************** FROM MAIN HTML ***************/
    function checkStatus(){
   
           if($('#ov_mai_sa').attr('checked')) {
                $('.ov_mai_sa').show();
            }else{
                $('.ov_mai_sa').hide();
           }
     
           if($('#ov_mai_ta').attr('checked')) {
                $('.ov_mai_ta').show();
            }else{
                $('.ov_mai_ta').hide();
           }
        
         if($('#ov_mai_fra').attr('checked')) {
                $('.ov_mai_fra').show();
            }else{
                $('.ov_mai_fra').hide();
           }
        
   }
   
   $(document).ready(function(){
        checkStatus();
            
        jQuery("input:file[class=fileParticipant]").unbind('change').bind('change', function(e) {            
//            jQuery('#form_upload_participants').submit();
var form = $(this).closest('form');
form.submit();
        });
   
        // get values on reloading of ifrmae 
        $(".target_participants").on("load", function () {
            var id = $(this).attr('id');
            var str  =  $('#'+id).contents().find('.data_upload_result').text();
            var myData  = $.parseJSON(str);
            var grid_id  = $(this).find('.div_grid').attr('id');   
            showMydataGrid(myData,grid_id);
            
        });
        // get values on reloading of ifrmae 
        $(".uplad_target_participants").on("load", function () {
            var id = $(this).closest('').attr('id');
            var str  =  $('#'+id).contents().find('.data_upload_result').text();
            var myData  = $.parseJSON(str);
            
            
        });
        
        
        /** add new item to advisor grid **/
        $(".addNew").click(function() {
            var target = $(this).attr('target');
            var d = grid[target].columns;
            var noCols = d.length;
            var nd = grid[target].data;
            var newdata=[];
            for(i=0 ; i< nd.length; i++ ) {
                var o={};
                for(j=0; j< noCols; j++) {
                    var col = d[j].name;
                    var val = nd[i].columns[j];
                    o[col] =  val;
                
                }
                newdata[i] = {
                    'id': i+1, 
                    'values' : o
                };
            }
            var _participant_grid = {};
    
            _participant_grid.data = newdata;
            _participant_grid.metadata = grid[target].columns;
        
            //add column to tdata
            var last = _participant_grid.data.length;
        
            if(target=='grid_org'){
                var newitem = {
                "id" : last+1,
                "values" : {

                        designation:'',instororg:'',firstname:'',lastname:'',title:'',areaofexpr:'',dostrtasmntr:'',noofmenteesthisinst:'',email:'',phone:''
                }
                 };
            
            }
            else if(target=='grid_mentee'){
                var newitem = {
                "id" : last+1,
                "values" : {
                    firstname:'', lastname:'', institution:'', programme:'', yrofcompl:'', email:'', phone:'', businessidea:'', mentoremail:'', mentorphone:'', mentorfirstname:'', mentorlastname:''
                }
                 };
            }
            else if(target=='grid_mentor'){
                var newitem = {
                "id" : last+1,
                "values" : {
                   designation:'',instororg:'',firstname:'',lastname:'',title:'',areaofexpr:'',dostrtasmntr:'',noofmenteesthisinst:'',email:'',phone:''
                }
                 };
            }
            else if(target=='grid_venture'){
                var newitem = {
                "id" : last+1,
                "values" : {
                   venturename:'', areaofbusiness:'', targetstartupdate:'', menteefirstname:'', menteelastname:'', mentorfirstname:'', mentorlastname:'', totmntrngmeets:'', dateoflastmeet:'', mntesltstfdbk:'', mntrsltstfdbk:''
                }
                 };
            }
        
            _participant_grid.data[last] = newitem;
            grid[target].processData(_participant_grid, target);           
            //var output  = JSON.stringify(_participant_grid)
           
        });
        
		
        
   
    });
   $('#svmu-info-view,#svmu-contr-view,#svmu-parti-view').click(function(){     
       $('.p1,.p2,.p3').show();
    });
    $('#ov_mai_sa, #ov_mai_ta,#ov_mai_fra').click(function(){ 
        checkStatus();
    });
    
    function showMydataGrid(myData1,target){
          
            myData =  myData1;
            grid[target] = new EditableGrid(target); 
            
            grid[target].processData(myData,target );
          
            var temp = new Array();
            for(i=0;i<myData.length;i++){
                temp[i]  = myData[i].values.join(',');
            }
                    
            var output  = JSON.stringify(myData)
           
					
        }   
    
   function updateMydataGrid(myData,target){
        
        var jsonData = myData ;			
        
        grid[target].processData(myData,target );
      
					
    } 
function get_download_link(data){
        if(data){
            var vals  = data.split('/');
            var link1= vals.pop();
            var link2= vals.pop();
            var link3= vals.pop();
            var link = link3+'/'+ link2 +'/' + link1 ;
            return link;
        }
    }
    
   
       