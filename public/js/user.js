$(document).ready(function(){
    load_user();
    
    //checking for adding or editing part  by Pandey ji
    try { 
        var action = document.location.href.split('/')[4].substr(0,3);
    }
    catch(err){
        $('.add-user').hide();
        $('.save-user').show();
        $('#user-title').text('(Edit)');
    }  //getting the last part of url such as user/add
    if(action=='add'){
        $('.add-user').show();
        $('.save-user').hide();
        $('#user-title').text('(Add)');
        $('.required_user_element').show();
        
    }else{
        $('.add-user').hide();
        $('.save-user').show();
        $('#user-title').text('(Edit)');
        $('.required_user_element').hide();
        $('.not-required').hide();
    }
    //update_form();
    $('#create_institute_user').click(function(){
        var form  = $('form');
        var callback = 'result_create_institute_user';
        var url  = form.attr( 'action' );
        var data  = form.serialize();
        
        ajax_call(form,url,data,callback);
        
    }) 
    $('#btn_publish_user').click(function(){
        var form  = $(this).closest('.div_form').find(':input');
        var callback = 'load_user';
        var url  =  '/user/publish/';       
        var data  = {
            'user_id':$('input[name="user_id"]').val()
        };
        //$(form).submit();
        //// //console.log(url);
        //// //console.log(form);
        alert(url+':'+data);
        ajax_call(form,url,data,callback);
    });
    $('#save_user_details,#add_user_details').click(function(){      
        var form  = $(this).closest('.div_form').find(':input');
        var callback = 'user_details';
        
        if(this.id=='save_user_details'){
            var param = getParameter('id');
            var url  =  '/user/edit?id='+param;
            
        }else if(this.id=='add_user_details'){
            var url  =  '/user/add';
          
            var operation = 'add'; 
        }
        
        var data  = form.serialize();
        var valid = true;
        var msg = '';
        var fname =$('#fname').val();
        var email =$('#email').val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var pwd = $('#pwd').val();
        var repwd = $('#re-pwd').val();
        var opt = $('#user_role').find(":selected").text(); 
        var selectInst = $('#con_institute').find(":selected").text(); 
        if( fname==""){
            valid = false;
            $('#fname').focus().css("border","1px solid red");
            msg+='<li class="list-error"> First name is required</li>';
        }else{
              $('#fname').focus().css("border","1px solid #ccc");
        }
           if(email==""){
                valid = false;
                $('#email').focus().css("border","1px solid red");
                msg+='<li class="list-error">Please provide an email address</li>';
            }
            else if(!filter.test(email)){
                valid = false;
                $('#email').focus().css("border","1px solid red");
                msg+='<li class="list-error">Please provide valid email address</li>';
            }else{
                 $('#email').focus().css("border","1px solid #ccc");
            }
        
        if((pwd=="" || repwd=="")&& operation=='add'){
            valid = false;
            $('#pwd,#re-pwd').focus().css("border","1px solid red");
            msg+='<li class="list-error">Password can not be blank</li>';
        }
        else if(pwd.lenght<5){
            valid = false;
            $('#pwd').focus().css("border","1px solid red");
            msg+='<li class="list-error">Minimum password length is of 6 characters</li>';
        }
        else if(pwd!=repwd){
            valid = false;
            $('#re-pwd').focus().css("border","1px solid red");
            msg+='<li class="list-error">Passwords do not match</li>';
        }else{
            $('#pwd,#re-pwd').focus().css("border","1px solid #ccc");
        }
       
        if(opt=="Select Role"){
            valid = false;
            $('#s2id_user_role').focus().css({
                "border":"1px solid red", 
                "border-radius":"5px"
            });
            msg+='<li class="list-error">Please select user role</li>';
        }else{
                 $('#s2id_user_role').focus().css("border","none");
            }
           if (opt=="Consultant") {
                if(selectInst==""){
                 valid = false;
                 $('#s2id_con_institute').focus().css({
                     "border":"1px solid red", 
                     "border-radius":"5px"
                 });
                 msg+='<li class="list-error">Please select institute</li>';
                }else{
                      $('#s2id_con_institute').focus().css("border","1px solid #ccc");
                 }
           }
        if(valid){
            ajax_call(form,url,data,callback);
        }else{
            $('.alert-success').hide();
            $('#noti_user').html('<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-error"></i> \n\
                <span class="error-msg">\n\
            <strong>Error!</strong> '+ msg + '</span></div>');
             
        }
         
    }) ;
    //deleting user   
    $('#delete_user').click(function(){      
        $('#div_delete_user').show();
    }) ;
    $('.btn_cancel').click(function(){      
        $('#div_delete_user').hide();
    }) 
    
    

});
 
function result_create_institute_user(data){
    
    if(data['success']){
        window.location.href = data['url'];
    }
    else{
        alert('error happpend');
    }
}
    
function ajax_call(source,url,data,callback){

    var crnturl = url;
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        timeout: 3000,
        beforeSend: function() {
            //console.log(url);
            //console.log(data);
            //console.log(source);
            $('a.ajax-loader').trigger('click');
        },
        complete: function() {
            $('button.ajax-close').trigger('click');
          
        },          
        cache: false,
        success: function(result) {
            console.log(result);
            $('button.ajax-close').trigger('click');             
               
            var fn = window[callback];
            fn(result);
        //                }
                 
        },
        error: function(error) {
            $('button.ajax-close').trigger('click');
          
        //alert("Some problems have occured. Please try again later: " );
        //console.log(error);
        }
    });


}
function user_details(data){
      
    //update notfication 
    //   $('#noti_user').html('<p>Status:'+ data.status +' Data:'+data.data+'</p>');
    if(data){
        
        if(data.status){
            var htm = '<div class="alert alert-success"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-success"></i> <span class="success-msg"> <strong>Success!</strong> '+data.data+'</span></div>' ;
        }
        else{
            var htm = '<div class="alert alert-error"> <button class="close" data-dismiss="alert">×</button><i class="icon-back-error"></i> <span class="error-msg"> <strong>Error!</strong> '+data.data+'</span></div>' ;
        }
        $('#noti_user').html(htm);
    }
}
function load_user(){
    var source  = $('#tab2');
    var callback = 'load_user_result';
    var url  = '/user/getuserdata/';
    var data  = {
        'user_id':getParameter('id'),
        'institute_id':getParameter('instituteid')
           
    };
    
    
   
    var type = $('#user_type').val();
    if(type!='create'){
        ajax_call(source,url,data,callback);
    }
    
   
       
}

function load_user_result(data){
    if(!data){
        return false;
    }
    //   return true;
    update_elements(data);
       
    
}
function getParameter(paramName) {
    var searchString = window.location.search.substring(1),
    i, val, params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return unescape(val[1]);
        }
    }
    return null;
}



//script for page elements 

   
$(document).ready(function() {
	
    $('.remove-course').live('click',function(){
        $(this).parent().parent().remove();
    });
    
     
        
});

 

function  update_elements(data){
    if(!data){
        return false;
    }
        
   
        
    //now prepoulate the user details form 
    $('input:[name="user_first_name"]').val(data['user_data'].first_name);
    $('input:[name="user_last_name"]').val(data['user_data'].last_name);
    $('input:[name="user_email"]').val(data['user_data'].email);
        
    if(data['user_data'].category=='FAC'){              
        dd_htm('user_role','FAC');
            
    }
    else if(data['user_data'].category=='ADMIN'){
        dd_htm('user_role','ADMIN');
    }
    
    else if(data['user_data'].category=='CON'){
        dd_htm('user_role','CON');
    }
    
    if(data['user_data']['is_lead_faculty']){
        dd_htm('user_role','LEAD_FACULTY');
    }
    //show institute list only for the admin
        
    //if user is consultant show multiple dropdown 
    if(data['user_data'].category=='CON'){                 
        var selected_institute  = data['user_data'].con_institute;        
        
    }
    con_institute_list(data['admin_institute'],selected_institute);
    //if normal user show normal dropdown 
    if(data['user_data'].category=='FAC'){
        var selected_institute  = data['user_data']['user_institute'][0].id;
    }
    fac_institute_list(data['admin_institute'],selected_institute)
        
    $('.insti_dropdown').select2({
        placeholder: "Select Institute"
    });
        
    //show hide dropdowns 
    if(data['user_data'].category=='CON'){        
    
    }
    roleSelect();
    //if user is requesting the data and he is not admin , restrict access
    if(!data['is_admin']){
        disable_user_element();
    }
    return true;
}
 
 
function fac_institute_list(data,selected){
    if(!data){
        return ''; 
    }
    if(!selected){
        selected = '';
    }
    
    var htm = ' <select name="user_institute" class="insti_dropdown">';
    $.each(data,function(key,val){
        var flag  = (val['id']== selected) ? 'selected' :'' ;
        htm += '<option value="'+val['id']+'"  '+flag+' >'+val['name']+' </option>';  
    })
    htm+=' </select>';
    $('#div_institute').html(htm);
    //        $('.insti_dropdown').hide();
    $('#div_institute').show();
    return true;
}
function con_institute_list(data,selected){
    if(!data){
        return ''; 
    }
    if(!selected){
        selected = '';
    }  
      
    var htm = ' <select name="con_institute[]" id="con_institute" class="insti_dropdown" multiple>';
    var flag  =  '' ;
    var selected_insti = new Array();
    //get selected institutes in an array 
    $.each(selected,function(k,v){
        selected_insti.push(v['id']);
    });
        
    $.each(data,function(key,val){
        if($.inArray(val['id'], selected_insti) != -1 ){
            flag  =  'selected="selected"' ;
        }else{
            flag = '';
        }
        htm += '<option value="'+val['id']+'"  '+flag+' >'+val['name']+' </option>';  
    })
    htm+=' </select>';
        
    $('#div_con_institute').html(htm);
        
    //        $('.insti_dropdown').hide();
    $('#div_con_institute').show();
       
    return true;
}
    
function dd_htm(source,data){
    
    $('#'+source+' option').each(function(i){
        
        if( $(this).val()==data){
           
            $(this).attr('selected', true);
            var bt_id  = '#s2id_'+ source ;
            $(bt_id+ ' span' ).text($(this).text());
        }
      
    });
}
//select on change
$(document).ready(function(){
    $('#breadcrumb,#content-header').hide();
    $('#user_role').change(function(){
        roleSelect();    
    });
});


function roleSelect(){
    var opt = $('#user_role').find(":selected").text(); 
    if(opt=="Faculty" || opt=="Lead Faculty"){
        $('#single-inst').show();
        $('#multi-inst').hide();
    }else if(opt=="Consultant"){
        $('#single-inst').hide();
        $('#multi-inst').show();
    }else{
        $('#single-inst').hide();
        $('#multi-inst').hide();
    }
}

//hide user element

function disable_user_element(){
//    return true;
    document.getElementById('email').disabled=true;//email disable
    var u = document.getElementById("user_role");//select role
    var userRole = u.options[u.selectedIndex].text;
    document.getElementById('s2id_user_role').style.display = 'none';
    document.getElementById('div_con_institute').style.display = 'none';
    document.getElementById('role').style.display = 'block';
    document.getElementById('inst-list-consultant').style.display = 'block';
    document.getElementById('role').innerHTML = userRole;
    document.getElementById('select-inst').innerHTML = "Institute";
    document.getElementById('select-insts').innerHTML = "Institutes";
    document.getElementById('div_institute').style.display = 'none';
    var inst = $("#s2id_autogen1").text();
    document.getElementById('div_institute-show').innerHTML= inst;
    var htm='';
    $('#con_institute option:selected').each(function(){   
        var text=$(this).text();
        htm+= '<li> '+text+'  </li>';
            
    });
    $('#div_con_institute-show').html(htm);
}