<?php
class ActivityController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper( 'FlashMessenger' );
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_Activity();
    }
    /* default listing for activity page   */
    public function indexAction( )
    {
        $data                     = $this->mdl->getList( $this->institute_id );
        $this->view->data         = $data;
        $this->view->institute_id = $this->institute_id;
        echo $this->view->render( 'activity/index.phtml' );
    }
    /* Action:deleting an activity   */
    public function deleteAction( )
    {
        $output     = array( );
        $activityId = $this->_request->getPost( 'activity_id' );
        if ( $activityId ) {
            $output             = $this->mdl->deleteActivity( $activityId );
            $this->view->output = $output;
        } //$activityId
        //$this->_forward( 'index' );
        $url = '/activity?instituteid=' . $this->institute_id;
        $this->_redirect( $url );
    }
    /* Action: creating a new activity */
    public function addAction( )
    {
        if ( !$this->_request->getPost() ) {
            
            $data             = $this->mdl->getMain( null, $this->institute_id );
            $city             = $data[ 'institute' ][ 0 ][ 'city_id' ];
            $venue            = $this->mdl->getVenue( $city );
            $data[ 'venue' ]  = $venue;
            $this->view->data = $data;
           
            echo $this->view->render( 'activity/insertactivityinfo.phtml' );
            
            return true;
        } //!$this->_request->getPost()
        $post    = $this->_request->getPost();
        $isValid = True;
        if ( !$isValid ) {
            $data             = $this->mdl->getMain( null, $this->institute_id );
            $data[ 'posted' ] = $post;
            $this->view->data = $data;
            echo $this->view->render( 'activity/insertactivityinfo.phtml' );
            return true;
        } //!$isValid
        $output             = $this->mdl->createActivityInfo( $post );
        $this->view->output = $output;
        if ( isset( $output[ 'error' ] ) ) {
            $data               = $this->mdl->getMain( null, $this->institute_id );
            $city_id            = $data[ 'institute' ][ 0 ][ 'city_id' ];
            $venue              = $this->mdl->getVenue( $city_id );
            $data[ 'venue' ]    = $venue;
            $data[ 'posted' ]   = $post;
            $this->view->data   = $data;
            $this->view->output = $output;
            echo $this->view->render( 'activity/insertactivityinfo.phtml' );
            return true;
        } //isset( $output[ 'error' ] )
        $activity_id = $output[ 'activity_id' ];
        $url         = '/activity/edit?id=' . $activity_id . '&instituteid=' . $this->institute_id;
        $this->_redirect( $url );
    }
    /* Action: editing an existing activity */
    public function editAction( )
    {
        $activity_id = $this->_getParam( 'id', '0' );
       
        if ( $this->_request->getPost() ) {
            $data = $this->_request->getPost();
            $type = $data[ 'type' ];
            unset( $data[ 'type' ] );
            $isValid = array(
                 'error' => '' 
            );
            if ( $isValid[ 'error' ] == FALSE ) {
                $output             = $this->mdl->$type( $data );
                $this->view->output = $output;
            } //$isValid[ 'error' ] == FALSE
        } //$this->_request->getPost()
        $data             = $this->mdl->getMain( $activity_id, $this->institute_id );
        $this->view->data = $data;
        echo $this->view->render( 'activity/main.phtml' );
    }
    protected function getListActivity( )
    {
    }
    protected function _insertActivityInfo( )
    {
        $data    = $this->_request->getPost();
        $isValid = $this->_validation( $data, __METHOD__ );
        if ( $isValid[ 'status' ] == FALSE ) {
            $this->view->output[ 'error' ] = $isValid[ 'data' ];
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$isValid[ 'status' ] == FALSE
        $output             = $this->mdl->insertActivityInfo( $data );
        $this->view->output = $output;
        if ( $output[ 'error' ] ) {
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$output[ 'error' ]
        echo $this->view->render( 'activity/insertContributors.phtml' );
    }
    protected function _insertContributors( )
    {
        $data    = $this->_request->getPost();
        $isValid = $this->_validation( $data, __METHOD__ );
        if ( $isValid[ 'status' ] == FALSE ) {
            $this->view->output[ 'error' ] = $isValid[ 'data' ];
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$isValid[ 'status' ] == FALSE
        $output             = $this->mdl->insertActivityInfo( $data );
        $this->view->output = $output;
        if ( $output[ 'error' ] ) {
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$output[ 'error' ]
        echo $this->view->render( 'activity/insertContributors.phtml' );
    }
    protected function insertParticipants( )
    {
    }
    protected function insertOutcomes( )
    {
    }
    protected function _validation( $data, $type = 'insertActivityInfo' )
    {
        $output            = array( );
        $output[ 'error' ] = FALSE;
        return $output;
    }
    protected function _getList( )
    {
        $output = $this->mdl->getList();
        return $output;
    }
    /* Action: Uploading participant file and reading its content */
    public function uploadparticipantsAction( )
    {
        $this->_helper->layout()->disableLayout();
        $data = $_FILES[ 'fileParticipant' ];
        if ( $data[ 'size' ] != 0 ) {
            $output = $this->mdl->readParticipants( $data );
            $page   = '<p id="participantData" >' . $output . ' </p> ';
            echo $page;
        } //$data[ 'size' ] != 0
        else {
            echo 'Error Uploading file';
        }
    }
    /* Action: getting venue details from selected city */
    public function getvenueAction( )
    {
        $this->_helper->layout()->disableLayout();
        $city = $this->_request->getParam( 'city' );
        if ( $city ) {
            $output = $this->mdl->getVenue( $city );
            echo json_encode( $output, TRUE );
        } //$city
    }
    /* Action: getting faculty from selected institute */
    public function getfacultylistAction( )
    {
        $this->_helper->layout()->disableLayout();
        $inst = $this->_request->getPost( 'instituteid' );
        
        if ( $inst ) {
            $output = $this->mdl->getfacultylist( $inst );          
            echo json_encode( $output, TRUE );
        } //$inst
    }
}

