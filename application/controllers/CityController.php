<?php

class CityController extends Zend_Controller_Action
{
 /**
 * class InstituteController for showing institute page listing 
 * @author    Piyush Tripathi
 * @version    0.0.1
 */
    

    public function indexAction() {
       
        echo $this->view->render( 'city/list.phtml' );      
    }
    
    /* Action: creating a new institute */ 
    public function addAction( )
    {
        echo $this->view->render( 'city/add.phtml' );      
        
        
    }
    /* Action: editing an existing institute details */
    public function editAction( )
    {
       echo $this->view->render( 'city/edit.phtml' );      
       
    }
    
        
    
    public function deleteAction(){
       echo $this->view->render( 'city/delete.phtml' );      
    }

}

