<?php

class CampusController extends Zend_Controller_Action
{
     /**
 * class CampusController for controlling the company campus actions 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    private $commonModel;
    public function init() {
        $this->commonModel = New Application_Model_Common();
        /* Initialize action controller here */
    }
    /* Action:showing the default campus company page   */
    public function showAction() {
        if(isset($_GET['id'])) {
            $id = (int) $_GET['id'];
        }
        if(isset($_GET['instituteid']) && $_GET['instituteid']) {
            $instituteId = $_GET['instituteid'];
        }
        if(!$id && !$instituteId) {
            header('Location: /invalid/');  
        }
        $this->view->alertClass = 'hide-block';
        if($_GET['created'] == 'true') {
            $this->view->alertClass = 'alert-success';
            $this->view->message = '<i class="icon-back"></i><span class="error-msg1"><strong> Success! </strong> Campus Company created ...';
        }
        $this->view->id = $id;
        $this->view->instituteId = $instituteId;
        $campus = new Application_Model_Campus($id, true);
        $this->view->data= $campus->data->data;
    }
    /* Action:adding a new campus company under an institute   */
    public function createAction() {
        $this->view->alertClass ="hide-block";;
        if($this->_request->getPost()) {
            $data = $this->_request->getPost();
            $instituteId = $data['instituteid'];
            if($this->commonModel->validInstitute($instituteId)) {
                if($newCampusId = $this->commonModel->validInsertCampusData($data)) {
                    //echo '['.$newCampusId.']';
                    header('Location: /campus/show/?instituteid='.$data['instituteid'].'&id='.$newCampusId.'&created=true');
                } else {
                    $this->view->instituteId = $instituteId;
                    $this->view->alertClass ="alert-error";
                    $this->view->message = '<i class="icon-back-error"></i><span class="error-msg"><strong> Error! </strong> Please fill required field(s)';
                    $this->view->data = $data;
                }
            } else {
                header('Location: /invalid/');
            }
             
            
        }
        else{
            $instituteId = $this->_request->getParam('instituteid');
            if(!$instituteId || !$this->commonModel->validInstitute($instituteId)) {
                header('Location: /invalid/');
            }
            
            $this->view->instituteId = $instituteId;
        }
        //header('Location: /campus/?id=10');
        //
    }
    
    public function indexAction() {
        $this->view->alertClass = 'hide-block';
        if($this->_request->getPost()) {
            $campusId = $this->_request->getPost('delCampusId', 0);
        
            if($campusId) {
                if($this->commonModel->deleteCampus($campusId)) {
                    $this->view->alertClass ="alert-success";
                    $this->view->message = '<strong>Success!</strong> Campus company deleted';
                } else {
                    $this->view->alertClass ="alert-error";
                    $this->view->message = '<strong>Error!!</strong> Invalid Data';
                }
            
            }
        } 
        $instituteId = $this->_request->getParam('instituteid', 0);

        if($instituteId) {
            $list = $this->commonModel->getAllCapmpusCompany($instituteId);
            $this->view->list = $list;
            $this->view->instituteId = $instituteId;

            if($this->_request->getParam('action', 0)) {

            }
        } else {
            header('Location: /invalid/');
        }
        
    }
    
    public function deleteAction() {
       
    }
    
    

}

