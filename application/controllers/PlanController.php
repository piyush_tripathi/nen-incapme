<?php
class PlanController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper( 'FlashMessenger' );
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_Plan();
    }
    /* default listing for institute plan  page  */
    public function indexAction( )
    {
       
       
        $data                     = $this->mdl->institute_plan( $this->institute_id );
        $this->view->data         = $data;
        $this->view->institute_id = $this->institute_id;
        echo $this->view->render( 'plan/index.phtml' );
    }
    /* Action:deleting an activity   */
    public function deleteAction( )
    {
        $output     = array( );
        $planId = $this->_request->getPost( 'plan_id' );
        if ( $planId ) {
            $output             = $this->mdl->delete_plan( $planId );
            $this->view->output = $output;
        } //$activityId
        //$this->_forward( 'index' );
        $url = '/plan?instituteid=' . $this->institute_id;
        $this->_redirect( $url );
    }
    /* Action: creating a new activity */
    public function addAction( )
    {
       
        if ( !$this->_request->getPost() ) {
            $data['institute_id'] = $this->institute_id;           
            $this->view->data = $data;
            echo $this->view->render( 'plan/insert.phtml' );
            return true;
        } //!$this->_request->getPost()
        $post= $this->_request->getPost();
        
        $output= $this->mdl->create_institute_plan( $post,$this->institute_id );
        
        $this->view->output = $output;
        if ( isset( $output[ 'error' ] ) ) {
            $data['success'] = false;
            echo Zend_Json::encode($output); 
            exit();

        } //isset( $output[ 'error' ] )
        $plan_id = $output[ 'plan_id' ];      
        $url         = '/plan/edit?id=' . $plan_id . '&instituteid=' . $this->institute_id;
        $data['success'] = true;
        $data['url'] = $url;
        echo Zend_Json::encode($data); 
		exit();
    }
    /* Action: editing an existing activity */
    public function editAction( )
    {
        $plan_id = $this->_getParam( 'id', '0' );
        
        
        if ( $this->_request->getPost() ) {
           
            $data = $this->_request->getPost();
             
            $plan_stage = $data[ 'plan_stage' ];
            $plan_id  = $data['plan_id'];
            
            if($plan_id){
               $output             = $this->mdl->update_inst_plan( $data,$plan_stage,$plan_id ); 
            }
            
             $this->_helper->layout()->disableLayout();
             
             $out  = array('status'=>'success');
            $this->_helper->json($output);        
        }
        else{
        $data['institute_id'] = $this->institute_id;
        $data['plan_id'] = $plan_id;
        $this->view->data = $data;
        $this->_helper->layout()->enableLayout();
        echo $this->view->render( 'plan/main.phtml' );
        }//$this->_request->getPost()
       
    }
    function getplandataAction(){
    
       $this->_helper->layout()->disableLayout();
       $plan_id = $this->_request->getPost('plan_id');
     
       
       if(!$plan_id){
            return false;
        }
      
        $output = $this->mdl->get_plan_data( $plan_id );
        echo json_encode( $output );  
        
    }
    
    function publishAction(){
        
         $plan_id = $this->_request->getPost('plan_id');
     
        if(!$plan_id){
            return false;
        }
       
        $output = $this->mdl->publish_inst_plan( $plan_id,$this->institute_id );        
        $this->_redirect('/plan/edit?id='.$plan_id.'&instituteid='.$this->institute_id);
        
    }
    protected function _insertActivityInfo( )
    {
        $data    = $this->_request->getPost();
        $isValid = $this->_validation( $data, __METHOD__ );
        if ( $isValid[ 'status' ] == FALSE ) {
            $this->view->output[ 'error' ] = $isValid[ 'data' ];
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$isValid[ 'status' ] == FALSE
        $output             = $this->mdl->insertActivityInfo( $data );
        $this->view->output = $output;
        if ( $output[ 'error' ] ) {
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$output[ 'error' ]
        echo $this->view->render( 'activity/insertContributors.phtml' );
    }
    protected function _insertContributors( )
    {
        $data    = $this->_request->getPost();
        $isValid = $this->_validation( $data, __METHOD__ );
        if ( $isValid[ 'status' ] == FALSE ) {
            $this->view->output[ 'error' ] = $isValid[ 'data' ];
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$isValid[ 'status' ] == FALSE
        $output             = $this->mdl->insertActivityInfo( $data );
        $this->view->output = $output;
        if ( $output[ 'error' ] ) {
            echo $this->view->render( 'activity/insertActivityInfo.phtml' );
        } //$output[ 'error' ]
        echo $this->view->render( 'activity/insertContributors.phtml' );
    }
    protected function insertParticipants( )
    {
    }
    protected function insertOutcomes( )
    {
    }
    protected function _validation( $data, $type = 'insertActivityInfo' )
    {
        $output            = array( );
        $output[ 'error' ] = FALSE;
        return $output;
    }
    protected function _getList( )
    {
        $output = $this->mdl->getList();
        return $output;
    }
    /* Action: Uploading participant file and reading its content */
    public function uploadparticipantsAction( )
    {
        $this->_helper->layout()->disableLayout();
        $data = $_FILES[ 'fileParticipant' ];
        if ( $data[ 'size' ] != 0 ) {
            $output = $this->mdl->readParticipants( $data );
            $page   = '<p id="participantData" >' . $output . ' </p> ';
            echo $page;
        } //$data[ 'size' ] != 0
        else {
            echo 'Error Uploading file';
        }
    }
    /* Action: getting venue details from selected city */
    public function getvenueAction( )
    {
        $this->_helper->layout()->disableLayout();
        $city = $this->_request->getParam( 'city' );
        if ( $city ) {
            $output = $this->mdl->getVenue( $city );
            echo json_encode( $output, TRUE );
        } //$city
    }
    /* Action: getting faculty from selected institute */
    public function getfacultylistAction( )
    {
        $this->_helper->layout()->disableLayout();
        $inst = $this->_request->getParam( 'instituteid' );
        if ( $inst ) {
            $output = $this->mdl->getfacultylist( $inst );
            echo json_encode( $output, TRUE );
        } //$inst
    }
    private function testAction(){
         echo $this->view->render( 'plan/main.phtml' );
    }
    private function test1Action(){
         $this->mdl->update_plan_stage('4','29');
    }
    
    
    private function test3Action(){
        $output = $this->mdl->publish_inst_plan( 29,$this->institute_id );   
    }
 
}

