<?php
class IedcController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper( 'FlashMessenger' );
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_Iedc();
    }
    /* default listing for institute iedc  page  */
    public function indexAction( )
    {
       $iedc_id                     = $this->mdl->iedc_exists( $this->institute_id );
        if($iedc_id){
            $url = '/iedc/edit?id='.$iedc_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
        }
        else{           
            echo $this->view->render( 'iedc/insert.phtml' );
        }
       
    }
   
    /* Action: creating a new svmu */ 
    public function addAction( )
    {
        $iedc_id =  $this->mdl->iedc_exists($this->institute_id);
        
         if($iedc_id){
            $url = '/iedc/edit?id='.$iedc_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
             return true;
        }
        //if iedc dosent exists create one 
        
        $output  = $this->mdl->create_iedc( $this->institute_id );
        $this->view->output = $output;
        if ( isset( $output[ 'error' ] ) ) {           
             $this->_redirect( '/iedc?instituteid='.$this->institute_id );
             return true;
        } //isset( $output[ 'error' ] )
         
        $iedc_id = $output['iedc_id'];
        $url = '/iedc/edit?id='.$iedc_id.'&instituteid='.$this->institute_id ;
        $this->_redirect( $url );
        
        
    }
    /* Action: editing an existing activity */
    public function editAction( )
    {
        $iedc_id = $this->_getParam( 'id', '0' );
        
        
        if ( $this->_request->getPost() ) {
           
            $data = $this->_request->getPost();
             
            
            $iedc_id  = $data['iedc_id'];
            
            if($iedc_id){
               $output             = $this->mdl->update_iedc_details( $data,$iedc_id );
            }
            
             $this->_helper->layout()->disableLayout();
             
             $out  = array('status'=>'success');
            $this->_helper->json($output);        
        }
        else{
        $data['institute_id'] = $this->institute_id;
        $data['iedc_id'] = $iedc_id;
        $this->view->data = $data;
        $this->_helper->layout()->enableLayout();
        echo $this->view->render( 'iedc/main.phtml' );
        }//$this->_request->getPost()
       
    }
    function getiedcdataAction(){
    
       $this->_helper->layout()->disableLayout();
       $iedc_id = $this->_request->getPost('iedc_id');
     
       
       if(!$iedc_id){
            return false;
        }
      
        $output = $this->mdl->get_iedc_data( $iedc_id );
        echo json_encode( $output );  
        
    }
    
    function publishAction(){
        
         $iedc_id = $this->_request->getPost('iedc_id');
     
        if(!$iedc_id){
            return false;
        }
       
        //$output = $this->mdl->publish_inst_plan( $plan_id,$this->institute_id );        
        //$this->_redirect('/plan/edit?id='.$plan_id.'&instituteid='.$this->institute_id);
        
    }
    
    
    protected function _validation( $data, $type = 'insertActivityInfo' )
    { 
        $output            = array( );
        $output[ 'error' ] = FALSE;
        return $output;
    }
    public function testAction(){
         echo $this->view->render( 'iedc/main.phtml' );
    }
   /* Action: Uploading organising team file and reading its content */
    public function uploadFormFileAction( )
    {
       
        $options = array();
        $this->_helper->layout()->disableLayout();
        if($_FILES[ 'file_bi_uploadplanorMinutes' ]){
            
             $options['file_name'] = 'file_bi_uploadplanorMinutes'.$this->institute_id;
             $output = $this->mdl->upload_file( $_FILES[ 'file_bi_uploadplanorMinutes' ],$options );             
             $page   = '<script>parent.document.getElementById("bi_uploadplanorMinutes").value ="'.$output.'";</script> ';
        }
        else if($_FILES[ 'file_td_uploadlistofmembers' ]){
             $options['file_name'] = 'file_td_uploadlistofmembers'.$this->institute_id;
             $output = $this->mdl->upload_file( $_FILES[ 'file_td_uploadlistofmembers' ],$options );
             $page   = '<script>parent.document.getElementById("td_uploadlistofmembers").value ="'.$output.'";</script> ';
             
        }
        else if($_FILES[ 'file_pff_uploadcopyofproposal' ]){
            $options['file_name'] = 'file_pff_uploadcopyofproposal'.$this->institute_id;
            $output = $this->mdl->upload_file( $_FILES[ 'file_pff_uploadcopyofproposal' ],$options );
             $page   = '<script>parent.document.getElementById("pff_uploadcopyofproposal").value ="'.$output.'";</script> ';
//             $page = '<p>'.print_r($output).'</p>';
        }
        else if($_FILES[ 'file_ps_listofinternalfac' ]){
            $options['file_name'] = 'file_ps_listofinternalfac'.$this->institute_id;
            $output = $this->mdl->upload_file( $_FILES[ 'file_ps_listofinternalfac' ],$options );
            $page   = '<script>parent.document.getElementById("ps_listofinternalfac").value ="'.$output.'";</script> ';
        }
        else{}
        
        
        echo $page;
        
    }
 
}

