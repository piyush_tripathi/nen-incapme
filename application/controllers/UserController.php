<?php
class UserController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
               
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_User();
       
    }
    /* default listing for institute fdp  page  */
    public function indexAction( )
    {
       
      //if valid user show the index page for user function 
        $user_id  = $this->_getParam( 'id','0');
        
        if($this->_valid_user( $user_id)){
            echo $this->view->render( 'user/main.phtml' );
            return true;
        }
        else{
            $url = '/';
            $this->_redirect($url);
        }
        
    }
   
    /* Action: creating a new user */ 
    public function addAction( )
    {
        
        $is_admin = $this->mdl->is_admin(); 
        if(!$is_admin){
             $url = '/' ;
             $this->_redirect( $url );             
             return 'no entry allowed'; 
        }
        //user is admin :if data is not posted    - show form         
        $data = $this->_request->getPost();
        if(!$data){                
                echo $this->view->render( 'user/main.phtml' );
                return true;
        }
        
        //if data is posted check if user exists 
        $user_exists = $this->mdl->user_exists($data['user_email']);
        if($user_exists){
            $this->view->msg = "User with email: ".$data['user_email']." already exists";
            echo $this->view->render( 'user/main.phtml' );            
            return true;
        }
        
        //safe to create new user 
        
        $output  = $this->mdl->create_user( $data );
        //user created : redirect to user page 
        $this->_helper->layout()->disableLayout();
        $this->_helper->json($output);            
        return true;           
        
        
        
    }
    /* Action: editing an existing user details */
    public function editAction( )
    {
        $user_id  = $this->_getParam( 'id','0');
        
        if($this->_valid_user( $user_id)){
            if(!$this->_request->getPost()){
                echo $this->view->render( 'user/main.phtml' );
                return true;
            }
            //data posted from the valid user
             $data = $this->_request->getPost();
             $output = $this->mdl->update_user_details( $data,$user_id );
             $this->_helper->layout()->disableLayout();
             $this->_helper->json($output);  
        
        }
        else{
            $url = '/';
            $this->_redirect($url);
        }   
       
    }
    function getuserdataAction(){
    
        
        $this->_helper->layout()->disableLayout();
       //security check for user details edit page 
        $user_id = $this->_request->getPost('user_id');    
        $institute_id = $this->_request->getPost('institute_id');
        // is the user asking for his info only ?
        $is_admin = $this->mdl->is_admin(); 
       if($this->_valid_user($user_id) || $is_admin ){   
            $output = $this->mdl->get_user_data( $user_id,$institute_id );            
            echo json_encode( $output );   // user is requesting valid information
        }
        
         
        
    }
    function deleteAction(){
        //if not admiin then deny access
        $is_admin = $this->mdl->is_admin();
        if(!$is_admin){
            $url = '/';
            $this->_redirect( $url );
            return false;
        }
       
        //is admin so delete is possible      
        $user_id = $this->_request->getPost('user_id');          
        if($user_id ){
             $output = $this->mdl->delete_user( $user_id );  
             $this->view->msg  = $output;
        }
        
        echo $this->view->render( 'user/deleted.phtml' );
//        echo $this->view->render( 'user/main.phtml' );
        return true;
        
        
    }

    
    
    protected function _valid_user( $user_id)
    { 
         $auth = Zend_Auth::getInstance();
         if(!$auth->hasIdentity()) 
        { return false;
        }
         $user = $auth->getIdentity(); 
        
         if((int)$user_id == $user->human_id ){   
            return true;   // user is not requesting someone elses information
        }
         else if($user->role == 'ADMIN' ){
             return true;
         }
         else{
             return false;
         }
        
       
    }
    public function testAction(){
         echo $this->view->render( 'user/index.phtml' );
    }
    public function test2Action(){
         echo $this->view->render( 'institute/edit.phtml' );
    }
    
    function searchUserAction(){
        //searches user by given parameters
         $this->_helper->layout()->disableLayout();
         $post = $this->_request->getPost();
        $data = $post['data']; 
        $type = $post['type'] ;
//        echo json_encode( $type );
//        return true;
        if( !$data || !$type){
            return false;
        }
        $output = $this->mdl->search_user( $data,$type );            
        echo json_encode( $output );   // user is requesting valid information
        
        
    }
        
   
 
}

