<?php
class AjaxController extends Zend_Controller_Action
{   
    /**
 * class AjaxController for controlling the ajax actions 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender( TRUE );
        $this->_helper->getHelper( 'layout' )->disableLayout();
        if ( $this->_request->isXmlHttpRequest() ) {
            $this->_helper->viewRenderer->setNoRender( TRUE );
            $this->_helper->getHelper( 'layout' )->disableLayout();
        } //$this->_request->isXmlHttpRequest()
    }
     /* Action:default listing page  */
    public function indexAction( )
    {
        $data = $this->_request->getPost();
        //if($data['method'])
        list( $class, $method ) = explode( '.', $data[ 'method' ] );
        if ( $class && $method ) {
            $class   = 'Application_Model_' . ucfirst( $class );
            $handler = new $class();
            $result  = $handler->$method( $data );
        } //$class && $method
        $uploadMethodNames = array(
             'ecell.uploadmemberscsv',
            'ecell.uploadfiles',
            'ecell.uploadestabprofessionals',
            'campus.uploadstudentscsv',
            'campus.uploadadvisorscsv',
            'edc.uploadmemberscsv',
            'edc.uploadadvisorscsv',
            'edc.uploadfiles' 
        );
        if ( in_array( $data[ 'method' ], $uploadMethodNames ) ) {
            $json = json_encode( $result );
            switch ( $data[ 'method' ] ) {
                case 'ecell.uploadmemberscsv':
                    $js = 'if(top.uploaEcellMembers) top.uploaEcellMembers();';
                    break;
                case 'ecell.uploadestabprofessionals':
                    $js = 'if(top.uploaEstabProfessionals) top.uploaEstabProfessionals();';
                    break;
                case 'ecell.uploadfiles':
                    $js = 'if(top.uploadFiles) top.uploadFiles(\'' . $data[ 'type_of_file' ] . '\');';
                    break;
                case 'edc.uploadfiles':
                    $js = 'if(top.uploadFiles) top.uploadFiles(\'' . $data[ 'type_of_file' ] . '\');';
                    break;
                case 'campus.uploadstudentscsv':
                    $js = 'if(top.uploadCampusMembers) top.uploadCampusMembers();';
                    break;
                case 'campus.uploadadvisorscsv':
                    $js = 'if(top.uploadCampusAdvisors) top.uploadCampusAdvisors();';
                    break;
                case 'edc.uploadmemberscsv':
                    $js = 'if(top.uploadEdcMembers) top.uploadEdcMembers();';
                    break;
                case 'edc.uploadadvisorscsv':
                    $js = 'if(top.uploadEdcAdvisors) top.uploadEdcAdvisors();';
                    break;
                default:
                    $js = 'if(top.uploadFiles) top.uploadFiles(\'' . $data[ 'type_of_file' ] . '\');';
                    break;
            } //$data[ 'method' ]
            $out = <<< EOD
<html>
<head>
<script type="text/javascript">
function init() {
	$js //top means parent frame.
}
window.onload=init;
</script>
<body>
$json
</body>
</html>
EOD;
            echo $out;
        } //in_array( $data[ 'method' ], $uploadMethodNames )
        else {
            header( 'Content-type: text/json' );
            echo json_encode( $result );
            exit;
        }
    }
    
    public function uploadfileAction( )
    {
       
        $options = array();
        $this->_helper->layout()->disableLayout();
        $mod_folder = array('ecell'=>array('type1'=>'startup_calen','type2'=>'estab_uploadrep','type3'=>'hiperf_latestnews'),
            'edc'=>array('type1'=>'edcprog_upload'),'cc'=>''
                );
        $module  = $_GET[ 'module' ]; 
        $type= $_GET[ 'from' ]; 
        $folder = $module;
        $input_name = $mod_folder[$module][$type];
        $file_name = 'file_'.$input_name;
       
        if($_FILES[ $file_name ]){             
             $options['file_name'] = $file_name.'-'.$_GET['institute_id'];
              $this->commonModel = New Application_Model_Common();
             $output = $this->commonModel->upload_file( $_FILES[ $file_name ],$folder,$options );             
             $page   = '<script>parent.document.getElementById("'.$input_name.'").value ="'.$output.'";</script> ';
        }
        
        
        echo $page;
        
    }
    
    function getUploadedDataAction(){
        $fields = array('ecell'=>array('startup_calen','estab_uploadrep','hiperf_latestnews'),
            'edc'=>array('edcprog_upload'),'cc'=>''                );
         $module  = $_GET[ 'module' ];
         $id  = $_GET[ 'id' ];
         $field = implode(',',$fields[$module]);
         
         switch ($module) {
             case 'ecell':
                 $sql = "SELECT $field from ecell where institute_id=$id";
                 break;
             case 'edc':
                 $sql = "SELECT $field from edc where institute_id=$id";
                 break;             
             default:
                 break;
         }
         
        $this->_helper->layout()->disableLayout();
        $this->mdlDashboard          = new Application_Model_Dashboard();
        $output =  $this->mdlDashboard->execute_query($sql,'multi');
        echo json_encode($output);
    }
}