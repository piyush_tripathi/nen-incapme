<?php

class LoginController extends Zend_Controller_Action
{ /**
 * class LoginController for controllling the login behaviour of users 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    public function init()
    {
        /* Initialize action controller here */
        
    }

    public function indexAction() {
        if(Zend_Auth::getInstance()->hasIdentity()) {
            $auth = Zend_Auth::getInstance();
            $userInfo = $auth->getIdentity();
            //print_r($userInfo);
            //Zend_Auth::getInstance()->clearIdentity();
            $this->_redirect('index');
        }
//         die(print_r($_SERVER));
        $request = $this->getRequest();
       
        if ($request->isPost()) {
                if ($this->_process($request->getPost())) {
                    // We're authenticated! Redirect to the home page
                    $this->_helper->redirector('index', 'index');
                } else {
                     $err = '<span  style="color: red;position:relative;left:20px; top:2px; z-index:999;">Invalid username/password</span>';
                }
        }
        
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $this->_helper->getHelper('layout')->disableLayout();
        
        $htmLogin =<<< BOD
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>NEN Admin Home</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="/css/login.css" />
        <script src="/js/jquery.js"></script>
        <script src="/js/placeholder.js"></script>
         <script>
    $(function(){
      $(':input[placeholder]').placeholder();
    });
  </script>
     </head>
 <body>    
 <div id="logo">
 </div>
 <div id="loginbox">  
    $err
    <form id="loginform" name="loginForm" class="form-vertical"  action="/login/" method="POST" >
        <p>Enter email and password</p>
        <div style="text-align:right;margin-right: 60px;" >
        <div class="control-group">
            <div class="controls">
                <span class="login-label">Email :</span>
                <input type="text" name="username" required="required" placeholder="User name" />
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
              <span class="login-label">Password :</span>
                    <input type="password" name="password" required="required" placeholder="Password" />
             </div> 
        </div>
        </div>
         <input type="submit" class="btn btn-danger" value="Login" style="margin-left:50px; width:125px;" />
    </form>
</div>
</div>
BOD;

        
echo $htmLogin;        
    }

    protected function _process($values) {
        // Get our authentication adapter and check credentials
        $adapter = $this->_getAuthAdapter();
        $adapter->setIdentity($values['username']);
        $adapter->setCredential($values['password']);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);
        if ($result->isValid()) {
            $user = $adapter->getResultRowObject();
            $auth->getStorage()->write($user);
            return true;
        }
        return false;
    }

    protected function _getAuthAdapter()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $authAdapter->setTableName('users')
                    ->setIdentityColumn('email')
                    ->setCredentialColumn('password');
                   // ->setCredentialTreatment('SHA1(?)');

        return $authAdapter;
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_helper->redirector('index'); // back to login page
    }

}