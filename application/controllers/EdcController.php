<?php
class EdcController extends Zend_Controller_Action {

     /**
 * class EdcController for controlling the edc actions 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    public function init() {
        $this->commonModel = New Application_Model_Common();
    }

    
    
    public function indexAction() {
        $edc = new Application_Model_Edc();//hack
        //check if it is new EDC
        if ($this->_request->getPost()) {
            $data = $this->_request->getPost();
            $instituteId = $data['instituteid'];
            if (!$instituteId || !$this->commonModel->validInstitute($instituteId)) {
                header('Location: /invalid/');
            }
            
            if ($edcId = $this->commonModel->validInsertEdcData($data)) {
                $this->view->edcExists = true;
                $this->view->edcId = $edcId;
                $this->view->instituteId = $instituteId;
                $this->view->alertClass = 'alert-success';
                $this->view->message = '<i class="icon-back"></i><span class="error-msg1"><strong> Success! </strong> EDC created ...';
                //show alert
                //show edc
            } else {
                $this->view->instituteId = $instituteId;
                $this->view->edcExists = false;
                $this->view->alertClass = "alert-error";
                $this->view->message = '<i class="icon-back-error"></i><span class="error-msg"><strong> Error! </strong> Invalid data and/or requiered field(s) missing..';
                $this->view->data = $data;
            }
        } else {
            
            if (!$_GET['instituteid'] || !$this->commonModel->validInstitute($_GET['instituteid'])) {
                header('Location: /invalid/');
            }

            $this->view->instituteId = $_GET['instituteid'];
            $edcId = $this->commonModel->getEdcIdFromInstitueId($_GET['instituteid']);
            if($edcId) {
                $this->view->edcId = $edcId;
                $this->view->edcExists = true;
                $this->view->alertClass = 'hide-block';
            } else {
                $this->view->edcExists = false;
                $this->view->alertClass = 'hide-block';
                if(isset($_GET['action']) && $_GET['action'] == 'new') {
                    $this->view->alertClass = 'hide-block';
                }
            }

        }
    }
    
    
    
    

}