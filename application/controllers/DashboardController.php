<?php
class DashboardController extends Zend_Controller_Action
{/**
 * class ReportsController for displaying reports related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    private $commonModel;
   
    
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->commonModel = New Application_Model_Common();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->mdlDashboard          = new Application_Model_Dashboard();
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        
        
    }
    public function indexAction( )    
            {   
       
              $this->instituteAction();
            }
    public function instituteAction(){
        $data  = array();
        
        $institute_id = $this->_request->getParam( 'instituteid' );
        if(!$institute_id){
            return false;
        }
//        get all the data related to the institute from model dashboard
        $data['inst_name'] = $this->mdlDashboard->get_inst_name($institute_id);
        $data['inst_details'] = $this->mdlDashboard->get_inst_details($institute_id);
        $data['inst_count'] = $this->mdlDashboard->get_inst_count($institute_id);
        $data['inst_cc'] = $this->mdlDashboard->get_inst_cc($institute_id);
       //$data['inst_activities'] = $this->mdlDashboard->get_inst_activities($institute_id);
        
//        call view and assign the data retrieved 
        $this->view->data = $data;
        echo $this->view->render( 'dashboard/instituteDashboard.phtml' );
    }
    
    public function getinstactivityAction(){
        $this->_helper->layout()->disableLayout();
        if ( $this->_request->getPost() ) {
            $year = $this->_request->getPost('year');
            $institute_id = $this->_request->getPost('instituteid');
            if($year && $institute_id){
                 $data  = $this->mdlDashboard->get_inst_activities($institute_id,$year);
                 
            }
    }
    
    echo( json_encode($data));
   
    }
    
    public function test2Action(){
      //$this->_helper->layout()->disableLayout();
         ///get logged in consultant id 
        $auth = Zend_Auth::getInstance();          
        $user = $auth->getIdentity(); 
        $user_id = $user->human_id;
        $role =  $user->role;
         //if valid user         
         if(!user_id || $role!=='CON' ){
            return false;
        }
       
         
            $date = '07-2013';
           
            if($date){
                 $output = $this->mdlDashboard->consultant_calendar($date,$user_id);
                 echo '<pre>';
                 echo( print_r($output));
                 echo '</pre>';
                }
    
      
         
    }
    
    function leadFacultyDashboardAction(){
        //initial checks 
        
        
        //call dashboard for chek 
        $output = $this->mdlDashboard->lead_faculty_dashboard($this->institute_id);
        $this->view->data = $output;
        echo $this->view->render( 'dashboard/leadFacultyDashboard.phtml' ); 
    }
    function consultantDashboardAction(){
        //initial checks 
        
       
        //call dashboard for chek 
        $output = $this->mdlDashboard->consultant_dashboard();
        $this->view->data = $output;
        echo $this->view->render( 'dashboard/consultantDashboard.phtml' ); 
    }
    function consultantCalendarAction(){    
        echo $this->view->render( 'dashboard/consultantCalendar.phtml' ); 
    }
    public function getConsultantCalendarAction(){
        $this->_helper->layout()->disableLayout();
         ///get logged in consultant id 
        $auth = Zend_Auth::getInstance();          
        $user = $auth->getIdentity(); 
        $user_id = $user->human_id;
        $role =  $user->role;
         //if valid user         
        if(!user_id || $role!=='CON' || !$this->_request->getPost()){
            return false;
        }
       
            $date = $this->_request->getPost('date');            
            if($date){
                 $output = $this->mdlDashboard->consultant_calendar($date,$user_id);  
                 echo( json_encode($output));
                }
    
    }
   
}


