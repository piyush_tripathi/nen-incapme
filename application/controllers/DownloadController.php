<?php
class DownloadController extends Zend_Controller_Action
{
   
     /**
 * class DownloadController for controlling the download template and other  actions 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    private $id; 
    private $file ='';
    
    public function init() {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $this->_helper->getHelper('layout')->disableLayout();
        $this->id = $_GET['id'];
    }

    public function indexAction() {
    
         
    }  
    // if request type is edc , get the edc template
    public function edcAction() {
        $path = Application_Model_Csv::EDC_FILE_UPLOAD_PATH;
        if($id = $this->_request->getParam('program')) {
            $this->file = $path . Application_Model_Csv::EDC_PROG_FILE_NAME;
        } else if ($id = $this->_request->getParam('members')) {
            $this->file = $path . $id . '_' . Application_Model_Csv::EDC_MEMBERS_FILE_NAME;
        } else if ($id = $this->_request->getParam('advisors')) {
            $this->file = $path . $id . '_' . Application_Model_Csv::EDC_ADVISORS_FILE_NAME; 
        }
        $this->downloadfile();
        exit;
    }
    // if request type is ecell , get the ecelll template
    public function ecellAction() {
        $path = Application_Model_Csv::ECELL_FILE_UPLOAD_PATH;
        if($id = $this->_request->getParam('newsletter')) {
            $this->file = $path . $id . '_'. Application_Model_Csv::ECELL_LATEST_NEWSLETTER_FILE_NAME;
        } else if($id = $this->_request->getParam('activity_calender')) {
            $this->file = $path . $id.'_' . Application_Model_Csv::ECELL_ACTIVITY_CALENDER_FILE_NAME;
        } else if($id = $this->_request->getParam('establish_report')) {
            $this->file = $path . $id.'_' . Application_Model_Csv::ECELL_ESTABLISH_REPORT_FILE_NAME;
        }
        
        $this->downloadfile();
        exit;
        
    }
    // get the campus company template
    public function campuscompanyAction() {
        $path = Application_Model_Csv::CAMPUS_FILE_UPLOAD_PATH;
        if ($id = $this->_request->getParam('members')) {
            $this->file = $path . $id . '_' . Application_Model_Csv::CAMPUS_MEMBERS_FILE_NAME;
        } else if ($id = $this->_request->getParam('advisors')) {
            $this->file = $path . $id . '_' . Application_Model_Csv::CAMPUS_ADVISORS_FILE_NAME; 
        }
        $this->downloadfile();
        exit;
        
    }
    /* Action:output a  template file   */
    public function templateAction() {
        $template = $this->_request->getParam('tname');
        if($template == 'edc-advisors-template.csv') {
            $this->file = Application_Model_Csv::EDC_FILE_UPLOAD_PATH .'edc-advisors-template.csv';
        } else if($template == 'edc-members-template.csv') {
            $this->file = Application_Model_Csv::EDC_FILE_UPLOAD_PATH .'edc-members-template.csv';
        }else if($template == 'ecell-members-template.csv') {
            $this->file = Application_Model_Csv::ECELL_FILE_UPLOAD_PATH .'ecell-members-template.csv';
        }else if($template == 'ecell-guest-template.csv') {
            $this->file = Application_Model_Csv::ECELL_FILE_UPLOAD_PATH .'ecell-guest-template.csv';
        }else if($template == 'campus-members-template.csv') {
            $this->file = Application_Model_Csv::CAMPUS_FILE_UPLOAD_PATH .'campus-members-template.csv';
        }else if($template == 'campus-advisors-template.csv') {
            $this->file = Application_Model_Csv::CAMPUS_FILE_UPLOAD_PATH .'campus-advisors-template.csv';
        }
        $this->downloadfile();
    }
    /* private Action:output a  template file   */
    private function downloadfile() {
        if($this->file) {
            if(file_exists($this->file)) {
                header( 'Content-Description: File Transfer' );
                header( 'Content-Disposition: attachment; filename=' . basename($this->file) );
                header( 'Content-type: application/octet-stream');
                header( 'Content-type: text/csv');
                header( 'Content-Length: ' . filesize( $this->file) );
                readfile($this->file);
            } else {
                header( 'Content-Description: File Transfer' );
                header( 'Content-Disposition: attachment; filename=' . basename('incapme.txt') );
                header( 'Content-type: application/octet-stream');
                header( 'Content-Length: ' . filesize( '/uploads/incapme.txt') );                                                  
            }
        }
        exit;
    }
}