<?php
class ReportsController extends Zend_Controller_Action
{/**
 * class ReportsController for displaying reports related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    private $commonModel;
   
    
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->commonModel = New Application_Model_Common();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->institute_id = $this->_request->getParam( 'instituteid');
        $this->mdl          = new Application_Model_Reports();
        
        
    }
    public function indexAction( )
    {   
        $auth = Zend_Auth::getInstance();
        $user = $auth->getIdentity();
        //print_r($user);
        $list = $this->commonModel->getInstituteForUser($user->human_id, $user->role);
        $this->view->aggregateReportTitle = $user->first_name . ' '.$user->last_name ;
       
        $this->view->clsShowInstitutes = ' show ';
        $this->view->institutes = $list ;//$this->mdl->getInstitutes();
        $this->view->selectedInstituteId = null;
        $data['is_posted'] = false;

         //$data             = $this->mdl->fetchInstituteReport( $institute_id,$list );
        if(isset($_POST['institute_id']) ){
            $institute_id = $_POST['institute_id'];
            $this->view->selectedInstituteId =$institute_id;
           if($institute_id == 0){
               $this->view->reportTitle = '<span class="inst_report"> Combined Institute Overview Report </span>'.' <span class="username"> : '.$user->first_name . ' '.$user->last_name.'</span>';
               $data             = $this->mdl->fetchInstituteReport( $institute_id,$list );
           }
           else{
               foreach($list as $k=>$v){
                   if($v['id']==$_POST['institute_id']){
                       $instituteName  =$v['name']  ;
                   }
                   
               }
               $this->view->reportTitle = '<span class="inst_report"> Institute Overview Report :</span>'.$instituteName.'<span class="username"> : '.$user->first_name . ' '.$user->last_name.'</span>';
               $data             = $this->mdl->fetchInstituteReport( $institute_id );
           }
           
            
            $data['is_posted'] = true;
            // $data['form_data']             = $this->mdl->getInstitutes();
            $this->view->data = $data;
            $this->view->institute_id = $this->institute_id;
            //echo $this->view->render( 'reports/institutereport.phtml' );
        }
        else{
             
            $institute_id = $_GET['instituteid']; 
              
             $this->view->selectedInstituteId =0;
           
            if($user->role =='ADMIN' || $user->role=='CON'){
                            $prefix = 'Combined';                            
                            $data             = $this->mdl->fetchInstituteReport( $institute_id,$list );
                         
                            
                    }else{
                        $prefix = '';
                         
                         
                        }
                        
                        $this->view->reportTitle = $prefix.' Institute Overview Report '.' <span class="username"> : '.$user->first_name . ' '.$user->last_name.'</span>';
        }
        // 
        $this->view->data = $data;
        $this->view->institute_id = $this->institute_id;
       
        if ($user->role == 'FAC') {
            $this->view->clsShowInstitutes = ' hide ';
            $institute_id = $list[0]['id'];
            $data             = $this->mdl->fetchInstituteReport( $institute_id );
              $data['is_posted'] = true;
            $this->view->data = $data;
        }
         
         echo $this->view->render( 'reports/institutereport.phtml' );
    }
    function institutedetailedreportAction(){
        $auth = Zend_Auth::getInstance();
        $user = $auth->getIdentity();
        //print_r($user);
        $list = $this->commonModel->getInstituteForUser($user->human_id, $user->role);
        $this->view->aggregateReportTitle = $user->first_name . ' '.$user->last_name ;
       
        $this->view->clsShowInstitutes = ' show ';
        $this->view->institutes = $list ;//$this->mdl->getInstitutes();
        $this->view->selectedInstituteId = null;
        $data['is_posted'] = false;

         //$data             = $this->mdl->fetchInstituteReport( $institute_id,$list );
        if(isset($_POST['institute_id']) ){
            $institute_id = $_POST['institute_id'];
            $this->view->selectedInstituteId =$institute_id;
            
           if($institute_id == 0){
               $this->view->reportTitle = '<span class="inst_report"> Combined Institute Detailed Report </span>'.' <span class="username"> : '.$user->first_name . ' '.$user->last_name.'</span>';
               $data             = $this->mdl->instituteDetailedReport( $institute_id,$list );
           }
           else{
               foreach($list as $k=>$v){
                   if($v['id']==$_POST['institute_id']){
                       $instituteName  =$v['name']  ;
                   }
                   
               }
               $this->view->reportTitle = '<span class="inst_report"> Institute Detailed Report :</span>'.$instituteName.'<span class="username"> : '.$user->first_name . ' '.$user->last_name.'</span>';
               $data             = $this->mdl->instituteDetailedReport( $institute_id );
           }
           
            
            $data['is_posted'] = true;
            // $data['form_data']             = $this->mdl->getInstitutes();
            $this->view->data = $data;
            $this->view->institute_id = $this->institute_id;
            //echo $this->view->render( 'reports/institutereport.phtml' );
        }
        else{
             
//            $institute_id = $_GET['instituteid']; 
//              
//             $this->view->selectedInstituteId =0;
//           
//            if($user->role =='ADMIN' || $user->role=='CON'){
//                            $prefix = 'Default';                            
//                            $data             = $this->mdl->instituteDetailedReport( $institute_id );
//                         
//                            
//                    }else{
//                        $prefix = '';
//                         
//                         
//                        }
//                        
//                        $this->view->reportTitle = $prefix.' Institute Detailed Report '.' <span class="username"> : '.$user->first_name . ' '.$user->last_name.'</span>';
        }
        // 
         $this->view->selectedInstituteId =0;
        $this->view->data = $data;
        $this->view->institute_id = $this->institute_id;
       
        if ($user->role == 'FAC') {
            $this->view->clsShowInstitutes = ' hide ';
            $institute_id = $list[0]['id'];
            $data             = $this->mdl->institutedetailedreport( $institute_id );
             $data['is_posted'] = true;
            $this->view->data = $data;
        }
         
         echo $this->view->render( 'reports/institute_detailed_report.phtml' );
    }
    
    
    function instituteOverviewReportAction(){
         echo $this->view->render( 'reports/institute_overview_report.phtml' );
    }
    function facultyLevelReportAction(){
         echo $this->view->render( 'reports/faculty_level_report.phtml' );
    }
    function getflrdataAction(){
         $this->_helper->layout()->disableLayout();
         $institute_id=$this->_request->getParam( 'id' );         
         $data             = $this->mdl->getflrdata( $institute_id );
         echo json_encode($data);
    }
    
}


