<?php

class IndexController extends Zend_Controller_Action
{
     /**
 * class IndexController for default page listing 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    private $commonModel;
    public function init() {
        $this->commonModel = New Application_Model_Common();
        /* Initialize action controller here */
    }

    public function indexAction() {
       
        $filters = null;
        if($this->_request->getPost()) {
            $filters = $this->_request->getPost();
            
        }
        
        $auth = Zend_Auth::getInstance();
        $user = $auth->getIdentity();
        //print_r($user);
        $list = $this->commonModel->getInstituteForUser($user->human_id, $user->role, $filters);
        $mdl = new Application_Model_Common();
        $user_role = $mdl->get_user_role();
        if($user_role == 'ADMIN') {
            $this->view->showAdminFilter = true;
        }else if($user_role == 'CON') {
            $this->view->showConsultantFilter = true;
            $this->view->consultantId = $user->human_id;
        } 
        else if ($user_role == 'LEAD_FAC') {
            $ins = $list[0]['id'];
            header('Location: /dashboard/lead-faculty-dashboard/?instituteid='.$ins);
        }
        else if ($user_role == 'FAC') {
            $ins = $list[0]['id'];
            header('Location: /dashboard/institute?instituteid='.$ins);
        }
        $this->view->list = $list;
        
    }

}

