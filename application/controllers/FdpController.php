<?php
class FdpController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper( 'FlashMessenger' );
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_Fdp();
         $auth = Zend_Auth::getInstance(); 
        if($auth->hasIdentity()) 
        {
            $user = $auth->getIdentity(); 
            $this->human_id   =  $user->human_id;
        }
    }
    /* default listing for institute fdp  page  */
    public function indexAction( )
    {
       $fdp_id                     = $this->mdl->fdp_exists( $this->institute_id,$this->human_id);
       
      
       $lead_faculty = $this->mdl->is_lead_faculty($this->institute_id);
       $is_admin = $this->mdl->is_admin();
       $is_consultant = $this->mdl->is_consultant();
      
       $this->view->fdp_exist         = $fdp_id;
       $this->view->is_lead_faculty  = $lead_faculty;
       
      if($lead_faculty || $is_admin || $is_consultant){
             $this->view->data         = $this->mdl->get_fdp_list( $this->institute_id );             
             echo $this->view->render( 'fdp/index.phtml' );           
             return true;
        }
       
      
        
        if($fdp_id){
            $url = '/fdp/edit?id='.$fdp_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
        }
        else{ 
           $this->view->data         = $this->mdl->get_fdp_list( $this->institute_id );
             echo $this->view->render( 'fdp/insert.phtml' );           
             return true;           
        }
       
    }
   
    /* Action: creating a new svmu */ 
    public function addAction( )
    {
        $fdp_id =  $this->mdl->fdp_exists($this->institute_id,$this->human_id);
        
         if($fdp_id){
            $url = '/fdp/edit?id='.$fdp_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
             return true;
        }
        //if fdp dosent exists create one 
        
        $output  = $this->mdl->create_fdp( $this->institute_id,$this->human_id );
        $this->view->output = $output;
        if ( isset( $output[ 'error' ] ) ) {           
             $this->_redirect( '/fdp?instituteid='.$this->institute_id );
             return true;
        } //isset( $output[ 'error' ] )
         
        $fdp_id = $output['fdp_id'];
        $url = '/fdp/edit?id='.$fdp_id.'&instituteid='.$this->institute_id ;
        $this->_redirect( $url );
        
        
    }
    /* Action: editing an existing fdp */
    public function editAction( )
    {
        $fdp_id = $this->_getParam( 'id', '0' );
        
        
        if ( $this->_request->getPost() ) {
           
            $data = $this->_request->getPost();
             
            
            $fdp_id  = $data['fdp_id'];
            
            if($fdp_id){
               $output             = $this->mdl->update_fdp_details( $data,$fdp_id,$this->human_id );
            }
            
             $this->_helper->layout()->disableLayout();
             
             $out  = array('status'=>'success');
            $this->_helper->json($output);        
        }
        else{
        $data['institute_id'] = $this->institute_id;
        $data['fdp_id'] = $fdp_id;
        $this->view->data = $data;
        $this->_helper->layout()->enableLayout();
        echo $this->view->render( 'fdp/main.phtml' );
        }//$this->_request->getPost()
       
    }
    function getfdpdataAction(){
    
       $this->_helper->layout()->disableLayout();
       $fdp_id = $this->_request->getPost('fdp_id');
       $institute_id = $this->_request->getPost('institute_id');
       
       if(!$fdp_id){
            return false;
        }
      
        $output = $this->mdl->get_fdp_data( $fdp_id,$institute_id );
        echo json_encode( $output );  
        
    }
    
    function publishAction(){
        
         $fdp_id = $this->_request->getPost('fdp_id');
         
        if(!$fdp_id){
            return false;
        }
       
        $output = $this->mdl->publish_fdp( $fdp_id);        
        $this->_redirect('/plan/edit?id='.$plan_id.'&instituteid='.$this->institute_id);
       
        
    }
    
    
    protected function _validation( $data, $type = 'insertActivityInfo' )
    { 
        $output            = array( );
        $output[ 'error' ] = FALSE;
        return $output;
    }
    public function testAction(){
         echo $this->view->render( 'fdp/main.phtml' );
    }
   /* Action: Uploading organising team file and reading its content */
    public function uploadFormFileAction( )
    {
       
        $options = array();
        $this->_helper->layout()->disableLayout();
        if($_FILES[ 'file_bi_uploadplanorMinutes' ]){
            
             $options['file_name'] = 'file_bi_uploadplanorMinutes'.$this->institute_id;
             $output = $this->mdl->upload_file( $_FILES[ 'file_bi_uploadplanorMinutes' ],$options );             
             $page   = '<script>parent.document.getElementById("bi_uploadplanorMinutes").value ="'.$output.'";</script> ';
        }
        else if($_FILES[ 'file_td_uploadlistofmembers' ]){
             $options['file_name'] = 'file_td_uploadlistofmembers'.$this->institute_id;
             $output = $this->mdl->upload_file( $_FILES[ 'file_td_uploadlistofmembers' ],$options );
             $page   = '<script>parent.document.getElementById("td_uploadlistofmembers").value ="'.$output.'";</script> ';
        }
        else if($_FILES[ 'file_pff_uploadcopyofproposal' ]){
            $options['file_name'] = 'file_pff_uploadcopyofproposal'.$this->institute_id;
            $output = $this->mdl->upload_file( $_FILES[ 'file_pff_uploadcopyofproposal' ],$options );
             $page   = '<script>parent.document.getElementById("pff_uploadcopyofproposal").value ="'.$output.'";</script> ';
        }
        else if($_FILES[ 'file_vt_uploadreport' ]){
            $options['file_name'] = 'file_vt_uploadreport'.$this->institute_id;
            $output = $this->mdl->upload_file( $_FILES[ 'file_vt_uploadreport' ],$options );
            $page   = '<script>parent.document.getElementById("vt_uploadreport").value ="'.$output.'";</script> ';
        }
        else{}
        
        
        echo $page;
        
    }
 
}

