<?php
class EscController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1 the original one 
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper( 'FlashMessenger' );
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_Esc();
    }
    /* default listing for institute esc  page  */
    public function indexAction( )
    {
       $esc_id                     = $this->mdl->esc_exists( $this->institute_id );
        if($esc_id){
            $url = '/esc/edit?id='.$esc_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
        }
        else{           
            echo $this->view->render( 'esc/insert.phtml' );
        }
       
    }
   
    /* Action: creating a new svmu */ 
    public function addAction( )
    {
        $esc_id =  $this->mdl->esc_exists($this->institute_id);
        
         if($esc_id){
            $url = '/esc/edit?id='.$esc_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
             return true;
        }
        //if esc dosent exists create one 
        
        $output  = $this->mdl->create_esc( $this->institute_id );
        $this->view->output = $output;
        if ( isset( $output[ 'error' ] ) ) {           
             $this->_redirect( '/esc?instituteid='.$this->institute_id );
             return true;
        } //isset( $output[ 'error' ] )
         
        $esc_id = $output['esc_id'];
        $url = '/esc/edit?id='.$esc_id.'&instituteid='.$this->institute_id ;
        $this->_redirect( $url );
        
        
    }
    /* Action: editing an existing activity */
    public function editAction( )
    {
        $esc_id = $this->_getParam( 'id', '0' );
        
        
        if ( $this->_request->getPost() ) {
           
            $data = $this->_request->getPost();
             
            
            $esc_id  = $data['esc_id'];
            
            if($esc_id){
               $output             = $this->mdl->update_esc_details( $data,$esc_id );
            }
            
             $this->_helper->layout()->disableLayout();
             
             $out  = array('status'=>'success');
            $this->_helper->json($output);        
        }
        else{
        $data['institute_id'] = $this->institute_id;
        $data['esc_id'] = $esc_id;
        $this->view->data = $data;
        $this->_helper->layout()->enableLayout();
        echo $this->view->render( 'esc/main.phtml' );
        }//$this->_request->getPost()
       
    }
    function getescdataAction(){
    
       $this->_helper->layout()->disableLayout();
       $esc_id = $this->_request->getPost('esc_id');
     
       
       if(!$esc_id){
            return false;
        }
      
        $output = $this->mdl->get_esc_data( $esc_id );
        echo json_encode( $output );  
        
    }
    
    function publishAction(){
        
         $esc_id = $this->_request->getPost('esc_id');
     
        if(!$esc_id){
            return false;
        }
       
        //$output = $this->mdl->publish_inst_plan( $plan_id,$this->institute_id );        
        //$this->_redirect('/plan/edit?id='.$plan_id.'&instituteid='.$this->institute_id);
        
    }
//    
//    
//    protected function _validation( $data, $type = 'insertActivityInfo' )
//    { 
//        $output            = array( );
//        $output[ 'error' ] = FALSE;
//        return $output;
//    }
    public function testAction(){
         echo $this->view->render( 'esc/main.phtml' );
    }
   /* Action: Uploading organising team file and reading its content */
   public function uploadFormFileAction( )
    {
       
        $options = array();
        $this->_helper->layout()->disableLayout();
        if($_FILES[ 'file_bi_uploadplanorMinutes' ]){
            
             $options['file_name'] = 'file_bi_uploadplanorMinutes'.$this->institute_id;
             $output = $this->mdl->upload_file( $_FILES[ 'file_bi_uploadplanorMinutes' ],$options );             
             $page   = '<script>parent.document.getElementById("bi_uploadplanorMinutes").value ="'.$output.'";</script> ';
        }        
        else{}
        
        
        echo $page;
        
    }
 
}

