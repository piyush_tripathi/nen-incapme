<?php
class SvmuController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper( 'FlashMessenger' );
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_Svmu();
    }
    /* default listing for institute svmu  page  */
    public function indexAction( )
    {
       $svmu_id                     = $this->mdl->svmu_exists( $this->institute_id );
     
        if($svmu_id){
            $url = '/svmu/edit?id='.$svmu_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
        }
        else{           
            echo $this->view->render( 'svmu/insert.phtml' );
        }
       
    }
    
    /* Action: creating a new svmu */
    public function addAction( )
    {
        $svmu_id =  $this->mdl->svmu_exists($this->institute_id);
        
         if($svmu_id){
            $url = '/svmu/edit?id='.$svmu_id.'&instituteid='.$this->institute_id ;
             $this->_redirect( $url );
             return true;
        }
        //if svmu dosent exists create one 
        
        $output  = $this->mdl->create_svmu( $this->institute_id );
        $this->view->output = $output;
        if ( isset( $output[ 'error' ] ) ) {           
             $this->_redirect( '/svmu?instituteid='.$this->institute_id );
             return true;
        } //isset( $output[ 'error' ] )
         
        $svmu_id = $output['svmu_id'];
        $url = '/svmu/edit?id='.$svmu_id.'&instituteid='.$this->institute_id ;
        $this->_redirect( $url );
        
        
    }
    /* Action: editing an existing svmu */
    public function editAction( )
    {
        $svmu_id = $this->_getParam( 'id', '0' );
        
        if ( $this->_request->getPost() ) {
           
            $data = $this->_request->getPost();
             
           
            $svmu_id  = $data['svmu_id'];
            
            
            if($svmu_id){
               $output             = $this->mdl->update_svmu_details( $data,$svmu_id );
            }
            
             $this->_helper->layout()->disableLayout();
             
//             $out  = array('status'=>'success');
            $this->_helper->json($output);        
        }
        else{
        $data['institute_id'] = $this->institute_id;
        $data['svmu_id'] = $svmu_id;
        $this->mdl2             = new Application_Model_Activity();
        $data             = $this->mdl2->getMain( '10', '1044');
        $this->view->data = $data;
        
        $this->_helper->layout()->enableLayout();
        echo $this->view->render( 'svmu/main.phtml' );
        }//$this->_request->getPost()
       
    }
    function getsvmudataAction(){
    
       $this->_helper->layout()->disableLayout();
       $svmu_id = $this->_request->getPost('svmu_id');
     
       
       if(!$svmu_id){
            return false;
        }
      
        $output = $this->mdl->get_svmu_data( $svmu_id );
        echo json_encode( $output );  
        
    }
    
    function publishAction(){
        
         $svmu_id = $this->_request->getPost('svmu_id');
     
        if(!$svmu_id){
            return false;
        }
       
        //$output = $this->mdl->publish_inst_plan( $plan_id,$this->institute_id );        
        //$this->_redirect('/plan/edit?id='.$plan_id.'&instituteid='.$this->institute_id);
        
    }
    
    
    protected function _validation( $data, $type = 'insertActivityInfo' )
    { 
        $output            = array( );
        $output[ 'error' ] = FALSE;
        return $output;
    }
    public function testAction(){
        $test = $this->mdl->get_svmu_data(5);
        die( print_r($test));
    }
   /* Action: Uploading organising team file and reading its content */
    public function uploadFormFileAction( )
    {
       
        $options = array();
        $this->_helper->layout()->disableLayout();
        if($_FILES[ 'file_ov_organisingteam' ]){
            
             $options['file_name'] = 'file_ov_organisingteam'.$this->institute_id;
             $output = $this->mdl->upload_file( $_FILES[ 'file_ov_organisingteam' ],$options );             
             $page   = '<script>parent.document.getElementById("ov_organisingteam").value ="'.$output.'";</script> ';
        }
        else if($_FILES[ 'file_prgm_uploadschedule' ]){
             $options['file_name'] = 'file_prgm_uploadschedule'.$this->institute_id;
             $output = $this->mdl->upload_file( $_FILES[ 'file_prgm_uploadschedule' ],$options );
             $page   = '<script>parent.document.getElementById("prgm_uploadschedule").value ="'.$output.'";</script> ';
        }
        else if($_FILES[ 'file_prgm_uploadapplform' ]){
            $options['file_name'] = 'file_prgm_uploadapplform'.$this->institute_id;
            $output = $this->mdl->upload_file( $_FILES[ 'file_prgm_uploadapplform' ],$options );
             $page   = '<script>parent.document.getElementById("prgm_uploadapplform").value ="'.$output.'";</script> ';
        }
        else if($_FILES[ 'file_vt_uploadreport' ]){
            $options['file_name'] = 'file_vt_uploadreport'.$this->institute_id;
            $output = $this->mdl->upload_file( $_FILES[ 'file_vt_uploadreport' ],$options );
            $page   = '<script>parent.document.getElementById("vt_uploadreport").value ="'.$output.'";</script> ';
        }
        else{}
        
        
        echo $page;
        
    }
   
    
    
 
}

