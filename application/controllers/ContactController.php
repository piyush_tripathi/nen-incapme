<?php
class ContactController extends Zend_Controller_Action
{
   /**
 * class ActivityController for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.2 
 */
    /* initialization  */
    public function init( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->view = new Zend_View();
        $this->view->setScriptPath( APPLICATION_PATH . '/views/scripts/' );
        $this->view->setHelperPath( APPLICATION_PATH . '/views/helpers/' );
        $this->_flashMessenger = $this->_helper->getHelper( 'FlashMessenger' );
        $this->institute_id    = $this->_request->getParam( 'instituteid' );
        $this->mdl             = new Application_Model_Contact();
         $auth = Zend_Auth::getInstance(); 
        if($auth->hasIdentity()) 
        {
            $user = $auth->getIdentity(); 
            $this->human_id   =  $user->human_id;
        }
    }
    /* default listing for institute contact  page  */
    public function indexAction( )
    {
        $is_admin = $this->mdl->is_admin();
       $is_consultant = $this->mdl->is_consultant();
      if($is_admin){
             $this->view->data         = $this->mdl->get_contact_list(   );
             
        }
        echo $this->view->render( 'contact/index.phtml' );           
        return true;
      }
   
    /* Action: creating a new svmu */ 
    public function addAction( )
    {
        if($this->_request->getPost()){        
        $data = $this->_request->getPost();
        $output  = $this->mdl->create_contact( $data,$this->human_id );  
        $this->view->data = $output;
        
        
        }
        $this->view->user_details = $this->mdl->get_user_details( $this->human_id );
        echo $this->view->render( 'contact/insert.phtml' );    
        return true;
        
     }
     public function readAction( )
    {
        $contact_id = $this->_getParam( 'id', '0' );
        $this->view->data         = $this->mdl->get_contact_data(  $contact_id  );
       
        $this->_helper->layout()->enableLayout();
        echo $this->view->render( 'contact/main.phtml' );
        //$this->_request->getPost()
       
    }
    public function deleteAction( )
    {
        $output     = array( );
        $contact_id = $this->_request->getPost( 'contact_id' );
        if ( $contact_id ) {
            $output             = $this->mdl->delete_contact( $contact_id );
            $this->view->output = $output;
        } //$activityId
        //$this->_forward( 'index' );
        $url = '/contact/';
        $this->_redirect( $url );
    }
   
    
}

