<?php

class InstituteController extends Zend_Controller_Action
{
 /**
 * class InstituteController for showing institute page listing 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    private $commonModel;
    public function init() {
        $this->commonModel = New Application_Model_Common();
        //$this->mdlUser = New Application_Model_User();
        $this->mdl = New Application_Model_Institute;
        /* Initialize action controller here */
        
    }

    public function indexAction() {
        if(isset($_GET['instituteid'])) {
            $instituteId = (int) $_GET['instituteid'];
        }
        if(!$instituteId || !$this->commonModel->validInstitute($instituteId)) {
            header('Location: /invalid/');//    $instituteId =999;
        }
        $this->view->data = $this->commonModel->getMainInfoInstitute($instituteId);
        
    }
    
    /* Action: creating a new institute */ 
    public function addAction( )
    {
        $this->_helper->viewRenderer->setNoRender();
        $is_admin = $this->mdl->is_admin(); 
        if(!$is_admin){
             $url = '/' ;
             $this->_redirect( $url );             
             return false; //no entry allowed 
        }
        //institute is admin :if data is not posted    - show form         
        $data = $this->_request->getPost();
        if(!$data){                
                echo $this->view->render( 'institute/edit.phtml' );
                return true;
        }
        
        //if data is posted check if institute exists 
        $institute_exists = false;//$this->mdl->institute_exists($data['name']);
        if($institute_exists){
            $this->view->msg = "institute with name: ".$data['name']." already exists";
            echo $this->view->render( 'institute/edit.phtml' );            
            return true;
        }
        
        //safe to create new institute 
        
        $output  = $this->mdl->create_institute( $data );
        //institute created : redirect to institute page 
        $this->_helper->layout()->disableLayout();
        $this->_helper->json($output);            
        return true;
        
        
    }
    /* Action: editing an existing institute details */
    public function editAction( )
    {
       $this->_helper->viewRenderer->setNoRender();
        $institute_id  = $this->_getParam( 'instituteid','0');
        $valid  = $this->mdl->institute_exists($institute_id);
        if($valid){
            if(!$this->_request->getPost()){
                echo $this->view->render( 'institute/edit.phtml' );
                return true;
            }
            //data posted from the valid institute
             $data = $this->_request->getPost();
             $output = $this->mdl->update_institute_details( $data,$institute_id );
             $this->_helper->layout()->disableLayout();
             $this->_helper->json($output);  
        
        }
        else{
            $url = '/';
            $this->_redirect($url);
        }   
       
    }
    function getInstituteDataAction(){
        $this->_helper->viewRenderer->setNoRender();        
        $this->_helper->layout()->disableLayout();
        
       //security check for institute details edit page 
        $institute_id = $this->_request->getPost('institute_id'); 
        // is the institute asking for his info only ?
        $is_admin = $this->mdl->is_admin(); 
       if($this->mdl->institute_exists($institute_id) || $is_admin ){   
            $output = $this->mdl->get_institute_data( $institute_id );            
            echo json_encode( $output );   // user is requesting valid information
        }
        
         
        
    }
    public function testAction(){
        $this->_helper->viewRenderer->setNoRender();
        echo $this->view->render( 'institute/edit.phtml' );
        return true;
        
    }
//    public function deleteAction(){
//        //if not admiin then deny access
//       
////        $is_admin = $this->mdl->is_admin();
////        if(!$is_admin){
////            $url = '/';
////            $this->_redirect( $url );
////            return false;
////        }
////       
////        //is admin so delete is possible      
////        $institute_id = $this->_request->getPost('institute_id');   
////         
////        if($institute_id ){
////             $output = $this->mdl->delete_institute( $institute_id );  
////             $this->view->msg  = $output;
////        }
//       $this->_helper->viewRenderer->setNoRender(); 
//      echo $this->view->render( 'institute/edit.phtml' );
//        return true;
//        
//        
//    }

}

