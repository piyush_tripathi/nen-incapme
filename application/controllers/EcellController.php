<?php
class EcellController extends Zend_Controller_Action {

     /**
 * class EcellController for controlling the ecell  actions 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
    public function init() {
        /* Initialize action controller here */
        $this->commonModel = New Application_Model_Common();
    }

    public function xindexAction() {
        if(isset($_GET['id']) && $_GET['id']) {
            $id = (int) $_GET['id'];
        } else if(isset($_GET['instituteid']) && $_GET['instituteid']) {
            $id = $this->commonModel->fixAndgetEcellIdFromInstitueId((int)$_GET['instituteid']);
        }
        
        if(!$id) {
            header('Location: /invalid/');
        }
        $this->view->id = $id;
        $ecell = new Application_Model_Ecell($id);
        $this->view->data= $ecell->data->data;
        
        
    }
    
    public function indexAction() {
        $ecell = new Application_Model_Ecell();//hack bad hack  
        //check if it is new EDC
        if ($this->_request->getPost()) {
            $data = $this->_request->getPost();
            $instituteId = $data['instituteid'];
            if (!$instituteId || !$this->commonModel->validInstitute($instituteId)) {
                header('Location: /invalid/');
            }
            
            if ($ecellId = $this->commonModel->validInsertEcellData($data)) {
                $this->view->ecellExists = true;
                $this->view->ecellId = $ecellId;
                $this->view->instituteId = $instituteId;
                $this->view->alertClass = 'alert-success';
                $this->view->message = '<strong>E-Cell created ..</strong>';
                //show alert
                //show edc
            } else {
                $this->view->instituteId = $instituteId;
                $this->view->ecellExists = false;
                $this->view->alertClass = "alert-error";
                $this->view->message = '<strong>Error!</strong> Invalid data and/or requiered field(s) missing..';
                $this->view->data = $data;
            }
        } else {
            
            if (!$_GET['instituteid'] || !$this->commonModel->validInstitute($_GET['instituteid'])) {
                header('Location: /invalid/');
            }

            $this->view->instituteId = $_GET['instituteid'];
            $ecellId = $this->commonModel->getEcellIdFromInstitueId($_GET['instituteid']);
            if($ecellId) {
                
                $this->view->ecellId = $ecellId;
                $this->view->ecellExists = true;
                $this->view->alertClass = 'hide-block';
            } else {
                $instituteInfo = $this->commonModel->getMainInfoInstitute($_GET['instituteid']);
                $temp = $this->commonModel->getInstituteName($_GET['instituteid']);
                $instituteInfo['over_name'] =  $temp['name'];
                $this->view->ecellExists = false;
               
                $this->view->data = $instituteInfo;
                $this->view->alertClass = 'hide-block';
                if(isset($_GET['action']) && $_GET['action'] == 'new') {
                    $this->view->alertClass = 'hide-block';
                }
            }

        }
    }
    
   
    
    

}