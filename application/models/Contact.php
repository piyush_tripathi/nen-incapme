<?php
class Application_Model_Contact extends Application_Model_Db
{ /** 
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' );
       
            
    }
    
    public function create_contact( $data,$human_id )
    {
       $output = array( ); 
        $status = true;
        $from_name  = $this->isset_null($data['from_name']);
        $from_email  = $this->isset_null($data['from_email']);
        $from_mobile  = $this->isset_null($data['from_mobile'],$int);       
        $title =  $this->isset_null($data['contact_title']); 
        $message = $this->isset_null(($data['contact_message'])) ;
        
        
//        foreach($tos as $k=>$to){
//            $sql = "INSET INTO contacts(from_hid,to_hid,title,message,date) VALUES($from,$to,$title,$message,NOW())";
//            $status = $status && $this->execute_query($sql);
//        }
         $sql = "INSERT INTO contacts(from_name,from_email,from_mobile,title,message,date) VALUES($from_name,$from_email,$from_mobile,$title,$message,NOW())";
         //die($sql);
         $status =  $this->mysqli->query( $sql );
        return $status;
    }
  function get_user_details( $human_id ){
      $sql = "SELECT CONCAT(first_name,' ',last_name) as name,email  from human_resource where id=$human_id";      
      $output = $this->execute_query($sql);
      return $output; 
      
  }
    function get_contact_list(  )
    {
        $output = array( );
        $result = array( );
       
        $sql = "SELECT *  FROM contacts order by date desc";
        $qR  = $this->mysqli->query( $sql );
        if ( $qR->num_rows > 0 ) {
            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                $result[ ] = $qRR;
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
            $header = '<thead><th>SNo.</th><th>From</th>' . '<th>Email</th>' . '<th>Title</th>' . '<th>Date</th>' . '<th>Action</th></thead><tbody>' ;
            
            $html = '<div class="tpd-list-view">
                <h5 class="h5-heading">Contact List</h5>
                <div class="tpd-list-view-inner"><table class="table tpd-list tpd-list2">' . $header;
            $i    = 1;
            foreach ( $result as $row ) {
                $html .= '<tr>';
                
               
                $html .= '<td>' . $i . '</td>' . '<td><a href="/contact/read?id=' . $row[ 'id' ]  . '" >' . ucfirst($row[ 'from_name' ]) . '</a></td>' . '<td>' . $row[ 'from_email' ] . '</td>' . '<td>' . $row[ 'title' ] . '</td>' . '<td>' .$this->fix_date_format($row[ 'date' ]) . '</td>'  ;
                $html .= '<td> <a href="#delContact" data-toggle="modal" delContact" class="delContact" value="' . $row[ 'id' ] . '"  ><i class="icon-trash"> </i></a></td>';
                $html .= '</tr>';
                $i++;
            } //$result as $row
            $html .= '</tbody></table></div></div>';
            $output[ 'data' ] = $html;
        } //$qR->num_rows > 0
        else {
            $output[ 'error' ] = 'Could not retrieve the contact list';
        }
        return $output;
    } 
    
    function get_contact_data(  $contact_id  ){
        if(!$contact_id){
            return false;
        }
       $sql = "SELECT * FROM contacts  where id=$contact_id ";
        $output = $this->execute_query($sql);
        return $output; 
    }
    
   function is_lead_faculty($institute_id){
       
         $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $sql = 'SELECT count(*) as count from institute where head_of_institute='.$user->human_id.' and id='.$institute_id;
              $count = $this->execute_query($sql); 
              if($count['count']){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   
   
   function is_admin(){
       $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $role = $user->role;
              
              if($role=='ADMIN'){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   function is_consultant(){
       $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $role = $user->role;
              
              if($role=='CON'){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   
    function execute_query($query,$type=null){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                if($type=='multi'){
                    $result[] = $qR->fetch_assoc( );
                }
                else{
                    $result = $qR->fetch_assoc( );
                }
                  
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
            
        } catch (Exception $e) {
            //die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    function delete_contact( $contactId )
    {
        $output     = array( );
        $contactId = mysqli_real_escape_string( $this->mysqli, $contactId );
        $sql[ ]     = 'DELETE FROM contacts WHERE id=' . $contactId;
       
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) {
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = ' Deleted the contact';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Delete The contact:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( '; 
', $sql );
        }
    }
    
     function isset_null($data,$type=false){
        
        if(isset($data)&& !empty($data)){
            if($type=='int'){
                //return is_int($data) ? $data : 0;
                return (int)$data;
                
            }
            else if($type=='date'){
                return "'".$data."'";
            }
            else{
               return "'".$data."'";
            }
            
        }
        
        if($type=='int' || $type=='date'){
            $result  = 'NULL';
        }
        else if($type=='str'){
            $result  = "''";
        }
        else{
            $result  = "''";
        }
        return $result;
    }
    
     function fix_date_format($date){ 
          if(!$date || $date=='0000-00-00 00:00:00' || $date==''){
            return '';
        }
       $input_date  = New Zend_Date($date,'YYYY-mm-dd');
       $result = $input_date->get('dd-mm-YYYY') ;
        return $result;
    }
    
}

 