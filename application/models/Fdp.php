<?php
class Application_Model_Fdp extends Application_Model_Db
{ /** 
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' );
       
            
    }
    
    public function fdp_exists($institute_id,$human_id){
        $output = array( );
        $sql  = "SELECT fdp_id from fdp where institute_id=$institute_id  and faculty_id = $human_id limit 1";
        
        $result = $this->execute_query($sql);
       
        
        return $result['fdp_id'];
        
    }
    public function create_fdp( $institute_id,$human_id )
    {
       
        
        $output = array( ); 
        $status = true;
        $courses = array(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18); // array for default course id from db
        
         $sql  = "INSERT INTO fdp (fdp_id, faculty_id, institute_id, created) VALUES (NULL, $human_id, $institute_id,CURRENT_TIMESTAMP);";  
       
        $qR   = $this->mysqli->query( $sql );
        $fdp_id = $this->mysqli->insert_id;        
        
        foreach($courses as $course_id){
            $sql = "INSERT INTO fdp_to_course (id, fdp_courses_id, fdp_id, created) VALUES (NULL, $course_id, $fdp_id, CURRENT_TIMESTAMP);";           
            $this->mysqli->query( $sql );
            $status  = (bool)$this->mysqli->insert_id && $status;
        }
        
        if ( $status ) {$output[ 'success' ]     = 'Created';$output[ 'fdp_id' ] = $fdp_id;}
        else {$output[ 'error' ] = 'Could not Create the fdp:' . $this->mysqli->error;}
        
        return $output;
    }
    function publish_fdp( $fdp_id ){
        $sql = 'UPDATE fdp set fdp_published=1 where fdp_id='.$fdp_id;
        $output = $this->execute_query($sql);
        return $output ;
    }
    function array_to_csv($not_escaped){
//            if(!$not_escaped){
//                return false;
//            }
            
            $escaped = base64_encode($not_escaped);
            return $escaped ;  
    }
    function csv_to_array($escaped){
        if(!$escaped){
                return false;
            } 
           $not_escaped = base64_decode($escaped);
           return $not_escaped ; 
    }
    
    function get_fdp_data( $fdp_id,$institute_id){
         $output = array();
         $courses = array(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18); // array for default course id from db
         $type= 'multi';
         $sql = 'SELECT DISTINCT * FROM fdp_to_course where fdp_id='.(int)$fdp_id.' and fdp_courses_id IN('.implode(',',$courses).')';         
         $output['default_courses'] = $this->execute_query($sql,$type);
          
         $sql = 'SELECT DISTINCT x.*,y.course_name FROM fdp_to_course as x inner join fdp_courses as y on x.fdp_courses_id = y.id where x.fdp_id='.(int)$fdp_id.' and x.fdp_courses_id NOT IN('.implode(',',$courses).')';
        
         $output['other_courses'] = $this->execute_query($sql,$type);  
         
         //fdp details 
         $sql = 'SELECT x.*,CONCAT( y.first_name, " ", y.last_name ) as faculty_name from fdp as x join human_resource as y on x.faculty_id=y.id where x.fdp_id='.$fdp_id.' ';
         $output['fdp_details'] = $this->execute_query($sql);
         
         // now details about the person logged in
         
         $lead_faculty = $this->is_lead_faculty($institute_id);
         
         if($lead_faculty){
                  $output['is_lead_faculty'] = true;
              }else{
                  $output['is_lead_faculty'] = false;
              }
         
         return $output;
    }
    
     function get_fdp_list( $institute_id )
    {
        $output = array( );
        $result = array( );
        if ( !$institute_id ) {
            $output[ 'error' ] = 'No institute selected';
            return $output;
        } //!$institute_id
        $sql = "select x.*,CONCAT(y.first_name,' ',y.last_name) as faculty_name,z.name as institute_name from fdp as x join human_resource as y on x.faculty_id = y.id join institute as z on x.institute_id = z.id and x.institute_id =   $institute_id";
        $qR  = $this->mysqli->query( $sql );
        if ( $qR->num_rows > 0 ) {
            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                $result[ ] = $qRR;
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
            $header = '<thead><th>SNo.</th><th>Faculty Name</th>' . '<th>Institute Name</th>' . '<th>Published</th>' . '<th>Created</th>' . '<th>Changed</th><th>Actions</th></thead><tbody>' ;
            
            $html = '<div class="tpd-list-view">
                <h5 class="h5-heading">Faculty FDP List</h5>
                <div class="tpd-list-view-inner"><table class="table tpd-list tpd-list2">' . $header;
            $i    = 1;
            foreach ( $result as $row ) {
                $html .= '<tr>';
                $published  = $row['fdp_published']==1 ? 'YES' : 'NO' ;
                $created  =  date("d-M-Y", strtotime($row['created'])) ;
                if($row['changed']!='0000-00-00 00:00:00'){
                     $changed  =  date("d-M-Y", strtotime($row['changed'])) ;
                }else{
                     $changed  =  'Not Updated' ;
                }
               
                $html .= '<td>' . $i . '</td>' . '<td><a href="/fdp/edit?id=' . $row[ 'fdp_id' ] . '&instituteid=' . $row['institute_id'] . '" >' . $row[ 'faculty_name' ] . '</a></td>' . '<td>' . $row[ 'institute_name' ] . '</td>' . '<td>' . $published . '</td>' . '<td>' . $created . '</td>' . '<td>' . $changed . '</td>' ;
                $html .= '<td><a href="/fdp/edit?id=' . $row[ 'fdp_id' ] . '&instituteid=' . $row['institute_id'] . '" ><i class="icon-pencil"></i> Edit  </a>
                                                
                    ';
                $html .= '</tr>';
                $i++;
            } //$result as $row
            $html .= '</tbody></table></div></div>';
            $output[ 'data' ] = $html;
        } //$qR->num_rows > 0
        else {
            $output[ 'error' ] = 'Could not retrieve the FDP list';
        }
        return $output;
    } 
   function is_lead_faculty($institute_id){
       
         $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $sql = 'SELECT count(*) as count from institute where head_of_institute='.$user->human_id.' and id='.$institute_id;
              $count = $this->execute_query($sql); 
              if($count['count']){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   
   function update_fdp_details( $data,$fdp_id,$human_id ){
       $output = array( );
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $fdp_stage = $data['fdp_stage'];
        switch($fdp_stage){
             case 1:
                 $output = $this->update_fdp_stage1($data,$fdp_id,$human_id);
                break;
             case 2:
                 $output = $this->update_fdp_stage2($data,$fdp_id,$human_id);
                break;
             
            default;
                break;
        }
        
        return $output;
        
   }
   function update_fdp_stage1($data,$fdp_id,$human_id){
       $output = array( );
       $options = array();
       $updated_courses = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        $temp = array();
        /* get all the form values 
         * courses added to the course table 
         * 
         */
       
     //$courses = array(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18); // array for default course id from db
     //find the default courses 
        
        $query= "SELECT distinct  fdp_courses_id FROM `fdp` as x join fdp_to_course as y on x.fdp_id = y.fdp_id WHERE x.fdp_id =$fdp_id";
        $result = $this->execute_query($query);
       
        foreach($result as $key=>$value){
            $temp[] = $value['fdp_courses_id'];
        }
        $prev_courses = implode(',',$temp);         
        unset($temp);unset($result);
       
     foreach($data['course_id'] as $key=>$course_id){
             $suffix = '_'.$course_id;
             $course_provider=$this->isset_null($data['course_provider'.$suffix]);
             $lead_faculty_signoff=isset($data['signoff'.$suffix]) ? 1 : 0;
             $registered= $this->isset_null($data['reg'.$suffix],$int);
             $registered_date = $this->isset_null($data['reg_date'.$suffix],$date);
             $trained= $this->isset_null($data['train'.$suffix],$int);
             $trained_date= $this->isset_null($data['train_date'.$suffix],$date);
             $certified= $this->isset_null($data['certi'.$suffix],$int);
             $certified_date= $this->isset_null($data['certi_date'.$suffix],$date);
             $remarks= $this->isset_null($data['remark'.$suffix]);
          
           if(isset($course_id)){
               $sub_sql = 'UPDATE fdp_to_course SET ' ; 
               $sub_sql.="course_provider=$course_provider";
               $sub_sql.=",lead_faculty_signoff=$lead_faculty_signoff";
              
               if (isset($data['reg'.$suffix])){                   
                   $sub_sql.=",registered=1";
                   $sub_sql.= ",registered_date=$registered_date" ;
                   if(isset($data['train'.$suffix])){
                       $sub_sql.=',trained=1';
                       $sub_sql.=",trained_date=$trained_date";
                       if(isset($data['certi'.$suffix])){
                                $sub_sql.=',certified=1';
                                $sub_sql.= ",certified_date=$certified_date";
                                $sub_sql.=",remarks=$remarks";
                             }
                       else{
                           $sub_sql.=",certified=0,certified_date='',remarks=''";
                       }
                   }
                   else{
                       $sub_sql.=",trained=0,trained_date='',certified=0,certified_date='',remarks=''";
                   }
                   
               }
               else{
                   $sub_sql.=",registered=0,registered_date='',trained=0,trained_date='',certified=0,certified_date='',remarks='' ";
               }
               
            $sql[$course_id] = $sub_sql.' where  fdp_courses_id='.$course_id.' and fdp_id='.$fdp_id;
        }
        }
//        return $sql;
        //for updated date in main fdp table 
        $sql[] = "UPDATE fdp SET changed = CURRENT_TIMESTAMP WHERE fdp_id=$fdp_id" ;
        foreach($sql as $query){
           $result = $this->execute_query($query);
        }
      
       $updated_courses = $data['course_id']; 
       
      //now get the data from the user defined other courses 
        
      $sql = array();
      foreach($data['oth_course_id'] as $key=>$course_id){
          
             $suffix = '_'.(int)$course_id;
             $course_provider=$this->isset_null($data['oth_course_provider'.$suffix]);
             $lead_faculty_signoff=isset($data['oth_signoff'.$suffix]) ? 1 : 0;
             $registered= $this->isset_null($data['oth_reg'.$suffix],$int);
             $registered_date = $this->isset_null($data['oth_reg_date'.$suffix],$date);
             $trained= $this->isset_null($data['oth_train'.$suffix],$int);
             $trained_date= $this->isset_null($data['oth_train_date'.$suffix],$date);
             $certified= $this->isset_null($data['oth_certi'.$suffix],$int);
             $certified_date= $this->isset_null($data['oth_certi_date'.$suffix],$date);
             $remarks= $this->isset_null($data['oth_remark'.$suffix]);
             
             
          if(true){ //new course - add course oth_course_title[]
               $course_name  = $this->isset_null($data['oth_course_title'.$suffix]);
               $pre_sql = "INSERT INTO fdp_courses(id, course_name, added_on) VALUES(NULL,$course_name,NOW())";
               
               $qR   = $this->mysqli->query( $pre_sql );
               $course_id =  $this->mysqli->insert_id;
               $pre_sql = "INSERT INTO fdp_to_course(id, fdp_courses_id, fdp_id) VALUES(NULL,$course_id,$fdp_id)";
               $qR   = $this->mysqli->query( $pre_sql );
                 
             }    
          
             if(isset($course_id)){
               $sub_sql = 'UPDATE fdp_to_course SET ' ; 
               $sub_sql.="course_provider=$course_provider";
               $sub_sql.=",lead_faculty_signoff=$lead_faculty_signoff";
              
               if (isset($data['oth_reg'.$suffix])){                   
                   $sub_sql.=",registered=1";
                   $sub_sql.= ",registered_date=$registered_date" ;
                   if(isset($data['oth_train'.$suffix])){
                       $sub_sql.=',trained=1';
                       $sub_sql.=",trained_date=$trained_date";
                       if(isset($data['oth_certi'.$suffix])){
                                $sub_sql.=',certified=1';
                                $sub_sql.= ",certified_date=$certified_date";
                                $sub_sql.=",remarks=$remarks";
                             }
                      else{
                           $sub_sql.=",certified=0,certified_date='',remarks=''";
                       }
                   }
                     else{
                       $sub_sql.=",trained=0,trained_date='',certified=0,certified_date='',remarks=''";
                   }
                   
               }
               else{
                   $sub_sql.=",registered=0,registered_date='',trained=0,trained_date='',certified=0,certified_date='',remarks='' ";
               }
               
            $sql[$key] = $sub_sql.' where  fdp_courses_id='.$course_id.' and fdp_id='.$fdp_id;
            $updated_courses[] = $course_id;
        }
             
            
      }
       //now add the guest list to the system 
           
            foreach($sql as $query){
            $result = $this->execute_query($query);
            }
          
     //now cleanup the courses that was deleted by the user in the front end
            
            $sql = 'DELETE FROM fdp_courses where id NOT IN ('.implode(',',$updated_courses).') and id IN('.$prev_courses.')'; 
           
            $result = $this->execute_query($sql);
            
            $sql = 'DELETE FROM fdp_to_course where fdp_courses_id NOT IN ('.implode(',',$updated_courses).') and fdp_courses_id IN('.$prev_courses.')';  
            $result = $this->execute_query($sql);
            
            
       return $result;
       
       
   } 
   function is_admin(){
       $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $role = $user->role;
              
              if($role=='ADMIN'){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   function is_consultant(){
       $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $role = $user->role;
              
              if($role=='CON'){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   
    function execute_query($query,$type=null){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                if($type=='multi'){
                    $result[] = $qR->fetch_assoc( );
                }
                else{
                    $result = $qR->fetch_assoc( );
                }
                  
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
            
        } catch (Exception $e) {
            //die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    function sql_date($insertdate,$format = null){
        //$insertdate = mysqli_real_escape_string($insertdate);
        if(!$insertdate){
            return null;
        }
        $insertdate = strtotime($insertdate);
        if($format){
             $insertdate = date($format, $insertdate);
        }
        else{
             $insertdate = date('Y-m-d', $insertdate);
        }
        
        return $insertdate;
    }
    
    function delete_fdp( $fdpId )
    {
        $output     = array( );
        $activityId = mysqli_real_escape_string( $this->mysqli, $fdpId );
        $sql[ ]     = 'DELETE FROM institute_fdp WHERE iap_id=' . $fdpId;
       
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) {
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = ' Deleted the fdp';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Delete The fdp:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( '; 
', $sql );
        }
    }
    public function validate( $data, $type )
    {
        $status = array( );
        switch ( $type ) {
            case 'numeric':
                if ( is_numeric( $data ) && (int) $data >= 0 ) {
                    return true;
                } //is_numeric( $data ) && (int) $data >= 0
                else {
                    $status[ 'error' ] = 'Please Provide only non negative numbers';
                    return $status;
                }
                break;
            default:
                break;
        } //$type
    }
    
    
     function isset_null($data,$type=false){
        
        if(isset($data)&& !empty($data)){
            if($type=='int'){
                //return is_int($data) ? $data : 0;
                return (int)$data;
                
            }
            else if($type=='date'){
                return "'".$data."'";
            }
            else{
               return "'".$data."'";
            }
            
        }
        
        if($type=='int' || $type=='date'){
            $result  = 'NULL';
        }
        else if($type=='str'){
            $result  = "''";
        }
        else{
            $result  = "''";
        }
        return $result;
    }
    
}

 