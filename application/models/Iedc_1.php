<?php
class Application_Model_Iedc extends Application_Model_Db
{ /**
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' ); 
    }
    
    public function iedc_exists($institute_id){
        $output = array( );
        $sql  = 'SELECT iedc_id from iedc where institute_id='.$institute_id.' limit 1';
        $result = $this->execute_query($sql);
        return $result['iedc_id'];
        
    }
    public function create_iedc( $institute_id )
    {
        $output = array( );
        $sql  = "INSERT INTO `iedc` (`iedc_id`, `institute_id`, `datecreated`, `dateupdated`) VALUES (NULL, '".$institute_id."', CURRENT_TIMESTAMP, '0000-00-00 00:00:00');";
      
        $qR   = $this->mysqli->query( $sql );
        if ( $qR ) {
            $output[ 'success' ]     = 'Created';
            $output[ 'iedc_id' ] = $this->mysqli->insert_id;
        } //$qR
        else {
            $output[ 'error' ] = 'Could not Create the iedc:' . $this->mysqli->error;
            $output[ 'data' ]  = $data;
        }
        return $output;
    }
    function array_to_csv($not_escaped){
//            if(!$not_escaped){
//                return false;
//            }
            
            $escaped = base64_encode($not_escaped);
            return $escaped ;  
    }
    function csv_to_array($escaped){
        if(!$escaped){
                return false;
            } 
           $not_escaped = base64_decode($escaped);
           return $not_escaped ; 
    }
    
    function get_iedc_data( $iedc_id){
         $output = array();
         $sql = 'SELECT DISTINCT * FROM iedc where iedc_id='.(int)$iedc_id;
         
         $result = $this->execute_query($sql);
         $output['data_iedc'] = $result; 
      
       
         foreach($result[0] as $key=>$value){
             if($key=='iip_po_other' || $key=='iip_dkici_other'|| $key=='gs_oicg_other' || $key=='gs_po_other' && !empty($value)){
                 $val = explode(',',$value);
                 
                 foreach ($val as $k => $v) {
                     if(!empty($v)){
                      $output[$key][] = $this->csv_to_array($v) ;
                    }
                    
                 }
             }
             else if($key =='tc_course' || $key =='im_other' || $key=='activityplan'){
                 $val = explode(',',$value);
                 foreach ($val as $k => $v) {
                      if(!empty($v)){
                     $temp = explode(':', $v);
                     foreach($temp as $course){
                         $output[$key][$k][] = $this->csv_to_array($course) ;
                     }
                    } 
                 }
              
             }
             
             else{
                 $output[$key] = $value;
             }
         }
         
          //for grid project
          $sql = "SELECT `projecttitle`, `proponentfirstname`, `proponentlastname`,  `synopsis`, `whethershortlisted`, `finalselection`, `mentorname`, `pitchedtodstaicte`, `funded` from iedc_project_data where iedc_id=" . $iedc_id; 
          $type='grid_project';
          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
//          //for grid panel
          $sql = "SELECT `pp_designation`, `pp_institutionororganisation`, `pp_firstname`, `pp_lastname`, `pp_title`, `pp_areaofexpertise`, `pp_dateofstartingaspanelist`, `pp_noofprojectsreviewed`, `pp_email`, `pp_phone` 
 from iedc_external_panel_list where iedc_id=" . $iedc_id; 
          $type='grid_panel';
          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
           //for grid attendee
          $sql = "SELECT `td_category`, `td_role`, `td_firstname`, `td_lastname`, `td_areaofexpertise`, `td_dateofjoining`, `td_dateofleaving`, `td_email`, `td_phone`  
 from iedc_team_details where iedc_id=" . $iedc_id; 
          $type='grid_attendee';
          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
//          //for grid panel
//          $sql = "SELECT `pp_designation`, `pp_institutionororganisation`, `pp_firstname`, `pp_lastname`, `pp_title`, `pp_areaofexpertise`, `pp_dateofstartingaspanelist`, `pp_noofprojectsreviewed`, `pp_email`, `pp_phone` from iedc_external_panel_list where iedc_id=" . $iedc_id; 
//          $type='grid_panel';
//          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
          
         return $output;
    }
    
    
   
   
   function update_iedc_details( $data,$iedc_id ){
       $output = array( );
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
         
        
        $iedc_stage = $this->isset_null($data['iedc_stage'],$int);
        switch($iedc_stage){
             case 1:
                 $output = $this->update_iedc_stage1($data,$iedc_id);
                break;
             case 2:
                 $output = $this->update_iedc_stage2($data,$iedc_id);
                break;
             case 3:
                 $output = $this->update_iedc_stage3($data,$iedc_id);
                break;
            case 4:
                 $output = $this->update_iedc_stage4($data,$iedc_id);
                break;
            case 5:
                 $output = $this->update_iedc_stage5($data,$iedc_id);
                break;
            case 6:
                 $output = $this->update_iedc_stage6($data,$iedc_id);
                break;
            case 7:
                 $output = $this->update_iedc_stage7($data,$iedc_id);
           case 8:
                 $output = $this->update_iedc_stage8($data,$iedc_id);
                break;
            default;
                break;
        }
        
        return $output;
        
   }
   function update_iedc_stage1($data,$iedc_id){
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
        $bi_edcoriedc = $this->isset_null($data['bi_edcoriedc']);
       if((isset($data['bi_edcoriedc']) && $data['bi_edcoriedc']=='EDC' )|| (isset($data['bi_edcoriedc']) && $data['bi_edcoriedc']=='IEDC')){
            $bi_edcoriedc = $this->isset_null($data['bi_edcoriedc']);
            $bi_foundationyear = $this->isset_null($data['bi_foundationyear'],$int);
            $bi_mgmtbuyinsecured = $this->isset_null($data['bi_mgmtbuyinsecured']);
            $bi_prncplhodbriefed = $this->isset_null($data['bi_prncplhodbriefed']);
            $bi_domeet = $this->isset_null($data['bi_domeet'],$date);
            $bi_planofactiondiscussed = $this->isset_null($data['bi_planofactiondiscussed']);
            $bi_planofactionadopted = $this->isset_null($data['bi_planofactionadopted']);
            $sub_sql = "UPDATE  iedc SET  bi_edcoriedc =  $bi_edcoriedc,
                        bi_foundationyear =  $bi_foundationyear,
                       bi_mgmtbuyinsecured =  $bi_mgmtbuyinsecured,
                    bi_prncplhodbriefed =  $bi_prncplhodbriefed,
                    bi_domeet =  $bi_domeet,
                         bi_planofactionadopted =  $bi_planofactionadopted,
                     bi_planofactiondiscussed =  $bi_planofactiondiscussed";
            
            if($bi_planofactiondiscussed =="'Yes'" && $bi_planofactionadopted=="'Yes'"){
                
                if($data['bi_uploadplanorMinutes']){
                        $bi_uploadplanorMinutes = $data['bi_uploadplanorMinutes'];
                        $sub_sql.=",bi_uploadplanorMinutes =  '$bi_uploadplanorMinutes'";
                      }
                      else{
                          $sub_sql.=",bi_uploadplanorMinutes =  ''";
                      }
            }
            
            if($bi_planofactionadopted!="'Yes'"){
                $sub_sql.=",bi_uploadplanorMinutes =  ''";
            }
           
            
              if($data['bi_availableinfra']){
                  $chk_value = implode(';',$data['bi_availableinfra']);
                  $chk_value_other = isset($data['bi_availableinfra_other']) ? $chk_value.';Other|'.$data['bi_availableinfra_other_value'] : $chk_value ;
                  $sub_sql.=",bi_availableinfra =  '$chk_value_other'";
              }
              else if($data['bi_availableinfra_other']){
                  $chk_value_other = 'Other|'.$data['bi_availableinfra_other_value'] ;
                  $sub_sql.=",bi_availableinfra =  '$chk_value_other'";
                  
              }
              else{
                   $sub_sql.=",bi_availableinfra =  ''";
              }
       }
       
       else{
           $sub_sql = "UPDATE  iedc SET  bi_edcoriedc= $bi_edcoriedc, bi_foundationyear= NULL, bi_mgmtbuyinsecured= '', bi_prncplhodbriefed= '', bi_domeet= '', bi_planofactiondiscussed= '', bi_planofactionadopted= '', bi_uploadplanorMinutes= '', bi_availableinfra='' ";
       }
       
       
       
      
      
      $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
       $result = $this->execute_query($sql);
       //now add the guest list to the system 
       return $sql;
       return $result;
       
       
   }
   function update_iedc_stage2($data,$iedc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
       
            $td_leadfacname = $this->isset_null($data['td_leadfacname']);
            $td_coleadfacname = $this->isset_null($data['td_coleadfacname']);
            $td_noofactivestudents = $this->isset_null($data['td_noofactivestudents'],$int);
            $td_noofadvisorymembers = $this->isset_null($data['td_noofadvisorymembers'],$int);
            $td_dofabmeet = $this->isset_null($data['td_dofabmeet'],$date);
            $td_domrabmeet = $this->isset_null($data['td_domrabmeet'],$date);
            $sub_sql = "UPDATE  iedc SET  td_leadfacname =  $td_leadfacname,
                        td_coleadfacname =  $td_coleadfacname,
                       td_noofactivestudents =  $td_noofactivestudents,
                    td_noofadvisorymembers =  $td_noofadvisorymembers,
                    td_dofabmeet =  $td_dofabmeet,
                     td_domrabmeet =  $td_domrabmeet";
            
             if($data['td_uploadlistofmembers']){
                $td_uploadlistofmembers = $data['td_uploadlistofmembers'];
                $sub_sql.=",td_uploadlistofmembers =  '$td_uploadlistofmembers'";
              }
              
        $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
 $result = $this->execute_query($sql);
       //now add the guest list to the system 
       return $sql;
       return $result;
       
   }
   function update_iedc_stage3($data,$iedc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
         $pc_upgrade = $this->isset_null($data['pc_upgrade']);
         if( $data['pc_upgrade']=='Yes' ){
             $pc_upgrade = $this->isset_null($data['pc_upgrade']);
            $pc_processreviewed = $this->isset_null($data['pc_processreviewed']);
            $pc_nenprerequisites = $this->isset_null($data['pc_nenprerequisites']);
            $pc_isleveladequate = $this->isset_null($data['pc_isleveladequate']);
            $pc_explainadequate = $this->isset_null($data['pc_explainadequate']);
            $pc_iswfstructured = $this->isset_null($data['pc_iswfstructured']);
             $pc_nencriteriaused = $this->isset_null($data['pc_nencriteriaused']);
            $pc_explainwf = $this->isset_null($data['pc_explainwf']);
            $pc_pipused = $this->isset_null($data['pc_pipused']);
             
            
            $sub_sql = "UPDATE  iedc SET  pc_upgrade= $pc_upgrade , pc_processreviewed= $pc_processreviewed , pc_nenprerequisites= $pc_nenprerequisites , pc_isleveladequate= $pc_isleveladequate , pc_explainadequate= $pc_explainadequate , pc_iswfstructured= $pc_iswfstructured , pc_explainwf= $pc_explainwf , pc_nencriteriaused= $pc_nencriteriaused, pc_pipused= $pc_pipused";  
             
         }
         else{
              $sub_sql = "UPDATE  iedc SET  pc_upgrade= $pc_upgrade , pc_processreviewed= '' , pc_nenprerequisites= '' , pc_isleveladequate= '' , pc_explainadequate= '' , pc_iswfstructured= '' , pc_explainwf= '' , pc_nencriteriaused= '', pc_pipused= ''"; 
         }
       
                      
             
              
        $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
        $result = $this->execute_query($sql);
       //now add the guest list to the system 
       return $sql;
       return $result;
       
   }
   
   function update_iedc_stage4($data,$iedc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
        $pp_creatingprojectpipe = $this->isset_null($data['pp_creatingprojectpipe']);
        if($pp_creatingprojectpipe=="'Yes'"){
            $pp_creatingprojectpipe = $this->isset_null($data['pp_creatingprojectpipe']);
            $pp_ongoingproject = $this->isset_null($data['pp_ongoingproject'],$int);
            $pp_avgprojqual = $this->isset_null($data['pp_avgprojqual']);
            $pp_iicompeteplan = $this->isset_null($data['pp_iicompeteplan']);
            $pp_noofprojectsidentified = $this->isset_null($data['pp_noofprojectsidentified'],$int);
            $pp_reqforpanelistsent = $this->isset_null($data['pp_reqforpanelistsent']);
            $pp_extpanelistidentified = $this->isset_null($data['pp_extpanelistidentified']);
            $pp_prsntatnschdlfinalised = $this->isset_null($data['pp_prsntatnschdlfinalised']);
            $pp_dostudentpresentation = $this->isset_null($data['pp_dostudentpresentation'],$date);
            
            
            $sub_sql = "UPDATE  iedc SET   pp_creatingprojectpipe= $pp_creatingprojectpipe,            pp_ongoingproject= $pp_ongoingproject,            pp_avgprojqual= $pp_avgprojqual,            pp_iicompeteplan= $pp_iicompeteplan,            pp_noofprojectsidentified= $pp_noofprojectsidentified,            pp_reqforpanelistsent= $pp_reqforpanelistsent,            pp_extpanelistidentified= $pp_extpanelistidentified,            pp_prsntatnschdlfinalised= $pp_prsntatnschdlfinalised,            pp_dostudentpresentation= $pp_dostudentpresentation";            
             
            
            
            if(isset($data['pp_updatepanelistlist'])){
                    $columns = array('pp_designation', 'pp_institutionororganisation', 'pp_firstname', 'pp_lastname', 'pp_title', 'pp_areaofexpertise', 'pp_dateofstartingaspanelist', 'pp_noofprojectsreviewed', 'pp_email', 'pp_phone');
                    $panel_records = $this->process_datagrid($data['pp_updatepanelistlist'],$columns);
                    $sql = 'DELETE FROM  iedc_external_panel_list where iedc_id='.$iedc_id;
                    $result = $this->execute_query($sql);
                    $cols =  implode(',',$columns);
                    foreach ($panel_records as $key => $value) {
                         $row[]= '(NULL,'.$value.','.$iedc_id.')';
                     }
                     $rows  = implode(",",$row);
                     $sql = "INSERT INTO  iedc_external_panel_list ( id , $cols, iedc_id ) VALUES $rows ;";     
                     $result = $this->execute_query($sql);                     
                     unset($row);unset($result);unset($row);
            } 
        
        $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
        $result = $this->execute_query($sql);
        }
        else{
             $sub_sql = "UPDATE  iedc SET   pp_creatingprojectpipe= $pp_creatingprojectpipe,            pp_ongoingproject=NULL,            pp_avgprojqual= '',            pp_iicompeteplan= '',            pp_noofprojectsidentified=NULL,            pp_reqforpanelistsent= '',            pp_extpanelistidentified= '',            pp_prsntatnschdlfinalised= '',            pp_dostudentpresentation= ''"; 
             $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
             
             $result = $this->execute_query($sql);
              $sql = 'DELETE FROM iedc_project_data where iedc_id='.$iedc_id;
              $result = $this->execute_query($sql);
              $sql = 'DELETE FROM  iedc_external_panel_list where iedc_id='.$iedc_id;
              $result = $this->execute_query($sql);
        }
            
        
       //now add the guest list to the system 
//       return $sql;
       return $result;
       
   }
   function update_iedc_stage5($data,$iedc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
     
       
       //if top checkbox not selected then update with null values 
       $ps_proceeding = $this->isset_null($data['ps_proceeding']);
        if($ps_proceeding=="'Yes'"){
            $ps_proceeding = $this->isset_null($data['ps_proceeding']);
            $ps_selectioncomplete = $this->isset_null($data['ps_selectioncomplete']);
            $ps_domeetforselection = $this->isset_null($data['ps_domeetforselection'],$date);
            $ps_mentorsassigned = $this->isset_null($data['ps_mentorsassigned']);
            $ps_facfortechnopreneur = $this->isset_null($data['ps_facfortechnopreneur']);
            $ps_listofinternalfac = $this->isset_null(implode(';',$data['ps_listofinternalfac']));
            
            $ps_dateofcourse = $this->isset_null($data['ps_dateofcourse'],$date);
            
            
            
            $sub_sql = "UPDATE  iedc SET   			ps_proceeding = $ps_proceeding,            ps_selectioncomplete = $ps_selectioncomplete,            ps_domeetforselection = $ps_domeetforselection,            ps_mentorsassigned = $ps_mentorsassigned,            ps_facfortechnopreneur = $ps_facfortechnopreneur,            ps_listofinternalfac = $ps_listofinternalfac,            ps_dateofcourse = $ps_dateofcourse";            
             
              
        $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
        $result = $this->execute_query($sql);
       //now add the guest list to the system 
//       return $sql;
        
        if(isset($data['iedc_team_details'])){
                    $columns = array('td_category', 'td_role', 'td_firstname', 'td_lastname', 'td_areaofexpertise', 'td_dateofjoining', 'td_dateofleaving', 'td_email', 'td_phone');
                    $panel_records = $this->process_datagrid($data['iedc_team_details'],$columns);
                    $sql = 'DELETE FROM  iedc_team_details where iedc_id='.$iedc_id;
                    $result = $this->execute_query($sql);
                    $cols =  implode(',',$columns);
                    foreach ($panel_records as $key => $value) {
                         $row[]= '(NULL,'.$value.','.$iedc_id.')';
                     }
                     $rows  = implode(",",$row);
                     $sql = "INSERT INTO  iedc_team_details ( id , $cols, iedc_id ) VALUES $rows ;";     
                     $result = $this->execute_query($sql);                     
                     unset($row);unset($result);unset($row);
            }
            
         }
         else{
              $sub_sql = "UPDATE  iedc SET   			ps_proceeding = $ps_proceeding,            ps_selectioncomplete = '',            ps_domeetforselection = '',            ps_mentorsassigned = '',            ps_facfortechnopreneur = '',            ps_listofinternalfac = '',            ps_dateofcourse = ''";         
        $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
         $result = $this->execute_query($sql); 
         $sql = 'DELETE FROM  iedc_team_details where iedc_id='.$iedc_id;
         $result = $this->execute_query($sql);
             
         }
       return $result;
       
   }
   
   function update_iedc_stage6($data,$iedc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
       $pff_seekingfund = $this->isset_null($data['pff_seekingfund']);
        if($pff_seekingfund=="'Yes'"){
            $pff_seekingfund = $this->isset_null($data['pff_seekingfund']);
            $pff_guidelinefollowed = $this->isset_null($data['pff_guidelinefollowed']);
            $pff_feedbackcompleteddate = $this->isset_null($data['pff_feedbackcompleteddate'],$date);
            $pff_proposalssubmitteddate = $this->isset_null($data['pff_proposalssubmitteddate'],$date);
            $pff_uploadcopyofproposal = $this->isset_null($data['pff_uploadcopyofproposal']);
            $pff_dofinalpitch = $this->isset_null($data['pff_dofinalpitch'],$date);
            $pff_submittedtoentity = $this->isset_null($data['pff_submittedtoentity']);
            
            
            
            
            $sub_sql = "UPDATE  iedc SET   			pff_seekingfund = $pff_seekingfund,            pff_guidelinefollowed = $pff_guidelinefollowed,            pff_feedbackcompleteddate = $pff_feedbackcompleteddate,            pff_proposalssubmitteddate = $pff_proposalssubmitteddate,            pff_uploadcopyofproposal = $pff_uploadcopyofproposal,            pff_dofinalpitch = $pff_dofinalpitch,            pff_submittedtoentity = $pff_submittedtoentity";            
           if($data['pff_submittedtoentity']=='Other'){ 
                    $chk_value = 'Other|'.$data['pff_submittedtoentity_other_value'];
                  $sub_sql.=",pff_submittedtoentity =  '$chk_value'";
              } 
            else {
                $chk_value = isset($data['pff_submittedtoentity']) ? $data['pff_submittedtoentity'] : '' ;
                $sub_sql.=",pff_submittedtoentity =  '$chk_value'";
            }
              
        $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
        $result = $this->execute_query($sql);
       }
       else{
            $sub_sql = "UPDATE  iedc SET   			pff_seekingfund = $pff_seekingfund,            pff_guidelinefollowed = '',            pff_feedbackcompleteddate = '',            pff_proposalssubmitteddate = '',            pff_uploadcopyofproposal = '',            pff_dofinalpitch = '',            pff_submittedtoentity = ''";
            $sub_sql.=",pff_submittedtoentity =  ''";
            $sub_sql.=",pff_submittedtoentity =  ''";
             $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
              $result = $this->execute_query($sql);
       }
       //now add the guest list to the system 
       
       return $result;
       
   }
   
   function update_iedc_stage7($data,$iedc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
       $fund_seeking = $this->isset_null($data['fund_seeking']);
        if($fund_seeking=="'Yes'"){
            $fund_seeking = $this->isset_null($data['fund_seeking']);
            $fund_letterreceiveddate = $this->isset_null($data['fund_letterreceiveddate'],$date);
            $fund_amount = $this->isset_null($data['fund_amount'],$int);
            $fund_received = $this->isset_null($data['fund_received'],$int);
            
            
            
            
            
            $sub_sql = "UPDATE  iedc SET fund_seeking = $fund_seeking,            fund_letterreceiveddate = $fund_letterreceiveddate,            fund_amount = $fund_amount,            fund_received = $fund_received";      
            
           if($data['fund_source']=='Other'){ 
                    $chk_value = 'Other|'.$data['fund_source_other_value'];
                  $sub_sql.=",fund_source =  '$chk_value'";
              } 
            else {
                $chk_value = isset($data['fund_source']) ? $data['fund_source'] : '' ;
                $sub_sql.=",fund_source =  '$chk_value'";
            }
              
        $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
        $result = $this->execute_query($sql);
        }
        else{
             $sub_sql = "UPDATE  iedc SET fund_seeking = $fund_seeking,            fund_letterreceiveddate = '',            fund_amount = NULL,            fund_received = NULL";   
               $sub_sql.=",fund_source =  ''";
                $sub_sql.=",fund_source =  ''";
               $sql = $sub_sql.' WHERE  iedc.iedc_id ='.$iedc_id;
            $result = $this->execute_query($sql);
        }
       //now add the guest list to the system 
       return $sql;
       return $result;
       
   }
   function update_iedc_stage8($data,$iedc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
         if(isset($data['pp_updateprojectlist'])){
                    $columns = array('projecttitle', 'proponentfirstname', 'proponentlastname', 'synopsis', 'whethershortlisted', 'finalselection', 'mentorname', 'pitchedtodstaicte', 'funded');
                    $project_records = $this->process_datagrid($data['pp_updateprojectlist'],$columns);
                    $sql = 'DELETE FROM iedc_project_data where iedc_id='.$iedc_id;
                    $result = $this->execute_query($sql);
                    $cols =  implode(',',$columns);
                     foreach ($project_records as $key => $value) {
                         $row[]= '(NULL,'.$value.','.$iedc_id.')';
                     }
                     $rows  = implode(",",$row);
                     $sql = "INSERT INTO iedc_project_data ( id , $cols, iedc_id ) VALUES $rows ;";     
                     $result = $this->execute_query($sql);
                     unset($row);unset($result);unset($row);
                    
            } 
       //now add the guest list to the system 
       
       return $result;
       
   }
    function execute_query($query){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                  $result = $qR->fetch_assoc( );
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
            
        } catch (Exception $e) {
            die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    function sql_date($insertdate,$format = null){
        //$insertdate = mysqli_real_escape_string($insertdate);
        if(!$insertdate ||$insertdate=='' ){
            return '';
        }
//        $insertdate = strtotime($insertdate);
//        if($format){
//             $insertdate = date($format, $insertdate);
//        }
//        else{
//             $insertdate = date('Y-m-d', $insertdate);
//        }
//        
        $temp = explode('-',$insertdate);
        $out = $temp[2].'-'.$temp[1].'-'.$temp[0];
        return $out;
    }
    
    function delete_iedc( $iedcId )
    {
        $output     = array( );
        $activityId = mysqli_real_escape_string( $this->mysqli, $iedcId );
        $sql[ ]     = 'DELETE FROM institute_iedc WHERE iap_id=' . $iedcId;
       
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) {
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = ' Deleted the iedc';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Delete The iedc:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( '; 
', $sql );
        }
    }
    public function validate( $data, $type )
    {
        $status = array( );
        switch ( $type ) {
            case 'numeric':
                if ( is_numeric( $data ) && (int) $data >= 0 ) {
                    return true;
                } //is_numeric( $data ) && (int) $data >= 0
                else {
                    $status[ 'error' ] = 'Please Provide only non negative numbers';
                    return $status;
                }
                break;
            default:
                break;
        } //$type
    }
    
    
    function isset_null($data,$type=false){
        
        if(isset($data)&& !empty($data)){
            if($type=='int'){
                //return is_int($data) ? $data : 0;
                return (int)$data;
                
            }
            else if($type=='date'){
                return "'".$data."'";
            }
            else{
               return "'".$data."'";
            }
            
        }
        
        if($type=='int' || $type=='date'){
            $result  = 'NULL';
        }
        else if($type=='str'){
            $result  = "''";
        }
        else{
            $result  = "''";
        }
        return $result;
    }
    function upload_file( $data, $options=null )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;        
        $allowedMimes = $options[ 'mime' ];
        $fileFormat   = $options[ 'fileFormat' ];
        $size         = $options[ 'size' ];
        $file_name = $data['name'];
        $extension = end(explode(".", $data["name"]));
        if ( ( $data[ 'size' ] == 0 )  ) {
            $goodtogo          = false;
            $result[ 'error' ] = "Could not upload the file: Invalid File Format";
            return $result;
        } //( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size )
        try {
            $target = APPLICATION_PATH . "/../public/uploads/iedc/" . $file_name ;
            //die($target);
            $source = $data[ 'tmp_name' ];
            
            // die($target.'<br/>'.$source);
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = false;
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $result[ 'error' ] = $e->getmessage();
            return $result;
        }
        if ( !$goodtogo ) {
            $result[ 'error' ] = 'File Not Uploaded,Please Try again';
            return $result;
        } //!$goodtogo
        $result[ 'data' ]  = $target;
        $result[ 'error' ] = "";
        return $target;
    }
    public function get_grid_metaData($identifier) {
     $gridMeta['grid_project'] =
                array(
                    
                    array('name' => 'projecttitle', 'label' => 'Project Title', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'proponentfirstname', 'label' => 'Proponent Name (first name)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'proponentlastname', 'label' => 'Proponent Name (last name)', 'datatype' => 'string', 'editable' => true),
                    
                    array('name' => 'synopsis', 'label' => 'Synopsis (50 words)', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'whethershortlisted', 'label' => 'Whether shortlisted (Y/N)', 'datatype' => 'boolean', 'editable' => true),
                     array('name' => 'finalselection', 'label' => 'Final Selection (Y/N)', 'datatype' => 'boolean', 'editable' => false),
                     array('name' => 'mentorname', 'label' => 'Mentor name', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'pitchedtodstaicte', 'label' => 'Pitched to DST/AICTE (Y/N)', 'datatype' => 'boolean', 'editable' => false),
					 array('name' => 'funded', 'label' => 'Funded (Y/N)', 'datatype' => 'boolean', 'editable' => false)
        );
        
        $gridMeta['grid_panel'] =
                array(
                   array('name' => 'pp_designation', 'label' => 'Designation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_institutionororganisation', 'label' => 'Institution/Organisation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_firstname', 'label' => 'First name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_lastname', 'label' => 'Last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_title', 'label' => 'Title (Mr/Ms./Dr./Prof)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_areaofexpertise', 'label' => 'Area of expertise', 'datatype' => 'string', 'editable' => true),                
                    array('name' => 'pp_dateofstartingaspanelist', 'label' => 'Date of Starting as Panelist', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_noofprojectsreviewed', 'label' => 'Number of Projects reviewed', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_email', 'label' => 'email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_phone', 'label' => 'phone', 'datatype' => 'string', 'editable' => true),
        );
         $gridMeta['grid_attendee'] =
                array(
                   array('name' => 'td_category', 'label' => 'Category (Faculty, Student, Expert)', 'datatype' => 'string', 'editable' => true),
                  array('name' => 'td_role', 'label' => 'Role (Advisor, Executive Leader, Executive Member)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_firstname', 'label' => 'First name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_lastname', 'label' => 'Last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_areaofexpertise', 'label' => 'Area of expertise (if applicable)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_dateofjoining', 'label' => 'Date of joining', 'datatype' => 'string', 'editable' => true),                
                    array('name' => 'td_dateofleaving', 'label' => 'Date of leaving', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_email', 'label' => 'email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_phone', 'label' => 'phone', 'datatype' => 'string', 'editable' => true),
                    
        );
         
        
        
        
        if(!$identifier)
            return $gridMeta;
        return $gridMeta[$identifier];
 }
 function get_data_grid($sql,$type){
        $data_grid = array();           
        $data_grid['data'] = array();
        $data_grid['metadata'] = $this->get_grid_metaData($type);
        $i = 0;
        $qR = $this->mysqli->query( $sql );
        while($qRR = $qR->fetch_assoc( )){
                $grid_type_data[] = $qRR;
         }
        
            foreach ($grid_type_data as $mem) {
                
               $data_grid['data'][$i]['id'] = $i;
                $data_grid['data'][$i]['values'] = $mem;
                $i++;
            }
        
         
         return $data_grid;
  } 
  
  function process_datagrid($data,$columns){
        $out = array();
        $temp = array();
        $data_grid = json_decode($data);
        foreach($data_grid as $key=>$val){
                $data_grid_data =  (array)$val->values; 
                $data_grid_vals = (array)$val->columns; 
                foreach($data_grid_vals as $k=>$v){
                   if(TRUE){ //change true to column key comparision( if(in_array($k,$columns))   ) for letting only those entries as result
                        $temp[] = "'$v'";  
                    }
                }
                
                $out[$key] = implode(',',$temp);
                unset($temp);
                
            }
        
        return $out;
    }
  
  
}

 