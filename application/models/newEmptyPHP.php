 $cols = 'iap_id,
            iip_currentlevel,
            iip_po_entrepreneurcreated,
            iip_po_entrepreneursupported ,
            iip_po_jobscreated,
            iip_po_other,
            iip_dkici_humanresources ,
            iip_dkici_infrastructureplatforms,
            iip_dkici_programsnactivities,
            iip_dkici_other,
            created,
            institute_id,
            plan_stage';
        
        $vals = 'NULL,'
                .$iip_currentlevel.','.
                ''.$iip_po_entrepreneurcreated.','.
                ''.$iip_po_entrepreneursupported.','.
                ''.$iip_po_jobscreated.','.
                '"'.$iip_po_other.'",'.
                '"'.$iip_dkici_humanresources.'",'.
                '"'.$iip_dkici_infrastructureplatforms.'",'.
                '"'.$iip_dkici_programsnactivities.'",'.
                '"'.$iip_dkici_other.'",NOW(),"'.$institute_id.'","'.$plan_stage.'"';
        
        $sql  = 'INSERT INTO inst_annual_plan(' . $cols . ') VALUES(' . $vals . ')';