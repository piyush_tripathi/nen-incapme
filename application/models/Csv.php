<?php

class Application_Model_Csv {

    protected $_db;
    public $name;
    protected $targetPath;
    protected $errMsg = '';

    const MAX_MEMBER_FILE_SIZE = 90240; //1 MB
    const ECELL_FILE_UPLOAD_PATH = './uploads/ecell/';
    const ECELL_MEMBERS_FILE_NAME = 'members.csv';
    const ECELL_ACTIVITY_CALENDER_FILE_NAME = 'activity_calender.xls';
    const ECLL_ESTAB_PROFESSIONALS = 'estab_professionals.csv';
    const ECELL_ESTABLISH_REPORT_FILE_NAME = 'establish_report.xls';
    const ECELL_LATEST_NEWSLETTER_FILE_NAME = 'newsletter.xls';
    const CAMPUS_FILE_UPLOAD_PATH = './uploads/campus/';
    const CAMPUS_MEMBERS_FILE_NAME = 'students.csv';
    const CAMPUS_ADVISORS_FILE_NAME = 'advisors.csv';
    const EDC_FILE_UPLOAD_PATH = './uploads/edc/';
    const EDC_MEMBERS_FILE_NAME = 'students.csv';
    const EDC_ADVISORS_FILE_NAME = 'advisors.csv';
    const EDC_PROG_FILE_NAME = 'program_plan.xls';
    
    

    public function __construct($name) {
        if ($name)
            $this->name = $name;
    }

    public function isCsvFile() {
        //print_r($_FILES); 
        $mimes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/tsv', 'application/octet-stream');
        if (in_array($_FILES[$this->name]['type'], $mimes)) {
            return true;
        }
        $this->errMsg = 'Invalid file format';
        return false;
    }

    public function isValidSize() {
        if (($_FILES[$this->name]['size'] == 0) || ($_FILES[$this->name]['size'] > self::MAX_MEMBER_FILE_SIZE )) {
            $this->errMsg = 'File size error';
            return false;
        }
        return true;
    }

    function processCsv() {
        if (!($this->isValidSize() && $this->isCsvFile())) {
            return array('status' => 'error', 'errmsg' => $this->errMsg);
        }


        switch ($this->name) {
            case 'overview_members': // for ecell
                if ($this->errMsg)
                    return array('status' => 'error', 'errmsg' => $this->errMsg);
                $result = $this->processOverviewMembers();
                return $result;
                break;
            case 'estab_professionals': // for ecell
                if ($this->errMsg)
                    return array('status' => 'error', 'errmsg' => $this->errMsg);
                $result = $this->processEstabProfessionals();
                return $result;
                break;
                
            case 'students_members': //for campus company
                if ($this->errMsg)
                    return array('status' => 'error', 'errmsg' => $this->errMsg);
                $result = $this->processStudentAdvisors('students_members');
                return $result;
                break;
            case 'advisors': //for campus company
                if ($this->errMsg)
                    return array('status' => 'error', 'errmsg' => $this->errMsg);
                $result = $this->processStudentAdvisors('advisors');
                return $result;
                break;
            case 'edc_advisors': //for edc
                if ($this->errMsg)
                    return array('status' => 'error', 'errmsg' => $this->errMsg);
                $result = $this->processStudentAdvisors('edc_advisors'); 
                return $result;
                break;
            case 'edc_members': //for edc members
                if ($this->errMsg)
                    return array('status' => 'error', 'errmsg' => $this->errMsg);
                $result = $this->processStudentAdvisors('edc_members');
                return $result;
                break;
           
                
                
                
        }
    }

    function processUpload() {
        if (!($this->isValidSize())) {
            return array('status' => 'error', 'errmsg' => $this->errMsg);
        }

        switch ($this->name) {

            case 'startup_calen':
                $id = $_POST['ecellid'];
                $target = self::ECELL_FILE_UPLOAD_PATH . $id.'_'. self::ECELL_ACTIVITY_CALENDER_FILE_NAME;
                break;

            case 'estab_uploadrep':
                $id = $_POST['ecellid'];
                $target = self::ECELL_FILE_UPLOAD_PATH . $id.'_'. self::ECELL_ESTABLISH_REPORT_FILE_NAME;
                break;
            case 'hiperf_latestnews':
                $id = $_POST['ecellid'];
                $target = self::ECELL_FILE_UPLOAD_PATH . $id.'_'. self::ECELL_LATEST_NEWSLETTER_FILE_NAME;
                break;
            case 'edprog_upload': //for edc members
                $id = $_POST['eecid'];
                $target = self::EDC_FILE_UPLOAD_PATH . $id.'_'. self::EDC_PROG_FILE_NAME;
                break;

            default:
                $target = '';
        }

        if ($target) {
            $source = $_FILES[$this->name]['tmp_name'];
            if (!move_uploaded_file($source, $target)) {
                return array('status' => 'error',  'errmsg' => 'Unable to upload file');
            } else {
                return array('status' => 'success',  'link' => $target);
            }
        }
        return false;
    }

    public function processOverviewMembers() {
        $target = self::ECELL_FILE_UPLOAD_PATH . self::ECELL_MEMBERS_FILE_NAME;
        $goodtogo = true;

        try {
            $source = $_FILES[$this->name]['tmp_name'];
            if (!move_uploaded_file($source, $target)) {
                $goodtogo = false;
                throw new exception("Unable to move file");
            }
        } catch (exception $e) {
            $this->errMsg = $e->getmessage();
        }

        /** For Ecell members * */
        if ($goodtogo) {
            $row = 0;
//            $fields = array("name", "email", "contact_no", "year_grad", "is_e_leader");
             $fields = array("name", "email", "contact_no", "year_grad", "is_e_leader");
            
            $rowsMembers = $rowsEleaders = array();
            if (($handle = fopen($target, "r")) !== FALSE) {
                $eleadersCount = 0;
                
                $_itemELeaders = $_items = array();
                while (($line = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row === 0) {
                        $cols = $line;
                    } else {
                        
                        //print_r($line);
                        if (strtolower(trim($line[4])) == 'y' || strtolower(trim($line[4])) == 'yes' || strtolower(trim($line[4])) == 1) {
                            $line[4] = 1;
                            $eleadersCount++;
                            $isEleader = true;
                        } else {
                            $line[4] = 0;
                            $isEleader = false;
                        }
                        foreach ($fields as $pos => $fieldName) {
                            $rowsMembers[$row - 1][$fieldName] = $line[$pos];
                            if($isEleader) {
                                $rowsEleaders[$row - 1][$fieldName] = $line[$pos];
                            }
                        }
                        $_items[$row-1] = array('id' => $row, 'values' => $rowsMembers[$row-1]);
                        if($isEleader) {
                            $_itemELeaders[$eleadersCount-1] = array('id' => $row, 'values' => $rowsEleaders[$row-1]);
                            
                        }
                    }
                    $row++;
                }
                fclose($handle);
            }

            $ecell['over_num'] = (int) count($rowsMembers);
            $ecell['over_eleadnum'] = (int) count($rowsEleaders);
            $campus['student_members'] = $_items;
            
            $ecell['over_members'] = $_items;
            $ecell['over_eleaders'] = $_itemELeaders;
            return $ecell;
        }
        //return false;
    }

    public function processStudentAdvisors($type) {
        if($type == 'students_members') {
            $target = self::CAMPUS_FILE_UPLOAD_PATH . self::CAMPUS_MEMBERS_FILE_NAME;
            $fields = array('designation', 'first_name', 'last_name', 'dob', 'sex', 'email', 'contact_no', 'start_date', 'remarks');
        } else if($type == 'advisors') {
            $target = self::CAMPUS_FILE_UPLOAD_PATH . self::CAMPUS_ADVISORS_FILE_NAME;
            $fields = array('designation', 'institute', 'first_name', 'last_name', 'apellation', 'date_of_joining', 'date_of_leaving', 'remarks');
        }else if($type == 'edc_members') {
            $target = self::EDC_FILE_UPLOAD_PATH . self::EDC_MEMBERS_FILE_NAME;
            $fields = array('designation', 'first_name', 'last_name', 'dob', 'sex', 'email', 'contact_no', 'start_date', 'remarks');
        }else if($type == 'edc_advisors') {
            $target = self::EDC_FILE_UPLOAD_PATH . self::EDC_ADVISORS_FILE_NAME;
            $fields = array('designation', 'institute', 'first_name', 'last_name', 'apellation', 'date_of_joining', 'date_of_leaving', 'remarks');
        }
        $goodtogo = true;

        try {
            $source = $_FILES[$this->name]['tmp_name'];
            if (!move_uploaded_file($source, $target)) {
                $goodtogo = false;
                throw new exception("Unable to move file");
            }
        } catch (exception $e) {
            $this->errMsg = $e->getmessage();
        }

        /** For Campus members * */
        if ($goodtogo) {
            $row = 0;
            $rowsMembers = $rowsEleaders = array();
            if (($handle = fopen($target, "r")) !== FALSE) {
                $eleadersCount = 0;
                while (($line = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row === 0) {
                        $cols = $line;
                    } else {
                        foreach ($fields as $pos => $fieldName) {
                            //echo $line[$pos] ."|";
                            if($line[$pos] == null) {
                                //echo "null";
                            }
                            $rowsMembers[$row - 1][$fieldName] = $line[$pos];
                            
                        }
                        $_items[$row-1] = array('id' => $row, 'values' => $rowsMembers[$row-1]);
                    }
                    $row++;
                }
                fclose($handle);
            }
            if($type == 'students_members') {
                $campus['student_members'] = $_items;
            } else if ($type == 'advisors') {
                $campus['advisors'] = $_items;
            } elseif ($type == 'edc_members') {
                $campus['edc_members'] = $_items;
            } elseif($type == 'edc_advisors') {
                $campus['edc_advisors'] = $_items;
            }
            return $campus;
        }
    }
    
    public function processEstabProfessionals() {
        $goodtogo = true;
        $target = self::ECELL_FILE_UPLOAD_PATH . self::ECLL_ESTAB_PROFESSIONALS;
//        $fields = array("name", "organisation", "designation", "email", "contact_no");Organization
        $fields = array("name", "organization", "designation", "email", "contact_no");
        try {
            $source = $_FILES[$this->name]['tmp_name'];
            if (!move_uploaded_file($source, $target)) {
                $goodtogo = false;
                throw new exception("Unable to move file");
            }
        } catch (exception $e) {
            $this->errMsg = $e->getmessage();
        }

        /** For Campus members * */
        if ($goodtogo) {
            $row = 0;
            $rowsMembers =  array();
            if (($handle = fopen($target, "r")) !== FALSE) {
                $eleadersCount = 0;
                while (($line = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row === 0) {
                        $cols = $line;
                    } else {
                        foreach ($fields as $pos => $fieldName) {
                            $rowsMembers[$row - 1][$fieldName] = $line[$pos];
                        }
                        $_items[$row-1] = array('id' => $row, 'values' => $rowsMembers[$row-1]);
                    }
                    $row++;
                }
                fclose($handle);
            }
            $ecell['estab_professionals'] = $_items;
            return $ecell;
        }
    }

    //return false;
}

