<?php

class Ecell {

    private $id = 0;
    private $db = null;
    public $data = array();
    public $ecellId = '1';

    public function __construct($id = null, $fetch = false) {
        // error_reporting(0);
        $this->db = new Application_Model_Edb();
        $this->commonModel = new Application_Model_Common();
        if ($id) {
            $this->ecellId = $id;
        }
        if ($fetch) {
            $q = "
                Select *,
                over_date as over_date,
                date_format(over_launch, '%d-%m-%Y') as over_launch, 
                date_format(hiperf_signdate,  '%d-%m-%Y') as hiperf_signdate
                from ecell where id = " . $this->ecellId;
            $this->data = $this->db->fetchOne($q);
            $this->data['over_lead'] = $this->getFacultyNameAndId($this->data['over_lead']);
            $this->data['over_second'] = $this->getFacultyNameAndId($this->data['over_second']);
            $this->data['startup_board_faculty'] = $this->getFacultyNameAndId($this->data['startup_board_faculty']);
            $this->fetchMembers();
            $this->fetchEstabProfessionals();
            $this->fetchEstabSponsors();
        }
    }

    public function setEcellData($data) {
        $this->data = $data;
    }

    public function setId($id) {
        $this->ecellId = $id;
    }

    public function getFacultyNameAndId($id) {
        if (!$id)
            return null;
        $q = "select first_name, last_name from human_resource where id = $id";
        if ($res = $this->db->fetchOne($q)) {
            return array('value' => $id, 'caption' => $res['first_name'] . ' ' . $res['last_name']);
        }
        return false;
    }

    public function fetchMembers() {
        $q = "Select name, email, contact_no, year_grad, is_e_leader from ecell_over_mem where ecell_id=" . $this->ecellId;
        $this->data['over_members']['data'][0] = array();
        $this->data['over_members']['metadata'] = $this->commonModel->getGridMetaData('ecell_members');
        $i = 0;
        if ($over_members = $this->db->fetchAll($q)) {
            foreach ($over_members as $mem) {
                $mem = sanitize_null($mem);
                $this->data['over_members']['data'][$i]['id'] = $i;
                $this->data['over_members']['data'][$i]['values'] = $mem;
                $i++;
            }
        }
    }

    public function fetchEstabSponsors() {
        $q = "Select * from ecell_estab_sponsors where ecell_id=" . $this->ecellId;

        $this->data['estab_sponsors'] = $this->db->fetchAll($q);
    }

    public function fetchEstabProfessionals() {
        $q = "Select * from ecell_estab_professionals where ecell_id=" . $this->ecellId;
        $this->data['estab_professionals']['data'][0] = array();
        $this->data['estab_professionals']['metadata'] = $this->commonModel->getGridMetaData('ecell_estab_professionals');
        $i = 0;
        if ($estab_professionals = $this->db->fetchAll($q)) {
            foreach ($estab_professionals as $mem) {
                $mem = sanitize_null($mem);
                $this->data['estab_professionals']['data'][$i]['id'] = $i;
                $this->data['estab_professionals']['data'][$i]['values'] = $mem;
                $i++;
            }
        }
    }

    public function getJsonData() {
        return json_encode($this->data);
    }

    public function getData() {
        return $this->data;
    }

    public function updateOverview() {
        $over_date = $this->db->filter($this->data['over_date'], '');
        $over_name = $this->db->filter($this->data['over_name'], '');
        $over_lead = $this->db->filter($this->data['over_lead']['value'], '');
        $over_second = $this->db->filter($this->data['over_second']['value'], '');
        $over_strength = $this->db->filter($this->data['over_strength'], '');
        $over_num = $this->db->filter($this->data['over_num'], '');
        $over_eleadnum = $this->db->filter($this->data['over_eleadnum'], '');
        $over_pointofcontact1 = $this->db->filter($this->data['over_pointofcontact1']['value'], '');
        $over_pointofcontact2 = $this->db->filter($this->data['over_pointofcontact2']['value'], '');
        $over_launch = $this->db->filter($this->data['over_launch'], 'date');
        $qs = array();
        $q = "UPDATE ecell SET over_date = $over_date, over_name = $over_name, over_lead = $over_lead, over_second = $over_second, over_strength = $over_strength, over_num = $over_num, over_eleadnum = $over_eleadnum, over_pointofcontact1 = $over_pointofcontact1, over_pointofcontact2 = $over_pointofcontact2, over_launch = $over_launch  WHERE id = $this->ecellId";
        $qs[] = $q;
        $qm = $this->updateOverMembers();
        $query = array_merge($qs, $qm);
        return $this->db->mqueryTx($query);
    }

    private function updateOverMembers() {
        $overMembers = $this->data['over_members']['data'];
        //print_r($overMembers);
        $q = array();

        if (is_array($overMembers)) {
            $dq = "DELETE FROM ecell_over_mem WHERE ecell_id = '$this->ecellId'";
            $q = array($dq);
            foreach ($overMembers as $amem) {
                $mem = $amem['values'];
                $name = $this->db->filter($mem['name'], '');
                $email = $this->db->filter($mem['email'], '');
                $contact_no = $this->db->filter($mem['contact_no'], '');
                $year_grad = $this->db->filter($mem['year_grad'], '');
                $is_e_leader = $this->db->filter($mem['is_e_leader'], 'numeric_bool');

                $q[] = "INSERT INTO ecell_over_mem (name, email, contact_no, year_grad, is_e_leader, ecell_id) VALUES
                                                   ($name, $email, $contact_no, $year_grad, $is_e_leader, $this->ecellId)";
            }
        }
        return $q;
    }

    public function updateStartup() {
        $startup_roles = $this->db->filter($this->data['startup_roles'], '');
        $startup_facadv = $this->db->filter($this->data['startup_facadv'], '');
        $startup_board_faculty = $this->db->filter($this->data['startup_board_faculty']['value'], '');
        $startup_success = $this->db->filter($this->data['startup_success'], '');
        $startup_plan = $this->db->filter($this->data['startup_plan'], '');
        $startup_calen = $this->db->filter($this->data['startup_calen'],'');

        if (!$startup_facadv) {
            $startup_board_faculty = NULL;
        }
        
        if($this->data['startup_calen']!=''){            
            $sub_sql = " , startup_calen = $startup_calen ";
        }
        
        if ($this->data['startup_plan']=='0') {            
            $sub_sql = " , startup_calen = '' ";
        }
           

        $q = "UPDATE ecell SET startup_roles = $startup_roles, startup_facadv = $startup_facadv, startup_board_faculty = $startup_board_faculty, startup_success = $startup_success, startup_plan = $startup_plan $sub_sql  WHERE id = $this->ecellId";
        
        return $this->db->queryTx($q);
    }

    public function updateEstab() {
        $estab_prof = $this->db->filter($this->data['estab_prof'], 'numeric_bool');
        $estab_market = $this->db->filter($this->data['estab_market'], '');
        $estab_visible = $this->db->filter($this->data['estab_visible'], '');
        $estab_money = $this->db->filter($this->data['estab_money'], 'numeric_bool');
        $estab_report = $this->db->filter($this->data['estab_report'], 'numeric_bool');
        $estab_uploadrep = $this->db->filter($this->data['estab_uploadrep'], '');
        if($this->data['estab_uploadrep']!=''){
                    $sub_sql = ", estab_uploadrep = $estab_uploadrep ";
        }
        
         if($this->data['estab_report']=='0'){
                    $sub_sql = ", estab_uploadrep = '' ";
        }
        
        $q = "UPDATE ecell SET estab_prof = $estab_prof, estab_market = $estab_market, estab_visible = $estab_visible, estab_money = $estab_money , estab_report = $estab_report  $sub_sql WHERE id = $this->ecellId";
        $qs[] = $q;

        if ($estab_prof) {
            $qprofessionals = $this->updateEstabProfessionals();
        } else {
            $qprofessionals = array("DELETE FROM ecell_estab_professionals WHERE ecell_id = '$this->ecellId'");
        }


        if ($estab_money) {
            $qsponsors = $this->updateEstabSponsors();
        } else {
            $qsponsors = array("DELETE FROM ecell_estab_sponsors WHERE ecell_id = '$this->ecellId'");
        }

        $query = array_merge($qs, $qprofessionals, $qsponsors);
        return $this->db->mqueryTx($query);
    }

    public function updateEstabSponsors() {
        $estabSponsors = $this->data['estab_sponsors'];
        $q = array();
        $dq = "DELETE FROM ecell_estab_sponsors WHERE ecell_id = '$this->ecellId'";
        $q[] = $dq;
        if (is_array($estabSponsors)) {

            foreach ($estabSponsors as $sponsor) {
                $estab_sponsor = $this->db->filter($sponsor['estab_sponsor'], 'string');
                $estab_amount = $this->db->filter($sponsor['estab_amount'], 'amount');

                $q[] = "INSERT INTO ecell_estab_sponsors (estab_sponsor, estab_amount, ecell_id) VALUES($estab_sponsor, $estab_amount, $this->ecellId)";
            }
        }
        return $q;
    }

    private function updateEstabProfessionals() {
        $estabProfessionals = $this->data['estab_professionals']['data'];
        $q = array();

        if (is_array($estabProfessionals)) {
            $dq = "DELETE FROM ecell_estab_professionals WHERE ecell_id = '$this->ecellId'";
            $q = array($dq);
            foreach ($estabProfessionals as $amem) {
                $mem = $amem['values'];
                $name = $this->db->filter($mem['name'], '');
                $organization = $this->db->filter($mem['organization'], '');
                $designation = $this->db->filter($mem['designation'], '');
                $email = $this->db->filter($mem['email'], '');
                $contact_no = $this->db->filter($mem['contact_no'], '');


//                $q[] = "INSERT INTO ecell_estab_professionals (name, email, contact_no, designation, organization, ecell_id) VALUES
//                                                   ($name, $email, $contact_no, $designation, $organization, $this->ecellId)";
                $q[] = "INSERT INTO ecell_estab_professionals (name, email, contact_no, designation, organisation, ecell_id) VALUES
                                                   ($name, $email, $contact_no, $designation, $organization, $this->ecellId)";
            }
        }
        return $q;
    }

    public function updateHiperf() {
        $hiperf_news = $this->db->filter($this->data['hiperf_news'], 'numeric_bool');
        $hiperf_latestnews = $this->db->filter($this->data['hiperf_latestnews'], '');
        $hiperf_web = $this->db->filter($this->data['hiperf_web'], 'numeric_bool');
        $hiperf_link = $this->db->filter($this->data['hiperf_link'], '');
        $hiperf_support = $this->db->filter($this->data['hiperf_support'], 'numeric_bool');
        $hiperf_supportevidence = $this->db->filter($this->data['hiperf_supportevidence'], '');
        $hiperf_sign = $this->db->filter($this->data['hiperf_sign'], 'numeric_bool');
        $hiperf_signtitle = $this->db->filter($this->data['hiperf_signtitle'], '');
        $hiperf_signdate = $this->db->filter($this->data['hiperf_signdate'], 'date');
         if($this->data['hiperf_latestnews']!=''){
                    $sub_sql = ", hiperf_latestnews = $hiperf_latestnews ";
        }
        
          if($this->data['hiperf_news']=='0'){
                     $sub_sql = ", hiperf_latestnews = '' ";
        }
        
        if (!$hiperf_web) {
            $hiperf_link = "''";
        }
        if (!$hiperf_support) {
            $hiperf_supportevidence = "''";
        }
        if (!$hiperf_sign) {
            $hiperf_signtitle = "''";
            $hiperf_signdate = 'null';
        }

        $q = "UPDATE ecell SET hiperf_news = $hiperf_news, hiperf_web = $hiperf_web, hiperf_link = $hiperf_link, hiperf_support = $hiperf_support, hiperf_supportevidence = $hiperf_supportevidence, hiperf_sign = $hiperf_sign, hiperf_signtitle = $hiperf_signtitle, hiperf_signdate = $hiperf_signdate  $sub_sql WHERE id = $this->ecellId";
        return $this->db->queryTx($q);
    }

}

class Application_Model_Ecell {

    public function __construct($ecellId = null) {
        $this->ecellId = $ecellId;
        $this->data = new Ecell($this->ecellId, true);
        ;
    }

    public function update() {
        if (isset($_POST['data'])) {

            $ecell = new Ecell();
            if (isset($_POST['ecellid'])) {
                $ecell->setId($_POST['ecellid']);
            }
            $ecell->setEcellData($_POST['data']);

            if ($block = $_POST['block']) {
                switch ($block) {
                    case 'overview':
                        $res = $ecell->updateOverview();
                        break;
                    case 'startup':
                        $res = $ecell->updateStartup();
                        break;
                    case 'establish':
                        $res = $ecell->updateEstab();
                        break;
                    case 'hiperformance':
                        $res = $ecell->updateHiperf();
                        break;

                    default:
                        # code...
                        break;
                }
            }
            return $res;
        }
    }

    public function get() {
        if ($_POST['ecellid']) {
            $ecell = new Ecell($_POST['ecellid'], true);
            header('Content-Type: text/json');
            return $ecell->getData();
        }
    }

    public function uploadmemberscsv() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $ecellCsv = new Application_Model_Csv($name);

            $result = $ecellCsv->processCsv();
        }
        return $result;
    }

    public function uploadestabprofessionals() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $ecellCsv = new Application_Model_Csv($name);
            $result = $ecellCsv->processCsv();
        }
        return $result;
    }

    public function uploadfiles() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $ecellCsv = new Application_Model_Csv($name);
            $result = $ecellCsv->processUpload();
        }
        return $result;
    }
    function upload_file( $data, $options=null )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;        
        $allowedMimes = $options[ 'mime' ];
        $fileFormat   = $options[ 'fileFormat' ];
        $size         = $options[ 'size' ];
         $file_name = $data['name'];
        if ( ( $data[ 'size' ] == 0 )  ) {
            $goodtogo          = false;
            $result[ 'error' ] = "Could not upload the file: Invalid File Format";
            return null;
        } //( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size )
        try {
            $target = APPLICATION_PATH . "/../public/uploads/ecell/" . $file_name ;
            //die($target);
            $source = $data[ 'tmp_name' ];
            // die($target.'<br/>'.$source);
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = 'false';
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $result[ 'error' ] = $e->getmessage();
            return null;
        }
        if ( !$goodtogo ) {
            $result[ 'error' ] = 'File Not Uploaded,Please Try again';
            return null;
        } //!$goodtogo
        $result[ 'data' ]  = $target;
        $result[ 'error' ] = "";
        return $target;
    }

}

