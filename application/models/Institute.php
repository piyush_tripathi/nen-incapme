<?php
class Application_Model_Institute extends Application_Model_Db
{ /** 
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' );
       
            
    }
    
    
    
   function create_institute( $data ){
       //now checking which data element is top be updated 
       $name=$data['name'];
       $category=$this->isset_null($data['category']);
       $type=$this->isset_null($data['type']);
       $nen_joining_date=$this->isset_null($data['nen_joining_date'],$date);
       $city_id=$this->isset_null($data['city_id'],$int);
       $primary_zip_code=$this->isset_null($data['primary_zip_code']);
       $primary_address_1=$this->isset_null($data['primary_address_1']);
       $primary_address_2=$this->isset_null($data['primary_address_2']);
       
       if(!$name || !$city_id || !$type ){
         return false;
       }
       //good to go 
           $sql = "INSERT INTO institute(id,name,category,type,nen_joining_date,city_id,primary_zip_code,primary_address_1,primary_address_2) values (NULL,".'"'.$name.'"'.",$category,$type,$nen_joining_date,$city_id,$primary_zip_code,$primary_address_1,$primary_address_2)";;
           
           $qR   = $this->mysqli->query( $sql );
           $institute_id = $this->mysqli->insert_id; 
           
           //now update promary city text fpr index listing 
           $city_name  =  $this->get_city_name($city_id);
          
           $sql  = "UPDATE institute SET primary_city='$city_name' where id=$institute_id";
           $this->execute_query($sql);
           
           //add this institute to venue table for activity venue
           $sql  = "INSERT INTO venue (id, name, description, city_id, venue_type, institute_id) VALUES (NULL, $name,'' ,$city_id, 'INS', $institute_id)";
           $this->execute_query($sql);
           
       if(!$institute_id){
           $output['success']= false;
           return $output;
       }   
        $output['success']= true;
        $output['url'] = "/institute/edit?instituteid=$institute_id";
        return $output;
        
   }
    
    function get_institute_data( $institute_id){
         $output = array();        
         $type= 'multi';
        //if user is consultant then show his list of institutes 
         $sql = "SELECT * FROM institute where id=$institute_id";
         $output['institute_data'] = $this->execute_query($sql);
         $output['is_admin'] = $this->is_admin();
        //only admin can change institutes
         if($this->is_admin()){
             $sql   = 'SELECT * from city';
             $output['cities'] = $this->execute_query($sql,$type); 
         }
         return $output;
    }
   function get_city_name($city_id){
        $output = array();
        $query  = 'SELECT name from city where city_id ='.$city_id;
        $output = $this->execute_query($query);         
        return $output['name'];          
        
    } 
     
   function is_lead_faculty($human_id,$institute_id=NULL){
       
              $sql = 'SELECT count(*) as count from institute where head_of_institute='.$human_id;
              $count = $this->execute_query($sql); 
              if($count['count']){
                  $result = true;
              }else{
                 $result = false;
              }
              return $result; 
   }
   function is_admin(){
       $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $role = $user->role;
              
              if($role=='ADMIN'){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   
   function is_consultant($human_id){
       if($human_id){
           $sql = 'SELECT count(*) as count from institute where head_of_institute='.$human_id;
           $count = $this->execute_query($sql); 
              if($count['count']>=1){
                  $result = true;
              }else{
                 $result = false;
              }
              return $result;
       }  
   }
   
   function update_institute_details( $data,$institute_id){
       $output = array( );
       
      
        $int  = 'int';
        $rr = array();
        $str = 'str';
        $date  = 'date';
        $is_admin = false;
        $temp = array();
        $status = true;
        //now checking which data element is top be updated 
        $abbreviation=$this->isset_null($data['abbreviation']);
        $campus_company_exists=$this->check_exists($data['campus_company_exists']);
        $category=$this->isset_null($data['category']);
        $city_id=$this->isset_null($data['city_id'],$int);
        $consultant_in_charge=$this->isset_null($data['consultant_in_charge'],$int);
        $e_cell_exists=$this->check_exists($data['e_cell_exists']);
        $e_cell_level=$this->isset_null($data['e_cell_level'],$int);
        $edc_exists=$this->check_exists($data['edc_exists']);
        $geographic_region=$this->isset_null($data['geographic_region']);
        $head_of_institute=$this->isset_null($data['head_of_institute'],$int);
        $iedc_exists=$this->check_exists($data['iedc_exists']);
        $incubator_exists=$this->check_exists($data['incubator_exists']);
        $institute_level=$this->isset_null($data['institute_level']);
        $jurisdiction=$this->isset_null($data['jurisdiction']);
        $mailing_address_report=$this->isset_null($data['mailing_address_report']);
        $management=$this->isset_null($data['management']);
        $member_status=$this->isset_null($data['member_status']);
        $mentoring_unit_exists=$this->check_exists($data['mentoring_unit_exists']);
        $name=$this->isset_null($data['name']);
        $nen_administrative_classification=$this->isset_null($data['nen_administrative_classification']);
        $nen_category=$this->isset_null($data['nen_category']);
        $nen_joining_date=$this->isset_null($data['nen_joining_date'],$date);
        $nen_territory_grouping=$this->isset_null($data['nen_territory_grouping']);
        $number_of_students=$this->isset_null($data['number_of_students'],$int);
        $patent_office_exists=$this->check_exists($data['patent_office_exists']);
        $primary_address_1=$this->isset_null($data['primary_address_1']);
        $primary_address_2=$this->isset_null($data['primary_address_2']);
        $primary_city=$this->isset_null($data['primary_city']);
        $primary_state=$this->isset_null($data['primary_state']);
        $primary_zip_code=$this->isset_null($data['primary_zip_code']);
        $profile=$this->isset_null($data['profile']);
        $sustainability=$this->isset_null($data['sustainability']);
        $type=$this->isset_null($data['type']);

        
        //conditional check on fields
        $sql = "UPDATE institute SET abbreviation=$abbreviation,campus_company_exists=$campus_company_exists,category=$category,city_id=$city_id,consultant_in_charge=$consultant_in_charge,e_cell_exists=$e_cell_exists,e_cell_level=$e_cell_level,edc_exists=$edc_exists,geographic_region=$geographic_region,head_of_institute=$head_of_institute,iedc_exists=$iedc_exists,incubator_exists=$incubator_exists,institute_level=$institute_level,jurisdiction=$jurisdiction,mailing_address_report=$mailing_address_report,management=$management,member_status=$member_status,mentoring_unit_exists=$mentoring_unit_exists,name=$name,nen_administrative_classification=$nen_administrative_classification,nen_category=$nen_category,nen_joining_date=$nen_joining_date,nen_territory_grouping=$nen_territory_grouping,number_of_students=$number_of_students,patent_office_exists=$patent_office_exists,primary_address_1=$primary_address_1,primary_address_2=$primary_address_2,primary_city=$primary_city,primary_state=$primary_state,primary_zip_code=$primary_zip_code,profile=$profile,sustainability=$sustainability,type=$type where id=$institute_id" ;
        
        $this->execute_query($sql);
       //for updated city text in listing page  
           $city_name  =  $this->get_city_name($city_id);
           $sql  = "UPDATE institute SET primary_city='$city_name' where id=$institute_id";
           $this->execute_query($sql);
            
     if($status){
         $output['status'] = TRUE; 
         $output['data'] = "Institute has been updated" ;
         $sql  = "UPDATE venue SET name=$name where institute_id=$institute_id";
         $this->execute_query($sql);
      }
      else{
         $output['status'] = FALSE; 
         $output['data'] = "Institute could not be updated" ;
      }
       return $output;
       
       
   } 
   function check_exists($data){
       if(!isset($data)){
           return 0;
       }
       else if($data=='Yes'){
           return 1;
       }
       else{
           return 0;
       }
   }
   
   function delete_institute( $institute_id ){
    
       $output = array();
       //if user is faculty : delete all institute associated > delete from user table > delete from human_resource
       $query = "DELETE FROM institute where id=$institute_id";
       $result  = $this->execute_query($query);
       if(isset($result['success'])){
           $output['status'] = TRUE;
           $output['data'] = "The Institute with Institute ID:$institute_id has been deleted ";
       }else{
           $output['status'] = FALSE;
           $output['data'] = "Could not delete the Institute,Please Try again ";
       }
       return $output;
   }
   
   function safe_query($query){
       $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $query as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) { 
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } 
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = 'Institute Deleted';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Delete Institute:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( ';', $sql );
        }
   }
   
//   WRITE FUNCTION ABOVE THIS LINE
    function execute_query($query,$type=null){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                if($type=='multi'){
                    $result[] = $qR->fetch_assoc( );
                }
                else{
                    $result = $qR->fetch_assoc( );
                }
                  
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
            
        } catch (Exception $e) {
            //die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    function sql_date($insertdate,$format = null){
        //$insertdate = mysqli_real_escape_string($insertdate);
        if(!$insertdate){
            return null;
        }
        $insertdate = strtotime($insertdate);
        if($format){
             $insertdate = date($format, $insertdate);
        }
        else{
             $insertdate = date('Y-m-d', $insertdate);
        }
        
        return $insertdate;
    }
    
    
    
     function isset_null($data,$type=false){
        
        if(isset($data)&& !empty($data)){
            if($type=='int'){
                //return is_int($data) ? $data : 0;
                return (int)$data;
                
            }
            else if($type=='date'){
                return "'".$data."'";
            }
            else{
               return "'".$data."'";
            }
            
        }
        
        if($type=='int' || $type=='date'){
            $result  = 'NULL';
        }
        else if($type=='str'){
            $result  = "''";
        }
        else{
            $result  = "''";
        }
        return $result;
    }
    
    
    function institute_exists($id){
        if(!$id){
            return false;
        }
        $sql = "SELECT COUNT(*) as count from institute where id='$id'";
        $result = $this->execute_query($sql);
        if($result['count'] >= 1){
            return true;
        }else{
            false;
        }
    }
    
    
}

 
