<?php
class Application_Model_Esc extends Application_Model_Db
{ /**
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1 original 
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' ); 
    }
    
    public function esc_exists($institute_id){
        $output = array( );
        $sql  = 'SELECT esc_id from esc where institute_id='.$institute_id.' limit 1';
        $result = $this->execute_query($sql);
        return $result['esc_id'];
        
    }
    public function create_esc( $institute_id )
    {
        $output = array( );
        $sql  = "INSERT INTO `esc` (`esc_id`, `institute_id`, `dateofcreation`, `dateofupdation`) VALUES (NULL, '".$institute_id."', CURRENT_TIMESTAMP, '0000-00-00 00:00:00');";
      
        $qR   = $this->mysqli->query( $sql );
        if ( $qR ) {
            $output[ 'success' ]     = 'Created';
            $output[ 'esc_id' ] = $this->mysqli->insert_id;
        } //$qR
        else {
            $output[ 'error' ] = 'Could not Create the esc:' . $this->mysqli->error;
            $output[ 'data' ]  = $data;
        }
        return $output;
    }
    function array_to_csv($not_escaped){
//            if(!$not_escaped){
//                return false;
//            }
            
            $escaped = base64_encode($not_escaped);
            return $escaped ;  
    }
    function csv_to_array($escaped){
        if(!$escaped){
                return false;
            } 
           $not_escaped = base64_decode($escaped);
           return $not_escaped ; 
    }
    
    function get_esc_data( $esc_id){
         $output = array();
         $sql = 'SELECT DISTINCT * FROM esc where esc_id='.(int)$esc_id;
         
         $result = $this->execute_query($sql);
         $output['data_esc'] = $result; 
      
       
         foreach($result[0] as $key=>$value){
            
                 $output[$key] = $value;
          
         }
         
         return $output;
    }
    
    
   
   
   function update_esc_details( $data,$esc_id ){
       $output = array( );
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
         
        
        $esc_stage = $this->isset_null($data['esc_stage'],$int);
        switch($esc_stage){
             case 1:
                 $output = $this->update_esc_stage1($data,$esc_id);
                break;  
             case 2:
                 $output = $this->update_esc_stage2($data,$esc_id);
                break;  
            default;
                break;
        }
        
        return $output;
        
   }
   function update_esc_stage1($data,$esc_id){
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
        $hasesc = $this->isset_null($data['hasesc']);
       if(isset($data['hasesc']) && $data['hasesc']=='ESC' ){
            $hasesc = $this->isset_null($data['hasesc']);
            $bi_foundationyear = $this->isset_null($data['bi_foundationyear']);            
            $bi_prncplhodbriefed = $this->isset_null($data['bi_prncplhodbriefed']);
            $bi_domeet = $this->isset_null($data['bi_domeet'],$date);
            $bi_planofactiondiscussed = $this->isset_null($data['bi_planofactiondiscussed']);
            $bi_planofactionadopted = $this->isset_null($data['bi_planofactionadopted']);
            $sub_sql = "UPDATE  esc SET  hasesc =  $hasesc,
                        bi_foundationyear =  $bi_foundationyear,                       
                    bi_prncplhodbriefed =  $bi_prncplhodbriefed,
                    bi_domeet =  $bi_domeet,
                         bi_planofactionadopted =  $bi_planofactionadopted,
                     bi_planofactiondiscussed =  $bi_planofactiondiscussed";
            
            if($bi_planofactiondiscussed =="'Yes'" && $bi_planofactionadopted=="'Yes'"){
                
                if($data['bi_uploadplanorMinutes']){
                        $bi_uploadplanorMinutes = $data['bi_uploadplanorMinutes'];
                        $sub_sql.=",bi_uploadplanorMinutes =  '$bi_uploadplanorMinutes'";
                      }
                      else{
                          $sub_sql.=",bi_uploadplanorMinutes =  ''";
                      }
            }
            
            if($bi_planofactionadopted!="'Yes'"){
                $sub_sql.=",bi_uploadplanorMinutes =  ''";
            }
           
            
              if($data['bi_availableinfra'] || $data['bi_availableinfra_other']){
                  $chk_value = isset($data['bi_availableinfra']) ? implode(';',$data['bi_availableinfra']) : '';
                  $chk_value_other = isset($data['bi_availableinfra_other']) ? $chk_value.';Other|'.$data['bi_availableinfra_other_value'] : $chk_value ;
                  $sub_sql.=",bi_availableinfra =  '$chk_value_other'";
              }
              else{
                   $sub_sql.=",bi_availableinfra =  ''";
              }
       }
       
       else{
           $sub_sql = "UPDATE  esc SET  hasesc= $hasesc, bi_foundationyear= '', bi_prncplhodbriefed= '', bi_domeet= '', bi_planofactiondiscussed= '', bi_planofactionadopted= '', bi_uploadplanorMinutes= '', bi_availableinfra='' ";
       }
       
       
       
      
      
      $sql = $sub_sql.' WHERE  esc.esc_id ='.$esc_id;
       $result = $this->execute_query($sql);
       //now add the guest list to the system 
       return $sql;
       return $result;
       
       
   }
   function update_esc_stage2($data,$esc_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       
       
       //if top checkbox not selected then update with null values 
       
            $td_leadfacname = $this->isset_null($data['td_leadfacname']);
            $td_coleadfacname = $this->isset_null($data['td_coleadfacname']);
            $td_noofactivestudents = $this->isset_null($data['td_noofactivestudents'],$int);
            $td_noofadvisorymembers = $this->isset_null($data['td_noofadvisorymembers'],$int);
            $td_dofabmeet = $this->isset_null($data['td_dofabmeet'],$date);
            $td_domrabmeet = $this->isset_null($data['td_domrabmeet'],$date);
            $sub_sql = "UPDATE  esc SET  td_leadfacname =  $td_leadfacname,
                        td_coleadfacname =  $td_coleadfacname,
                       td_noofactivestudents =  $td_noofactivestudents";
            
             if($data['td_uploadlistofmembers']){
                $td_uploadlistofmembers = $data['td_uploadlistofmembers'];
                $sub_sql.=",td_uploadlistofmembers =  '$td_uploadlistofmembers'";
              }
              
        $sql = $sub_sql.' WHERE  esc.esc_id ='.$esc_id;
 $result = $this->execute_query($sql);
       //now add the guest list to the system 
       return $sql;
       return $result;
       
   }
   
    function delete_esc( $escId )
    {
        $output     = array( );
        $activityId = mysqli_real_escape_string( $this->mysqli, $escId );
        $sql[ ]     = 'DELETE FROM institute_esc WHERE iap_id=' . $escId;
       
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) {
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = ' Deleted the esc';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Delete The esc:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( '; 
', $sql );
        }
    }
    public function validate( $data, $type )
    {
        $status = array( );
        switch ( $type ) {
            case 'numeric':
                if ( is_numeric( $data ) && (int) $data >= 0 ) {
                    return true;
                } //is_numeric( $data ) && (int) $data >= 0
                else {
                    $status[ 'error' ] = 'Please Provide only non negative numbers';
                    return $status;
                }
                break;
            default:
                break;
        } //$type
    }
    
    
    function isset_null($data,$type=false){
        
        if(isset($data)&& !empty($data)){
            if($type=='int'){
                //return is_int($data) ? $data : 0;
                return (int)$data;
                
            }
            else if($type=='date'){
                return "'".$data."'";
            }
            else{
               return "'".$data."'";
            }
            
        }
        
        if($type=='int' || $type=='date'){
            $result  = 'NULL';
        }
        else if($type=='str'){
            $result  = "''";
        }
        else{
            $result  = "''";
        }
        return $result;
    }
    function upload_file( $data, $options=null )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;        
        $allowedMimes = $options[ 'mime' ];
        $fileFormat   = $options[ 'fileFormat' ];
        $size         = $options[ 'size' ];
        $file_name = $data['name'];
        $extension = end(explode(".", $data["name"]));
        if ( ( $data[ 'size' ] == 0 )  ) {
            $goodtogo          = false;
            $result[ 'error' ] = "Could not upload the file: Invalid File Format";
            return $result;
        } //( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size )
        try {
            $target = APPLICATION_PATH . "/../public/uploads/esc/" . $file_name ;
            //die($target);
            $source = $data[ 'tmp_name' ];
            
            // die($target.'<br/>'.$source);
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = false;
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $result[ 'error' ] = $e->getmessage();
            return $result;
        }
        if ( !$goodtogo ) {
            $result[ 'error' ] = 'File Not Uploaded,Please Try again';
            return $result;
        } //!$goodtogo
        $result[ 'data' ]  = $target;
        $result[ 'error' ] = "";
        return $target;
    }
    public function get_grid_metaData($identifier) {
     $gridMeta['grid_project'] =
                array(
                    
                    array('name' => 'projecttitle', 'label' => 'Project Title', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'proponentfirstname', 'label' => 'Proponent Name (first name)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'proponentlastname', 'label' => 'Proponent Name (last name)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'proponenttelno', 'label' => 'Proponent Tel no.', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'proponentemail', 'label' => 'Proponent Email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'synopsis', 'label' => 'Synopsis (50 words)', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'whethershortlisted', 'label' => 'Whether shortlisted (Y/N)', 'datatype' => 'boolean', 'editable' => true),
                     array('name' => 'finalselection', 'label' => 'Final Selection (Y/N)', 'datatype' => 'boolean', 'editable' => true),
                     array('name' => 'mentorname', 'label' => 'Mentor name', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'pitchedtodstaicte', 'label' => 'Pitched to DST/AICTE (Y/N)', 'datatype' => 'boolean', 'editable' => true),
					 array('name' => 'funded', 'label' => 'Funded (Y/N)', 'datatype' => 'boolean', 'editable' => true)
        );
        
        $gridMeta['grid_panel'] =
                array(
                   array('name' => 'pp_designation', 'label' => 'Designation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_institutionororganisation', 'label' => 'Institution/Organisation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_firstname', 'label' => 'First name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_lastname', 'label' => 'Last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_title', 'label' => 'Title (Mr/Ms./Dr./Prof)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_areaofexpertise', 'label' => 'Area of expertise', 'datatype' => 'string', 'editable' => true),                
                    array('name' => 'pp_dateofstartingaspanelist', 'label' => 'Date of Starting as Panelist', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_noofprojectsreviewed', 'label' => 'Number of Projects reviewed', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_email', 'label' => 'email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'pp_phone', 'label' => 'phone', 'datatype' => 'string', 'editable' => true),
        );
         $gridMeta['grid_attendee'] =
                array(
                   array('name' => 'td_category', 'label' => 'Category (Faculty, Student, Expert)', 'datatype' => 'string', 'editable' => true),
                  array('name' => 'td_role', 'label' => 'Role (Advisor, Executive Leader, Executive Member)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_firstname', 'label' => 'First name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_lastname', 'label' => 'Last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_areaofexpertise', 'label' => 'Area of expertise (if applicable)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_dateofjoining', 'label' => 'Date of joining', 'datatype' => 'string', 'editable' => true),                
                    array('name' => 'td_dateofleaving', 'label' => 'Date of leaving', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_email', 'label' => 'email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'td_phone', 'label' => 'phone', 'datatype' => 'string', 'editable' => true),
                    
        );
         
        
        
        
        if(!$identifier)
            return $gridMeta;
        return $gridMeta[$identifier];
 }
 function get_data_grid($sql,$type){
        $data_grid = array();           
        $data_grid['data'] = array();
        $data_grid['metadata'] = $this->get_grid_metaData($type);
        $i = 0;
        $qR = $this->mysqli->query( $sql );
        while($qRR = $qR->fetch_assoc( )){
                $grid_type_data[] = $qRR;
         }
        
            foreach ($grid_type_data as $mem) {
                
               $data_grid['data'][$i]['id'] = $i;
                $data_grid['data'][$i]['values'] = $mem;
                $i++;
            }
        
         
         return $data_grid;
  } 
  
  function process_datagrid($data,$columns){
        $out = array();
        $temp = array();
        $data_grid = json_decode($data);
        foreach($data_grid as $key=>$val){
                $data_grid_data =  (array)$val->values; 
                $data_grid_vals = (array)$val->columns; 
                foreach($data_grid_vals as $k=>$v){
                   if(TRUE){ //change true to column key comparision( if(in_array($k,$columns))   ) for letting only those entries as result
                        $temp[] = "'$v'";  
                    }
                }
                
                $out[$key] = implode(',',$temp);
                unset($temp);
                
            }
        
        return $out;
    }
  
  function execute_query($query){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                  $result = $qR->fetch_assoc( );
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
            
        } catch (Exception $e) {
            die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
}

 