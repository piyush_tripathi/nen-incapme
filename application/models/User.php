<?php
class Application_Model_User extends Application_Model_Db
{ /** 
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' );
//        error_reporting(E_ALL);
//ini_set('display_errors', '1');

        
            
    }
    
    
    
   function create_user( $data ){
       $query  = array();
       $result  = array();
       $role = $this->isset_null($data['user_role']);
       $first_name  = $this->isset_null($data['user_first_name']);
       $last_name  = $this->isset_null($data['user_last_name']);
       $password  = $this->isset_null($data['user_new_password']);
       $email  = $this->isset_null($data['user_email']);
       $user_institute  = $data['user_institute'];
       $con_institute  = $data['con_institute'];
       if(!$role || !$email || !$password ){
         return false;
       }
        //for lead faculty 
       if($data['user_role']=='LEAD_FACULTY'){
           $role = "'FAC'";
       }
       //now checking if user already exists 
       $sql = "SELECT id from users where email=$email";
       $email_exists = $this->execute_query($sql); 
       if($email_exists['id']){
           $result['data'] = "User with email $email already exists";
           $result['status']  = FALSE;
           return $result;
       }
      
//       good to go 
           $sql = "INSERT INTO human_resource(id,first_name,last_name,email,category) values(null,$first_name,$last_name,$email,$role)";
           $qR   = $this->mysqli->query( $sql );
           $user_id = $this->mysqli->insert_id; 
       if(!$user_id){
           return 'Human ID not created';
       }   
//       $user_id = 4708;
       //good to go 
       //add to user table
      
       $query[] = "INSERT INTO users(id,first_name,last_name,role,password,human_id,email,status) values(null,$first_name,$last_name,$role,$password,$user_id,$email,'ACTIVE')";
       switch($data['user_role']) {
           case 'FAC':                
                $query[] = "INSERT INTO human_institute(human_id,institute_id) values($user_id,$user_institute)";                
                break;
           case 'LEAD_FACULTY':
                $query[] = "INSERT INTO human_institute(human_id,institute_id) values($user_id,$user_institute)";
                $query[] = "UPDATE institute SET head_of_institute=$user_id where id=$user_institute";
               break;
           case 'CON':
               $query[] = "UPDATE institute SET consultant_in_charge=$user_id where id IN (".implode(',',$con_institute).")";
               break;  
           case 'ADMIN':
               
               break;        
           default:
               break;              
                    
        }
        //create additional table entries
        
           $output  = $this->safe_query($query);
           
           if(isset($output['success']) ){               
               $result['data'] = 'User created with User ID:'.$user_id;
               $result['status']  = TRUE;
           }else{
                
               $result['data'] = 'Unable to create user, Please try again ';
               $result['status']  = FALSE;
               
           }
        return $result;
   }
    
    function get_user_data( $human_id,$institute_id){
         $output = array();        
         $type= 'multi';
        //if user is consultant then show his list of institutes 
         $sql = "SELECT * FROM human_resource where id=$human_id";
         $output['user_data'] = $this->execute_query($sql); 
          
         
        
          
         $output['is_admin'] = $this->is_admin();
         $output['user_data']['is_lead_faculty'] = $this->is_lead_faculty($human_id,$institute_id);
        
        
         //only admin can change institutes
         
             $sql   = 'SELECT id,name from institute';
             $output['admin_institute'] = $this->execute_query($sql,$type); 
         
         //if user is consultant then get all instiutes under him 
         $is_consultant = $this->is_consultant($human_id);
         if( $output['user_data']['category']=='CON'){
             $sql = "SELECT id from institute where consultant_in_charge =".$human_id;
             $output['user_data']['con_institute'] = $this->execute_query($sql,$type);  
         }
         
         
         if($output['user_data']['category']=='FAC' ){
             $sql = "SELECT x.id,y.institute_id  from institute as x left join human_institute as y on x.id=y.institute_id  where x.head_of_institute =$human_id  OR y.human_id=$human_id limit 1" ;             
             
             $output['user_data']['user_institute'] =$this->execute_query($sql,$type);
             
             
//              $output['user_data']['user_institute'] =$temp1;
             
             
         }
         
         
         
            
         
//         //fdp details 
//         $sql = 'SELECT * from fdp where fdp_id='.$fdp_id;
//         $output['fdp_details'] = $this->execute_query($sql);
//         
//         // now details about the person logged in
//         
        
         
         return $output;
    }
    
     
   function is_lead_faculty($human_id,$institute_id=NULL){
       
              $sql = 'SELECT count(*) as count from institute where head_of_institute='.$human_id;
              $count = $this->execute_query($sql); 
              if($count['count']){
                  $result = true;
              }else{
                 $result = false;
              }
              return $result; 
   }
   function is_admin(){
       $auth = Zend_Auth::getInstance(); 
         if($auth->hasIdentity()) {
              $user = $auth->getIdentity(); 
              $role = $user->role;
              
              if($role=='ADMIN'){
                  $result = true;
              }else{
                 $result = false;
              }
              
         }
        
        return $result; 
   }
   
   function is_consultant($human_id){
       if($human_id){
           $sql = 'SELECT count(*) as count from institute where head_of_institute='.$human_id;
           $count = $this->execute_query($sql); 
              if($count['count']>=1){
                  $result = true;
              }else{
                 $result = false;
              }
              return $result;
       }  
   }
   
   function update_user_details( $data,$user_id){
       $output = array( );
       $status = TRUE;
       
        $int  = 'int';
        $rr = array();
        $str = 'str';
        $date  = 'date';
        $is_admin = false;
        $temp = array();
        //now checking which data element is top be updated 
        $user_first_name = $this->isset_null($data['user_first_name']) ;
        $user_last_name = $this->isset_null($data['user_last_name']) ;
        $user_email = $this->isset_null($data['user_email']) ;
        $user_new_password = $this->isset_null($data['user_new_password']) ;
        $user_confirm_password = $this->isset_null($data['user_confirm_password']) ;
        $user_role = $this->isset_null($data['user_role']) ;
        $user_institute = $this->isset_null($data['user_institute'],$int) ;
        
        //conditional check on fields
        $sub_sql_a = "UPDATE users SET human_id=$user_id" ; 
        $sub_sql_b = "UPDATE human_resource SET id=$user_id" ; 
        if(isset($data['user_first_name'])){
            $sub_sql_a.=",first_name=$user_first_name";
            $sub_sql_b.=",first_name=$user_first_name";
         }
         //last name
         if(isset($data['user_last_name'])){
            $sub_sql_a.=",last_name=$user_last_name";
            $sub_sql_b.=",last_name=$user_last_name";
         }
         //email
         if(isset($data['user_email'])){
            $sub_sql_a.=",email=$user_email";
            $sub_sql_b.=",email=$user_email";
         }
         //password confirmation
         if( $data['user_new_password']!='' && $data['user_confirm_password']!='' && ($user_new_password==$user_confirm_password)  ){
            $sub_sql_a.=",password=$user_new_password";          
         }
        
         
         // do not move forward if user is not admin
         $is_admin = $this->is_admin();
          //user_role
         
         if(isset($data['user_role'] ) && $is_admin){
             if($data['user_role'] =='LEAD_FACULTY'){
                $user_role = "'FAC'";
              }
            
            $sub_sql_a.=",role=$user_role";
            $sub_sql_b.=",category=$user_role";
         }
         $sql_a = $sub_sql_a.'  where human_id='.$user_id;
         $sql_b = $sub_sql_b.'  where id='.$user_id;
         
//         return $sql_b;
         $result = $this->execute_query($sql_a);
         $result = $this->execute_query($sql_b);
//         if(!$result){
//             $status =  FALSE;
//         }
        
          //user_institute
         // cleaning up association due to previous roles
         
         if(  $is_admin){
             unset($temp);
         $temp[] =  "UPDATE institute SET head_of_institute='' where  head_of_institute=$user_id";
         $temp[] =  "UPDATE institute SET consultant_in_charge='' where  consultant_in_charge=$user_id";
          $temp[] =  "Delete from human_institute  where  human_id=$user_id";
         foreach($temp as $k=>$v){
             $this->execute_query($v);
//            if(!$this->execute_query($v)){
//             $status =  FALSE;
//         }
         }
         unset($temp);
             if($data['user_role']=='FAC'){
                 $sub_sql =  "INSERT INTO human_institute(human_id,institute_id) Values($user_id,$user_institute)";
                 $result = $this->execute_query($sub_sql);
//                 if(!$result){
//                    $status =  FALSE;
//                }
                 
             }
             if($data['user_role']=='LEAD_FACULTY'){
                 $sub_sql =  "INSERT INTO human_institute(human_id,institute_id) Values($user_id,$user_institute)";
                 
                 $result = $this->execute_query($sub_sql);
//                 if(!$result){
//                    $status =  FALSE;
//                }
                 $sub_sql =  "UPDATE institute SET head_of_institute=$user_id where id=".$data['user_institute'];
                 $result = $this->execute_query($sub_sql);
//                 if(!$result){
//                    $status =  FALSE;
//                }
                 
             }
             if($data['user_role']=='CON'){
                 foreach($data['con_institute'] as $key=>$institute_id){
                     $institute_id =  $this->isset_null($institute_id,$int);
                     $sub_sql =  "UPDATE institute SET consultant_in_charge=$user_id where id= $institute_id";
                     $result = $this->execute_query($sub_sql);
//                     $sub_sql =  "DELETE from fdp where user_id= $institute_id";
//                     $result = $this->execute_query($sub_sql);
//                     if(!$result){
//                            $status =  FALSE;
//                        }
                 
                 }
                 
             }
           
         }
     
            
      if($status){
         $output['status'] = TRUE; 
         $output['data'] = "User has been updated" ;
      }
      else{
         $output['status'] = FALSE; 
         $output['data'] = "User could not be updated" ;
      }
       return $output;
       
       
   } 
   
   function delete_user( $user_id ){
       $query = array();
       $output = array();
      
       //if user is faculty : delete all institute associated > delete from user table > delete from human_resource
       $role = $this->get_user_role($user_id);
        
       switch($role) {
           case 'FAC':
                $query[] = "DELETE FROM human_institute where human_id=$user_id";
                $query[] = "DELETE FROM users where human_id=$user_id";
                $query[] = "DELETE FROM human_resource where id=$user_id";
               break;
           case 'LEAD_FACULTY':
                $query[] = "DELETE FROM human_institute where human_id=$user_id";
                $query[] = "DELETE FROM users where human_id=$user_id";
                $query[] = "DELETE FROM human_resource where id=$user_id";
                $query[] = "UPDATE institute SET head_of_institute='' where head_of_institute=$user_id";
               break;
           case 'CON':
               $query[] = "UPDATE institute SET consultant_in_charge='' where consultant_in_charge=$user_id";
               $query[] = "DELETE FROM users where human_id=$user_id";
               $query[] = "DELETE FROM human_resource where id=$user_id";
               break;  
           case 'ADMIN':
               $query[] = "DELETE FROM users where human_id=$user_id";
               $query[] = "DELETE FROM human_resource where id=$user_id";
               break;        
           default:
               break;              
                    
        }
       
       
       $result  = $this->safe_query($query);
       if($result['success']){
           $output['status'] = TRUE;
           $output['data'] = "The user with User ID:$user_id has been deleted ";
       }else{
           $output['status'] = FALSE;
           $output['data'] = "Could not delete the user,Please Try again ";
       }
       return $output;
   }
   
   function safe_query($query){
      $output = array();
       $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $query as $key => $value ) {
                           
                $result = $this->mysqli->query( $value );                
                $qR = $qR && $result;
            
        } 
        
        if (  $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = 'SUCCESS';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'NOT SUCCESS:' . $this->mysqli->error;
            $output[ 'data' ]  = 'An error occured ,Please try again.';
        }
        return $output;
   }
   
//   WRITE FUNCTION ABOVE THIS LINE
    function execute_query($query,$type=null){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                if($type=='multi'){
                    $result[] = $qR->fetch_assoc( );
                }
                else{
                    $result = $qR->fetch_assoc( );
                }
                  
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
            
        } catch (Exception $e) {
            //die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    function sql_date($insertdate,$format = null){
        //$insertdate = mysqli_real_escape_string($insertdate);
        if(!$insertdate){
            return null;
        }
        $insertdate = strtotime($insertdate);
        if($format){
             $insertdate = date($format, $insertdate);
        }
        else{
             $insertdate = date('Y-m-d', $insertdate);
        }
        
        return $insertdate;
    }
    
    
    public function validate( $data, $type )
    {
        $status = array( );
        switch ( $type ) {
            case 'numeric':
                if ( is_numeric( $data ) && (int) $data >= 0 ) {
                    return true;
                } //is_numeric( $data ) && (int) $data >= 0
                else {
                    $status[ 'error' ] = 'Please Provide only non negative numbers';
                    return $status;
                }
                break;
            default:
                break;
        } //$type
    }
    
    
     function isset_null($data,$type=false){
        
        if(isset($data)&& !empty($data)){
            if($type=='int'){
                //return is_int($data) ? $data : 0;
                return (int)$data;
                
            }
            else if($type=='date'){
                return "'".$data."'";
            }
            else{
               return "'".$data."'";
            }
            
        }
        
        if($type=='int' || $type=='date'){
            $result  = 'NULL';
        }
        else if($type=='str'){
            $result  = "''";
        }
        else{
            $result  = "''";
        }
        return $result;
    }
    
    function get_user_role($user_id){
         
        if(!$user_id){
            return false;
        }
        $sql  = "SELECT category FROM human_resource where id=$user_id";
        $result = $this->execute_query($sql);
        
        switch($result['category']) {
           case 'FAC':
                        if($this->is_lead_faculty($user_id) ){
                           $role = 'LEAD_FACULTY'; 
                        }
                        else{
                           $role = 'FAC'; 
                        };
                        break;
           case 'CON':
                        $role = 'CON'; 
                        break;         
        
           case 'ADMIN':
                        $role = 'ADMIN'; 
                        break;        
           default:
                        break;              
                    
        }
        
        return $role;
    }
    
    function search_user( $data,$type ){
        switch($type){
            case 'email':
                $sub_query =  "email like '%$data%' or first_name like '%$data%' or last_name like '%$data%'";
                break;
            default:
                break;
        }
        
        $query = "SELECT DISTINCT * FROM human_resource where $sub_query";
        $result = $this->execute_query($query,'multi');
        
        return $result;
        
    }
    
    function user_exists($email){
        if(!$email){
            return false;
        }
        $sql = "SELECT COUNT(*) as count from human_resource where email=$email";
        $result = $this->execute_query($sql);
        if($result['count'] >= 1){
            return true;
        }else{
            false;
        }
    }
    
    
    
}

 