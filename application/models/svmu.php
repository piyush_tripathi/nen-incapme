<?php
class Application_Model_Svmu extends Application_Model_Db
{ /**
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' ); 
    }
    
    public function svmu_exists($institute_id){
        $output = array( );
        $sql  = 'SELECT svmu_id from svmu where institute_id='.$institute_id.' limit 1';
        $result = $this->execute_query($sql);
        return $result['svmu_id'];
        
    }
    public function create_svmu( $institute_id )
    {
        $output = array( );
        $sql  = "INSERT INTO `svmu` (`svmu_id`, `ov_text`, `ov_mai_sa`, `ov_mai_ta`, `ov_mai_fra`, `ov_mai_savalue`, `ov_mai_tavalue`, `ov_mai_fravalue`, `ov_leadfaculty`, `ov_organisingteam`, `ov_dateoflaunch`, `prgmfinalised`, `prgm_uploadschedule`, `prgm_bdma`, `prgm_uploadapplform`, `prgm_noofapplicants`, `prgm_totnoofmentors`, `prgm_totnoofmentees`, `vt_noofventure`, `vt_menteefeedreviewed`, `vt_dateoflastreview`, `vt_dateoflastreport`, `vt_uploadreport`, `institute_id`, `createdtime`, `changedtime`) VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '".$institute_id."', CURRENT_TIMESTAMP, '0000-00-00 00:00:00');";
      
        $qR   = $this->mysqli->query( $sql );
        if ( $qR ) {
            $output[ 'success' ]     = 'Created';
            $output[ 'svmu_id' ] = $this->mysqli->insert_id;
        } //$qR
        else {
            $output[ 'error' ] = 'Could not Create the svmu:' . $this->mysqli->error;
            $output[ 'data' ]  = $data;
        }
        return $output;
    }
    function array_to_csv($not_escaped){
//            if(!$not_escaped){
//                return false;
//            }
            
            $escaped = base64_encode($not_escaped);
            return $escaped ;  
    }
    function csv_to_array($escaped){
        if(!$escaped){
                return false;
            } 
           $not_escaped = base64_decode($escaped);
           return $not_escaped ; 
    }
    
    function get_svmu_data( $svmu_id){
         $output = array();
         $grid_org_data = array();
         $sql = 'SELECT DISTINCT * FROM svmu where svmu_id='.(int)$svmu_id;
         
         $result = $this->execute_query($sql);
          $output['data_svmu'] = $result;
      
        
      
          //for grid org
          $sql = "SELECT designation,instororg,firstname,lastname,title,areaofexpr, dostrtasmntr,noofmenteesthisinst,email,phone from svmu_ov_guestlist where svmu_id=" . $svmu_id; 
          $type='grid_org';
          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
          //for grid mentee
          $sql = "SELECT firstname, lastname, institution, programme, yrofcompl, email, phone, businessidea, mentoremail, mentorphone, mentorfirstname, mentorlastname FROM svmu_prgm_mentees WHERE svmu_id = " . $svmu_id; 
          $type='grid_mentee';
          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
          //for grid mentor
          $sql = "SELECT designation, instororg, firstname, lastname, title, areaofexpr, dostrtasmntr, noofmenteesthisinst, email, phone from svmu_prgm_mentors WHERE svmu_id = " . $svmu_id; 
          $type='grid_mentor';
          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
          //for grid venture
          $sql = "SELECT venturename, areaofbusiness, targetstartupdate, menteefirstname, menteelastname, mentorfirstname, mentorlastname, totmntrngmeets, dateoflastmeet, mntesltstfdbk, mntrsltstfdbk from svmu_vt_details WHERE svmu_id = " . $svmu_id; 
          $type='grid_venture';
          $output['data_grid'][$type] =  $this->get_data_grid($sql,$type);
         return $output;
    }
  function get_data_grid($sql,$type){
        $data_grid = array();           
        $data_grid['data'] = array();
        $data_grid['metadata'] = $this->get_grid_metaData($type);
        $i = 0;
        $qR = $this->mysqli->query( $sql );
        while($qRR = $qR->fetch_assoc( )){
                $grid_type_data[] = $qRR;
         }
        
            foreach ($grid_type_data as $mem) {
                
               $data_grid['data'][$i]['id'] = $i;
                $data_grid['data'][$i]['values'] = $mem;
                $i++;
            }
        
         
         return $data_grid;
  }  
    
   
   
   function update_svmu_details( $data,$svmu_id ){
       $output = array( );
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $svmu_stage = $this->isset_null($data['svmu_stage']);
        switch($svmu_stage){
             case 1:
                 $output = $this->update_svmu_stage1($data,$svmu_id);
                break;
             case 2:
                 $output = $this->update_svmu_stage2($data,$svmu_id);
                break;
             case 3:
                 $output = $this->update_svmu_stage3($data,$svmu_id);
                break;
            default;
                break;
        }
        
        return $output;
        
   }
   function update_svmu_stage1($data,$svmu_id){
      
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
        
       $ov_mai_sa = $this->isset_null($data['ov_mai_sa'],$int);
       $ov_mai_savalue = isset($data['ov_mai_sa']) ? $this->isset_null($data['ov_mai_savalue']) : '';
       $ov_mai_ta = $this->isset_null($data['ov_mai_ta'],$int);
       $ov_mai_tavalue = isset($data['ov_mai_ta']) ? $this->isset_null($data['ov_mai_tavalue']) : '';
       $ov_mai_fra = $this->isset_null($data['ov_mai_fra'],$int);
       $ov_mai_fravalue = isset($data['ov_mai_fra']) ? $this->isset_null($data['ov_mai_fravalue']) : '';
       $ov_leadfaculty = $this->isset_null($data['ov_leadfaculty'],$int);
       
//       $options[ 'file_name' ]  = 'ov_organisingteam-' . $svmu_id;
//       $ov_organisingteam = $this->upload_file($_FILES['ov_organisingteam'],$options);   
      
       $ov_dateoflaunch = $this->isset_null($data['ov_dateoflaunch'],$date);
       
       $columns = array('designation','instororg','firstname','lastname','title','areaofexpr','dostrtasmntr','noofmenteesthisinst','email','phone');
       $svmu_org_records = $this->process_datagrid($data['svmu_org'],$columns);
      
       
        $sub_sql = "UPDATE  svmu SET  ov_mai_sa =  $ov_mai_sa,
                ov_mai_ta =  $ov_mai_ta,
                ov_mai_fra =  $ov_mai_fra,
                ov_mai_savalue =  '$ov_mai_savalue',
                ov_mai_tavalue =  '$ov_mai_tavalue',
                ov_mai_fravalue =  '$ov_mai_fravalue',
                ov_leadfaculty =  $ov_leadfaculty,               
                ov_dateoflaunch =  $ov_dateoflaunch";    
        
        
              
        if($data['ov_organisingteam']){
            $ov_organisingteam =$data['ov_organisingteam'];         
            $sub_sql.=",ov_organisingteam ='$ov_organisingteam'";
           }
     
       
        $sql  = $sub_sql." WHERE  svmu.svmu_id =$svmu_id";
        
       $result = $this->execute_query($sql);
       //now add the guest list to the system 
      $sql = 'DELETE FROM svmu_ov_guestlist where svmu_id='.$svmu_id; 
     $result = $this->execute_query($sql);
      $cols =  implode(',',$columns);
      
      foreach ($svmu_org_records as $key => $value) {
          $row[]= '(NULL,'.$value.','.$svmu_id.')';
      }
      $rows  = implode(",",$row);
      $sql = "INSERT INTO svmu_ov_guestlist ( id , $cols, svmu_id ) VALUES $rows ;";     
      $result = $this->execute_query($sql);
      return $result;
       
       
   }
   function update_svmu_stage2($data,$svmu_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
       
       $prgmfinalised =  isset($data['prgmfinalised'])? implode(',',$data['prgmfinalised']) : '';
       $prgm_bdma = $this->isset_null($data['prgm_bdma']);
      $prgm_noofapplicants = $this->isset_null($data['prgm_noofapplicants'],$int);
       $prgm_totnoofmentors = $this->isset_null($data['prgm_totnoofmentors'],$int);
       $prgm_totnoofmentees = $this->isset_null($data['prgm_totnoofmentees'],$int);
       
      
       
//       $columns = array('designation','instororg','firstname','lastname','title','areaofexpr','dostrtasmntr','noofmenteesthisinst','email','phone');
//       $svmu_mentor_records = $this->process_datagrid($data['svmu_mentor'],$columns);
       
       
      $sub_sql = "UPDATE  svmu SET  prgmfinalised =  '$prgmfinalised',
          prgm_noofapplicants =  $prgm_noofapplicants,
       prgm_totnoofmentors =  $prgm_totnoofmentors,
       prgm_totnoofmentees =  $prgm_totnoofmentees";                
              
      if($data['prgm_uploadschedule']){
          $prgm_uploadschedule = $data['prgm_uploadschedule'];
          $sub_sql.=",prgm_uploadschedule =  '$prgm_uploadschedule'";
      }
      if($data['prgm_uploadapplform']){
          $prgm_uploadapplform = $data['prgm_uploadapplform'];
          $sub_sql.=",prgm_uploadapplform =  '$prgm_uploadapplform'";
      }
      if(in_array('Marketing-Plan',$data['prgmfinalised']) ){                    
          $sub_sql.=",prgm_bdma =  '$prgm_bdma'";
           
      }else{
           $sub_sql.=",prgm_bdma =  ''";
      }
      
      $sql = $sub_sql.' WHERE  svmu.svmu_id ='.$svmu_id;
     
       $result = $this->execute_query($sql);
      
        //now add the mentee list to the system 
        $columns = array('firstname','lastname','institution','programme','yrofcompl','email','phone','businessidea','mentoremail','mentorphone','mentorfirstname','mentorlastname');
        $svmu_mentee_records = $this->process_datagrid($data['svmu_mentee'],$columns);
        $sql = 'DELETE FROM svmu_prgm_mentees where svmu_id='.$svmu_id;
        $result = $this->execute_query($sql);
         $cols =  implode(',',$columns);
         
         foreach ($svmu_mentee_records as $key => $value) {
             $row[]= '(NULL,'.$value.','.$svmu_id.')';
         }
         $rows  = implode(",",$row);
         $sql = "INSERT INTO svmu_prgm_mentees ( id , $cols, svmu_id ) VALUES $rows ;";     
         $result = $this->execute_query($sql);
         unset($row);
         unset($result);
         
//         //now add the mentor list to the system 
        $columns = array('designation', 'instororg', 'firstname', 'lastname', 'title', 'areaofexpr', 'dostrtasmntr', 'noofmenteesthisinst', 'email', 'phone');
        $svmu_mentor_records = $this->process_datagrid($data['svmu_mentor'],$columns);
        $sql = 'DELETE FROM svmu_prgm_mentors where svmu_id='.$svmu_id;
        $result = $this->execute_query($sql);
         $cols =  implode(',',$columns);

         foreach ($svmu_mentor_records as $key => $value) {
             $row[]= '(NULL,'.$value.','.$svmu_id.')';
         }
         $rows  = implode(",",$row);
         $sql = "INSERT INTO svmu_prgm_mentors ( id , $cols, svmu_id ) VALUES $rows ;";     
         $result = $this->execute_query($sql);
      return $result;
       
       
   }
   function update_svmu_stage3($data,$svmu_id){
       
       $output = array( );
       $options = array();
        $aR     = array( );
        $int  = 'int';
        $str = 'str';
        $date  = 'date';
       
       $vt_noofventure =$this->isset_null( $data['vt_noofventure'],$int);
        
       $options[ 'file_name' ]  = 'upload_venture-' . $svmu_id;
       $upload_venture = $this->upload_file($data['upload_venture'],$options);     
       
       $vt_menteefeedreviewed = $data['vt_menteefeedreviewed'];
       $vt_dateoflastreview = $this->isset_null($data['vt_dateoflastreview'],$date);
       $vt_dateoflastreport = $this->isset_null($data['vt_dateoflastreport'],$date);
       $vt_uploadreport = $this->isset_null($data['vt_uploadreport'],$date);
      
       
       
      $sub_sql = "UPDATE  svmu SET  vt_noofventure =  $vt_noofventure,               
                vt_menteefeedreviewed =  $vt_menteefeedreviewed,
                vt_dateoflastreview =  $vt_dateoflastreview,                 
                vt_dateoflastreport =  $vt_dateoflastreport";
               
      
       if($data['vt_uploadreport']!=''){          
          $sub_sql.=",vt_uploadreport =  $vt_uploadreport";
      }
       $sql = $sub_sql.' WHERE  svmu.svmu_id ='.$svmu_id;
    
      $result = $this->execute_query($sql);
       
    //now add the venture list to the system 
        $columns = array('venturename', 'areaofbusiness', 'targetstartupdate', 'menteefirstname', 'menteelastname', 'mentorfirstname', 'mentorlastname', 'totmntrngmeets', 'dateoflastmeet', 'mntesltstfdbk', 'mntrsltstfdbk');
        $svmu_venture_records = $this->process_datagrid($data['svmu_venture'],$columns);
        $sql = 'DELETE FROM svmu_vt_details  where svmu_id='.$svmu_id;
        $result = $this->execute_query($sql);
         $cols =  implode(',',$columns);

         foreach ($svmu_venture_records as $key => $value) {
             $row[]= '(NULL,'.$value.','.$svmu_id.')';
         }
         $rows  = implode(",",$row);
         $sql = "INSERT INTO svmu_vt_details ( id , $cols, svmu_id ) VALUES $rows ;";     
         $result = $this->execute_query($sql);
       return $result;
       
   }
   
    function execute_query($query){
        //$result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                  $result = $qR->fetch_assoc( );
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
            
        } catch (Exception $e) {
            die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    function sql_date($insertdate,$format = null){
        //$insertdate = mysqli_real_escape_string($insertdate);
        if(!$insertdate){
            return null;
        }
        $insertdate = strtotime($insertdate);
        if($format){
             $insertdate = date($format, $insertdate);
        }
        else{
             $insertdate = date('Y-m-d', $insertdate);
        }
        
        return $insertdate;
    }
    
    function delete_svmu( $svmuId )
    {
        $output     = array( );
        $activityId = mysqli_real_escape_string( $this->mysqli, $svmuId );
        $sql[ ]     = 'DELETE FROM svmu WHERE svmu_id=' . $svmuId;
       
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) {
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = ' Deleted the svmu';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Delete The svmu:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( '; 
', $sql );
        }
    }
    public function validate( $data, $type )
    {
        $status = array( );
        switch ( $type ) {
            case 'numeric':
                if ( is_numeric( $data ) && (int) $data >= 0 ) {
                    return true;
                } //is_numeric( $data ) && (int) $data >= 0
                else {
                    $status[ 'error' ] = 'Please Provide only non negative numbers';
                    return $status;
                }
                break;
            default:
                break;
        } //$type
    }
    
    
    function isset_null($data,$type=false){
        
        if(isset($data)&& !empty($data)){
            if($type=='int'){
                //return is_int($data) ? $data : 0;
                return (int)$data;
                
            }
            else if($type=='date'){
                return "'".$data."'";
            }
            else{
               return $data; 
            }
            
        }
        
        if($type=='int' || $type=='date'){
            $result  = 'NULL';
        }
        else if($type=='str'){
            $result  = '';
        }
        else{
            $result  = '';
        }
        return $result;
    }
    function upload_file( $data, $options=null )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;        
        $allowedMimes = $options[ 'mime' ];
        $fileFormat   = $options[ 'fileFormat' ];
        $size         = $options[ 'size' ];
         $file_name = $data['name'];
        if ( ( $data[ 'size' ] == 0 )  ) {
            $goodtogo          = false;
            $result[ 'error' ] = "Could not upload the file: Invalid File Format";
            return null;
        } //( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size )
        try {
            $target = APPLICATION_PATH . "/../public/uploads/svmu/" . $file_name ;
            //die($target);
            $source = $data[ 'tmp_name' ];
            // die($target.'<br/>'.$source);
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = 'false';
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $result[ 'error' ] = $e->getmessage();
            return null;
        }
        if ( !$goodtogo ) {
            $result[ 'error' ] = 'File Not Uploaded,Please Try again';
            return null;
        } //!$goodtogo
        $result[ 'data' ]  = $target;
        $result[ 'error' ] = "";
        return $target;
    }
    /****** FUNCTION LIST FOR UPLOADING AND SENDING THE FILE DATA BACK FOR DATAGRID DISPLAY  *****/
    // for reading the uploaded data for the data grid in front end - file uploads - read - create json - show in datagrid 
    
    function read_org( $data ){        
        $output          = array( );
        $aR              = array( );
        $vals            = array( );
       
        
       // $file = $_FILES[ 'fileParticipant' ];
        if ( $data ) { $options = array( 'mime' => 'text/csv', 'size' => 50000 ); 
 $options[ 'fileFormat' ] = array( "S. No.", "Category", "First name", "Last name", "Date of birth", "M/F", "email", "phone", "Performance remarks", "Fee" );
            $options[ 'cols' ]       = "(category,first_name,last_name,dob,gender,email,phone,performance_remarks,Fee,activity_id,human_id)";
            $options[ 'table' ]      = 'activity_participants';
            $options[ 'file_name' ]  = 'ActivityPaticipantRead-' . $activityId;
            $resultOne               = $this->_uploadFile( $data, $options );
            if ( $resultOne[ 'error' ] ) {
                $output[ 'error' ] = $resultOne[ 'error' ];
                return $output;
            } //$resultOne[ 'error' ]
            $resultTwo = $this->processFile( $resultOne[ 'data' ], $options[ 'fileFormat' ] );
            if ( isset( $resultTwo[ 'error' ] ) ) {
                return $resultTwo;
            } //isset( $resultTwo[ 'error' ] )
            $vals               = $resultTwo[ 'data' ];
            $fields = array( "serialno", "sore", "fname", "lname", "dob", "gender", "email", "mobileno", "performance", "feespaid" ); 
$data[ 'metadata' ] = array( array( "name" => "serialno", "label" => "SNO.", "datatype" => "integer", "editable" => true ), array( "name" => "sore", "label" => "STUDENT / ENTREPRENEUR", "datatype" => "string", "editable" => true ), array( "name" => "fname", "label" => "FIRST NAME", "datatype" => "string", "editable" => true ), array( "name" => "lname", "label" => "LAST NAME", "datatype" => "string", "editable" => true ), array( "name" => "dob", "label" => "D.O.B", "datatype" => "string", "editable" => true ), array( "name" => "gender", "label" => "M / F", "datatype" => "string", "editable" => true ), array( "name" => "email", "label" => "EMAIL ID", "datatype" => "string", "editable" => true ), array( "name" => "mobileno", "label" => "MOBILE NO", "datatype" => "string", "editable" => true ), array( "name" => "performance", "label" => "PERFORMANCE", "datatype" => "string", "editable" => true ), array( "name" => "feespaid", "label" => "FEES PAID", "datatype" => "string", "editable" => true ) );
            $count              = 0;
            foreach ( $vals as $key => $val ) {
                $temp                                 = array(
                     $fields[ 0 ] => $count + 1,
                    $fields[ 1 ] => $val[ 0 ],
                    $fields[ 2 ] => $val[ 1 ],
                    $fields[ 3 ] => $val[ 2 ],
                    $fields[ 4 ] => $val[ 3 ],
                    $fields[ 5 ] => $val[ 4 ],
                    $fields[ 6 ] => $val[ 5 ],
                    $fields[ 7 ] => $val[ 6 ],
                    $fields[ 8 ] => $val[ 7 ],
                    $fields[ 9 ] => $val[ 8 ] 
                );
                $data[ 'data' ][ $count ][ 'id' ]     = $count + 1;
                $data[ 'data' ][ $count ][ 'values' ] = $temp;
                $count++;
            } //$vals as $key => $val
            return json_encode( $data );
        } //$participantFile
    }
    
    
    /****** END FUNCTION LIST FOR UPLOADING AND SENDING THE FILE DATA BACK FOR DATAGRID DISPLAY  *****/
    
    
    /** FILE PROCESSING FUNCTIONS - COMMON TO ALL ACTIONS **/
    function processFile( $fileHandle, $columnFormat )
    {
        $vals = array( );
        if ( ( $handle = fopen( $fileHandle, "r" ) ) == FALSE ) {
            $output[ 'error' ] = 'Could not open the file,Please Try again';
            return $output;
        } //( $handle = fopen( $fileHandle, "r" ) ) == FALSE
        $row = 1;
        while ( ( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE ) {
            if ( $row == 1 ) {
                for ( $count = 0; $count < count( $line ); $count++ ) {
                    if ( !( $line[ $count ] == $columnFormat[ $count ] ) ) {
                        $output[ 'error' ] = 'Incorrect File Format uploaded , please correct the file column format and try again';
                        return $output;
                    } //!( $line[ $count ] == $columnFormat[ $count ] )
                } //$count = 0; $count < count( $line ); $count++
            } //$row == 1
            else {
                array_shift( $line );
                $vals[ ] = $line;
            }
            $row++;
        } //( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE
        fclose( $handle );
        $output[ 'data' ] = $vals;
        return $output;
    }
    
    protected function _uploadFile( $data, $options )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;
        $allowedMimes = $options[ 'mime' ];
        $fileFormat   = $options[ 'fileFormat' ];
        $size         = $options[ 'size' ];
        if ( ( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size ) ) {
            $goodtogo          = false;
            $result[ 'error' ] = "Could not upload the file: Invalid File Format";
            return $result;
        } //( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size )
        try {
            $target = APPLICATION_PATH . "/../public/uploads/activity/" . $data[ 'name' ];
            //die($target);
            $source = $data[ 'tmp_name' ];
            // die($target.'<br/>'.$source);
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = false;
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $result[ 'error' ] = $e->getmessage();
            return $result;
        }
        if ( !$goodtogo ) {
            $result[ 'error' ] = 'File Not Uploaded,Please Try again';
            return $result;
        } //!$goodtogo
        $result[ 'data' ]  = $target;
        $result[ 'error' ] = "";
        return $result;
    }
    /** END FILE PROCESSING FUNCTIONS - COMMON TO ALL ACTIONS **/
    
    function process_datagrid($data,$columns){
        $out = array();
        $temp = array();
        $data_grid = json_decode($data);
        foreach($data_grid as $key=>$val){
                $data_grid_data =  (array)$val->values; 
                $data_grid_vals = (array)$val->columns; 
                foreach($data_grid_vals as $k=>$v){
                   if(TRUE){ //change true to column key comparision( if(in_array($k,$columns))   ) for letting only those entries as result
                        $temp[] = "'$v'";  
                    }
                }
                
                $out[$key] = implode(',',$temp);
                unset($temp);
                
            }
        
        return $out;
    }
    
 public function get_grid_metaData($identifier) {
     $gridMeta['grid_org'] =
                array(
                    
                    array('name' => 'designation', 'label' => 'Designation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'instororg', 'label' => 'Institution/ Organisation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'firstname', 'label' => 'First name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'lastname', 'label' => 'Last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'title', 'label' => 'Title (Mr/Ms./Dr./Prof.)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'areaofexpr', 'label' => 'Area of expertise', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'dostrtasmntr', 'label' => 'Date of Starting as Mentor', 'datatype' => 'date', 'editable' => true),
                     array('name' => 'noofmenteesthisinst', 'label' => 'Number of Mentees in this institute', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'email', 'label' => 'Email', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'phone', 'label' => 'Phone', 'datatype' => 'string', 'editable' => true)
        );
        
        $gridMeta['grid_mentee'] =
                array(
                    array('name' => 'firstname', 'label' => 'First Name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'lastname', 'label' => 'Last Name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'institution', 'label' => 'Institution', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'programme', 'label' => 'Programme', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'yrofcompl', 'label' => 'Year of Graduation', 'datatype' => 'string', 'editable' => true),
					array('name' => 'email', 'label' => 'Email', 'datatype' => 'string', 'editable' => true),                
                    array('name' => 'phone', 'label' => 'Phone', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'businessidea', 'label' => 'Business Idea', 'datatype' => 'string', 'editable' => true),
					array('name' => 'mentoremail', 'label' => 'Mentor email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'mentorphone', 'label' => 'Mentor phone', 'datatype' => 'string', 'editable' => true),
					array('name' => 'mentorfirstname', 'label' => 'Mentor First Name', 'datatype' => 'string', 'editable' => true),
					array('name' => 'mentorlastname', 'label' => 'Mentor Last Name', 'datatype' => 'string', 'editable' => true),
        );
         
        
        $gridMeta['grid_mentor'] =
                array(
                    array('name' => 'designation', 'label' => 'Designation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'instororg', 'label' => 'Institution/Organisation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'firstname', 'label' => 'First name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'lastname', 'label' => 'Last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'title', 'label' => 'Title (Mr/Ms./Dr./Prof.)', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'areaofexpr', 'label' => 'Area of expertise', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'dostrtasmntr', 'label' => 'Date of Starting as Mentor', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'noofmenteesthisinst', 'label' => 'Number of Mentees in this institute', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'email', 'label' => 'Email', 'datatype' => 'string', 'editable' => true),
                     array('name' => 'phone', 'label' => 'Phone', 'datatype' => 'string', 'editable' => true), 
        );
        $gridMeta['grid_venture'] =
                array(
                    array('name' => 'venturename', 'label' => 'Venture name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'areaofbusiness', 'label' => 'Area of business', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'targetstartupdate', 'label' => 'Target start-up date', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'menteefirstname', 'label' => 'Mentee first name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'menteelastname', 'label' => 'Mentee last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'mentorfirstname', 'label' => 'Mentor first name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'mentorlastname', 'label' => 'Mentor last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'totmntrngmeets', 'label' => 'Total Mentoring meetings held', 'datatype' => 'string', 'editable' => true),
					array('name' => 'dateoflastmeet', 'label' => 'Date of last meeting', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'mntesltstfdbk', 'label' => 'Mentee`s latest feedback', 'datatype' => 'string', 'editable' => true),
					array('name' => 'mntrsltstfdbk', 'label' => 'Mentor`s latest feedback', 'datatype' => 'string', 'editable' => true),
        );
        
        if(!$identifier)
            return $gridMeta;
        return $gridMeta[$identifier];
 }  
 
    
    
    
}

 