<?php

class Application_Model_Edb {
     /**
 * class Application_Model_Edb  for controlling the edb  actions 
 * @author     Kanchan Karjee <kanchan@inkoniq.com>
 * @version    0.0.1
 */
	protected $_db;
	public function __construct() {
        $dbconfig = Zend_Registry::get("dbconfig");
        $this->_db = new mysqli($dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname);
    }

    public function fetchAll($q) {
        if (!$q)
            return false;
        $result = false;
        $r = $this->_db->query($q);
        if ($r) {
            while ($row = mysqli_fetch_array($r, MYSQL_ASSOC)) {
                $result[] = $row;
            }
        }
        return $result;
    }

    public function fetchOne($q) {
        if (!$q)
            return false;
        $result = false;
        $r = $this->_db->query($q);
        //echo $q . '--';
        $result = mysqli_fetch_array($r, MYSQL_ASSOC);
        return $result;
    }

    public function queryTx($q) {
        //echo $q;
        $this->_db->autocommit(false);
        if (!$this->_db->query($q)) {
            $this->_db->rollback();
            return false;
        }
        
        $this->_db->commit();
        return true;
    }

    public function mqueryTx($mq) {
        $this->_db->autocommit(false);
        if (is_array($mq)) {
            foreach ($mq as $q) {
                //echo $q;
                if (!$this->_db->query($q)) {
                    $this->_db->rollback();
                    return false;
                }
            }
            $this->_db->commit();
            return true;
        }
        return false;
    }
    
    public function insertQuery($q) {
        //echo $q;
        if($this->_db->query($q)) {
            return $this->_db->insert_id;
        }
        return false;
    }
    
    function filter($f, $n = null) {
    
        if ($f == 'null' || is_null($f)) {
            $f1 = 'NULL';
        }
        else if (!trim($f)) {
            $f1 = 'NULL';
        }
        else if ($n != 'string') {
            $f1 = "'" . mysqli_real_escape_string($this->_db, $f) . "'";
        }

        if($n == 'numeric_bool') {
            if(!$f) {
                $f1=0;
            } else 
                $f1 =1;
        } else if ($n == 'date') {
            if($f) {
                $_f = explode('-', $f);
                $f1 = "'".$_f[2].'-'.$_f[1].'-'.$_f[0]."'";
            }   
        } else if ($n == 'amount') {
            if(!$f) {
                $f1 = 0;
            } else {
                $f1 = (float)$f;
            }
        } else if ($n == 'string') {
            if(!$f) {
                $f1 = "''";
            } else {
                $f1 = "'".mysqli_real_escape_string($this->_db, $f)."'";
            }
        }

        return $f1;
    }
}

function filter($f, $n = null) {
    
    if ($f == 'null' || is_null($f)) {
        $f1 = 'NULL';
    }
    else if (!trim($f)) {
        $f1 = 'NULL';
    }
    else
        $f1 = "'" . $f . "'";
    
    if($n == 'numeric_bool') {
        if(!$f) {
            $f1=0;
        } else 
            $f1 =1;
    } else if ($n == 'date') {
        if($f) {
            $_f = explode('-', $f);
            $f1 = "'".$_f[2].'-'.$_f[1].'-'.$_f[0]."'";
        }   
    } else if ($n == 'amount') {
        if(!$f) {
            $f1 = 0;
        } else {
            $f1 = (float)$f;
        }
    } else if ($n == 'string') {
        if(!$f) {
            $f1 = "''";
        } else {
            $f1 = "'".$f."'";
        }
    }
    
    return  $f1;
}

function sanitize_null($data) {
    if(is_array($data)) {
        foreach($data as $key => $val) {
            if($val == null || $val == 'null') {
                $data[$key] = '';
            }
        }
    }
    return $data;
}