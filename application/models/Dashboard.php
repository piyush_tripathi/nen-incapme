<?php
class Application_Model_Dashboard extends Application_Model_Db
{ /**
 * class Application_Model_Activity for controlling the dashboard actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' ); 
    }
     function get_inst_name($institute_id){
        $output = array();
        $query  = 'SELECT name from institute where id ='.$institute_id;
        $output = $this->execute_query($query,'multi');         
        return $output;          
        
    }
    function get_inst_details($institute_id){
        $output = array();
        $query  = 'SELECT primary_address_1,primary_address_2,primary_city,primary_zip_code from institute where id ='.$institute_id;
        $output = $this->execute_query($query,'multi'); 
       //die(print_r($query));
        return $output;            
        
    }
    function get_inst_count($institute_id){
      
        $output = array();
        
     
        
//        get data for campus compnay for the institute 
        $query  = 'select count(*) as count from activity where activityinfo_organisingentity="Campus Company" and institute_id ='.$institute_id;
        $output['campus_company'] = $this->execute_query($query,'multi');        
           

    //        get data for trained_fac for the institute 
        $query  = "SELECT count(*) as count from fdp where fdp_published = 1 and institute_id = $institute_id";
        $output['trained_fac'] = $this->execute_query($query,'multi'); 
    //        get data for ecell for the institute 
        $query  = "SELECT distinct over_num as count from ecell where institute_id = $institute_id";
        $output['ecell'] = $this->execute_query($query,'multi'); 
        //        get data for activity for the institute 
        $query  = "SELECT count(*) as count  from activity where institute_id = $institute_id";
        $output['activity'] = $this->execute_query($query,'multi'); 
        //        get data for funding for the institute 
        $query  = "SELECT SUM(
COALESCE(x.edcfund_sanction,0) + 
COALESCE(y.fund_received,0) )as count FROM edc as x join iedc as y on x.institute_id=y.institute_id and x.institute_id=$institute_id";
        $output['funding'] = $this->execute_query($query,'multi'); 
        //        get data for venture for the institute 
        $query  = "SELECT vt_noofventure as count  from svmu where institute_id = $institute_id";
        $output['venture'] = $this->execute_query($query,'multi'); 
        //        get data for iedc for the institute 
        $query  = "select COUNT(*) as count from iedc_project_data where iedc_id in (SELECT iedc_id from iedc where institute_id=$institute_id)";
        $output['iedc'] = $this->execute_query($query,'multi'); 
       
        
        return $output;
        
    }
    function get_inst_cc($institute_id){
        $output = array();
        $type= 'multi';
        $query  = 'select * from campus_company where institute_id='.$institute_id;
        $output = $this->execute_query($query,$type);
        return $output;            
        
    }
    function get_inst_activities($institute_id,$year){
        $output = array();
        $result = array();
        $query  = 'select * from activity where DATE_FORMAT(activityplacetime_date_start,"%Y")="'.$year.'" and institute_id='.$institute_id.' order by activityplacetime_date_start asc';
         $type= 'multi';
        $output = $this->execute_query($query,$type);
        $result  = $this->process_activity_data($output);
        return $result;            
        
    }
    
    function process_activity_data($data){
        $result = array();
         
        //puts data in format for displaying activites monthwise 
        $month = array("1"=>"January","2"=>"February","3"=>"March","4"=>"April","5"=>"May","6"=>"June","7"=>"July","8"=>"August","9"=>"September","10"=>"October","11"=>"November","12"=>"December");
        foreach($data as $key=>$value){
           
            $m = explode('-',$value['activityplacetime_date_start']);
             
            switch ($month[(int)$m[1]]) {   // $m has year month date as array elements
                case 'January':
                    $result['January'][]= $value;
                    break;
                 case 'February':
                     $result['February'][]= $value;
                    break;
                 case 'March':
                     $result['March'][]= $value;
                    break;
                 case 'April':
                     $result['April'][]= $value;
                    break;
                 case 'May':
                     $result['May'][]= $value;
                    break;
                 case 'June':
                     $result['June'][]= $value;
                    break;
                 case 'July':
                     $result['July'][]= $value;
                    break;
                 case 'August':
                     $result['August'][]= $value;
                    break;
                 case 'September':
                     $result['September'][]= $value;
                    break;
                 case 'October':
                     $result['October'][]= $value;
                    break;
                 case 'November':
                     $result['November'][]= $value;
                    break;                
                   case 'December':
                       $result['December'][]= $value;
                    break;

                default:
                    break;
            }
        }
        
        return $result;
  
    }
    
    function lead_faculty_dashboard($institute_id,$show_activities='show'){
        $output = array();
       
        /** for ecell  **/
        $sql = "SELECT * FROM ecell where institute_id=$institute_id limit 1";
        $upper_limit = 33;
        $type= 'ecell' ;
        $columns = $this->get_columns($type);
        $result = $this->execute_query($sql);   
        $score = $this->calculate_completion($columns,$sql,$upper_limit,$result);
        $output[$type]['score'] = $score;
        $output[$type]['pie'] = $this->pie_score($score);
        $output[$type]['last_changed'] = $result['changed'];
        $output[$type]['last_changed_text'] = ($result['changed']!='0000-00-00 00:00:00' && $result['changed']!=null)  ? date_format(date_create($result['changed']),'d M Y') : '';
        unset($result);
//        die(print_r($output));
       /** for edc  **/
        $sql = "SELECT * FROM edc where institute_id=$institute_id limit 1";
        $upper_limit = 24;
        $type= 'edc' ;
        $columns = $this->get_columns($type);
        $result = $this->execute_query($sql);   
        $score = $this->calculate_completion($columns,$sql,$upper_limit,$result);
        $output[$type]['score'] = $score;
        $output[$type]['pie'] = $this->pie_score($score);
        $output[$type]['last_changed'] = $result['changed'];
        $output[$type]['last_changed_text'] = ($result['changed']!='0000-00-00 00:00:00' && $result['changed']!=null)  ? date_format(date_create($result['changed']),'d M Y') : '';
        unset($result);
        
        /** for esc  **/
        $sql = "SELECT * FROM esc where institute_id=$institute_id limit 1";
        $upper_limit = 11;
        $type= 'esc' ;
        $columns = $this->get_columns($type);
        $result = $this->execute_query($sql);   
        $score = $this->calculate_completion($columns,$sql,$upper_limit,$result);
        $output[$type]['score'] = $score;
        $output[$type]['pie'] = $this->pie_score($score);
        $output[$type]['last_changed'] = $result['dateofcreation'];
        $output[$type]['last_changed_text'] = ($result['dateofcreation']!='0000-00-00 00:00:00' && $result['dateofcreation']!=null)  ? date_format(date_create($result['dateofcreation']),'d M Y') : '';
        unset($result);
        
         /** for iedc  **/
        $sql = "SELECT * FROM iedc where institute_id=$institute_id limit 1";
        $upper_limit = 53;
        $type= 'iedc' ;
        $columns = $this->get_columns($type);
        $result = $this->execute_query($sql);   
        $score = $this->calculate_completion($columns,$sql,$upper_limit,$result);
        $output[$type]['score'] = $score;
        $output[$type]['pie'] = $this->pie_score($score);        
        $output[$type]['last_changed'] = $result['datecreated'];
        $output[$type]['last_changed_text'] = ($result['datecreated']!='0000-00-00 00:00:00' && $result['datecreated']!=null)  ? date_format(date_create($result['datecreated']),'d M Y') : '';
        unset($result);
        
         /** for svmu  **/
        $sql = "SELECT * FROM svmu where institute_id=$institute_id limit 1";
        $upper_limit = 22;
        $type= 'svmu' ;
        $columns = $this->get_columns($type);
        $result = $this->execute_query($sql);   
        $score = $this->calculate_completion($columns,$sql,$upper_limit,$result);
        $output[$type]['score'] = $score;
        $output[$type]['pie'] = $this->pie_score($score);
        $output[$type]['last_changed'] = $result['createdtime'];
        $output[$type]['last_changed_text'] = ($result['createdtime']!='0000-00-00 00:00:00' && $result['createdtime']!=null)  ? date_format(date_create($result['createdtime']),'d M Y') : '';
        unset($result);
        
          /** for campus company  **/
        $sql = "SELECT * FROM campus_company where institute_id=$institute_id limit 1";
        $upper_limit = 14;
        $type= 'cc' ;
        $columns = $this->get_columns($type);
        $result = $this->execute_query($sql);   
        $score = $this->calculate_completion($columns,$sql,$upper_limit,$result);
        $output[$type]['score'] = $score;
        $output[$type]['pie'] = $this->pie_score($score);
        $output[$type]['last_changed'] = $result['changed'];
        $output[$type]['last_changed_text'] = ($result['changed']!='0000-00-00 00:00:00' && $result['changed']!=null) ? date_format(date_create($result['changed']),'d M Y') : '';
        unset($result);
        
        
        /*  FOR ACTIVITIES DISPLAY */
        if($show_activities=='show'){
        $today  = Date("Y-m-d");
        $next_week = date("Y-m-d",strtotime("+1 week"));
        $next_month = date("Y-m-d",strtotime("+1 month"));
        $next_qtr = date("Y-m-d",strtotime("+3 months"));
        $advance = '("Mentoring","Start-up Support","Webinar","National Platform","Outreach","Review")';
        $training = '("Course")';
        $type = array('week'=>$next_week,'month'=>$next_month,'qtr'=>$next_qtr);
        foreach($type as $k=>$v){
            $sql = "SELECT * FROM activity where activityplacetime_date_start between '$today' AND  '$v' and activityinfo_type IN$advance AND institute_id=$institute_id";
            $output['activities'][$k]['advance'] = $this->execute_query($sql,'multi');
            
            $sql = "SELECT * FROM activity where activityplacetime_date_start between '$today' AND  '$v' and activityinfo_type NOT IN$training and activityinfo_type NOT IN$advance  AND institute_id=$institute_id";
            $output['activities'][$k]['upcoming'] = $this->execute_query($sql,'multi');
            
            $sql = "SELECT * FROM activity where activityplacetime_date_start between '$today' AND  '$v' and activityinfo_type IN$training  AND institute_id=$institute_id";
            $output['activities'][$k]['training'] = $this->execute_query($sql,'multi');
             
        }
        }
        //$this->print_array($output);
        return $output;
        
    }
   
    
    function consultant_dashboard(){
        //define variables
        $updated = array();
        $completed = array();
        $temp_update = array();
        $temp_complete = array();
        $temp = array();
        $output = array();
        $institute_data =  array();
        $institute_ids = array();
        $sum = '';
        //get logged in consultant id 
        $auth = Zend_Auth::getInstance();          
        $user = $auth->getIdentity(); 
        $user_id = $user->human_id;
        $role =  $user->role;
        //if valid user
        if(!user_id || $role!='CON'){
            return false;
        }
//        return true;
        // get consultant institutes
        $sql = "SELECT DISTINCT * FROM institute where consultant_in_charge =$user_id";
        $institute_list  = $this->execute_query($sql,'multi');
//        die(print_r($institute_list));

            //get updated data 
            foreach($institute_list  as $key=>$val){
                $institute_ids[$key]  = $val['id'];
                $institute_data[$key] = $this->lead_faculty_dashboard($val['id'],'hide');
            }unset($key);unset($val); 
            
              
//            die(print_r($institute_data));
            foreach($institute_data  as $key=>$val){
                $sum = 0;
                foreach($val as $k=>$v){
                    $last_changed = $v['last_changed'];
                    $score  = $v['score'];
                    $temp_update[ $last_changed ] = $v;
                    $temp_update[ $last_changed ]['institute_id'] = $institute_ids[$key];
                    $temp_update[ $last_changed ]['type'] = $k;
                    $temp_update[ $last_changed ]['institute_name'] =  $this->get_inst_name($institute_ids[$key]);
                    //for competeness
                    $temp_complete[ $score ] = $v;
                    $temp_complete[ $score ]['institute_id'] = $institute_ids[$key];
                    $temp_complete[ $score ]['type'] = $k;
                    $temp_complete[ $score ]['institute_name'] =  $this->get_inst_name($institute_ids[$key]);
                    //for calculating average                    
                    $sum+=$score;
                }     
                
                //for last updaetd module under this institute 
                //die(print_r($institute_data[6]));
                
                
                 krsort($temp_update,SORT_STRING);                  
                 $temp_update_result = array_shift( $temp_update); 
                 $changed  =  $temp_update_result['last_changed'];
                 
                    $date1 = strtotime(date('Y-m-d H:i:s'));      //todays date 
                    $date2 = strtotime($changed) ;
                    $interval = $date1 - $date2;
                    $days    = floor($interval/(60*60*24));                    
             // conditionals for last updated 
            if( $days <= 7){
                $updated[A][] = array($temp_update_result);   
            }else if($days <=30 && $days>7){
                $updated[B][] = array($temp_update_result);   
            }
            else if($days > 30 ){
                $updated[C][] = array($temp_update_result);   
            }
            else{
                $updated[D][] = array($temp_update_result);   
            }
            
        //for  complete  module under this institute             
           //conditionals for data completion 
             $avg  = round( ($sum)/6 ) ; 
//            if($institute_ids[$key]==1028){
//                die(print_r($temp_complete));
//            }
             krsort($temp_complete,SORT_STRING);
                
             $temp_complete_result = array_shift( $temp_complete);             
             $percent = $temp_complete_result[0]['score'] ;
            // conditionals for  completed 
            if($avg <= 100 && $avg > 40){
                $completed[A][] = array('average'=>$avg,$temp_complete_result);  
            }else if($avg <= 40 && $avg > 20){
                $completed[B][] = array('average'=>$avg,$temp_complete_result);
                
            }
            else if( $avg <= 20 ){
                $completed[C][] = array('average'=>$avg,$temp_complete_result);
                
            }
            else{
                $completed[D][] = array('average'=>$avg,$temp_complete_result);
                
            }
//            $test = $this->get_inst_name($institute_ids[$key]);
//            echo print_r($test).'<br/>';
               unset($key);unset($val);unset($temp_complete);unset($temp_update);unset($temp);unset($temp_complete_result);unset($temp_update_result);     
            
          } 
          
          $output['completed'] = $completed;
          $output['updated'] = $updated;
          
//          die(print_r($output['completed']));
          return $output;
    }
    function consultant_calendar($date,$user_id){
        //init
        $output = array();
        $institute_ids = array();
        $date = explode('-',$date);
        $month = $date[0];
        $year = $date[1];
        $timestamp = mktime(0, 0, 0, $month, 1);
        $monthName = date('M', $timestamp );


        
        //access check
      
//        return true;
        // get consultant institutes
        $sql = "SELECT DISTINCT * FROM institute where consultant_in_charge =$user_id";
        $institute_list  = $this->execute_query($sql,'multi');
        foreach($institute_list as $key=>$val){
            $institute_ids[] = $val['id'];
        }
        $institute_ids_list  = '('.implode(',',$institute_ids).')'; 
            //get updated data

              
                //for week1
                $sql =  "SELECT  DISTINCT x.*,y.name FROM activity as x join institute as y on x.institute_id=y.id WHERE  x.institute_id IN $institute_ids_list and MONTH(x.activityplacetime_date_start) = $month AND  YEAR(x.activityplacetime_date_start)=$year  AND  DAY(x.activityplacetime_date_start) between 1 and 7 order by x.activityplacetime_date_start asc";
                $temp =$this->execute_query($sql,'multi');
                if(!empty($temp)){
                    $output['week1'][] = $temp;
                }
                unset($temp);
                
                
               
                //for week 2
                 $sql =  "SELECT  DISTINCT x.*,y.name FROM activity as x join institute as y on x.institute_id=y.id WHERE  x.institute_id IN $institute_ids_list and MONTH(x.activityplacetime_date_start) = $month AND  YEAR(x.activityplacetime_date_start)=$year  AND  DAY(x.activityplacetime_date_start) between 8 and 14 order by activityplacetime_date_start asc";
               $temp =$this->execute_query($sql,'multi');
              
                if(!empty($temp)){
                    $output['week2'][] = $temp;
                }
                unset($temp);
                
                //for week 3 
                 $sql =  "SELECT  DISTINCT x.*,y.name FROM activity as x join institute as y on x.institute_id=y.id WHERE  x.institute_id IN $institute_ids_list and MONTH(x.activityplacetime_date_start) = $month AND  YEAR(x.activityplacetime_date_start)=$year  AND  DAY(x.activityplacetime_date_start) between 15 and 21 order by activityplacetime_date_start asc";
                $temp =$this->execute_query($sql,'multi');
                if(!empty($temp)){
                    $output['week3'][] = $temp;
                }
                unset($temp);
                
                 //for week 4 
                 $sql =  "SELECT  DISTINCT x.*,y.name FROM activity as x join institute as y on x.institute_id=y.id WHERE  x.institute_id IN $institute_ids_list and MONTH(x.activityplacetime_date_start) = $month AND  YEAR(x.activityplacetime_date_start)=$year  AND  DAY(x.activityplacetime_date_start) between 22 and 31 order by activityplacetime_date_start asc";
                $temp =$this->execute_query($sql,'multi');
                if(!empty($temp)){
                    $output['week4'][] = $temp;
                }
                unset($temp);
                
                
               
           
            array_filter($output, $c);
        $output['date'] =  $monthName.'-'.$year;
        
        return $output;
       
        //get the required month   
        //get all the institutes related to the consultant  
        
        // foreach institute get actvitites for the required month 
                //conditions for weekly activities 
                  
       
    }
    
    function calculate_completion($columns,$sql,$upper_limit,$result){
        if(empty($columns)|| !$sql || !$upper_limit){
            return false;
        }
        $score = 0;            
        foreach($columns as $column){
           if( isset($result[$column]) ){
               $score++;
           }
        }
        
        $complete_percent = ( $score/$upper_limit )*100; 
        
        return round((int)$complete_percent);
        
    }
    
    
    function execute_query($query,$type=null){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                if($type=='multi'){
                    $result[] = $qR->fetch_assoc( );
                }
                else{
                    $result = $qR->fetch_assoc( );
                }
                  
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
           
        } catch (Exception $e) {
            //die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    
    function get_columns($type){
        switch($type){
            case 'ecell': 
                $out  = array('over_date','over_name','over_lead_name','over_second_lead_name','over_lead','over_second','over_strength','over_num','over_eleadnum','over_pointofcontact1','over_pointofcontact2','over_launch','startup_roles','startup_facadv','startup_board_faculty','startup_success','startup_plan','startup_calen','estab_prof','estab_market','estab_visible','estab_money','estab_report','estab_uploadrep','hiperf_news','hiperf_latestnews','hiperf_web','hiperf_link','hiperf_support','hiperf_supportevidence','hiperf_sign','hiperf_signtitle','hiperf_signdate');
                break;
            case 'edc': 
                $out = array('edcinfo_year','edcteam_lf','edcteam_colead','edcteam_stu','stu_mem_id','edcadvboard_adv','edcadvboard_date1','edcadvboard_date2','edcprog_disc','edcprog_adopt','edcprog_upload','edcfund_sent','edcfund_sanction','edcfund_other','edcfund_date','edcinfra_avlbl','institute_id','infra_space','infra_equipment','infra_library','infra_staff','infra_budget','infra_other','infra_other_text');
                break;
        
            case 'esc':
                $out = array('hasesc','bi_foundationyear','bi_prncplhodbriefed','bi_domeet','bi_planofactiondiscussed','bi_planofactionadopted','bi_uploadplanorMinutes','bi_availableinfra','td_leadfacname','td_coleadfacname','td_noofactivestudents');
                break;
            case 'iedc': 
                $out = array('bi_edcoriedc','bi_foundationyear','bi_mgmtbuyinsecured','bi_prncplhodbriefed','bi_domeet','bi_planofactiondiscussed','bi_planofactionadopted','bi_uploadplanorMinutes','bi_availableinfra','td_leadfacname','td_coleadfacname','td_noofactivestudents','td_noofadvisorymembers','td_uploadlistofmembers','td_dofabmeet','td_domrabmeet','pc_upgrade','pc_processreviewed','pc_nenprerequisites','pc_isleveladequate','pc_explainadequate','pc_iswfstructured','pc_explainwf','pc_nencriteriaused','pc_pipused','pp_creatingprojectpipe','pp_ongoingproject','pp_avgprojqual','pp_iicompeteplan','pp_noofprojectsidentified','pp_reqforpanelistsent','pp_extpanelistidentified','pp_prsntatnschdlfinalised','pp_dostudentpresentation','ps_proceeding','ps_selectioncomplete','ps_domeetforselection','ps_mentorsassigned','ps_facfortechnopreneur','ps_listofinternalfac','ps_dateofcourse','pff_seekingfund','pff_guidelinefollowed','pff_feedbackcompleteddate','pff_proposalssubmitteddate','pff_submittedtoentity','pff_uploadcopyofproposal','pff_dofinalpitch','fund_seeking','fund_letterreceiveddate','fund_amount','fund_received','fund_source');
                break;
            case 'svmu': 
                $out = array('ov_text','ov_mai_sa','ov_mai_ta','ov_mai_fra','ov_mai_savalue','ov_mai_tavalue','ov_mai_fravalue','ov_leadfaculty','ov_organisingteam','ov_dateoflaunch','prgmfinalised','prgm_uploadschedule','prgm_bdma','prgm_uploadapplform','prgm_noofapplicants','prgm_totnoofmentors','prgm_totnoofmentees','vt_noofventure','vt_menteefeedreviewed','vt_dateoflastreview','vt_dateoflastreport','vt_uploadreport'); 
                break;
            case 'cc': 
                $out = array('cc_name','cc_date','cc_area','ccdetail_areadetail','ccdetail_outlay','ccdetail_status','cchealth_remarks','cchealth_closure','cchealth_revenue','cchealth_review','stu_mem_id','ccteam_advisors','ccteam_fac','faculty_name');
                break;
            
            default:
                break;
        }
   
       return $out;     
    }
    
    
    function print_array($data){
       echo '<pre>';
       print_r($data);
       echo '</pre>';
       
    }
    
    function pie_score($complete){
        return true;
        $incomplete  = 100 - (int)$complete;
        $pie = "                   var pieData = [           {               value: $incomplete,               color:'#E64c3c'                         },           {               value : $complete,               color : '#2ecc71',               label : 'Completed',               labelColor : 'white',               labelAlign : 'center',               labelFontSize : '8',               fontFamily : 'Helvetica Neue'           }";
        
        return $pie ;

    }
    
    
}

