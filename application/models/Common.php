<?php

class Application_Model_Common {
    public function __construct() {
        //error_reporting(0);
        $this->db = new Application_Model_Edb();
    }
    
    public function getFacultyOfInstitute($instituteId){
        $q = "Select h.id as fid, h.first_name as fname, h.last_name as lname from human_resource h join human_institute hi on (h.id=hi.human_id) where hi.institute_id=$instituteId";
        
        if($data = $this->db->fetchAll($q)) {
            
            return $data;
        }
       
        return false;
    }
    
    public function getFacultyDDData($instituteId) {
        $result = $this->getFacultyOfInstitute($instituteId);
        //die(print_r($result));
        $r = array();
        if(is_array($result)) {
            foreach($result as $row) {
                $r[] = array('value' => $row['fid'], 'caption' => $row['fname'] . ' '.$row['lname']);
            }
        }
        return $r;
    }
    
    
    public function getCitiesDDData() {
        $result = $this->db->fetchAll("Select city_id, name, teritory from city where city_id < 9999");
        $r = array();
        if(is_array($result)) {
            foreach($result as $row) {
                //$r[] = array('value' => $row['city_id'], 'caption' => $row['name']);
                $r[] = array('value' => $row['name'], 'caption' => $row['name']);
            }
        }
       // print_r($r);
        return $r;
    }
    
    public function getConsultantCitiesDDData($consultantId) {
        $result = $this->db->fetchAll("Select distinct i.city_id as city_id, c.name as name, c.teritory from city c join institute i on (c.city_id = i.city_id) where i.consultant_in_charge = '$consultantId' order by name ASC");
        $r = array();
        if(is_array($result)) {
            foreach($result as $row) {
                //$r[] = array('value' => $row['city_id'], 'caption' => $row['name']);
                $r[] = array('value' => $row['name'], 'caption' => $row['name']);
            }
        }
       // print_r($r);
        return $r;
    }
    
    
    
    
    public function getConsultantDDData() {
        $result = $this->db->fetchAll("Select id, first_name, last_name from human_resource where category = 'CON'");
        $r = array();
        if(is_array($result)) {
            foreach($result as $row) {
                $r[] = array('value' => $row['id'], 'caption' => $row['first_name'] . ' '.$row['last_name']);
            }
        }
        return $r;
    }
    
    
    
    public function getEleadersDDData($ecellId) {
        $q = "Select name from ecell_over_mem where ecell_id = '$ecellId' and is_e_leader = 1";
        //echo $q;
        if($result = $this->db->fetchAll($q)) {
            $r = array();
            if(is_array($result)) {
                foreach($result as $row) {
                    $r[] = array('value' => $row['name'], 'caption' => $row['name']);
                }
            }
            
            return $r;
        }
        return false;
    }
    
    public function prepareDDData($result) {
        
        
    }
    
    public function getStaticDDData($identifier) {
        if($identifier == 'institute_type') {
            $q = "SELECT distinct type FROM institute WHERE type is not null and trim(type) !=''";
            //echo $q;
            $result = $this->db->fetchAll($q);
            foreach($result as $item) {
                $r[] = $item['type'];
            }
            //print_R($result);
            return $r;
        }
        
        if($identifier == 'edc_foundation_year') {
            for($yr = 2000; $yr <= date('Y'); $yr++) {
                $yrs[] = $yr;
            }
            return $yrs;
        }
        
        $items = array(
            'campus_industry_sector' => array("Advertising", "Agriculture", "Automotive", "Biotech", "BPO/KPO", "CleanTech", "Consulting", "Education", "E-commerce", "Export/Import", "Finance", "Garments/Textile", "Healthcare", "HR/Training", "Hospitality/Travel", "IT/Internet/Software", "Infrastructure/Real Estate", "Legal", "Media/Entertainment", "Manufacturing", "Retail/Consumer", "Sports", "Social Venture", "Telecom/Mobiles", "Others"),
            'institute_type' => array("Technical", "Management", "General" ),
            
        );
        
        $result = $items[$identifier];
        return $result;
    }
    
    public function getGridMetaData($identifier) {
        
        $gridMeta['ecell_members'] =
                array(
                    array('name' => 'name', 'label' => 'Name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'email', 'label' => 'Email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'contact_no', 'label' => 'Contact No', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'year_grad', 'label' => 'Graduation Year', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'is_e_leader', 'label' => 'E Leader', 'datatype' => 'boolean', 'editable' => true),
        );
        
        $gridMeta['ecell_estab_professionals'] =
                array(
                    array('name' => 'name', 'label' => 'Name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'organization', 'label' => 'Organization', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'designation', 'label' => 'Designation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'email', 'label' => 'Email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'contact_no', 'label' => 'Contact No', 'datatype' => 'string', 'editable' => true),
        );
         
        
        $gridMeta['campus_edc_students'] =
                array(
                    array('name' => 'designation', 'label' => 'Designation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'first_name', 'label' => 'First Name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'last_name', 'label' => 'Last Name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'dob', 'label' => 'Date of birth', 'datatype' => 'date', 'editable' => true),
                    array('name' => 'sex', 'label' => 'M/F', 'datatype' => 'select', 'editable' => true),
                    array('name' => 'email', 'label' => 'Email', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'contact_no', 'label' => 'Contact No', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'start_date', 'label' => 'Start Date', 'datatype' => 'date', 'editable' => true),
                    array('name' => 'remarks', 'label' => 'Preformance Remarks', 'datatype' => 'string', 'editable' => true),
        );
        $gridMeta['campus_edc_advisors'] =
                array(
                    array('name' => 'designation', 'label' => 'Designation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'institute', 'label' => 'Institution/Dept', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'first_name', 'label' => 'First name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'last_name', 'label' => 'Last name', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'apellation', 'label' => 'Apellation', 'datatype' => 'string', 'editable' => true),
                    array('name' => 'date_of_joining', 'label' => 'Date of joining', 'datatype' => 'date', 'editable' => true),
                    array('name' => 'date_of_leaving', 'label' => 'Date of leaving', 'datatype' => 'date', 'editable' => true),
                    array('name' => 'remarks', 'label' => 'Remarks', 'datatype' => 'string', 'editable' => true),
        );
        
        if(!$identifier)
            return $gridMeta;
        return $gridMeta[$identifier];
    }
    
    public function isFileExist($fileIdentifier) {
        switch ($fileIdentifier) {
            case 'ecell_activity_calender':
                $file = Application_Model_Csv::ECELL_FILE_UPLOAD_PATH . Application_Model_Csv::ECELL_ACTIVITY_CALENDER_FILE_NAME; 
                break;
            case 'ecell_newsletter':
                $file = Application_Model_Csv::ECELL_FILE_UPLOAD_PATH . Application_Model_Csv::ECELL_LATEST_NEWSLETTER_FILE_NAME;
                break;
            case 'edc_program':
                $file = Application_Model_Csv::EDC_FILE_UPLOAD_PATH . Application_Model_Csv::EDC_PROG_FILE_NAME;
                break;

            default:
                break;
        }
        
        if(file_exists($file)){
            return true;
        }
        return false;
    }
    
    public function validInstitute($id) {
        if($result = $this->db->fetchOne("Select id from institute where id = $id")) {
            return true;
        }
        
        return false;
    }
    
    public function validInsertCampusData($data) {
        if(trim($data['cc_name'])== '') 
            return false;
        $q = "Select count(cc_id) as count from campus_company where institute_id = ".$data['instituteid']." and cc_name= '".$data['cc_name']."'";
        $result = $this->db->fetchOne($q);
        if($result['count']) {
            return false;
        } else {
            $cc_date = $this->db->filter($data['cc_date'], 'date');
            $cc_name = $this->db->filter($data['cc_name'], 'string');
            $cc_area = $this->db->filter($data['cc_area'], 'string');
            $q = "insert into campus_company(cc_id, cc_name, cc_date, cc_area, institute_id) values('', $cc_name, $cc_date, $cc_area, ".$data['instituteid'].")";
            return $this->db->insertQuery($q);
        }
        return true;
        
    }
    
    public function validInsertEdcData($data) {
        if(trim($data['edcinfo_year'])== '' ) {
            return false;
        }
        //die(print_r($data));
        if($data['action']=='new'){
             $edcteam_lf = $this->db->filter($data['edcteam_lf']); 
             $edcteam_colead = $this->db->filter($data['edcteam_colead']);  
        }
        else{
             $edcteam_lf = $this->db->filter($data['edcteam_lf']['value']); 
        $edcteam_colead = $this->db->filter($data['edcteam_colead']['value']);  
        }
        $edcinfo_year = $this->db->filter($data['edcinfo_year']);
      
        $edcteam_stu = $this->db->filter($data['edcteam_stu']);
        
        $q = "insert into edc(edc_id, edcinfo_year, edcteam_lf, edcteam_colead, edcteam_stu, institute_id) values('', $edcinfo_year, $edcteam_lf, $edcteam_colead, $edcteam_stu, ".$data['instituteid'].")";
        //die(print_r($q));
        return $this->db->insertQuery($q);
        return true;
        
    }
    
    public function validInsertEcellData($data) {
        if(trim($data['over_date'])== '' || trim($data['over_name']) == '') {
            return false;
        }
        if($data['action']=='new'){
             $over_lead = $this->db->filter($data['over_lead'], '');
            $over_second = $this->db->filter($data['over_second'], '');  
        }
        else{
            $over_lead = $this->db->filter($data['over_lead']['value'], '');
        $over_second = $this->db->filter($data['over_second']['value'], '');  
        }
        $over_date = $this->db->filter($data['over_date'], '');
        $over_name = $this->db->filter($data['over_name'], '');
        
        $over_strength = $this->db->filter($data['over_strength'], '');
        
        $q = "insert into ecell(id, over_date, over_name, over_lead, over_second, over_strength, institute_id) values('', $over_date, $over_name, $over_lead, $over_second, $over_strength, ".$data['instituteid'].")";
        return $this->db->insertQuery($q);
        return true;
        
    }
    
    public function getAllCapmpusCompany($instituteId) {
        $q = "Select cc_id, cc_name, date_format(cc_date, '%d-%m-%Y') as cc_date, cc_area, ccdetail_areadetail, ccdetail_status from campus_company where institute_id= $instituteId";
        $result = $this->db->fetchAll($q);
        return $result;
    }
    
    public function getInstituteNameFromIC($capacityName, $id) {
        switch ($capacityName) {
            case 'campus':
                $tName = 'campus_company';
                $idColName = 'c.cc_id';

                break;
            case 'ecell':
                $tName = 'ecell';
                $idColName = 'c.id';
                break;
            case 'edc':
                $tName = 'edc';
                $idColName = 'c.edc_id';
            default:
                break;
        }
        
        
        $q = "SELECT i.name, i.id  FROM institute i join $tName c on (i.id = c.institute_id) WHERE $idColName = $id";
        $result = $this->db->fetchOne($q);
        return $result;
    }
    
    public function getInstituteForUser($userId = null, $role=null, $filters = null) {
        $columns = " id, name, primary_city ";
        if(!$userid) {
            $userid = 1;
        }
        if(!$role) {
            $role = 'ADMIN';
        }
        switch ($role) {
            case 'ADMIN':
                $whereClause = array();
                $whereCond = '';
                if($filters['city']) {
                    $whereClause[]= " primary_city = '".$filters['city']."'";
                }
                if($filters['consultant']) {
                    $whereClause[]= " consultant_in_charge = '".$filters['consultant']."'";
                }
                if($filters['type']) {
                    $whereClause[]= " type = '".$filters['type']."'";
                }
                if($whereClause) {
                    $whereCond = ' WHERE '. implode(' AND ', $whereClause);
                }
                
                $q = "Select $columns from institute" .$whereCond . " Order by name";
                break;
            case 'CON':
                
                $whereClause = array();
                $whereCond = '';
                if($filters['city']) {
                    $whereClause[]= " primary_city = '".$filters['city']."'";
                }
                
                if($filters['type']) {
                    $whereClause[]= " type = '".$filters['type']."'";
                }
                if($whereClause) {
                    $whereCond = ' AND '. implode(' AND ', $whereClause);
                }
                
                $q = "Select $columns from institute where consultant_in_charge = '$userId' ".$whereCond. " Order by name";;
                break;
            case 'FAC':
                $q = "Select $columns from institute join human_institute on (institute.id = human_institute.institute_id) where human_institute.human_id = $userId";
                
                break;

            default:
                break;
        }
        //echo $q;
        
        $result = $this->db->fetchAll($q);
        return $result;
        
    }
    
    public function getMainInfoInstitute($instituteId){
        $columns = " institute.id as id, name, institute.category as category, primary_city, concat(human_resource.first_name, ' ', human_resource.last_name) as consultant, nen_joining_date";
        $q = "Select $columns from institute join human_resource on (institute.consultant_in_charge = human_resource.id) where institute.id = $instituteId";
       
        $result = $this->db->fetchOne($q);
        return $result;
    }
    public function getInstituteName($instituteId){
        
        $q = "Select institute.id as id, name, institute.category as category, primary_city from institute where institute.id = $instituteId";
        
        $result = $this->db->fetchOne($q);
        
        return $result;
    }
    
    public function fixAndgetEcellIdFromInstitueId($instituteId) {
        $q = "Select id from ecell where institute_id = $instituteId";
        $result = $this->db->fetchOne($q);
        if(!$result) {
            if($this->validInstitute($instituteId)) {
                $iq = "insert into ecell (id, institute_id) values ('', $instituteId)";
                $newEcellId = $this->db->insertQuery($iq);
                return $newEcellId;
            } else {
                return false;
            }
        } else {
            return $result['id'];
        }
            
        return false;
    }
    
    public function fixAndgetEdcIdFromInstitueId($instituteId) {
        $q = "Select edc_id from edc where institute_id = $instituteId";
        $result = $this->db->fetchOne($q);
            
        if(!$result) {
            if($this->validInstitute($instituteId)) {
                $iq = "insert into edc (edc_id, institute_id) values ('', $instituteId)";
                $newEcellId = $this->db->insertQuery($iq);
                return $newEcellId;
            } else {
                return false;
            }
        } else {
            return $result['edc_id'];
        }
            
        return false;
    }
    
    public function getCampusIdFromInstitueId($instituteId) {
        $q = "Select campus_id from campus_company where institute_id = $instituteId";
        $result = $this->db->fetchOne($q);
        if($result['campus_id'])
            return $result['campus_id'];
        return false;
    }
    
    public function getEdcIdFromInstitueId($instituteId) {
        $q = "Select edc_id from edc where institute_id = $instituteId";
        $result = $this->db->fetchOne($q);
        if($result['edc_id'])
            return $result['edc_id'];
        return false;
    }
    
    public function getEcellIdFromInstitueId($instituteId) {
        $q = "Select id from ecell where institute_id = $instituteId";
        $result = $this->db->fetchOne($q);
        if($result['id'])
            return $result['id'];
        return false;
    }
    
    public function deleteCampus($campusId) {
        $q[] = "Delete from campus_additional_faculty where cc_id = $campusId";
        $q[] = "Delete from student_members where cc_id = $campusId";
        $q[] = "Delete from advisors where cc_id = $campusId";
        $q[] = "Delete from campus_company where cc_id = $campusId";
        if($this->db->mqueryTx($q))
            return true;
        return false;
    }
 
    
    public function getActivityName($id) {
        $q = "Select activityinfo_title from activity where activity_id = $id";
        $result = $this->db->fetchOne($q);
        if($result['activityinfo_title'])
            return $result['activityinfo_title'];
        return false;
        
    }
    
    public function getCampusName($id) {
        $q = "Select cc_name from campus_company where cc_id = $id";
        $result = $this->db->fetchOne($q);
        if($result['cc_name'])
            return $result['cc_name'];
        return false;
        
    }
    function get_user_role(){
        $auth = Zend_Auth::getInstance();          
        $user = $auth->getIdentity(); 
        $user_id = $user->human_id;
        $role =  $user->role;
        if($role=='FAC'){
           $sql = "SELECT COUNT(*) as count from institute where head_of_institute=$user_id";
            $this->mdlDashboard          = new Application_Model_Dashboard();
            $result =  $this->mdlDashboard->execute_query($sql);
           if($result['count']>0){
               $role = 'LEAD_FAC';
           }
        }
        return $role;
    }
 
    function display_date($date){
        if(!$date || $date=='0000-00-00 00:00:00' || $date==''){
            return '';
        }
        
        $input_date = DateTime::createFromFormat('Y-m-d', $date);
        $output_date = $input_date->format('d-m-Y');
        return $output_date;
        
    }
    
    
    
    function upload_file( $data,$folder, $options=null )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;        
        $allowedMimes = $options[ 'mime' ];
        $fileFormat   = $options[ 'fileFormat' ];
        $size         = $options[ 'size' ];
         $file_name = $data['name'];
        if ( ( $data[ 'size' ] == 0 )  ) {
            $goodtogo          = false;
            $result[ 'error' ] = "Could not upload the file: Invalid File Format";
            return null;
        } //( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size )
        try {
            $target = APPLICATION_PATH . "/../public/uploads/$folder/" . $file_name ;
            //die($target);
            $source = $data[ 'tmp_name' ];
            // die($target.'<br/>'.$source);
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = 'false';
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $result[ 'error' ] = $e->getmessage();
            return null;
        }
        if ( !$goodtogo ) {
            $result[ 'error' ] = 'File Not Uploaded,Please Try again';
            return null;
        } //!$goodtogo
        $result[ 'data' ]  = $target;
        $result[ 'error' ] = "";
        return $target;
    }
    
    function upload_file_data( $module,$id ){
     $sql = "SELECT startup_calen,estab_report,hiperf_latestnews as count from ecell where institute_id=$id";
        $this->mdlDashboard          = new Application_Model_Dashboard();
        $result =  $this->mdlDashboard->execute_query($sql);
        return $result;
    }
    
    
    function execute_query($query,$type=null){
        $result = array();
        try {
            $qR = $this->mysqli->query( $query );
            if($qR->num_rows == 1){
                if($type=='multi'){
                    $result[] = $qR->fetch_assoc( );
                }
                else{
                    $result = $qR->fetch_assoc( );
                }
                  
            }
            else if($qR->num_rows > 1){
                while($qRR = $qR->fetch_assoc( )){
                $result[] = $qRR;
                }
            }
            else{
                 return false;
            }
           
        } catch (Exception $e) {
            //die( $e->getTraceAsString());
            return false;
        }
        if(empty($result)){
            return null;
        }
        return $result;
   
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>



