<?php

class Campus {

    private $id = 0;
    private $db = null;
    public $data = array();
    public $campusId = '1';
    protected $commonModel;

    public function __construct($id = null, $fetch = false) {
        error_reporting(0);
        $this->db = new Application_Model_Edb();
        $this->commonModel = new Application_Model_Common();

        if ($id) {
            $this->id = $id;
        }
        if ($fetch) {
            $q = "Select *,
                date_format(cc_date, '%d-%m-%Y') as cc_date,
                date_format(cchealth_closure, '%d-%m-%Y') as cchealth_closure, 
                date_format(cchealth_review,  '%d-%m-%Y') as cchealth_review  
            from campus_company where cc_id = " . $this->id;
            
            $this->data = $this->db->fetchOne($q);
            
            $this->data['ccteam_fac'] = $this->getFacultyNameAndId($this->data['ccteam_fac']);
            $this->fetchAdditionalFaculties();
            $this->fetchStudentMembers();
            $this->fetchAdvisors();
            //$this->getStudents();
            //$this->getAdvisors();

            //$this->fetchMembersData();
            //$this->fetchSponsorsData();
        }
    }

    public function setCampusData($data) {
        $this->data = $data;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getFacultyNameAndId($id) {
        if (!$id)
            return null;
        $q = "select first_name, last_name from human_resource where id = $id";
        if ($res = $this->db->fetchOne($q)) {
            return array('value' => $id, 'caption' => $res['first_name'] . ' ' . $res['last_name']);
        }
        return false;
    }

    public function fetchAdditionalFaculties() {
        $q = "SELECT human_id as value, concat(first_name, ' ', last_name) as caption FROM campus_additional_faculty AS c JOIN human_resource AS h ON (c.human_id = h.id) WHERE cc_id = $this->id";
        $this->data['ccteam_additional_fac'] = array();
        if ($fs = $this->db->fetchAll($q)) {
            $this->data['ccteam_additional_fac'] = $fs;
        }
    }

    public function fetchStudentMembers() {
        $q = "SELECT designation, first_name, last_name, date_format(dob, '%d-%m-%Y') as  dob, sex, email, contact_no, date_format(start_date, '%d-%m-%Y') as start_date,  remarks FROM student_members	WHERE cc_id=" . $this->id; //AND member_type = 'CC' implicit 
        $this->data['student_members']['data'] = array();
        $this->data['student_members']['metadata'] = $this->commonModel->getGridMetaData('campus_edc_students');
        $i = 0;
        if ($student_members = $this->db->fetchAll($q)) {
            foreach ($student_members as $mem) {
                //print_r($student_members);
                $this->data['student_members']['data'][$i]['id'] = $i;
                $this->data['student_members']['data'][$i]['values'] = $mem;
                $i++;
            }
        }
        
    }

    public function fetchAdvisors() {
        $q = "SELECT designation, institute, dept, first_name, last_name, apellation, date_format(date_of_joining, '%d-%m-%Y') as date_of_joining, date_format(date_of_leaving, '%d-%m-%Y') as date_of_leaving FROM advisors WHERE cc_id=" . $this->id; //AND member_type = 'CC' implicit 
        $this->data['advisors'] = array();
        $this->data['advisors']['metadata'] = $this->commonModel->getGridMetaData('campus_edc_advisors');
        $i = 0;
        if ($advisors = $this->db->fetchAll($q)) {
            foreach ($advisors as $mem) {
                //print_r($advisors);
                $this->data['advisors']['data'][$i]['id'] = $i;
                $this->data['advisors']['data'][$i]['values'] = $mem;
                $i++;
            }
        }
        
    }

    public function getJsonData() {
        return json_encode($this->data);
    }

    public function getData() {
        return $this->data;
    }

    

    public function getAdvisors() {
        $advisorsGrid = array(
            'metadata' => $this->commonModel->getGridMetaData('campus_edc_advisors'),
            'data' => array(),
        );
        $this->data['advisors'] = $advisorsGrid;
        return $advisorsGrid;
    }

    public function updateOverview() {
        //print_r($this->data);
        $cc_name = $this->db->filter($this->data['cc_name'], '');
        $cc_date = $this->db->filter($this->data['cc_date'], 'date');
        $cc_area = $this->db->filter($this->data['cc_area'], '');

        $q = "UPDATE campus_company SET cc_name = $cc_name, cc_date = $cc_date, cc_area = $cc_area  WHERE cc_id = $this->id";
        $this->db->queryTx($q);
        //print_r($query);
    }

    public function updateDetail() {
        //print_r($this->data);
        $ccdetail_areadetail = $this->db->filter($this->data['ccdetail_areadetail'], '');
        $ccdetail_outlay = $this->db->filter($this->data['ccdetail_outlay'], '');
        $ccdetail_status = $this->db->filter($this->data['ccdetail_status'], '');

        $q = "UPDATE campus_company SET ccdetail_areadetail = $ccdetail_areadetail, ccdetail_outlay = $ccdetail_outlay, ccdetail_status = $ccdetail_status  WHERE cc_id = $this->id";
        $this->db->queryTx($q);
        //print_r($query);
    }
    
    public function updateHealthStatus() {
        //print_r($this->data);
        $cchealth_remarks = $this->db->filter($this->data['cchealth_remarks'], '');
        $cchealth_closure = $this->db->filter($this->data['cchealth_closure'], 'date');
        $cchealth_revenue = $this->db->filter($this->data['cchealth_revenue'], '');
        $cchealth_review = $this->db->filter($this->data['cchealth_review'], 'date');

        $q = "UPDATE campus_company SET cchealth_remarks = $cchealth_remarks, cchealth_closure = $cchealth_closure, cchealth_revenue = $cchealth_revenue,  cchealth_review = $cchealth_review  WHERE cc_id = $this->id";
        $this->db->queryTx($q);
        //print_r($query);
    }

    public function updateTeam() {
        $ccteam_fac = $this->db->filter($this->data['ccteam_fac']['value'], '');
        $q = "UPDATE campus_company SET ccteam_fac = $ccteam_fac  WHERE cc_id = $this->id";
        $qs = array($q);
        $qMembers = $this->updateMembers();
        $qAdvisors = $this->updateAdvisors();
        $qAdditionalFaculties = $this->updateAdditionalFaculties();
        $query = array_merge($qs, $qMembers, $qAdvisors, $qAdditionalFaculties);
        //print_r($query) ;
        $this->db->mqueryTx($query);
    }
    
    public function updateAdditionalFaculties() {
        $additionalFaculties = $this->data['ccteam_additional_fac'];
        if (is_array($additionalFaculties)) {
            $dq = "DELETE FROM campus_additional_faculty WHERE cc_id = $this->id";
            $q = array($dq);
            foreach ($additionalFaculties as $amem) {
                $facid = $amem['value'];
                $q[] = "INSERT INTO campus_additional_faculty (cc_id, human_id) values ($this->id, $facid)";
            }
            return $q;
        }
        return array();
    }

    public function updateMembers() {
        $teamMembers = $this->data['student_members']['data'];
      //  print_R($teamMembers);
        if (is_array($teamMembers)) {
            $dq = "DELETE FROM student_members WHERE cc_id = $this->id";
            $q = array($dq);
            foreach ($teamMembers as $amem) {
                $mem = $amem['values'];
                //print_r($mem['values']);
                $sno = $this->db->filter($mem['sno'], '');
                $designation = $this->db->filter($mem['designation'], '');
                $first_name = $this->db->filter($mem['first_name'], '');
                $last_name = $this->db->filter($mem['last_name'], '');
                $dob = $this->db->filter($mem['dob'], 'date');
                $sex = $this->db->filter($mem['sex'], '');
                $email = $this->db->filter($mem['email'], '');
                $contact_no = $this->db->filter($mem['contact_no'], '');
                $start_date = $this->db->filter($mem['start_date'], 'date');
                $remarks = $this->db->filter($mem['remarks'], '');

                $q[] = "INSERT INTO student_members (cc_id, edc_id, sno, designation, first_name, last_name, dob, sex, email, contact_no, start_date, remarks) VALUES($this->id, NULL, $sno, $designation, $first_name, $last_name, $dob, $sex, $email, $contact_no, $start_date, $remarks)";
            }
            //print_r($q);
            return $q;
            //$db->queryTx($q);
        }
        return array();
    }

    public function updateAdvisors() {
        $teamAdvisors = $this->data['advisors']['data'];
        //print_r($teamAdvisors);
        if (is_array($teamAdvisors)) {
            $dq = "DELETE FROM advisors WHERE cc_id = $this->id";
            $q = array($dq);
            foreach ($teamAdvisors as $amem) {
                $mem = $amem['values'];
                $sno = $this->db->filter($mem['sno'], '');
                $designation = $this->db->filter($mem['designation'], '');
                $institute = $this->db->filter($mem['institute'], '');
                $first_name = $this->db->filter($mem['first_name'], '');
                $last_name = $this->db->filter($mem['last_name'], '');
                $apellation = $this->db->filter($mem['apellation'], '');
                $date_of_joining = $this->db->filter($mem['date_of_joining'], 'date');
                $date_of_leaving = $this->db->filter($mem['date_of_leaving'], 'date');
                $remarks = $this->db->filter($mem['remarks'], '');

                $q[] = "INSERT INTO advisors (cc_id, edc_id, sno, designation, institute, first_name, last_name, apellation, date_of_joining, date_of_leaving, remarks) VALUES($this->id, NULL, $sno, $designation, $institute, $first_name, $last_name, $apellation, $date_of_joining, $date_of_leaving, $remarks)";
            }
            return $q;
            //$db->queryTx($q);
        }
        return array();
    }

    public function updateHiperf() {
        $hiperf_news = $this->db->filter($this->data['hiperf_news'], '');
        $hiperf_latestnews = $this->db->filter($this->data['hiperf_latestnews'], '');
        $hiperf_web = $this->db->filter($this->data['hiperf_web'], '');
        $hiperf_link = $this->db->filter($this->data['hiperf_link'], '');
        $hiperf_support = $this->db->filter($this->data['hiperf_support'], '');
        $hiperf_supportevidence = $this->db->filter($this->data['hiperf_supportevidence'], '');
        $hiperf_sign = $this->db->filter($this->data['hiperf_sign'], '');
        $hiperf_signtitle = $this->db->filter($this->data['hiperf_signtitle'], '');
        $hiperf_signdate = $this->db->filter($this->data['hiperf_signdate'], 'date');

        $q = "UPDATE ecell SET hiperf_news = '$hiperf_news', hiperf_latestnews = '$hiperf_latestnews', hiperf_web = '$hiperf_web', hiperf_link = '$hiperf_link', hiperf_support = '$hiperf_support', hiperf_supportevidence = '$hiperf_supportevidence', hiperf_sign = '$hiperf_sign', hiperf_signtitle = '$hiperf_signtitle', hiperf_signdate = '$hiperf_signdate'  WHERE id = $this->ecellId";

        $this->db->queryTx($q);
    }

}

class Application_Model_Campus {
    public function __construct($campusId = null) {
        $this->campusId = $campusId;
        $this->data = new Campus($this->campusId, true);
    }
    
    public function update() {
        if (isset($_POST['data'])) {

            $campus = new Campus();
            
            if (isset($_POST['campusid'])) {
                $campus->setId((int) $_POST['campusid']);
            }
            $campus->setCampusData($_POST['data']);
            if ($block = $_POST['block']) {
                switch ($block) {
                    case 'overview':
                        $campus->updateOverview();
                        break;
                    case 'team':
                        $campus->updateTeam();
                        break;
                    case 'detail':
                        $campus->updateDetail();
                        break;
                    case 'healthstatus':
                        $campus->updateHealthStatus();
                        break;
                            
                    default:
                        # code...
                        break;
                }
            }
            return true;
        }
    }

    public function get() {
        if ($_POST['campusid']) {
            $campus = new Campus($_POST['campusid'], true);
            header('Content-Type: text/json');
            return $campus->getData();
        }
        
    }

    public function uploadstudentscsv() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $campusCsv = new Application_Model_Csv($name);

            $result = $campusCsv->processCsv();
        }
        //print_r($result);
        return $result;
    }
    
    public function uploadadvisorscsv() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $campusCsv = new Application_Model_Csv($name);

            $result = $campusCsv->processCsv();
        }
        //print_r($result);
        return $result;
    }
    
    

    public function uploadfiles() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $campusCsv = new Application_Model_Csv($name);
            $result = $campusCsv->processUpload();
        }
        return $result;
    }

}

