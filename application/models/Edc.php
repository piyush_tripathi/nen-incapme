<?php

class Edc {

    private $id = 0;
    private $db = null;
    public $data = array();
    public $edcId = '1';
    public $commonModel;

    public function __construct($id = null, $fetch = false) {
        //error_reporting(0);
        $this->db = new Application_Model_Edb();
        $this->commonModel = new Application_Model_Common();

        if ($id) {
            $this->id = $id;
        }
        if ($fetch) {
            $q = "Select *, 
                date_format(edcadvboard_date1, '%d-%m-%Y') as edcadvboard_date1, 
                date_format(edcadvboard_date2,  '%d-%m-%Y') as edcadvboard_date2,
                date_format(edcfund_date,  '%d-%m-%Y') as edcfund_date from edc where edc_id = " . $this->id;
            $this->data = $this->db->fetchOne($q);
            
            $this->fetchStudentMembers();
            $this->fetchAdvisors();
            
        }
    }

    public function setEdcData($data) {
        $this->data = $data;
        
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getFacultyNameAndId($id) {
        $q = "select first_name, last_name from human_resource where id = $id";
        if($res = $this->db->fetchOne($q)) {
            return array('value' => $id, 'caption' => $res['first_name'].' '.$res['last_name']);
        }
        return false;
    }

   

    public function fetchStudentMembers() {
        $q = "SELECT designation, first_name, last_name, date_format(dob, '%d-%m-%Y') as dob, sex, email, contact_no, date_format(start_date, '%d-%m-%Y') as start_date, remarks FROM student_members	WHERE edc_id=" . $this->id; //AND member_type = 'CC' implicit 
        $this->data['edc_members']['data'] = array();
        $this->data['edc_members']['metadata'] = $this->commonModel->getGridMetaData('campus_edc_students');
        $i = 0;
        if ($edc_members = $this->db->fetchAll($q)) {
            foreach ($edc_members as $mem) {
                //print_r($edc_members);
                $this->data['edc_members']['data'][$i]['id'] = $i;
                $this->data['edc_members']['data'][$i]['values'] = $mem;
                $i++;
            }
        }
        
    }

    public function fetchAdvisors() {
        $q = "SELECT *, date_format(date_of_joining, '%d-%m-%Y') as date_of_joining, date_format(date_of_leaving, '%d-%m-%Y') as date_of_leaving FROM advisors WHERE edc_id=" . $this->id; //AND member_type = 'CC' implicit 
        $this->data['edc_advisors']['data'] = array();
        $this->data['edc_advisors']['metadata'] = $this->commonModel->getGridMetaData('campus_edc_advisors');
        $i = 0;
        if ($advisors = $this->db->fetchAll($q)) {
            foreach ($advisors as $mem) {
                //print_r($advisors);
                $this->data['edc_advisors']['data'][$i]['id'] = $i;
                $this->data['edc_advisors']['data'][$i]['values'] = $mem;
                $i++;
            }
        }
        
    }

    public function getJsonData() {
        return json_encode($this->data);
    }

    public function getData() {
        $this->data['edcteam_lf'] = $this->getFacultyNameAndId($this->data['edcteam_lf']);
        $this->data['edcteam_colead'] = $this->getFacultyNameAndId($this->data['edcteam_colead']);
        return $this->data;
    }

    

    public function getAdvisors() {
        $advisorsGrid = array(
            'metadata' => $this->commonModel->getGridMetaData('campus_edc_advisors'),
            'data' => array(),
        );
        $this->data['advisors'] = $advisorsGrid;
        //return $advisorsGrid;
    }

    public function updateTeam() {
        $edcinfo_year = $this->db->filter($this->data['edcinfo_year']['value'], '');
        $edcteam_lf = $this->db->filter($this->data['edcteam_lf']['value'], '');
        $edcteam_colead = $this->db->filter($this->data['edcteam_colead']['value'], '');
        $edcteam_stu = $this->db->filter($this->data['edcteam_stu'], '');
        $edcadvboard_adv = $this->db->filter($this->data['edcadvboard_adv'], '');
        $edcadvboard_date1 = $this->db->filter($this->data['edcadvboard_date1'], 'date');
        $edcadvboard_date2 = $this->db->filter($this->data['edcadvboard_date2'], 'date');
        
        $q = "UPDATE edc SET edcinfo_year = $edcinfo_year, edcteam_lf = $edcteam_lf, edcteam_colead = $edcteam_colead, edcteam_stu = $edcteam_stu, edcadvboard_adv = $edcadvboard_adv, edcadvboard_date1 = $edcadvboard_date1, edcadvboard_date2 = $edcadvboard_date2  WHERE edc_id = $this->id";
        $qs = array($q);
        $qMembers = $this->updateMembers();
        $qAdvisors = $this->updateAdvisors();
        $query = array_merge($qs, $qMembers, $qAdvisors);
        $this->db->mqueryTx($query);
    }

    public function updateMembers() {
        //print_r($this->data);
        $teamMembers = $this->data['edc_members']['data'];
        if (is_array($teamMembers)) {
            $dq = "DELETE FROM student_members WHERE edc_id = $this->id";
            $q = array($dq);
            foreach ($teamMembers as $amem) {
                $mem = $amem['values'];
                //print_r($mem['values']);
                $sno = $this->db->filter($mem['sno'], '');
                $designation = $this->db->filter($mem['designation'], '');
                $first_name = $this->db->filter($mem['first_name'], '');
                $last_name = $this->db->filter($mem['last_name'], '');
                $dob = $this->db->filter($mem['dob'], 'date');
                $sex = $this->db->filter($mem['sex'], '');
                $email = $this->db->filter($mem['email'], '');
                $contact_no = $this->db->filter($mem['contact_no'], '');
                $start_date = $this->db->filter($mem['start_date'], 'date');
                $remarks = $this->db->filter($mem['remarks'], '');

                $q[] = "INSERT INTO student_members (cc_id, edc_id, sno, designation, first_name, last_name, dob, sex, email, contact_no, start_date, remarks) VALUES(NULL, $this->id, $sno, $designation, $first_name, $last_name, $dob, $sex, $email, $contact_no, $start_date, $remarks)";
            }
            return $q;
            //$db->queryTx($q);
        }
        return array("Select 1");
    }

    public function updateAdvisors() {
        $teamAdvisors = $this->data['edc_advisors']['data'];
        ;
        //print_r($teamAdvisors);
        if (is_array($teamAdvisors)) {
            $dq = "DELETE FROM advisors WHERE edc_id = $this->id";
            $q = array($dq);
            foreach ($teamAdvisors as $amem) {
                $mem = $amem['values'];
                $sno = $this->db->filter($mem['sno'], '');
                $designation = $this->db->filter($mem['designation'], '');
                $institute = $this->db->filter($mem['institute'], '');
                $first_name = $this->db->filter($mem['first_name'], '');
                $last_name = $this->db->filter($mem['last_name'], '');
                $apellation = $this->db->filter($mem['apellation'], '');
                $date_of_joining = $this->db->filter($mem['date_of_joining'], 'date');
                $date_of_leaving = $this->db->filter($mem['date_of_leaving'], 'date');
                $remarks = $this->db->filter($mem['remarks'], '');

                $q[] = "INSERT INTO advisors (cc_id, edc_id, sno, designation, institute, first_name, last_name, apellation, date_of_joining, date_of_leaving, remarks) VALUES(NULL, $this->id, $sno, $designation, $institute, $first_name, $last_name, $apellation, $date_of_joining, $date_of_leaving, $remarks)";
            }
            return $q;
            //$db->queryTx($q);
        }
        return array("Select 1");
    }

    public function updateProgram() {
        $edcprog_disc = $this->db->filter($this->data['edcprog_disc'], '');
        $edcprog_adopt = $this->db->filter($this->data['edcprog_adopt'], '');
        $edcprog_upload = $this->db->filter($this->data['edcprog_upload'], '');
         if($this->data['edcprog_upload']!=''){
                    $sub_sql = ", edcprog_upload = $edcprog_upload ";
        }
         if($this->data['edcprog_adopt']=='No'){
                    $sub_sql = ", edcprog_upload = '' ";
        }
        $q ="UPDATE edc SET edcprog_disc = $edcprog_disc, edcprog_adopt = $edcprog_adopt  $sub_sql WHERE edc_id = $this->id";
        //echo $q;
        $this->db->queryTx($q);
    }
    
    public function updateFunding() {
        $edcfund_sent = $this->db->filter($this->data['edcfund_sent'], '');
        $edcfund_sanction = $this->db->filter($this->data['edcfund_sanction'], '');
        $edcfund_other = $this->db->filter($this->data['edcfund_other'], '');
        $edcfund_date = $this->db->filter($this->data['edcfund_date'], 'date');
        
        $q = "UPDATE edc SET edcfund_sent = $edcfund_sent, edcfund_sanction = $edcfund_sanction, edcfund_other = $edcfund_other, edcfund_date = $edcfund_date  WHERE edc_id = $this->id";
        $this->db->queryTx($q);
    }
    
    public function updateInfrastructure() {
        $infra_space = $this->db->filter($this->data['infra_space'], '');
        $infra_equipment = $this->db->filter($this->data['infra_equipment'], '');
        $infra_library = $this->db->filter($this->data['infra_library'], '');
        $infra_staff = $this->db->filter($this->data['infra_staff'], '');
        $infra_budget = $this->db->filter($this->data['infra_budget'], '');
        $infra_other = $this->db->filter($this->data['infra_other'], '');
        $infra_other_text = $this->db->filter($this->data['infra_other_text'], '');
        
        $q = "UPDATE edc SET infra_space = $infra_space, infra_equipment = $infra_equipment, infra_library = $infra_library, infra_staff = $infra_staff , infra_budget = $infra_budget, infra_other = $infra_other ,  infra_other_text = $infra_other_text WHERE edc_id = $this->id";
        //echo $q;
        $this->db->queryTx($q);
    }

}

class Application_Model_Edc {

    public $edcId;
    public $data;
    
    public function __construct($edcId = null) {
        $this->edcId = $edcId;
        $this->data = new Edc($this->edcId, true);
    }
    public function getInstituteId() {
        
    }
    
    
    public function update() {
        if (isset($_POST['data'])) {

            $edc = new Edc();
            $edc->setEdcData($_POST['data']);
            if (isset($_POST['edcid'])) {
                $edc->setId((int) $_POST['edcid']);
            }
            if ($block = $_POST['block']) {
                switch ($block) {
                    case 'team':
                        $edc->updateTeam();
                        break;
                    case 'program':
                        $edc->updateProgram();
                        break;
                    case 'funding':
                        $edc->updateFunding();
                        break;
                    case 'infrastructure':
                        $edc->updateInfrastructure();
                        break;

                    default:
                        # code...
                        break;
                }
            }
            return true;
        }
    }

    public function get() {
        if ($_POST['edcid']) {
            $edc = new Edc($_POST['edcid'], true);
            header('Content-Type: text/json');
            return $edc->getData();
        }
    }

    public function uploadmemberscsv() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $edcCsv = new Application_Model_Csv($name);

            $result = $edcCsv->processCsv();
        }
        //print_r($result);
        return $result;
    }
    
    public function uploadadvisorscsv() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $edcCsv = new Application_Model_Csv($name);

            $result = $edcCsv->processCsv();
        }
        //print_r($result);
        return $result;
    }
    
    public function uploadfiles() {
        $name = $_POST['type_of_file'];
        $result = false;
        if ($name) {
            $edcCsv = new Application_Model_Csv($name);
            $result = $edcCsv->processUpload();
        }
        return $result;
    }

}

