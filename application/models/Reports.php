<?php
class Application_Model_Reports extends Application_Model_Db
{
    public function __construct( )
    {
	$dbconfig = Zend_Registry::get("dbconfig");
        $this->mysqli = new mysqli($dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname);
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' ); 

    }
    
    function fetchRecords($query){
        $record = array();
        
        $qR = $this->mysqli->query( $query ); 

            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                $record[] = $qRR; 

            }
        
        return $record;
        
    }
    function fetchInstituteReport($institute_id, $instituteList = NULL){
        if(count($instituteList)) {
            $instituteIds=array();
            foreach($instituteList as $institute) {
				  $instituteIds[] = $institute['id']  ;
            }
            $instituteCond = ' in ('.implode(',', $instituteIds).')';
        } else {
            $instituteCond = '='.$institute_id;
            
        }
        //die($instituteCond);
        $output = array();
        $ent=$eleaders=$eclub=0;
        $sel_entrepreneur_faculty=" SELECT COUNT(*) total FROM human_resource h JOIN human_institute hi ON (h.id=hi.human_id) WHERE hi.institute_id  ".$instituteCond ." AND h.Entrepreneur=1";
        
        $records=$this->fetchRecords($sel_entrepreneur_faculty);
         
        $ent=$records[0]['total'];
        
        $sel_eleaders="SELECT COUNT(*) total FROM `ecell_over_mem` m join ecell e on (m.ecell_id = e.id) WHERE e.institute_id  ".$instituteCond ." AND m.is_e_leader=1";
        $records=$this->fetchRecords($sel_eleaders);
        $eleaders=$records[0]['total'];
        
        //for participants 
        $num1=$num2=$num3=0;
        $sel_activity_participants="SELECT a.activityinfo_type Activity,a.activitybeneficiaries_number Participants,a.activity_id Id,i.name FROM activity a JOIN institute i ON a.institute_id=i.id WHERE institute_id  ".$instituteCond ."";
        $records=$this->fetchRecords($sel_activity_participants);
         
        foreach($records as $record){
        if($record['Activity']=="Lecture/ Talk" || $record['Activity']=="Panel Discussion"){
        
         
        $participant_number = $this->getParticipantCount($record['Id']); 
       
        $num1+=$participant_number;
        //$id1.=$record['Id'].",";
        }
        if($record['Activity']=="Course" || $record['Activity']=="Workshop"){
        //$activity_name2.=$record['Activity'].",";
        $participant_number = $this->getParticipantCount($record['Id']);    
        $num2+=$participant_number;
        //$id2.=$record['Id'].",";
        }
        if($record['Activity']=="Conference" || $record['Activity']=="Competition" || $record['Activity']=="Outreach"){
        //$activity_name3.=$record['Activity'].",";
        $participant_number = $this->getParticipantCount($record['Id']);
        $num3+=$participant_number;
       
        //print_r($participant_number[0]);
        //$id3.=$record['Id'].",";
        }
        }
        
        
        //for campus company 
        
        $CC=$students=$revenue=0;
        $sel_number_of_CC="SELECT COUNT(*) total FROM campus_company WHERE institute_id  ".$instituteCond ."";
        $records=$this->fetchRecords($sel_number_of_CC);
        $CC=$records[0]['total'];
        $sel_students_involved="SELECT COUNT(*) total FROM student_members sm JOIN campus_company cc ON sm.cc_id=cc.cc_id WHERE cc.institute_id  ".$instituteCond ."";
       
        $records=$this->fetchRecords($sel_students_involved);
        $students=$records[0]['total'];
        
        $sel_total_revenue="SELECT SUM(cchealth_revenue) total FROM campus_company WHERE institute_id  ".$instituteCond ."";
        $records=$this->fetchRecords($sel_total_revenue);
        $revenue=$records[0]['total'];
        
        
        
        
        //for outcomes
        $start_ups=$join_start_ups=$graduate_start_ups=$alumni_start_ups=$ent_supported=$jobs_created=0;
//        $sel_ent_supported="SELECT SUM(activitybeneficiaries_type) total FROM activity WHERE institute_id  ".$instituteCond ."";
//        $records=$this->fetchRecords($sel_ent_supported);
//        $ent_supported=$records[0]['total'];
        $ent_supported = $this->getEntSupportedCount($instituteCond);
        
        //for revenues 
        $fees=$pri_sponsors=$inst_fund=0;
        $sel_fees="SELECT SUM(activitybeneficiaries_fees) total FROM activity WHERE institute_id  ".$instituteCond ."";
        $records=$this->fetchRecords($sel_fees);
        $fees=$records[0]['total'];
        $sel_pri_sponsors="SELECT activitycontributor_sponsors sponsors FROM activity WHERE institute_id  ".$instituteCond ."";

        $records=$this->fetchRecords($sel_pri_sponsors);
        if($records)
            foreach($records as $record){
            $sponsor_record=$record['sponsors'];

            $each_record=explode(',',$sponsor_record);

            $count_sponsors=count($each_record);
                for($pqr=0;$pqr<$count_sponsors;$pqr++){
                $each_record[$pqr]=str_replace("(","",$each_record[$pqr]);
                $pri_sponsors += intval($each_record[$pqr]);

                }
            }
        $sel_inst_fund="SELECT SUM(edcfund_sanction) total FROM edc WHERE institute_id  ".$instituteCond ."";
        $records=$this->fetchRecords($sel_inst_fund);
        $inst_fund=$records[0]['total'];
        
        
        
        //for branding 
        $sel_prizes_won="SELECT count(*) total FROM activity WHERE activityoutcome_institution  ".$instituteCond ."";
        $records=$this->fetchRecords($sel_prizes_won);
        $prizes_won=$records[0]['total'];
        
        //for institute records 
        $sel = "SELECT id,name from institute";
        $records = $this->fetchRecords($sel);
        
        
        $output = array(
                $ent,$eleaders,$num1,$num2,$num3,$CC,$students,$revenue,$ent_supported,$fees,$pri_sponsors,$inst_fund,$prizes_won               
             );
        
        return $output;
      
    }
    function getInstitutes(){
        //for institute records 
        $sel = "SELECT id,name from institute";
        $records = $this->fetchRecords($sel);       
        return $records;
    }
    function getParticipantCount($activity_id){
       $q = 'SELECT COUNT(*) total from activity_participants where activity_id ='. $activity_id.' ';
//       $participant_number  = $this->fetchRecords($q); 
        $qR = $this->mysqli->query( $q );
       $qRR = $qR->fetch_array( MYSQLI_ASSOC);
       
       return $qRR['total'];
    }
    function getEntSupportedCount($instituteCond){
        $q = 'select count(*) total from activity_participants where activity_id in(SELECT activity_id FROM `activity` WHERE institute_id  '.$instituteCond .') and category!="student"';
       // print($q);
//       $participant_number  = $this->fetchRecords($q); 
        $qR = $this->mysqli->query( $q );
       $qRR = $qR->fetch_array( MYSQLI_ASSOC);
       
       return $qRR['total'];
    }
    function instituteDetailedReport($institute_id){
        $sql = 'SELECT DISTINCT * FROM activity where institute_id=' . (int)$institute_id;
        
        $qR  = $this->mysqli->query( $sql );
        while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
            $main[ ] = $qRR;
        } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
        $data = $this->instituteDetailedReportData($main,$institute_id);
        return $data;
    }
    function instituteDetailedReportData($main,$institute_id){
        
//        if(empty($main)){
//            return NULL;
//        }
        $data  = array();
        $parti = array();
        $type    = array(
             "1" => "Lecture/ Talk",
            "2" => "Panel Discussion",
            "3" => "Exercise/ Games",
            "4" => "Conference",
            "5" => "Competition",
            "6" => "Workshop",
            "7" => "Course",
            "8" => "Mentoring",
            "9" => "Start-up Support",
            "10" => "Webinar",
            "11" => "National Platform",            
             "14" => "E-Club" 
        );
        foreach ($main as $key => $value) {            
           
            
if($value['activityinfo_type'] == $type['1'] ){         $data['lecture_talk'] = $this->getActivityData($value,$data['lecture_talk'],$data['parti']['lecture_talk']); $data['lecture_talk']['total']++;  }
            else if($value['activityinfo_type'] == $type['2']){    $data['panel_discussion'] = $this->getActivityData($value,$data['panel_discussion'],$data['parti']['panel_discussion'] ); $data['panel_discussion']['total']++;   }
            else if($value['activityinfo_type'] == $type['3']){    $data['exercise_games'] = $this->getActivityData($value,$data['exercise_games'],$data['parti']['exercise_games'] );$data['exercise_games']['total']++;     }
            else if($value['activityinfo_type'] == $type['4']){    $data['conference'] = $this->getActivityData($value,$data['conference'],$data['parti']['conference'] );  $data['conference']['total']++;  }
            else if($value['activityinfo_type'] == $type['5']){    $data['competition'] = $this->getActivityData($value,$data['competition'],$data['parti']['competition'] ); $data['competition']['total']++;  }
            else if($value['activityinfo_type'] == $type['6']){    $data['workshop'] = $this->getActivityData($value,$data['workshop'],$data['parti']['workshop'] ); $data['workshop']['total']++;  }
            else if($value['activityinfo_type'] == $type['7']){    $data['course'] = $this->getActivityData($value,$data['course'],$data['parti']['course'] ); $data['course']['total']++;  }
            else if($value['activityinfo_type'] == $type['8']){    $data['mentoring'] = $this->getActivityData($value,$data['mentoring'] ,$data['parti']['mentoring']); $data['mentoring']['total']++;  }
            else if($value['activityinfo_type'] == $type['9']){    $data['start_up'] = $this->getActivityData($value,$data['start_up'] ,$data['parti']['start_up']); $data['start_up']['total']++;  }
            else if($value['activityinfo_type'] == $type['10']){    $data['webinar'] = $this->getActivityData($value,$data['webinar'] ,$data['parti']['webinar']); $data['webinar']['total']++;  }
            else if($value['activityinfo_type'] == $type['11']){    $data['national_platform'] = $this->getActivityData($value,$data['national_platform'],$data['parti']['national_platform'] ); $data['national_platform']['total']++;  }           
            else if($value['activityinfo_type'] == $type['14']){    $data['E-Club'] = $this->getActivityData($value,$data['E-Club'],$data['parti']['E-Club'] ); $data['E-Club']['total']++;  }
            else{}
        }
          // now getting data for activity participants 
       foreach($data as $type=>$value){
           $data[$type]['parti'] = $this->get_participant_count($data[$type]['parti']);
       }
        //die(print_r($data).'main');
        //now getting the rest of the data for report 
       $output['actvity_data'] = $data;
       $output['participant_data'] = $this->get_participant_data($institute_id);
       $output['enterp_data'] = $this->get_enterp_data($institute_id);
       $output['report_other'] = $this->get_report_other_data($institute_id);
      
        return $output;
        
        
    }
    
    function getActivityData($row,$container,$parti){
        if(empty($row)){
            return null;
        }
        $keys  = array('activity_id' , 'activityinfo_type' , 'activityinfo_scale' , 'activityinfo_level' , 'activityinfo_title' , 'activityinfo_organisingentity' , 'activityoutcome_competition' , 'activityinfo_course_credits' , 'activity_city_old' , 'activityplacetime_venue' , 'activityplacetime_date_start' , 'activityplacetime_duration' , 'activitycontributor_sponsors' , 'activitycontributor_partner' , 'activitybeneficiaries_number' , 'activitybeneficiaries_institute' , 'activitybeneficiaries_type' , 'activitybeneficiaries_fees' , 'activityoutcome_instructor' , 'activityoutcome_course' , 'activityoutcome_org' , 'activityoutcome_plan' , 'activityoutcome_detail' , 'activityoutcome_idea' , 'activityoutcome_institution' , 'activityoutcome_mentees' , 'activityoutcome_menteeslist' , 'activityoutcome_mentors' , 'activityoutcome_mentorslist' , 'activityunivcourse_title' , 'activityselfcourse_title' , 'institute_id' , 'activity_city');
        
   
 


 foreach($keys as $key){
                if('Local' == $row[$key]){   
                $container['Local'] = $container['Local']+1; 
                 $container['parti']['Local'][] = $row['activity_id'] ;} else {  $container['Local'] = $container['Local'] ; 
                 }
                           if('National' == $row[$key]){   
                 $container['National'] = $container['National']+1; 
                 $container['parti']['National'][] = $row['activity_id'];} else {   $container['National'] = $container['National'] ; 
                 }
                            if('International' == $row[$key]){   
                $container['International'] = $container['International']+1; 
                 $container['parti']['International'][] = $row['activity_id'];} else {  $container['International'] = $container['International'] ; 
                 }
                           if('E-cell' == $row[$key]){   
                 $container['E-cell'] = $container['E-cell']+1; 
                 $container['parti']['E-cell'][] = $row['activity_id'];} else {   $container['E-cell'] = $container['E-cell'] ; 
                 }
                           if('ESC' == $row[$key]){   
                 $container['ESC'] = $container['ESC']+1; 
                 $container['parti']['ESC'][] = $row['activity_id'];} else {   $container['ESC'] = $container['ESC'] ; 
                 }
                  if('EDC' == $row[$key]){ 
                     
                 $container['EDC'] = $container['EDC']+1; 
                 $container['parti']['EDC'][] = $row['activity_id'];
                 } else {   $container['EDC'] = $container['EDC'] ; 
                 }
                            if('Campus Company' == $row[$key]){   
                $container['Campus Company'] = $container['Campus Company']+1; 
                 $container['parti']['Campus Company'][] = $row['activity_id'];} else {  $container['Campus Company'] = $container['Campus Company'] ; 
                 }
                            if('IEDC' == $row[$key]){   
                $container['IEDC'] = $container['IEDC']+1; 
                 $container['parti']['IEDC'][] = $row['activity_id'];} else {  $container['IEDC'] = $container['IEDC'] ; 
                 }
                           if('Mentoring Unit (SVMU)' == $row[$key]){   
                 $container['Mentoring Unit (SVMU)'] = $container['Mentoring Unit (SVMU)']+1; 
                 $container['parti']['Mentoring Unit (SVMU)'][] = $row['activity_id'];} else {   $container['Mentoring Unit (SVMU)'] = $container['Mentoring Unit (SVMU)'] ; 
                 }
                            if('Incubator (TBI)' == $row[$key]){   
                $container['Incubator (TBI)'] = $container['Incubator (TBI)']+1; 
                 $container['parti']['Incubator (TBI)'][] = $row['activity_id'];} else {  $container['Incubator (TBI)'] = $container['Incubator (TBI)'] ; 
                 }
                            if('Faculty' == $row[$key]){   
                $container['Faculty'] = $container['Faculty']+1; 
                 $container['parti']['Faculty'][] = $row['activity_id'];} else {  $container['Faculty'] = $container['Faculty'] ; 
                 }
                           if('Alumni Association' == $row[$key]){   
                 $container['Alumni Association'] = $container['Alumni Association']+1; 
                 $container['parti']['Alumni Association'][] = $row['activity_id'];} else {   $container['Alumni Association'] = $container['Alumni Association'] ; 
                 }
                            if('E-Club' == $row[$key]){   
                $container['E-Club'] = $container['E-Club']+1; 
                 $container['parti']['E-Club'][] = $row['activity_id'];} else {  $container['E-Club'] = $container['E-Club'] ; 
                 }
                           if('NEN' == $row[$key]){   
                 $container['NEN'] = $container['NEN']+1; 
                 $container['parti']['NEN'][] = $row['activity_id'];} else {   $container['NEN'] = $container['NEN'] ; 
                 }
                           if('Other' == $row[$key]){   
                 $container['Other'] = $container['Other']+1; 
                 $container['parti']['Other'][] = $row['activity_id'];} else {   $container['Other'] = $container['Other'] ; 
                 }
                 //for local and edc,esc data
                  $container['ESC/EDC'] = $container['ESC'] + $container['EDC'];
                  $container['National/International'] = $container['National'] + $container['International'];
                   $container['parti']['ESC/EDC']['student'] = count($container['parti']['ESC']) + count($container['parti']['EDC']);
                  $container['parti']['National/International']['student'] = count($container['parti']['National']) + count($container['parti']['International']);
                  
           
        }
        //die(print_r($container));
        //die(print_r($container));
         return $container ;
    }
    function get_participant_data($institute_id){
        $sql = array();
        $keys = array(
                 "1" => "E-cell",
                "2" => "ESC/EDC",
                "4" => "Campus Company",
                "5" => "IEDC",
                "6" => "Mentoring Unit (SVMU)",
                "7" => "Incubator (TBI)",
                "8" => "Faculty",
                "9" => "Alumni Association",
                "10" => "E-Club",
                "11" => "NEN",
                "12" => "Other" ,
				"13" => "Local",
                "14" => "National/International"	
				
            );
        
        foreach($keys as $k=>$v){
            
            if($v=="ESC/EDC" ){
                $sql[$v] = 'SELECT COUNT( * ) as count 
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category =  "Student" AND  ( a.activityinfo_organisingentity =  "ESC" OR  a.activityinfo_organisingentity =  "EDC" ) 
           and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review" ' ;
            }
            else if($v=="National/International" ){
                $sql[$v] = 'SELECT COUNT( * ) as count
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category =  "Student" AND ( a.activityinfo_scale =  "National" OR a.activityinfo_scale =  "International" ) 
           and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review" ';
              
            }
             else if($v=="Local" ){
                $sql[$v] = 'SELECT COUNT( * ) as count
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category =  "Student" AND ( a.activityinfo_scale =  "Local"  ) 
           and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review" ';
              
            }
            else{
            $sql[$v] = 'SELECT COUNT( * ) as count 
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category =  "Student" AND a.activityinfo_organisingentity = "'.$v.'" and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review" ';        
                }
             }
             $sql['total'] = 'SELECT COUNT( * ) as count
                                FROM activity a
                                JOIN activity_participants ap ON a.activity_id = ap.activity_id
                                WHERE a.institute_id ='.$institute_id.'
                                AND ap.category =  "Student" and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review"';
            //die(print_r($sql));
            foreach ( $sql as $key => $query ) {
                
            $qR = $this->mysqli->query( $query );
            
             
            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                
                $main[ $key ] = $qRR;
               
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
            }
          
            return $main;
         }
    function get_enterp_data($institute_id){
        $sql = array();
        $keys = array(
                 "1" => "E-cell",
                "2" => "ESC/EDC",
                "4" => "Campus Company",
                "5" => "IEDC",
                "6" => "Mentoring Unit (SVMU)",
                "7" => "Incubator (TBI)",
                "8" => "Faculty",
                "9" => "Alumni Association",
                "10" => "E-Club",
                "11" => "NEN",
                "12" => "Other" ,
				"13" => "Local",
                "14" => "National/International"	
				
            );
        
        foreach($keys as $k=>$v){
            
            if($v=="ESC/EDC" ){
                $sql[$v] = 'SELECT COUNT( * ) as count 
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category !=  "Student" AND ( a.activityinfo_organisingentity =  "ESC" OR  a.activityinfo_organisingentity =  "EDC" )   and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review" ' ;
            }
            else if($v=="National/International" ){
                $sql[$v] = 'SELECT COUNT( * ) as count
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category !=  "Student" AND  ( a.activityinfo_scale =  "National" OR a.activityinfo_scale =  "International" )   and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review"  ';
            }
             else if($v=="Local" ){
                $sql[$v] = 'SELECT COUNT( * ) as count
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category !=  "Student" AND   a.activityinfo_scale =  "Local"     and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review"  ';
            }
            else{
            $sql[$v] = 'SELECT COUNT( * ) as count 
                        FROM activity a
                        JOIN activity_participants ap ON a.activity_id = ap.activity_id
                        WHERE a.institute_id ='.$institute_id.'
                        AND ap.category !=  "Student" AND a.activityinfo_organisingentity = "'.$v.'"   and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review" ';        
                }
             }
              $sql['total'] = 'SELECT COUNT( * ) as count
                                FROM activity a
                                JOIN activity_participants ap ON a.activity_id = ap.activity_id
                                WHERE a.institute_id ='.$institute_id.'
                                AND ap.category !=  "Student"   and a.activityinfo_type!="Outreach" and a.activityinfo_type!="Review" '; 
            foreach ( $sql as $key => $query ) {
                
            $qR = $this->mysqli->query( $query );
            
             
            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                
                $main[ $key ] = $qRR;
               
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
            }
          
            return $main;
    } 
    function get_report_other_data($institute_id){
         $sql = array();
         //for REPORT I2.5: PLATFORMS
         $sql['platforms']['over_num'] = 'SELECT SUM( over_num ) as count 
                            FROM ecell where institute_id='.$institute_id;
         $sql['platforms']['over_eleadnum'] = 'SELECT SUM( over_eleadnum ) as count 
                            FROM ecell where institute_id='.$institute_id;
         //$sql['platforms']['field2'] = 'SELECT count(*) FROM  `advisors`  where ecell id in (select ecell.id from ecell where insti = '.$institute_id.')';        
         $sql['platforms']['ecell_adv'] = 'select COUNT(*) as count from ecell as x join ecell_estab_professionals as y on x.id = y.ecell_id where x.institute_id = '.$institute_id;
         $sql['platforms']['hiperf_news'] = 'SELECT SUM(hiperf_news) as count FROM  `ecell`  where institute_id='.$institute_id;
         $sql['platforms']['cc'] = 'SELECT COUNT(*) as count FROM  `campus_company`  where institute_id='.$institute_id;
         $sql['platforms']['cc_revenue'] = 'SELECT SUM(cchealth_revenue) as count FROM  `campus_company`  where institute_id='.$institute_id;
         $sql['platforms']['cc_area'] = 'SELECT GROUP_CONCAT(DISTINCT cc_area)  as count FROM  `campus_company`  where institute_id='.$institute_id;
         $sql['platforms']['edcteam_stu'] = 'SELECT SUM(edcteam_stu) as count FROM  `edc`  where institute_id='.$institute_id;
         $sql['platforms']['edcadvboard_adv'] = 'SELECT SUM(edcadvboard_adv) as count FROM  `edc`  where institute_id='.$institute_id;
         $sql['platforms']['gov_funds'] = 'SELECT SUM(edcfund_sanction) as count FROM  `edc`  where edcfund_sent="Yes" and institute_id='.$institute_id;
         $sql['platforms']['other_funds'] = 'SELECT SUM(edcfund_sanction) as count FROM  `edc`  where edcfund_sent="No" and institute_id='.$institute_id;
         //for report REPORT I2.3: IDEATION
          $sql['ideation']['idea'] = 'SELECT  SUM(activityoutcome_competition) count
                                        FROM  `activity` 
                                        WHERE institute_id ='.$institute_id.'
                                        AND activityinfo_type =  "Competition"';
          $sql['ideation']['won'] = 'SELECT COUNT( * ) as count
                                    FROM  `activity` 
                                    WHERE activityoutcome_institution ='.$institute_id.'
                                    AND activityinfo_type =  "ompetition"';
         //for REPORT I2.2: LEVERAGE
          $sql['leverage']['no_act'] = "SELECT  COUNT(*) as count 
                                   FROM activity
                                   WHERE activityplacetime_venue in (select id from venue where institute_id = ".$institute_id.") and institute_id =".$institute_id ;
                                        
            $sql['leverage']['no_act_staff'] = "SELECT  COUNT(*) as count 
                                   FROM activity
                                   WHERE activityplacetime_venue not in (select id from venue where institute_id = ".$institute_id.")and institute_id =".$institute_id ;
                $sql['leverage']['tot_revenue'] = "SELECT GROUP_CONCAT(DISTINCT activitycontributor_sponsors) as count
                                                FROM activity
                                                WHERE institute_id =".$institute_id ;
            $sql['leverage']['tot_fees'] = "SELECT SUM( activitybeneficiaries_fees ) as count 
                                            FROM activity
                                            WHERE institute_id =".$institute_id ;                            
          
            $sql['leverage']['ent_supported'] ='select count(*) count from activity_participants where activity_id in(SELECT activity_id FROM `activity` WHERE institute_id = '.$institute_id .') and category!="student"';
            $sql['leverage']['stu_supported'] ='select count(*) count from activity_participants where activity_id in(SELECT activity_id FROM `activity` WHERE institute_id = '.$institute_id .') and category ="student" ';
         foreach($sql as $k=>$v){
            foreach ( $v as $key => $query ) {
                
            $qR = $this->mysqli->query( $query );
            
             
            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                
                
                $main[$k][ $key ] = $qRR;
               
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
            } 
         }
         //calculation for activity leverage revenue 
         $lev_sposor = explode(';',$main['leverage']['tot_revenue']['count']);         
         $sum = 0;   
         foreach($lev_sposor as $key=>$value){
             $sum+= intval($value);
             
         }
         unset($main['leverage']['tot_revenue']['count']);
         $main['leverage']['tot_revenue']['count'] = $sum;
         // die(print_r($main));
            return $main;
    }
   
    function get_participant_count($parti_array){
        $data = array();
        foreach ($parti_array as $key => $value) {
            //for student participants 
            $query  = 'SELECT COUNT(*) count from activity_participants where activity_id in ('.implode(',',$value).') and category="student" ';
             $qR = $this->mysqli->query( $query );
              $qRR = $qR->fetch_array( MYSQLI_ASSOC);
              $data[$key]['student'] = $qRR['count'];
              //for non student participants 
              $query  = 'SELECT COUNT(*) count from activity_participants where activity_id in ('.implode(',',$value).') and category!="student" ';
             $qR = $this->mysqli->query( $query );
              $qRR = $qR->fetch_array( MYSQLI_ASSOC);
              $data[$key]['non-student'] = $qRR['count'];
              
        }
        
        
         //die(print_r($query));
        return $data;
    }
    
    
     //retrieve records from database based on the select query
function fetchRecords_old($query){
    $result=mysql_query($query);
    $records=array();
    if(mysql_num_rows($result)!=0){
        while($record=mysql_fetch_assoc($result)){
            $records[]=$record;
            }
        }
    return $records;
}

function getflrdata( $institute_id ){
    $output =  array();
            $query = "Select f.id id,f.first_name, f.last_name from human_resource f join human_institute hi ON f.id=hi.human_id JOIN institute i on (hi.institute_id=i.id) where i.id=$institute_id AND f.category='FAC'"; 
            
             $qR = $this->mysqli->query( $query );
            
             if($qR->num_rows){
                 while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                             $output[] = $qRR; 
                }
              
              }
              else{
                  $output=array(0=>array("id"=>'none',"first_name"=>"No","last_name"=>"Values"));
              }
            
            return $output;
}
    
}

