<?php
class Application_Model_Activity extends Application_Model_Db
{ /**
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */
    public function __construct( )
    {
        $dbconfig     = Zend_Registry::get( "dbconfig" );
        $this->mysqli = new mysqli( $dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname );
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' ); 
    }
    public function getMain( $activity_id = NULL, $institute_id = NULL )
    {
        $activityId = mysqli_real_escape_string( $this->mysqli, $activity_id );
        $auth       = Zend_Auth::getInstance();
        $user       = $auth->getIdentity();
        if ( $user->role == 'FAC' ) {
            $main[ 'orgarray' ] = array(
                 "1" => "E-cell",
                "2" => "ESC",
                "3" => "EDC",
                "4" => "Campus Company",
                "5" => "IEDC",
                "6" => "Mentoring Unit (SVMU)",
                "7" => "Incubator (TBI)",
                "8" => "Faculty",
                "9" => "Alumni Association",
                "11" => "NEN",
                "12" => "Other" 
            );
        } //$user->role == 'FAC'
        else {
            $main[ 'orgarray' ] = array(
                 "1" => "E-cell",
                "2" => "ESC",
                "3" => "EDC",
                "4" => "Campus Company",
                "5" => "IEDC",
                "6" => "Mentoring Unit (SVMU)",
                "7" => "Incubator (TBI)",
                "8" => "Faculty",
                "9" => "Alumni Association",
                "10" => "E-Club",
                "11" => "NEN",
                "12" => "Other" 
            );
        }
        $main[ 'activity_id' ]  = $activityId;
        $main[ 'institute_id' ] = $institute_id;
        $main[ 'typearray' ]    = array(
             "1" => "Lecture/ Talk",
            "2" => "Panel Discussion",
            "3" => "Exercise/ Games",
            "4" => "Conference",
            "5" => "Competition",
            "6" => "Workshop",
            "7" => "Course",
            "8" => "Mentoring",
            "9" => "Start-up Support",
            "10" => "Webinar",
            "11" => "National Platform",
            "12" => "Outreach",
            "13" => "Review" 
        );
        
        $main[ 'scalearray' ]   = array(
             "1" => "Local",
            "2" => "National",
            "3" => "International" 
        );
        $main[ 'levelarray' ]   = array(
             "1" => "Basic",
            "2" => "Intermediate",
            "3" => "Advanced" 
        );
        $main[ 'creditarray' ]  = array(
             "1" => "1",
            "2" => "2",
            "3" => "3",
            "4" => "NA" 
        );
        if ( $activityId ) {
            
            $sql[ 'activity' ]                            = 'SELECT DISTINCT activity.*,city.name as city_name,venue.name as venue_name FROM activity join venue on activity.activityplacetime_venue=venue.id join city on activity.activity_city=city.city_id   WHERE activity_id=' . $activityId;
            $sql[ 'activity_contributor_corodinator' ]    = 'SELECT DISTINCT * FROM activity_contributor_corodinator as x join human_resource as y on x.human_id=y.id WHERE activity_id=' . $activityId;
            $sql[ 'activity_contributor_lead_presenter' ] = 'SELECT DISTINCT * FROM activity_contributor_lead_presenter as x join human_resource as y on x.human_id=y.id WHERE activity_id=' . $activityId;
            $sql[ 'activity_outcome_mentees_list' ]       = 'SELECT * FROM activity_outcome_mentees_list WHERE activity_id=' . $activityId;
            $sql[ 'activity_outcome_mentors_list' ]       = 'SELECT * FROM activity_outcome_mentors_list WHERE activity_id=' . $activityId;
            $sql[ 'activity_participants' ]               = 'SELECT * FROM activity_participants WHERE activity_id=' . $activityId;
        } //$activityId
        $sql[ 'faculty' ] = 'SELECT * FROM human_resource where category="FAC"';
        if ( $institute_id ) {
            $sql[ 'institute' ]         = 'SELECT * FROM institute WHERE id=' . $institute_id;
            $sql[ 'institute_faculty' ] = 'select DISTINCT * from human_resource as x join human_institute as y on x.id=y.human_id where y.institute_id=' . $institute_id;
        } //$institute_id
        $sql[ 'city' ]           = 'SELECT * FROM city where city_id!=9999';
        $sql[ 'institute_list' ] = 'SELECT * FROM institute';
        
        foreach ( $sql as $key => $query ) {
            $qR = $this->mysqli->query( $query );
            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                $main[ $key ][ ] = $qRR;
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
        } //$sql as $key => $query
        $metaData = '{"metadata": [{"name": "serialno","label": "SNO.","datatype": "integer","editable": true},{"name": "sore","label": "STUDENT / ENTREPRENEUR","datatype": "string","editable": true},{"name": "fname","label": "FIRST NAME","datatype": "string","editable": true},{"name": "lname","label": "LAST NAME","datatype": "string","editable": true},{"name": "dob","label": "D.O.B","datatype": "string","editable": true},{"name": "gender","label": "M / F","datatype": "string","editable": true},{"name": "email","label": "EMAIL ID","datatype": "string","editable": true},{"name": "mobileno","label": "MOBILE NO","datatype": "string","editable": true},{"name": "performance","label": "PERFORMANCE","datatype": "string","editable": true},{"name": "feespaid","label": "FEES PAID","datatype": "string","editable": true}]';
        if ( isset( $main[ 'activity_participants' ] ) ) {
            $count = 1;
            foreach ( $main[ 'activity_participants' ] as $key => $val ) {
                $feePaid = isset( $val[ 'Fee' ] ) || is_numeric( $val[ 'Fee' ] ) ? $val[ 'Fee' ] : 0;
                $mobile  = isset( $val[ 'phone' ] ) || is_numeric( $val[ 'phone' ] ) ? $val[ 'phone' ] : 0;
                $vals[ ] = '
               {
                   "id": ' . $count . ',"values": {
                       "serialno": "' . $count . '","sore": "' . $val[ 'category' ] . '","fname": "' . $val[ 'first_name' ] . '","lname": "' . $val[ 'last_name' ] . '","dob": "' . $val[ 'dob' ] . '","gender": "' . $val[ 'gender' ] . '","email": "' . $val[ 'email' ] . '","mobileno": ' . $mobile . ',"feespaid": ' . $feePaid . ',"performance": "' . $val[ 'performance_remarks' ] . '"
                   }
               }';
                $count++;
            } //$main[ 'activity_participants' ] as $key => $val
            $body                                     = implode( ',', $vals );
            $output                                   = $metaData . ',"data": [' . $body . ']}';
            $main[ 'selected_activity_participants' ] = $output;
        } //isset( $main[ 'activity_participants' ] )
        else {
            $main[ 'selected_activity_participants' ] = $metaData . '}';
        }
        
        return $main;
    }
    public function createActivityInfo( $data )
    {
        $output = array( );
        $aR     = array( );
        foreach ( $data as $col => $val ) {
            if ( $col == 'activityinfo_title' ) {
                // make activity name compulsory
                if ( empty( $val ) || trim( $val ) == '' ) {
                    $output[ 'error' ] = 'Please provide activity name.';
                    $output[ 'data' ]  = $data;
                    return $output;
                } //empty( $val ) || trim( $val ) == ''
            } //$col == 'activityinfo_title'
            if ( $col != 'activity_id' ) {
                $col = mysqli_real_escape_string( $this->mysqli, $col );
                //for date format 
                if ( $col == 'activityplacetime_date_start' ) {
                    $temp = explode( '-', $val );
                    $val  = '"' . $temp[ 2 ] . '-' . $temp[ 1 ] . '-' . $temp[ 0 ] . '" ';
                } //$col == 'activityplacetime_date_start'
                else {
                    $val = '"' . mysqli_real_escape_string( $this->mysqli, $val ) . '"';
                }
                $aR[ $col ] = $val;
            } //$col != 'activity_id'
        } //$data as $col => $val
        $cols = implode( ',', array_keys( $aR ) );
        $vals = implode( ',', array_values( $aR ) );
        $sql  = 'INSERT INTO activity(' . $cols . ') VALUES(' . $vals . ')';
        
        //die(var_dump($aR));
//        die(print_r($sql));
        $qR   = $this->mysqli->query( $sql );
        if ( $qR ) {
            $output[ 'success' ]     = 'Created';
            $output[ 'activity_id' ] = $this->mysqli->insert_id;
        } //$qR
        else {
            $output[ 'error' ] = 'Could not Create the activity:' . $this->mysqli->error;
            $output[ 'data' ]  = $data;
        }
        return $output;
    }
    function insertContributors( $data )
    {
        $output                        = array( );
        $aR                            = array( );
        $vals                          = array( );
        $sql                           = array( );
        $contributors                  = isset( $data[ 'activity_contributor_corodinator' ] ) ? $data[ 'activity_contributor_corodinator' ] : NULL;
        $leadPresenters                = isset( $data[ 'activity_contributor_lead_presenter' ] ) ? $data[ 'activity_contributor_lead_presenter' ] : NULL;
        $activityId                    = mysqli_real_escape_string( $this->mysqli, $data[ 'activity_id' ] );
        $sponsorsMoney                 = array_filter( $data[ 'activitycontributor_sponsors_money' ] );
        $sponsorsInstitute             = array_filter( $data[ 'activitycontributor_sponsors' ] );
        $partner                       = array_filter( $data[ 'activitycontributor_partner' ] );
        $activity_contributor_sponsors = array( );
        $activitycontributor_partner   = array( );
        for ( $count = 0; $count < count( $sponsorsMoney ); $count++ ) {
            $activity_contributor_sponsors[ ] = '(' . mysqli_real_escape_string( $this->mysqli, $sponsorsInstitute[ $count ] ) . '; 
' . mysqli_real_escape_string( $this->mysqli, $sponsorsMoney[ $count ] ) . ')';
        } //$count = 0; $count < count( $sponsorsMoney ); $count++
        for ( $count = 0; $count < count( $partner ); $count++ ) {
            $activitycontributor_partner[ ] = '(' . mysqli_real_escape_string( $this->mysqli, $partner[ $count ] ) . ')';
        } //$count = 0; $count < count( $partner ); $count++
        //contributors details 
        $sql[ ] = 'DELETE FROM activity_contributor_corodinator WHERE activity_id=' . $activityId;
        if ( isset( $contributors ) ) {
            foreach ( $contributors as $key => $val ) {
                $vals[ ] = '(' . $activityId . ',null,"' . mysqli_real_escape_string( $this->mysqli, $val ) . '")';
            } //$contributors as $key => $val
            $sql[ ] = 'INSERT INTO activity_contributor_corodinator(activity_id,unmapped_coordinator,human_id) VALUES' . implode( ',', $vals );
            $vals   = array( );
        } //isset( $contributors )
        //lead speaker details 
        $sql[ ] = 'DELETE FROM activity_contributor_lead_presenter WHERE activity_id=' . $activityId;
        if ( isset( $leadPresenters ) ) {
            foreach ( $leadPresenters as $key => $val ) {
                $vals[ ] = '(' . $activityId . ',null,"' . mysqli_real_escape_string( $this->mysqli, $val ) . '")';
            } //$leadPresenters as $key => $val
            $sql[ ] = 'INSERT INTO activity_contributor_lead_presenter(activity_id,unmapped_presenter,human_id) VALUES' . implode( ',', $vals );
            $vals   = array( );
        } //isset( $leadPresenters )
        $stmt   = 'activitycontributor_sponsors="' . implode( '|', $activity_contributor_sponsors ) . '",activitycontributor_partner="' . implode( '|', $activitycontributor_partner ) . '"';
        $cols   = implode( ',', array_keys( $aR ) );
        $vals   = implode( ',', array_values( $aR ) );
        $sql[ ] = 'UPDATE activity SET ' . $stmt . ' WHERE activity_id=' . $activityId;
        $query  = implode( '; 
', $sql );
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            $qR = $qR & $this->mysqli->query( $value );
        } //$sql as $key => $value
        if ( $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = 'updated ';
        } //$qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Insert the activity:' . $this->mysqli->error;
        }
        $output[ 'activity_id' ] = $activityId;
        return $output;
    }
    public function insertParticipants( $data )
    {
        $output                     = array( );
        $aR                         = array( );
        $sql                        = array( );
        $activityId                 = mysqli_real_escape_string( $this->mysqli, $data[ 'activity_id' ] );
        $output[ 'activity_id' ]    = $activityId;
        $options[ 'cols' ]          = "(activity_id,category,first_name,last_name,dob,gender,email,phone,performance_remarks,Fee)";
        $options[ 'table' ]         = 'activity_participants';
        $activity_participants_data = $data[ 'activity_participants' ];
        $org                        = array(
             '[[],',
            ']]',
            ']',
            ',["","","","","","","","",""]',
            '[' 
        );
        $replace                    = array(
             '',
            ')',
            ')',
            '',
            '(' . $activityId . ',' 
        );
        //        if($activity_participants_data){ 
        //            
        //               $activity_participants           =  str_replace( $org, $replace, $activity_participants_data );
        //                       
        //               }
        //               else{
        //                  $activity_participants = false;
        //               }; 
        if ( empty( $data[ 'activitybeneficiaries_number' ] ) ) {
            $activitybeneficiaries_number = 0;
        } //empty( $data[ 'activitybeneficiaries_number' ] )
        else {
            $activitybeneficiaries_number = mysqli_real_escape_string( $this->mysqli, $data[ 'activitybeneficiaries_number' ] );
            $valid                        = $this->validate( $activitybeneficiaries_number, 'numeric' );
            if ( isset( $valid[ 'error' ] ) ) {
                $output = $valid;
                return $output;
            } //isset( $valid[ 'error' ] )
            $valid = array( );
        }
        if ( empty( $data[ 'activitybeneficiaries_institute' ] ) ) {
            $activitybeneficiaries_institute = 0;
        } //empty( $data[ 'activitybeneficiaries_institute' ] )
        else {
            $activitybeneficiaries_institute = mysqli_real_escape_string( $this->mysqli, $data[ 'activitybeneficiaries_institute' ] );
            $valid                           = $this->validate( $activitybeneficiaries_institute, 'numeric' );
            if ( isset( $valid[ 'error' ] ) ) {
                $output = $valid;
                return $output;
            } //isset( $valid[ 'error' ] )
            $valid = array( );
        }
        if ( empty( $data[ 'activitybeneficiaries_fees' ] ) ) {
            $activitybeneficiaries_fees = 0;
        } //empty( $data[ 'activitybeneficiaries_fees' ] )
        else {
            $activitybeneficiaries_fees = mysqli_real_escape_string( $this->mysqli, $data[ 'activitybeneficiaries_fees' ] );
            $valid                      = $this->validate( $activitybeneficiaries_fees, 'numeric' );
            if ( isset( $valid[ 'error' ] ) ) {
                $output = $valid;
                return $output;
            } //isset( $valid[ 'error' ] )
            $valid = array( );
        }
        //check if participant data grid has values in it
        $status = json_decode( $activity_participants_data );
        if ( empty( $status[ 1 ] ) ) {
            $activity_participants = false;
        } //empty( $status[ 1 ] )
        else {
            $activity_participants        = str_replace( $org, $replace, $activity_participants_data );
            $activitybeneficiaries_number = count( $status ) - 1;
        }
        // if list is uploaded then take the data from the list else take the posted data for : activitybeneficiaries_number,activitybeneficiaries_fees
        if ( $activity_participants ) {
            $this->mysqli->autocommit( FALSE );
            $sql   = 'DELETE FROM ' . $options[ 'table' ] . ' WHERE activity_id=' . $activityId;
            $qR    = $this->mysqli->query( $sql );
            $query = 'INSERT INTO ' . $options[ 'table' ] . $options[ 'cols' ] . ' VALUES' . $activity_participants;
            //die(var_dump($query));
            $qR    = $this->mysqli->query( $query );
            if ( !$qR ) {
                $this->mysqli->rollback();
                $output[ 'error' ] = 'Could not Insert the data:' . $this->mysqli->error;
                return $output;
            } //!$qR
            $out = $this->mysqli->commit();
        } //$activity_participants
        $vals = array( );
        $stmt = 'activitybeneficiaries_fees="' . $activitybeneficiaries_fees . '",activitybeneficiaries_number="' . $activitybeneficiaries_number . '",activitybeneficiaries_institute="' . $activitybeneficiaries_institute . '"';
        $sql  = 'UPDATE activity SET ' . $stmt . ' WHERE activity_id=' . $activityId;
        $qR   = $this->mysqli->query( $sql );
        if ( !$qR ) {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Insert the activity:' . $this->mysqli->error;
            return $output;
        } //!$qR
        $this->mysqli->commit();
        $output[ 'success' ] = 'updated';
        return $output;
    }
    function insertOutcomes( $data )
    {
        $output                  = array( );
        $aR                      = array( );
        $vals                    = array( );
        $sql                     = array( );
        $valsA                   = array( );
        $activityId              = mysqli_real_escape_string( $this->mysqli, $data[ 'activity_id' ] );
        $output[ 'activity_id' ] = $activityId;
        $menteeslistFile         = $_FILES[ 'activityoutcome_menteeslist' ];
        $mentorslistFile         = $_FILES[ 'activityoutcome_mentorslist' ];
        if ( $menteeslistFile[ 'size' ] != 0 ) {
            $options                 = array(
                 'mime' => 'text/csv',
                'size' => 50000 
            );
            $options[ 'fileFormat' ] = array(
                 "S. No.",
                "Category",
                "First name",
                "Last name",
                "Date of birth",
                "M/F",
                "email",
                "phone",
                "Mentored by whom (first name, last name)" 
            );
            $options[ 'cols' ]       = array( );
            $options[ 'cols' ]       = "(category,first_name,last_name,dob,gender,email,phone,mentored_by,activity_id,human_id)";
            $options[ 'table' ]      = 'activity_outcome_mentees_list';
            $options[ 'file_name' ]  = 'ActivityMentees-' . $activityId;
            $resultOne               = $this->_uploadFile( $menteeslistFile, $options );
            if ( $resultOne[ 'error' ] ) {
                $output[ 'error' ] = $resultOne[ 'error' ];
                return $output;
            } //$resultOne[ 'error' ]
            $resultTwo = $this->processFile( $resultOne[ 'data' ], $options[ 'fileFormat' ] );
            if ( isset( $resultTwo[ 'error' ] ) ) {
                return $resultTwo;
            } //isset( $resultTwo[ 'error' ] )
            $vals = $resultTwo[ 'data' ];
            foreach ( $vals as $key => $val ) {
                $valsA[ ] = '("' . implode( '","', array_values( $val ) ) . '",' . $activityId . ',NULL)';
            } //$vals as $key => $val
            $sql[ ] = 'DELETE FROM activity_outcome_mentees_list WHERE activity_id=' . $activityId;
            $sql[ ] = 'INSERT INTO activity_outcome_mentees_list' . $options[ 'cols' ] . ' VALUES' . implode( ',', $valsA );
            $vals   = array( );
            $valsA  = array( );
        } //$menteeslistFile[ 'size' ] != 0
        else {
            if ( isset( $data[ 'activityoutcome_menteeslist' ] ) ) {
                $menteeslist = $data[ 'activityoutcome_menteeslist' ];
                foreach ( $menteeslist as $key => $val ) {
                    $val     = explode( '|', mysqli_real_escape_string( $this->mysqli, $val ) );
                    $vals[ ] = '(' . $activityId . ',"' . implode( '","', $val ) . '")';
                } //$menteeslist as $key => $val
            } //isset( $data[ 'activityoutcome_menteeslist' ] )
        }
        $options                 = array(
             'mime' => 'text/csv',
            'size' => 50000 
        );
        $options[ 'fileFormat' ] = array(
             'S. No.',
            'Apellation',
            'First name',
            'Last name',
            'Designation',
            'Organisation',
            'email',
            'phone',
            'Mentoring whom (first name, last name)' 
        );
        $options[ 'cols' ]       = array( );
        $options[ 'cols' ]       = "(category,first_name,last_name,dob,gender,email,phone,mentoring_whom,activity_id,human_id)";
        $options[ 'table' ]      = 'activity_outcome_mentors_list';
        $options[ 'file_name' ]  = 'ActivityMentor-' . $activityId;
        if ( $mentorslistFile[ 'size' ] != 0 ) {
            $resultThree = $this->_uploadFile( $mentorslistFile, $options );
            if ( $resultThree[ 'error' ] ) {
                $output[ 'error' ] = $resultThree[ 'error' ];
                return $output;
            } //$resultThree[ 'error' ]
            $resultFour = $this->processFile( $resultThree[ 'data' ], $options[ 'fileFormat' ] );
            if ( isset( $resultFour[ 'error' ] ) ) {
                return $resultFour;
            } //isset( $resultFour[ 'error' ] )
            $vals = $resultFour[ 'data' ];
            foreach ( $vals as $key => $val ) {
                $valsA[ ] = '("' . implode( '","', array_values( $val ) ) . '",' . $activityId . ',NULL)';
            } //$vals as $key => $val
            $sql[ ] = 'DELETE FROM activity_outcome_mentors_list WHERE activity_id=' . $activityId;
            $sql[ ] = 'INSERT INTO activity_outcome_mentors_list' . $options[ 'cols' ] . ' VALUES' . implode( ',', $valsA );
            $vals   = array( );
            $valsA  = array( );
        } //$mentorslistFile[ 'size' ] != 0
        else {
            if ( isset( $data[ 'activityoutcome_mentorslist' ] ) ) {
                $mentorslist = $data[ 'activityoutcome_mentorslist' ];
                foreach ( $mentorslist as $key => $val ) {
                    $val     = explode( '|', mysqli_real_escape_string( $this->mysqli, $val ) );
                    $vals[ ] = '(' . $activityId . ',"' . implode( '","', $val ) . ',null")';
                } //$mentorslist as $key => $val
            } //isset( $data[ 'activityoutcome_mentorslist' ] )
        }
        $stmt   = 'activityoutcome_instructor="' . $data[ 'activityoutcome_instructor' ] . '",activityoutcome_course="' . $data[ 'activityoutcome_course' ] . '",activityoutcome_org="' . $data[ 'activityoutcome_org' ] . '",activityoutcome_plan="' . $data[ 'activityoutcome_plan' ] . '",activityoutcome_detail="' . $data[ 'activityoutcome_detail' ] . '",activityoutcome_idea="' . $data[ 'activityoutcome_idea' ] . '",activityoutcome_competition="' . $data[ 'activityoutcome_competition' ] . '",activityoutcome_institution="' . $data[ 'activityoutcome_institution' ] . '"';
        $sql[ ] = 'UPDATE activity SET ' . $stmt . ' WHERE activity_id=' . $activityId;
        $query  = implode( '; ', $sql );
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) {
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = 'updated ';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Insert the activity:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( '; 
', $sql );
        }
        return $output;
    }
    public function insertActivityInfo( $data )
    {
        $output      = array( );
        $aR          = array( );
        $stmt        = array( );
        $activity_id = $data[ 'activity_id' ];
        foreach ( $data as $col => $val ) {
            if ( $col == 'activityinfo_title' ) {
                // make activity name compulsory
                if ( empty( $val ) || trim( $val ) == '' ) {
                    $output[ 'error' ] = 'Please provide activity name.';
                    $output[ 'data' ]  = $data;
                    return $output;
                } //empty( $val ) || trim( $val ) == ''
            } //$col == 'activityinfo_title'
            if ( $col != 'activity_id' ) {
                $col = mysqli_real_escape_string( $this->mysqli, $col );
                //for date format 
                if ( $col == 'activityplacetime_date_start' ) {
                    $temp = explode( '-', $val );
                    $val  = '"' . $temp[ 2 ] . '-' . $temp[ 1 ] . '-' . $temp[ 0 ] . '" ';
                } //$col == 'activityplacetime_date_start'
                else {
                    $val = '"' . mysqli_real_escape_string( $this->mysqli, $val ) . '"';
                }
                $stmt[ ] .= $col . '=' . $val;
            } //$col != 'activity_id'
        } //$data as $col => $val
        $sql = 'UPDATE activity SET ' . implode( ',', $stmt ) . ' WHERE activity_id=' . $activity_id;
        //die($sql);
        $qR  = $this->mysqli->query( $sql );
        if ( $qR ) {
            $output[ 'success' ] = 'updated ';
        } //$qR
        else {
            $output[ 'error' ] = 'Could not update the activity:' . $this->mysqli->error;
        }
        return $output;
    }
    function deleteActivity( $activityId )
    {
        $output     = array( );
        $activityId = mysqli_real_escape_string( $this->mysqli, $activityId );
        $sql[ ]     = 'DELETE FROM activity_contributor_corodinator WHERE activity_id=' . $activityId;
        $sql[ ]     = 'DELETE FROM activity_contributor_lead_presenter WHERE activity_id=' . $activityId;
        $sql[ ]     = 'DELETE FROM activity_outcome_mentees_list WHERE activity_id=' . $activityId;
        $sql[ ]     = 'DELETE FROM activity_outcome_mentors_list WHERE activity_id=' . $activityId;
        $sql[ ]     = 'DELETE FROM activity_participants WHERE activity_id=' . $activityId;
        $sql[ ]     = 'DELETE FROM activity WHERE activity_id=' . $activityId;
        $this->mysqli->autocommit( FALSE );
        $qR = TRUE;
        foreach ( $sql as $key => $value ) {
            try {
                $qR = $qR && $this->mysqli->query( $value );
            }
            catch ( Exception $e ) {
                $output[ 'error' ] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ( (bool) $qR ) {
            $out                 = $this->mysqli->commit();
            $output[ 'success' ] = ' Deleted the activity';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output[ 'error' ] = 'Could not Delete The activity:' . $this->mysqli->error;
            $output[ 'data' ]  = implode( '; 
', $sql );
        }
    }
    public function validate( $data, $type )
    {
        $status = array( );
        switch ( $type ) {
            case 'numeric':
                if ( is_numeric( $data ) && (int) $data >= 0 ) {
                    return true;
                } //is_numeric( $data ) && (int) $data >= 0
                else {
                    $status[ 'error' ] = 'Please provide valid numbers';
                    return $status;
                }
                break;
            default:
                break;
        } //$type
    }
    function getList( $institute_id )
    {
        $output = array( );
        $result = array( );
        if ( !$institute_id ) {
            $output[ 'error' ] = 'No institute selected';
            return $output;
        } //!$institute_id
        $sql = 'SELECT * FROM activity where institute_id=' . $institute_id;
        $qR  = $this->mysqli->query( $sql );
        if ( $qR->num_rows > 0 ) {
            while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
                $result[ ] = $qRR;
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
            $header = '<thead><tr><th>SNo.</th><th>Activity Name</th>' . '<th>Type</th>' . '<th>Scale</th>' . '<th>Level</th>' . '<th>Organised By</th>' . '<th>Date</th>';
            $header .= '<th>Actions</th></tr></thead><tbody>';
            $html = '<div class="tpd-list-view">
                    <h5 class="h5-heading">Activity List</h5>
                    <div class="tpd-list-view-inner"><table class="table tpd-list tpd-list2">' . $header;
            $i    = 1;
            foreach ( $result as $row ) {
                $html .= '<tr>';
                
                $html .= '<td>' . $i . '</td>' . '<td><a href="/activity/edit?id=' . $row[ 'activity_id' ] . '&instituteid=' . $institute_id . '" >' . $row[ 'activityinfo_title' ] . '</a></td>' . '<td>' . $row[ 'activityinfo_type' ] . '</td>' . '<td>' . $row[ 'activityinfo_scale' ] . '</td>' . '<td>' . $row[ 'activityinfo_level' ] . '</td>' . '<td>' . $row[ 'activityinfo_organisingentity' ] . '</td>' . '<td>' . ( $this->fix_date($row[ 'activityplacetime_date_start' ]) ) . '</td>';
                $html .= '<td><a id="activityList" href="/activity/edit?id=' . $row[ 'activity_id' ] . '&instituteid=' . $institute_id . '" ><i class="icon-pencil"></i>  </a>
                                                
                     <a href="#delActivity" data-toggle="modal" delActivity" class="delActivity" value="' . $row[ 'activity_id' ] . '"  ><i class="icon-trash"> </i></a></td>';
                $html .= '</tr>';
                $i++;
            } //$result as $row
            $html .= '</tbody></table></div></div>';
            $output[ 'data' ] = $html;
        } //$qR->num_rows > 0
        else {
            $output[ 'error' ] = 'Could not retrieve the activity list';
        }
        return $output;
    }
    public function saveActivity( $data )
    {
        $output = array( );
        $aR     = array( );
        foreach ( $data as $col => $val ) {
            $col = mysqli_real_escape_string( $this->mysqli, $col );
            if ( $col == 'activity_id' ) {
                $activity_id = mysqli_real_escape_string( $this->mysqli, $val );
            } //$col == 'activity_id'
            else {
                $val   = '"' . mysqli_real_escape_string( $this->mysqli, $val ) . '"';
                $aR[ ] = $col . '=' . $val;
            }
        } //$data as $col => $val
        $stmt = implode( ',', $aR );
        $sql  = 'UPDATE activity SET ' . $stmt . ' WHERE activity_id=' . $activity_id;
        $qR   = $this->_db->query( $sql );
        $qR   = $mysqli->query( $sql );
        if ( $qR ) {
            $output[ 'success' ]     = 'updated';
            $output[ 'activity_id' ] = $activity_id;
        } //$qR
        else {
            $output[ 'error' ] = 'Could not Insert the activity:' . $mysqli->error;
        }
        return $output;
    }
    function processCsv( $data )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;
        $allowedmimes = array(
             "" 
        );
        try {
            if ( ( $_FILES[ $data[ 'name' ] ][ 'size' ] == 0 ) || ( $_FILES[ $data[ 'name' ] ][ 'size' ] > $data[ 'size' ] ) ) {
                $goodtogo = false;
                throw new exception( "Could not upload the file: Invalid File Format" );
            } //( $_FILES[ $data[ 'name' ] ][ 'size' ] == 0 ) || ( $_FILES[ $data[ 'name' ] ][ 'size' ] > $data[ 'size' ] )
        }
        catch ( exception $e ) {
            $output[ 'error' ][ ] = $e->getmessage();
        }
        try {
            $target = "uploads/" . $_FILES[ $data[ 'name' ] ][ 'name' ] . ".csv";
            $source = $_FILES[ $data[ 'name' ] ][ 'tmp_name' ];
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = false;
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $output[ 'error' ][ ] = $e->getmessage();
        }
        if ( $goodtogo ) {
            $row = 1;
            if ( ( $handle = fopen( $target, "r" ) ) !== FALSE ) {
                while ( ( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE ) {
                    if ( $row == 1 ) {
                        $cols = '("' . implode( '","', $line ) . '")';
                    } //$row == 1
                    else {
                        $vals[ ] = '("' . implode( '","', $line ) . '")';
                    }
                    $row++;
                } //( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE
                fclose( $handle );
            } //( $handle = fopen( $target, "r" ) ) !== FALSE
        } //$goodtogo
        $query = 'INSERT INTO ' . $data[ 'table' ] . $cols . ' VALUES' . implode( ',', $vals );
        return $query;
    }
    protected function _uploadFile( $data, $options )
    {
        $query        = array( );
        $vals         = array( );
        $goodtogo     = true;
        $allowedMimes = $options[ 'mime' ];
        $fileFormat   = $options[ 'fileFormat' ];
        $size         = $options[ 'size' ];
        if ( ( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size ) ) {
            $goodtogo          = false;
            $result[ 'error' ] = "Could not upload the file: Invalid File Format";
            return $result;
        } //( $data[ 'size' ] == 0 ) || ( $data[ 'size' ] > $size )
        try {
            $target = APPLICATION_PATH . "/../public/uploads/activity/" . $options[ 'file_name' ] . ".csv";
            //die($target);
            $source = $data[ 'tmp_name' ];
            // die($target.'<br/>'.$source);
            if ( !move_uploaded_file( $source, $target ) ) {
                $goodtogo = false;
                throw new exception( "There was an error moving the file." );
            } //!move_uploaded_file( $source, $target )
        }
        catch ( exception $e ) {
            $result[ 'error' ] = $e->getmessage();
            return $result;
        }
        if ( !$goodtogo ) {
            $result[ 'error' ] = 'File Not Uploaded,Please Try again';
            return $result;
        } //!$goodtogo
        $result[ 'data' ]  = $target;
        $result[ 'error' ] = "";
        return $result;
    }
    function uploadParticipants( $data )
    {
        $output                  = array( );
        $aR                      = array( );
        $vals                    = array( );
        $sql                     = array( );
        $activityId              = mysqli_real_escape_string( $this->mysqli, $data[ 'activity_id' ] );
        $output[ 'activity_id' ] = $activityId;
        $participantFile         = $_FILES[ 'fileParticipant' ];
        if ( $participantFile ) {
            $options                 = array(
                 'mime' => 'text/csv',
                'size' => 50000 
            );
            $options[ 'fileFormat' ] = array(
                 "S. No.",
                "Category",
                "First name",
                "Last name",
                "Date of birth",
                "M/F",
                "email",
                "phone",
                "Performance remarks",
                "Fee"
            );
            $options[ 'cols' ]       = "(category,first_name,last_name,dob,gender,email,phone,performance_remarks,Fee,activity_id,human_id)";
            $options[ 'table' ]      = 'activity_participants';
            $options[ 'file_name' ]  = 'ActivityPaticipant-' . $activityId;
            $result                  = $this->_uploadFile( $participantFile, $options );
            if ( $result[ 'error' ] ) {
                $output[ 'error' ] = $result[ 'error' ];
                return $output;
            } //$result[ 'error' ]
            if ( ( $handle = fopen( $result[ 'data' ], "r" ) ) == FALSE ) {
                $output[ 'error' ] = 'Could not open the file,Please Try again';
                return $output;
            } //( $handle = fopen( $result[ 'data' ], "r" ) ) == FALSE
            $row = 1;
            while ( ( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE ) {
                if ( $row == 1 ) {
                    for ( $count = 0; $count < count( $line ); $count++ ) {
                        if ( !( $line[ $count ] == $options[ 'fileFormat' ][ $count ] ) ) {
                            $output[ 'error' ] = 'Incorrect File Format uploaded , please correct the file column format and try again';
                            return $output;
                        } //!( $line[ $count ] == $options[ 'fileFormat' ][ $count ] )
                    } //$count = 0; $count < count( $line ); $count++
                } //$row == 1
                else {
                    array_shift( $line );
                    $vals[ ] = '("' . implode( '","', $line ) . '",' . $activityId . ',null)';
                }
                $row++;
            } //( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE
            fclose( $handle );
            $cols  = $options[ 'cols' ];
            $query = 'INSERT INTO ' . $options[ 'table' ] . $cols . ' VALUES' . implode( ',', $vals );
            $this->mysqli->autocommit( FALSE );
            $qR = $this->mysqli->query( $query );
            if ( !$qR ) {
                $this->mysqli->rollback();
                $output[ 'error' ] = 'Could not Insert the data:' . $this->mysqli->error;
                return $output;
            } //!$qR
            $out = $this->mysqli->commit();
            $this->mysqli->autocommit( TRUE );
            $qR                              = $this->mysqli->query( 'SELECT DISTINCT COUNT(*) as count FROM activity_participants WHERE activity_id=' . $activityId );
            $row                             = $qR->fetch_assoc();
            $activitybeneficiaries_number    = $row[ 'count' ];
            $qR                              = $this->mysqli->query( 'SELECT DISTINCT COUNT(*) as count FROM activity_participants WHERE activity_id=' . $activityId . ' AND category="student"' );
            $row                             = $qR->fetch_assoc();
            $activitybeneficiaries_institute = $row[ 'count' ];
            $output                          = array(
                 'activitybeneficiaries_institute' => $activitybeneficiaries_institute,
                'activitybeneficiaries_number' => $activitybeneficiaries_number,
                'data' => $vals 
            );
            return json_encode( $output );
        } //$participantFile
    }
    function getVenue( $city )
    {
        $sql = 'SELECT DISTINCT * FROM venue where city_id=' . $city;
        $qR  = $this->mysqli->query( $sql );
        if($qR->num_rows!=0){
             while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
            $main[ ] = $qRR;
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
        }else{
            $main['error'] = TRUE;
            $main['data'] = 'No venue found for selected city';
        }
        return $main;
    }
    function getfacultylist( $instituteid )
    {
        $main = array();
        $sql = 'SELECT DISTINCT * FROM human_resource as x join human_institute as y on x.id=y.human_id where x.category="FAC" AND y.institute_id=' . $instituteid;
//        return $sql;
        $qR  = $this->mysqli->query( $sql );
        if($qR->num_rows!=0){
             while ( $qRR = $qR->fetch_array( MYSQLI_ASSOC ) ) {
            $main[ ] = $qRR;
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
        }else{
            $main['error'] = TRUE;
            $main['data'] = 'No faculty found for selected institute';
        }
       
        return $main;
    }
    function readParticipants( $data )
    {
        $output          = array( );
        $aR              = array( );
        $vals            = array( );
        $data            = array( );
        $participantFile = $_FILES[ 'fileParticipant' ];
        if ( $participantFile ) {
            $options                 = array(
                 'mime' => 'text/csv',
                'size' => 50000 
            );
            $options[ 'fileFormat' ] = array(
                 "S. No.",
                "Category",
                "First name",
                "Last name",
                "Date of birth",
                "M/F",
                "email",
                "phone",
                "Performance remarks",
                "Fee" 
            );
            $options[ 'cols' ]       = "(category,first_name,last_name,dob,gender,email,phone,performance_remarks,Fee,activity_id,human_id)";
            $options[ 'table' ]      = 'activity_participants';
            $options[ 'file_name' ]  = 'ActivityPaticipantRead-' . $activityId;
            $resultOne               = $this->_uploadFile( $participantFile, $options );
            if ( $resultOne[ 'error' ] ) {
                $output[ 'error' ] = $resultOne[ 'error' ];
                return $output;
            } //$resultOne[ 'error' ]
            $resultTwo = $this->processFile( $resultOne[ 'data' ], $options[ 'fileFormat' ] );
            if ( isset( $resultTwo[ 'error' ] ) ) {
                return $resultTwo;
            } //isset( $resultTwo[ 'error' ] )
            $vals               = $resultTwo[ 'data' ];
            $fields             = array(
                 "serialno",
                "sore",
                "fname",
                "lname",
                "dob",
                "gender",
                "email",
                "mobileno",
                "performance",
                "feespaid" 
            );
            $data[ 'metadata' ] = array(
                 array(
                     "name" => "serialno",
                    "label" => "SNO.",
                    "datatype" => "integer",
                    "editable" => true 
                ),
                array(
                     "name" => "sore",
                    "label" => "STUDENT / ENTREPRENEUR",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "fname",
                    "label" => "FIRST NAME",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "lname",
                    "label" => "LAST NAME",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "dob",
                    "label" => "D.O.B",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "gender",
                    "label" => "M / F",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "email",
                    "label" => "EMAIL ID",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "mobileno",
                    "label" => "MOBILE NO",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "performance",
                    "label" => "PERFORMANCE",
                    "datatype" => "string",
                    "editable" => true 
                ),
                array(
                     "name" => "feespaid",
                    "label" => "FEES PAID",
                    "datatype" => "string",
                    "editable" => true 
                ) 
            );
            $count              = 0;
            foreach ( $vals as $key => $val ) {
                $temp                                 = array(
                     $fields[ 0 ] => $count + 1,
                    $fields[ 1 ] => $val[ 0 ],
                    $fields[ 2 ] => $val[ 1 ],
                    $fields[ 3 ] => $val[ 2 ],
                    $fields[ 4 ] => $val[ 3 ],
                    $fields[ 5 ] => $val[ 4 ],
                    $fields[ 6 ] => $val[ 5 ],
                    $fields[ 7 ] => $val[ 6 ],
                    $fields[ 8 ] => $val[ 7 ],
                    $fields[ 9 ] => $val[ 8 ] 
                );
                $data[ 'data' ][ $count ][ 'id' ]     = $count + 1;
                $data[ 'data' ][ $count ][ 'values' ] = $temp;
                $count++;
            } //$vals as $key => $val
            return json_encode( $data );
        } //$participantFile
    }
    function processFile( $fileHandle, $columnFormat )
    {
        $vals = array( );
        if ( ( $handle = fopen( $fileHandle, "r" ) ) == FALSE ) {
            $output[ 'error' ] = 'Could not open the file,Please Try again';
            return $output;
        } //( $handle = fopen( $fileHandle, "r" ) ) == FALSE
        $row = 1;
        while ( ( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE ) {
            if ( $row == 1 ) {
                for ( $count = 0; $count < count( $line ); $count++ ) {
                    if ( !( $line[ $count ] == $columnFormat[ $count ] ) ) {
                        $output[ 'error' ] = 'Incorrect File Format uploaded , please correct the file column format and try again';
                        return $output;
                    } //!( $line[ $count ] == $columnFormat[ $count ] )
                } //$count = 0; $count < count( $line ); $count++
            } //$row == 1
            else {
                array_shift( $line );
                $vals[ ] = $line;
            }
            $row++;
        } //( $line = fgetcsv( $handle, 1000, "," ) ) !== FALSE
        fclose( $handle );
        $output[ 'data' ] = $vals;
        return $output;
    }
    
    
    
    function fix_date($date){      
        if(!$this->mdlCmn){
          $this->mdlCmn =   new Application_Model_Common();
        } 
        
        $result  =  $this->mdlCmn->display_date($date);
        return $result;
    }
}

