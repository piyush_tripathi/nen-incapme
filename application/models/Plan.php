<?php

class Application_Model_Plan extends Application_Model_Db { /**
 * class Application_Model_Activity for controlling the activity actions related to the institute
 * @author     Piyush Tripathi <piyush.tripathi@inkoniq.com>
 * @version    0.0.1
 */

    public function __construct() {
        $dbconfig = Zend_Registry::get("dbconfig");
        $this->mysqli = new mysqli($dbconfig->hostname, $dbconfig->username, $dbconfig->password, $dbconfig->dbname);
        //$this->mysqli = new mysqli( 'localhost', 'root', '', 'incapmeqa' ); 
    }

    public function create_institute_plan($data, $institute_id) {
        $output = array();
        $aR = array();
        $int = 'int';
        $str = 'str';
        $plan_stage = $this->isset_null($data['plan_stage']);
        $iip_currentlevel = $this->isset_null($data['iip_currentlevel'], $int);
        $iip_po_entrepreneurcreated = $this->isset_null($data['iip_po_entrepreneurcreated'], $int);
        $iip_po_entrepreneursupported = $this->isset_null($data['iip_po_entrepreneursupported'], $int);
        $iip_po_jobscreated = $this->isset_null($data['iip_po_jobscreated'], $int);


        if (!empty($data['iip_po_other'])) {
            foreach ($data['iip_po_other'] as $key => $value) {
                if (!empty($value)) {
                    $temp[] = $this->array_to_csv($value);
                    $iip_po_other = implode(',', $temp);
                } else {
                    $iip_po_other = '';
                }
            }
            unset($temp);
            unset($value);
        } else {
            $iip_po_other = '';
        }



        if (!empty($data['iip_dkici_other'])) {
            foreach ($data['iip_dkici_other'] as $key => $value) {
                if (!empty($value)) {
                    $temp[] = $this->array_to_csv($value);
                    $iip_dkici_other = implode(',', $temp);
                } else {
                    $iip_dkici_other = '';
                }
            }

            unset($temp);
            unset($value);
        } else {
            $iip_dkici_other = '';
        }
        $iip_dkici_humanresources = $this->isset_null($data['iip_dkici_humanresources'], $str);
        $iip_dkici_infrastructureplatforms = $this->isset_null($data['iip_dkici_infrastructureplatforms'], $str);
        $iip_dkici_programsnactivities = $this->isset_null($data['iip_dkici_programsnactivities'], $str);



        $cols = 'iap_id,
            iip_currentlevel,
            iip_po_entrepreneurcreated,
            iip_po_entrepreneursupported ,
            iip_po_jobscreated,
            iip_po_other,
            iip_dkici_humanresources ,
            iip_dkici_infrastructureplatforms,
            iip_dkici_programsnactivities,
            iip_dkici_other,
            created,
            institute_id,
            plan_stage';

        $vals = 'NULL,'
                . $iip_currentlevel . ',' .
                '' . $iip_po_entrepreneurcreated . ',' .
                '' . $iip_po_entrepreneursupported . ',' .
                '' . $iip_po_jobscreated . ',' .
                '"' . $iip_po_other . '",' .
                '"' . $iip_dkici_humanresources . '",' .
                '"' . $iip_dkici_infrastructureplatforms . '",' .
                '"' . $iip_dkici_programsnactivities . '",' .
                '"' . $iip_dkici_other . '",NOW(),"' . $institute_id . '","' . $plan_stage . '"';

        $sql = 'INSERT INTO inst_annual_plan(' . $cols . ') VALUES(' . $vals . ')';

        //die($sql);
        //return $sql;
        $qR = $this->mysqli->query($sql);
        if ($qR) {
            $output['success'] = 'Created';
            $output['plan_id'] = $this->mysqli->insert_id;
        } //$qR
        else {
            $output['error'] = 'Could not Create the plan:' . $this->mysqli->error;
            $output['data'] = $data;
        }
        return $output;
    }

    function array_to_csv($not_escaped) {
//            if(!$not_escaped){
//                return false;
//            }

        $escaped = base64_encode($not_escaped);
        return $escaped;
    }

    function csv_to_array($escaped) {
        if (!$escaped) {
            return false;
        }
        $not_escaped = base64_decode($escaped);
        return $not_escaped;
    }

    function get_plan_data($plan_id) {
        $output = array();
        $sql = 'SELECT DISTINCT * FROM inst_annual_plan where iap_id=' . (int) $plan_id;

        $result = $this->execute_query($sql);



        foreach ($result[0] as $key => $value) {
            if ($key == 'iip_po_other' || $key == 'iip_dkici_other' || $key == 'gs_oicg_other' || $key == 'gs_po_other' || $key == 'eep_list' && !empty($value)) {
                $val = explode(',', $value);

                foreach ($val as $k => $v) {
                    if (!empty($v)) {
                        $output[$key][] = $this->csv_to_array($v);
                    }
                }
            } else if ($key == 'tc_course' || $key == 'im_other' || $key == 'activityplan') {
                $val = explode(',', $value);
                foreach ($val as $k => $v) {
                    if (!empty($v)) {
                        $temp = explode(':', $v);
                        foreach ($temp as $course) {
                            $output[$key][$k][] = $this->csv_to_array($course);
                        }
                    }
                }
            } else {
                $output[$key] = $value;
            }
        }

        return $output;
    }

    function publish_inst_plan($plan_id, $institute_id) {

//        select all the activities fromt the plan 
        $act_list = array();
        $query = array();
        $sql = 'SELECT activityplan from inst_annual_plan where iap_id=' . $plan_id;
        $type = array(
            "1" => "Lecture/ Talk",
            "2" => "Panel Discussion",
            "3" => "Exercise/ Games",
            "4" => "Conference",
            "5" => "Competition",
            "6" => "Workshop",
            "7" => "Course",
            "8" => "Mentoring",
            "9" => "Start-up Support",
            "10" => "Webinar",
            "11" => "National Platform",
            "12" => "Outreach",
            "13" => "Review"
        );
        $audience = array("0" => "Faculty",
            "1" => "Students/ Entrepreneur Pipeline",
            "2" => "Entrepreneurs", "3" => "Other");

        $result = $this->execute_query($sql);

        if (!empty($result)) {
            $activities = explode(',', $result[0]['activityplan']);

            foreach ($activities as $key => $val) {

                $temp = explode(':', $val);
                foreach ($temp as $act) {
                    $act_list[$key][] = $this->csv_to_array($act);
                }
            }
        }


        if (!empty($act_list)) {
            foreach ($act_list as $key => $value) {
                $act_name = $value['0'];
                $temp = explode('-', $value[1]);
                $act_date = $temp[2] . '-' . $temp[1] . '-' . $temp[0];
                $act_type = $type[$value['2'] + 1];
                $act_audi = $audience[$value['3']];
                //getiing city and venue details 
                $sql = 'SELECT x.city_id AS id
                            FROM city AS x
                            INNER JOIN institute AS y ON x.city_id = y.city_id
                            WHERE y.id =' . $institute_id . ' limit 1';
                $result = $this->execute_query($sql);
                $city_id = $result[0]['id'];

                $sql = 'select id from venue where city_id=' . $city_id . ' limit 1';
                $result = $this->execute_query($sql);
                $venue_id = $result[0]['id'];

                $query[] = "INSERT INTO activity(activity_id,activityinfo_title,activityplacetime_date_start,activityinfo_type,audience,institute_id,activity_city,activityplacetime_venue) values(null,'$act_name','$act_date','$act_type','$act_audi','$institute_id','$city_id','$venue_id')";
            }

            $this->mysqli->autocommit(FALSE);
            $qR = TRUE;
            foreach ($query as $key => $value) {
                $qR = $qR & $this->mysqli->query($value);
            } //$sql as $key => $value
            if ($qR) {
                $out = $this->mysqli->commit();
                $this->mysqli->autocommit(TRUE);
                $this->update_plan_stage('5', $plan_id);
                $output['success'] = 'updated ';
                return $output;
            } //$qR
            else {
                $this->mysqli->rollback();
                return $output;
                $output['error'] = 'Could not Insert the activity:' . $this->mysqli->error;
            }
        }
    }

    function update_inst_plan($data, $plan_stage, $plan_id) {
        if (!$data || !$plan_stage) {
            return FALSE;
        }


        switch ((int) $plan_stage) {
            case 1:
                $output = $this->update_stage_1($data, $plan_id);
                break;
            case 2:
                $output = $this->update_stage_2($data, $plan_id);
                break;

            case 3:
                $output = $this->update_stage_3($data, $plan_id);
                break;

            case 4:
                $output = $this->update_stage_4($data, $plan_id);
                break;


            default:
                break;
        }

        return $output;
    }

    function update_stage_1($data, $plan_id) {
        $output = array();
        $aR = array();
        $int = 'int';
        $str = 'str';
        $plan_stage = $this->isset_null($data['plan_stage']);
        $iip_currentlevel = $this->isset_null($data['iip_currentlevel'], $str);
        $iip_po_entrepreneurcreated = $this->isset_null($data['iip_po_entrepreneurcreated'], $int);
        $iip_po_entrepreneursupported = $this->isset_null($data['iip_po_entrepreneursupported'], $int);
        $iip_po_jobscreated = $this->isset_null($data['iip_po_jobscreated'], $int);
        if (!empty($data['iip_po_other'])) {
            foreach ($data['iip_po_other'] as $key => $value) {
                if (!empty($value)) {
                    $temp[] = $this->array_to_csv($value);
                    
                }  
            }
            $iip_po_other = implode(',', $temp);
            unset($temp);
            unset($value);
        } else {
            $iip_po_other = '';
        }



        if (!empty($data['iip_dkici_other'])) {
            foreach ($data['iip_dkici_other'] as $key => $value) {
                if (!empty($value)) {
                    $temp[] = $this->array_to_csv($value);
                    
                } 
            }
            $iip_dkici_other = implode(',', $temp);
            unset($temp);
            unset($value);
        } else {
            $iip_dkici_other = '';
        }


        $iip_dkici_humanresources = $this->isset_null($data['iip_dkici_humanresources']);
        $iip_dkici_infrastructureplatforms = $this->isset_null($data['iip_dkici_infrastructureplatforms']);
        $iip_dkici_programsnactivities = $this->isset_null($data['iip_dkici_programsnactivities']);

        $stmt = 'iip_currentlevel="' . $iip_currentlevel . '" ,iip_po_entrepreneurcreated=' . $iip_po_entrepreneurcreated . ' ,iip_po_entrepreneursupported =' . $iip_po_entrepreneursupported . ' ,iip_po_jobscreated=' . $iip_po_jobscreated . ' ,iip_po_other="' . $iip_po_other . '" ,iip_dkici_humanresources ="' . $iip_dkici_humanresources . '" ,iip_dkici_infrastructureplatforms="' . $iip_dkici_infrastructureplatforms . '" ,iip_dkici_programsnactivities="' . $iip_dkici_programsnactivities . '" ,iip_dkici_other="' . $iip_dkici_other . '"';

        $sql = 'UPDATE inst_annual_plan  SET ' . $stmt . ' WHERE iap_id=' . $plan_id;



        $result = $this->execute_query($sql);
        $this->update_plan_stage($plan_stage, $plan_id);

        return $result;
    }

    function update_stage_2($data, $plan_id) {
        $output = array();
        $aR = array();
        $int = 'int';
        $plan_stage = $this->isset_null($data['plan_stage']);
        $gs_targeted_level = $this->isset_null($data['gs_targeted_level'], $int);
        $gs_po_entrpnrcreated = $this->isset_null($data['gs_po_entrpnrcreated'], $int);
        $gs_po_entrpnrsupported = $this->isset_null($data['gs_po_entrpnrsupported'], $int);
        $gs_po_jobscreated = $this->isset_null($data['gs_po_jobscreated'], $int);
        if (empty($data['gs_po_other'])) {
            $data['gs_po_other'] = null;
        } else {
            foreach ($data['gs_po_other'] as $key => $value) {
                if ($value) {
                    $temp[] = $this->array_to_csv($value);
                }
            }
            $gs_po_other = implode(',', $temp);
            unset($temp);
            unset($value);
        }
        //for other fields 
        if (empty($data['gs_oicg_other'])) {
            $data['gs_oicg_other'] = null;
        } else {
            foreach ($data['gs_oicg_other'] as $key => $value) {
                if ($value) {
                    $temp[] = $this->array_to_csv($value);
                }
            }
            $gs_oicg_other = implode(',', $temp);
            unset($temp);
            unset($value);
        }


        $gs_oicg_humanresources = $this->isset_null($data['gs_oicg_humanresources']);
        $gs_oicg_infrastructureplatforms = $this->isset_null($data['gs_oicg_infrastructureplatforms']);
        $gs_oicg_programsnactivities = $this->isset_null($data['gs_oicg_programsnactivities']);

        $stmt = 'gs_targeted_level="' . $gs_targeted_level . '" ,gs_po_entrpnrcreated=' . $gs_po_entrpnrcreated . ' ,gs_po_entrpnrsupported =' . $gs_po_entrpnrsupported . ' ,gs_po_jobscreated=' . $gs_po_jobscreated . ' ,gs_po_other="' . $gs_po_other . '" ,gs_oicg_humanresources ="' . $gs_oicg_humanresources . '" ,gs_oicg_infrastructureplatforms="' . $gs_oicg_infrastructureplatforms . '" ,gs_oicg_programsnactivities="' . $gs_oicg_programsnactivities . '" ,gs_oicg_other="' . $gs_oicg_other . '"';

        $sql = 'UPDATE inst_annual_plan  SET ' . $stmt . ' WHERE iap_id=' . $plan_id;

//        return $sql;

        $result = $this->execute_query($sql);
        $this->update_plan_stage($plan_stage, $plan_id);
        return $result;
    }

    function update_stage_3($data, $plan_id) {
        $output = array();
        $aR = array();
        $temp = array();
        $date = 'date';
        $int = 'int';
        $plan_stage = $this->isset_null($data['plan_stage']);
        $trainingplan = $this->isset_null($data['trainingplan']);
        $attractingexternalexperts = $this->isset_null($data['attractingexternalexperts']);


        $eep_noofsessions = $this->isset_null($data['eep_noofsessions'], $int);

        $ecellmilestones_dropdown = $this->isset_null($data['ecellmilestones_dropdown']);
        $ecellmilestones_dropdown_other = $this->isset_null($data['ecellmilestones_dropdown_other']);
        $ecellmilestones_date = $this->isset_null($data['ecellmilestones_date']);
        $ecellmilestones_otheractioncomments = $this->isset_null($data['ecellmilestones_otheractioncomments']);

        $edcmilestones_dropdown = $this->isset_null($data['edcmilestones_dropdown']);
        $edcmilestones_dropdown_other = $this->isset_null($data['edcmilestones_dropdown_other']);
        $edcmilestones_date = $this->isset_null($data['edcmilestones_date'], $date);
        $edcmilestones_otheractioncomments = $this->isset_null($data['edcmilestones_otheractioncomments']);

        $iedcesc_dropdown = $this->isset_null($data['iedcesc_dropdown']);
        $iedcesc_dropdown_other = $this->isset_null($data['iedcesc_dropdown_other']);
        $iedcesc_date = $this->isset_null($data['iedcesc_date'], $date);
        $iedcesc_otheractioncomments = $this->isset_null($data['iedcesc_otheractioncomments']);

        $svmu_dropdown = $this->isset_null($data['svmu_dropdown']);
        $svmu_dropdown_other = $this->isset_null($data['svmu_dropdown_other']);
        $svmu_date = $this->isset_null($data['svmu_date'], $date);
        $svmu_otheractioncomments = $this->isset_null($data['svmu_otheractioncomments']);

        $incubator_dropdown = $this->isset_null($data['incubator_dropdown']);
        $incubator_dropdown_other = $this->isset_null($data['incubator_dropdown_other']);
        $incubator_date = $this->isset_null($data['incubator_date'], $date);
        $incubator_otheractioncomments = $this->isset_null($data['incubator_otheractioncomments']);


        // expert list multi row element      

        if (!empty($data['eep_list'])) {
            foreach ($data['eep_list'] as $key => $value) {
                if (!empty($value)) {
                    $temp[] = $this->array_to_csv($value);
                    
                } 
            }
           
               $eep_list = implode(',', $temp); 
          
           
            unset($temp);
            unset($value);
        } else {
            $eep_list = 'no2';
        }


//        now getting multiple value fields

        foreach ($data['tc_course_type'] as $key => $value) {
            if ($value == 'Other') {
                $value = $value . '|' . $data['tc_course_type_other'][$key];
                $temp[] = $this->array_to_csv($value) . ':' . $this->array_to_csv($data['tc_startdate'][$key]);
            } else if ($value != 'none') {
                $temp[] = $this->array_to_csv($value) . ':' . $this->array_to_csv($data['tc_startdate'][$key]);
            }
        }
        $tc_course = implode(',', $temp);
        unset($temp);


//        for activity calander entries 

        foreach ($data['ac_type'] as $key => $value) {
            if ($value != 'none' && $data['ac_name'][$key] && $data['ac_planneddate'][$key] && $data['ac_audience'][$key] != 'none') {
                $temp[] = $this->array_to_csv($data['ac_name'][$key]) .
                        ':' . $this->array_to_csv($data['ac_planneddate'][$key]) .
                        ':' . $this->array_to_csv($value) .
                        ':' . $this->array_to_csv($data['ac_audience'][$key]);
            }
        }
        $activityplan = implode(',', $temp);
        unset($temp);

//        for other field in infrstructure milestones im_other_text[] im_other_date[] im_other_otheractioncomments[]        
        foreach ($data['im_other_text'] as $key => $value) {
            if ($value) {
                $temp[] = $this->array_to_csv($value) .
                        ':' . $this->array_to_csv($data['im_other_date'][$key]) .
                        ':' . $this->array_to_csv($data['im_other_otheractioncomments'][$key]);
            }
        }
        $im_other = implode(',', $temp);
        unset($temp);

//        Now create the statemtn for sql 
        $stmt = 'plan_stage ="' . $plan_stage . '",trainingplan="' . $trainingplan . '",attractingexternalexperts="' . $attractingexternalexperts . '",ecellmilestones_dropdown ="' . $ecellmilestones_dropdown . '",ecellmilestones_date="' . $ecellmilestones_date . '",ecellmilestones_otheractioncomments ="' . $ecellmilestones_otheractioncomments . '",edcmilestones_dropdown="' . $edcmilestones_dropdown . '",edcmilestones_date =' . $edcmilestones_date . ',edcmilestones_otheractioncomments="' . $edcmilestones_otheractioncomments . '",iedcesc_dropdown="' . $iedcesc_dropdown . '",iedcesc_date =' . $iedcesc_date . ',iedcesc_otheractioncomments="' . $iedcesc_otheractioncomments . '",svmu_dropdown ="' . $svmu_dropdown . '",svmu_date=' . $svmu_date . ',svmu_otheractioncomments ="' . $svmu_otheractioncomments . '",incubator_dropdown ="' . $incubator_dropdown . '",incubator_date=' . $incubator_date . ',incubator_otheractioncomments ="' . $incubator_otheractioncomments . '",tc_course = "' . $tc_course . '",eep_list ="' . $eep_list . '",eep_noofsessions =' . $eep_noofsessions . ',im_other ="' . $im_other . '",activityplan ="' . $activityplan . '",ecellmilestones_dropdown_other ="' . $ecellmilestones_dropdown_other . '",edcmilestones_dropdown_other ="' . $edcmilestones_dropdown_other . '",iedcesc_dropdown_other ="' . $iedcesc_dropdown_other . '",svmu_dropdown_other ="' . $svmu_dropdown_other . '",incubator_dropdown_other ="' . $incubator_dropdown_other . '"';

        $sql = 'UPDATE inst_annual_plan  SET ' . $stmt . ' WHERE iap_id=' . $plan_id;

//            return $sql;
        $result = $this->execute_query($sql);
        $this->update_plan_stage($plan_stage, $plan_id);
        return $result;
    }

    function update_stage_4($data, $plan_id) {

        $plan_stage = $this->isset_null($data['plan_stage']);
        $stmt = array();
        if ($data['leadfacultysignoff_yesno']) {
            $stmt[] = 'leadfacultysignoff_yesno="1" ,leadfacultysignoff_name="' . $data['leadfacultysignoff_name'] . '" ,leadfacultysignoff_date =NOW()';
        } else {
            $stmt[] = 'leadfacultysignoff_yesno="0" ,leadfacultysignoff_name="" ,leadfacultysignoff_date =NOW()';
        }
        if ($data['nensignoff_yesno']) {
            $stmt[] = 'nensignoff_yesno="1" ,nensignoff_name="' . $data['nensignoff_name'] . '" ,nensignoff_date =NOW()';
        } else {
            $stmt[] = 'nensignoff_yesno="0" ,nensignoff_name="" ,nensignoff_date =NOW()';
        }
        if ($data['instituteheadsignoff_yesno']) {
            $stmt[] = 'instituteheadsignoff_yesno="1" ,instituteheadsignoff_name="' . $data['instituteheadsignoff_name'] . '" ,instituteheadsignoff_date =NOW()';
        } else {
            $stmt[] = 'instituteheadsignoff_yesno="0" ,instituteheadsignoff_name="" ,instituteheadsignoff_date =NOW()';
        }

        if (!empty($stmt)) {
            $sql = 'UPDATE inst_annual_plan  SET ' . implode(',', $stmt) . ' WHERE iap_id=' . $plan_id;
            $result = $this->execute_query($sql);
            $this->update_plan_stage($plan_stage, $plan_id);
        }
        return $result;
    }

    function update_plan_stage($plan_stage, $plan_id) {
        if (!$plan_stage || !$plan_id) {
            return false;
        }

        $sql = 'SELECT plan_stage from inst_annual_plan where iap_id=' . $plan_id;
        $result = $this->execute_query($sql);

        $old_stage = $result[0]['plan_stage'];

        if ($old_stage <= (int) $plan_stage) {
            (int) $plan_stage = (int) $plan_stage;
            $sql = 'UPDATE inst_annual_plan set plan_stage=' . $plan_stage . ' where iap_id=' . $plan_id;

            $result = $this->execute_query($sql);
            return true;
        }
        return false;
    }

    function execute_query($query) {
        $result = array();
        try {
            $qR = $this->mysqli->query($query);
            if ($qR->num_rows == 1) {
                $result[] = $qR->fetch_assoc();
            } else if ($qR->num_rows > 1) {
                while ($qRR = $qR->fetch_assoc()) {
                    $result[] = $qRR;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            die($e->getTraceAsString());
            return false;
        }
        if (empty($result)) {
            return null;
        }
        return $result;
    }

    function sql_date($insertdate, $format = null) {
        //$insertdate = mysqli_real_escape_string($insertdate);
        if (!$insertdate) {
            return null;
        }
        $insertdate = strtotime($insertdate);
        if ($format) {
            $insertdate = date($format, $insertdate);
        } else {
            $insertdate = date('Y-m-d', $insertdate);
        }

        return $insertdate;
    }

    function insert_ap($data) {
        $output = array();
        $aR = array();
        $vals = array();
        $sql = array();
        $contributors = isset($data['activity_contributor_corodinator']) ? $data['activity_contributor_corodinator'] : NULL;
        $leadPresenters = isset($data['activity_contributor_lead_presenter']) ? $data['activity_contributor_lead_presenter'] : NULL;
        $activityId = mysqli_real_escape_string($this->mysqli, $data['activity_id']);
        $sponsorsMoney = array_filter($data['activitycontributor_sponsors_money']);
        $sponsorsInstitute = array_filter($data['activitycontributor_sponsors']);
        $partner = array_filter($data['activitycontributor_partner']);
        $activity_contributor_sponsors = array();
        $activitycontributor_partner = array();
        for ($count = 0; $count < count($sponsorsMoney); $count++) {
            $activity_contributor_sponsors[] = '(' . mysqli_real_escape_string($this->mysqli, $sponsorsInstitute[$count]) . '; 
' . mysqli_real_escape_string($this->mysqli, $sponsorsMoney[$count]) . ')';
        } //$count = 0; $count < count( $sponsorsMoney ); $count++
        for ($count = 0; $count < count($partner); $count++) {
            $activitycontributor_partner[] = '(' . mysqli_real_escape_string($this->mysqli, $partner[$count]) . ')';
        } //$count = 0; $count < count( $partner ); $count++
        //contributors details 
        $sql[] = 'DELETE FROM activity_contributor_corodinator WHERE activity_id=' . $activityId;
        if (isset($contributors)) {
            foreach ($contributors as $key => $val) {
                $vals[] = '(' . $activityId . ',null,"' . mysqli_real_escape_string($this->mysqli, $val) . '")';
            } //$contributors as $key => $val
            $sql[] = 'INSERT INTO activity_contributor_corodinator(activity_id,unmapped_coordinator,human_id) VALUES' . implode(',', $vals);
            $vals = array();
        } //isset( $contributors )
        //lead speaker details 
        $sql[] = 'DELETE FROM activity_contributor_lead_presenter WHERE activity_id=' . $activityId;
        if (isset($leadPresenters)) {
            foreach ($leadPresenters as $key => $val) {
                $vals[] = '(' . $activityId . ',null,"' . mysqli_real_escape_string($this->mysqli, $val) . '")';
            } //$leadPresenters as $key => $val
            $sql[] = 'INSERT INTO activity_contributor_lead_presenter(activity_id,unmapped_presenter,human_id) VALUES' . implode(',', $vals);
            $vals = array();
        } //isset( $leadPresenters )
        $stmt = 'activitycontributor_sponsors="' . implode('|', $activity_contributor_sponsors) . '",activitycontributor_partner="' . implode('|', $activitycontributor_partner) . '"';
        $cols = implode(',', array_keys($aR));
        $vals = implode(',', array_values($aR));
        $sql[] = 'UPDATE activity SET ' . $stmt . ' WHERE activity_id=' . $activityId;
        $query = implode('; 
', $sql);
        $this->mysqli->autocommit(FALSE);
        $qR = TRUE;
        foreach ($sql as $key => $value) {
            $qR = $qR & $this->mysqli->query($value);
        } //$sql as $key => $value
        if ($qR) {
            $out = $this->mysqli->commit();
            $output['success'] = 'updated ';
        } //$qR
        else {
            $this->mysqli->rollback();
            $output['error'] = 'Could not Insert the activity:' . $this->mysqli->error;
        }
        $output['activity_id'] = $activityId;
        return $output;
    }

    public function insertParticipants($data) {
        $output = array();
        $aR = array();
        $sql = array();
        $activityId = mysqli_real_escape_string($this->mysqli, $data['activity_id']);
        $output['activity_id'] = $activityId;
        $options['cols'] = "(activity_id,category,first_name,last_name,dob,gender,email,phone,performance_remarks,Fee)";
        $options['table'] = 'activity_participants';
        $activity_participants_data = $data['activity_participants'];
        $org = array(
            '[[],',
            ']]',
            ']',
            ',["","","","","","","","",""]',
            '['
        );
        $replace = array(
            '',
            ')',
            ')',
            '',
            '(' . $activityId . ','
        );
        //        if($activity_participants_data){ 
        //            
        //               $activity_participants           =  str_replace( $org, $replace, $activity_participants_data );
        //                       
        //               }
        //               else{
        //                  $activity_participants = false;
        //               }; 
        if (empty($data['activitybeneficiaries_number'])) {
            $activitybeneficiaries_number = 0;
        } //empty( $data[ 'activitybeneficiaries_number' ] )
        else {
            $activitybeneficiaries_number = mysqli_real_escape_string($this->mysqli, $data['activitybeneficiaries_number']);
            $valid = $this->validate($activitybeneficiaries_number, 'numeric');
            if (isset($valid['error'])) {
                $output = $valid;
                return $output;
            } //isset( $valid[ 'error' ] )
            $valid = array();
        }
        if (empty($data['activitybeneficiaries_institute'])) {
            $activitybeneficiaries_institute = 0;
        } //empty( $data[ 'activitybeneficiaries_institute' ] )
        else {
            $activitybeneficiaries_institute = mysqli_real_escape_string($this->mysqli, $data['activitybeneficiaries_institute']);
            $valid = $this->validate($activitybeneficiaries_institute, 'numeric');
            if (isset($valid['error'])) {
                $output = $valid;
                return $output;
            } //isset( $valid[ 'error' ] )
            $valid = array();
        }
        if (empty($data['activitybeneficiaries_fees'])) {
            $activitybeneficiaries_fees = 0;
        } //empty( $data[ 'activitybeneficiaries_fees' ] )
        else {
            $activitybeneficiaries_fees = mysqli_real_escape_string($this->mysqli, $data['activitybeneficiaries_fees']);
            $valid = $this->validate($activitybeneficiaries_fees, 'numeric');
            if (isset($valid['error'])) {
                $output = $valid;
                return $output;
            } //isset( $valid[ 'error' ] )
            $valid = array();
        }
        //check if participant data grid has values in it
        $status = json_decode($activity_participants_data);
        if (empty($status[1])) {
            $activity_participants = false;
        } //empty( $status[ 1 ] )
        else {
            $activity_participants = str_replace($org, $replace, $activity_participants_data);
            $activitybeneficiaries_number = count($status) - 1;
        }
        // if list is uploaded then take the data from the list else take the posted data for : activitybeneficiaries_number,activitybeneficiaries_fees
        if ($activity_participants) {
            $this->mysqli->autocommit(FALSE);
            $sql = 'DELETE FROM ' . $options['table'] . ' WHERE activity_id=' . $activityId;
            $qR = $this->mysqli->query($sql);
            $query = 'INSERT INTO ' . $options['table'] . $options['cols'] . ' VALUES' . $activity_participants;
            //die(var_dump($query));
            $qR = $this->mysqli->query($query);
            if (!$qR) {
                $this->mysqli->rollback();
                $output['error'] = 'Could not Insert the data:' . $this->mysqli->error;
                return $output;
            } //!$qR
            $out = $this->mysqli->commit();
        } //$activity_participants
        $vals = array();
        $stmt = 'activitybeneficiaries_fees="' . $activitybeneficiaries_fees . '",activitybeneficiaries_number="' . $activitybeneficiaries_number . '",activitybeneficiaries_institute="' . $activitybeneficiaries_institute . '"';
        $sql = 'UPDATE activity SET ' . $stmt . ' WHERE activity_id=' . $activityId;
        $qR = $this->mysqli->query($sql);
        if (!$qR) {
            $this->mysqli->rollback();
            $output['error'] = 'Could not Insert the activity:' . $this->mysqli->error;
            return $output;
        } //!$qR
        $this->mysqli->commit();
        $output['success'] = 'updated';
        return $output;
    }

    function insert_gtip($data) {
        $output = array();
        $aR = array();
        $vals = array();
        $sql = array();
        $valsA = array();
        $activityId = mysqli_real_escape_string($this->mysqli, $data['activity_id']);
        $output['activity_id'] = $activityId;
        $menteeslistFile = $_FILES['activityoutcome_menteeslist'];
        $mentorslistFile = $_FILES['activityoutcome_mentorslist'];
        if ($menteeslistFile['size'] != 0) {
            $options = array(
                'mime' => 'text/csv',
                'size' => 50000
            );
            $options['fileFormat'] = array(
                "S. No.",
                "Category",
                "First name",
                "Last name",
                "Date of birth",
                "M/F",
                "email",
                "phone",
                "Mentored by whom (first name, last name)"
            );
            $options['cols'] = array();
            $options['cols'] = "(category,first_name,last_name,dob,gender,email,phone,mentored_by,activity_id,human_id)";
            $options['table'] = 'activity_outcome_mentees_list';
            $options['file_name'] = 'ActivityMentees-' . $activityId;
            $resultOne = $this->_uploadFile($menteeslistFile, $options);
            if ($resultOne['error']) {
                $output['error'] = $resultOne['error'];
                return $output;
            } //$resultOne[ 'error' ]
            $resultTwo = $this->processFile($resultOne['data'], $options['fileFormat']);
            if (isset($resultTwo['error'])) {
                return $resultTwo;
            } //isset( $resultTwo[ 'error' ] )
            $vals = $resultTwo['data'];
            foreach ($vals as $key => $val) {
                $valsA[] = '("' . implode('","', array_values($val)) . '",' . $activityId . ',NULL)';
            } //$vals as $key => $val
            $sql[] = 'DELETE FROM activity_outcome_mentees_list WHERE activity_id=' . $activityId;
            $sql[] = 'INSERT INTO activity_outcome_mentees_list' . $options['cols'] . ' VALUES' . implode(',', $valsA);
            $vals = array();
            $valsA = array();
        } //$menteeslistFile[ 'size' ] != 0
        else {
            if (isset($data['activityoutcome_menteeslist'])) {
                $menteeslist = $data['activityoutcome_menteeslist'];
                foreach ($menteeslist as $key => $val) {
                    $val = explode('|', mysqli_real_escape_string($this->mysqli, $val));
                    $vals[] = '(' . $activityId . ',"' . implode('","', $val) . '")';
                } //$menteeslist as $key => $val
            } //isset( $data[ 'activityoutcome_menteeslist' ] )
        }
        $options = array(
            'mime' => 'text/csv',
            'size' => 50000
        );
        $options['fileFormat'] = array(
            'S. No.',
            'Apellation',
            'First name',
            'Last name',
            'Designation',
            'Organisation',
            'email',
            'phone',
            'Mentoring whom (first name, last name)'
        );
        $options['cols'] = array();
        $options['cols'] = "(category,first_name,last_name,dob,gender,email,phone,mentoring_whom,activity_id,human_id)";
        $options['table'] = 'activity_outcome_mentors_list';
        $options['file_name'] = 'ActivityMentor-' . $activityId;
        if ($mentorslistFile['size'] != 0) {
            $resultThree = $this->_uploadFile($mentorslistFile, $options);
            if ($resultThree['error']) {
                $output['error'] = $resultThree['error'];
                return $output;
            } //$resultThree[ 'error' ]
            $resultFour = $this->processFile($resultThree['data'], $options['fileFormat']);
            if (isset($resultFour['error'])) {
                return $resultFour;
            } //isset( $resultFour[ 'error' ] )
            $vals = $resultFour['data'];
            foreach ($vals as $key => $val) {
                $valsA[] = '("' . implode('","', array_values($val)) . '",' . $activityId . ',NULL)';
            } //$vals as $key => $val
            $sql[] = 'DELETE FROM activity_outcome_mentors_list WHERE activity_id=' . $activityId;
            $sql[] = 'INSERT INTO activity_outcome_mentors_list' . $options['cols'] . ' VALUES' . implode(',', $valsA);
            $vals = array();
            $valsA = array();
        } //$mentorslistFile[ 'size' ] != 0
        else {
            if (isset($data['activityoutcome_mentorslist'])) {
                $mentorslist = $data['activityoutcome_mentorslist'];
                foreach ($mentorslist as $key => $val) {
                    $val = explode('|', mysqli_real_escape_string($this->mysqli, $val));
                    $vals[] = '(' . $activityId . ',"' . implode('","', $val) . ',null")';
                } //$mentorslist as $key => $val
            } //isset( $data[ 'activityoutcome_mentorslist' ] )
        }
        $stmt = 'activityoutcome_instructor="' . $data['activityoutcome_instructor'] . '",activityoutcome_course="' . $data['activityoutcome_course'] . '",activityoutcome_org="' . $data['activityoutcome_org'] . '",activityoutcome_plan="' . $data['activityoutcome_plan'] . '",activityoutcome_detail="' . $data['activityoutcome_detail'] . '",activityoutcome_idea="' . $data['activityoutcome_idea'] . '",activityoutcome_competition="' . $data['activityoutcome_competition'] . '",activityoutcome_institution="' . $data['activityoutcome_institution'] . '"';
        $sql[] = 'UPDATE activity SET ' . $stmt . ' WHERE activity_id=' . $activityId;
        $query = implode('; ', $sql);
        $this->mysqli->autocommit(FALSE);
        $qR = TRUE;
        foreach ($sql as $key => $value) {
            try {
                $qR = $qR && $this->mysqli->query($value);
            } catch (Exception $e) {
                $output['error'] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ((bool) $qR) {
            $out = $this->mysqli->commit();
            $output['success'] = 'updated ';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output['error'] = 'Could not Insert the activity:' . $this->mysqli->error;
            $output['data'] = implode('; 
', $sql);
        }
        return $output;
    }

    public function insert_iip($data) {
        $output = array();
        $aR = array();
        $stmt = array();
        $activity_id = $data['activity_id'];
        foreach ($data as $col => $val) {
            if ($col == 'activityinfo_title') {
                // make activity name compulsory
                if (empty($val) || trim($val) == '') {
                    $output['error'] = 'Please provide activity name.';
                    $output['data'] = $data;
                    return $output;
                } //empty( $val ) || trim( $val ) == ''
            } //$col == 'activityinfo_title'
            if ($col != 'activity_id') {
                $col = mysqli_real_escape_string($this->mysqli, $col);
                //for date format 
                if ($col == 'activityplacetime_date_start') {
                    $temp = explode('-', $val);
                    $val = '"' . $temp[2] . '-' . $temp[1] . '-' . $temp[0] . '" ';
                } //$col == 'activityplacetime_date_start'
                else {
                    $val = '"' . mysqli_real_escape_string($this->mysqli, $val) . '"';
                }
                $stmt[] .= $col . '=' . $val;
            } //$col != 'activity_id'
        } //$data as $col => $val
        $sql = 'UPDATE activity SET ' . implode(',', $stmt) . ' WHERE activity_id=' . $activity_id;
        //die($sql);
        $qR = $this->mysqli->query($sql);
        if ($qR) {
            $output['success'] = 'updated ';
        } //$qR
        else {
            $output['error'] = 'Could not update the activity:' . $this->mysqli->error;
        }
        return $output;
    }

    function delete_plan($planId) {
        $output = array();
        $activityId = mysqli_real_escape_string($this->mysqli, $planId);
        $sql[] = 'DELETE FROM institute_plan WHERE iap_id=' . $planId;

        $this->mysqli->autocommit(FALSE);
        $qR = TRUE;
        foreach ($sql as $key => $value) {
            try {
                $qR = $qR && $this->mysqli->query($value);
            } catch (Exception $e) {
                $output['error'] = $e->getMessage();
                return $output;
            }
        } //$sql as $key => $value
        if ((bool) $qR) {
            $out = $this->mysqli->commit();
            $output['success'] = ' Deleted the Plan';
        } //(bool) $qR
        else {
            $this->mysqli->rollback();
            $output['error'] = 'Could not Delete The plan:' . $this->mysqli->error;
            $output['data'] = implode('; 
', $sql);
        }
    }

    public function validate($data, $type) {
        $status = array();
        switch ($type) {
            case 'numeric':
                if (is_numeric($data) && (int) $data >= 0) {
                    return true;
                } //is_numeric( $data ) && (int) $data >= 0
                else {
                    $status['error'] = 'Please Provide only non negative numbers';
                    return $status;
                }
                break;
            default:
                break;
        } //$type
    }

    function institute_plan($institute_id) {


        $output = array();
        $result = array();

        if (!$institute_id) {
            $output['error'] = 'No institute selected';
            return $output;
        } //!$institute_id
        $sql = 'SELECT * FROM inst_annual_plan where institute_id=' . $institute_id;

        $qR = $this->mysqli->query($sql);
        if ($qR->num_rows > 0) {
            while ($qRR = $qR->fetch_array(MYSQLI_ASSOC)) {
                $result[] = $qRR;
            } //$qRR = $qR->fetch_array( MYSQLI_ASSOC )
            $header = '<th>SNo.</th><th>Institute Plan Year</th>' . '<th>Last Modified</th>' . '<th>% Completeness</th>';
            //$header .= '<th>Actions</th>';


            $html = '<table class="table table-bordered table-striped">' . $header;
            $i = 1;
            $created_year = array();
            date_default_timezone_set('Asia/Kolkata');
            foreach ($result as $row) {

                $temp = explode('-', $row['created']);
                $created_year[] = $temp[0];
                $percent = (int) $row['plan_stage'] * 25;                
                $changed = $this->fix_date_format($row['changed']);
                if ((int) $row['plan_stage'] > 4) {
                    $progress = '<div class="progress progress-striped active">
                                <div class="bar bar-success" style="width: 100%;">Published</div>
                              </div>';
                } else {
                    $progress = '<div class="progress progress-striped active">
                                <div class="bar bar-success" style="width: ' . $percent . '%;">' . $percent . '%</div>
                              </div>';
                }





                $html .= '<tr>';

                $html .= '<td>' . $i . '</td>' . '<td><a href="/plan/edit?id=' . $row['iap_id'] . '&instituteid=' . $institute_id . '" >' . date("Y", strtotime($row['created'])) . '</a></td>' . '<td>' . $changed . '</td>' . '<td>' . $progress . '</td>';
//                $html .= '<td><a href="/plan/edit?id=' . $row[ 'iap_id' ] . '&instituteid=' . $institute_id . '" ><i class="icon-pencil"></i>  </a>
//                    &nbsp; &nbsp;                              
//                     <a href="#delPlan" data-toggle="modal" delPlan" class="delPlan" value="' . $row[ 'iap_id' ] . '"  ><i class="icon-trash"> </i></a></td>';
                $html .= '</tr>';
                $i++;
            } //$result as $row


            $html .= '</table>';

            //                  if already insti plan for this year then disable the add new button
            if (!in_array(date('Y'), $created_year)) {
                $btn_add = '<a href="/plan/add?instituteid=' . $institute_id . '" name="Add" value=""><span class="btn btn-top"><i class="icon-edit"></i>  Add New Institute Plan </span></a><br/>';
            } else {
                $btn_add = '';
            }

            $output['data'] = $btn_add . $html;
        } //$qR->num_rows > 0
        else {
            $btn_add = '<a href="/plan/add?instituteid=' . $institute_id . '" name="Add" value=""><span class="btn btn-top btn-danger pull-right" style="margin-top:-24px;"><i class="icon-white icon-edit"></i>  Add New Institute Plan </span></a>
                 <div class="institute-search row-fluid" style="width:96%; margin-top:20px ">
    <div class="span2 left-margin"><hr class="record-hr"></div>
    <div class="span2 no-record2">No Institute Plan Found</div>
    <div class="span2"><hr class="record-hr"></div>
</div>';
            $output['data'] = $btn_add;
            $output['error'] = 'Could not retrieve the activity list';
        }
        return $output;
    }

    function isset_null($data, $type = false) {

        if (isset($data) && !empty($data)) {
            if ($type == 'int') {
                //return is_int($data) ? $data : 0;
                return (int) $data;
            } else if ($type == 'date') {
                return '"' . $data . '"';
            } else {
                return $data;
            }
        }

        if ($type == 'int' || $type == 'date') {
            $result = 'NULL';
        } else if ($type == 'str') {
            $result = '';
        } else {
            $result = '';
        }
        return $result;
    }
    
    function fix_date_format($date){  
        if(!$date || $date=='0000-00-00 00:00:00' || $date==''){
            return '';
        }
       $input_date  = New Zend_Date($date,'YYYY-mm-dd');
       $result = $input_date->get('dd-mm-YYYY') ;
        return $result;
    }

}

