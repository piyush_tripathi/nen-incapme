<?php

class Zend_View_Helper_Dropdown extends Zend_View_Helper_Abstract {
    protected $model;
    public function __construct() {
        $this->model = new Application_Model_Common();
    }


    public function dropdown($identifier, $param='', $selectedValue='') {
        
       
        if($identifier == 'multiple_faculty_of_institute') {
            $instituteId = $param;
            if($instituteId) {
                $items = $this->model->getFacultyDDData($instituteId);
            }
            return $this->getMultiOptionsHtml($items, $selectedValue);
        }
        
        
        switch($identifier) {
            case 'faculty_of_institute':
                
                $instituteId = $param;
                 
                if($instituteId) {
                    //$model = new Application_Model_Common();
                   
                    $items = $this->model->getFacultyDDData($instituteId);
                }
                break;
            
            case 'ecell_eleaders':
                $ecellId = $param;
               
                $items = $this->model->getEleadersDDData($ecellId);
                break;
            case 'campus_industry_sector':
            case 'institute_type' :
            case 'edc_foundation_year':
                $items = $this->model->getStaticDDData($identifier);
                break;
            case 'cities' :
                $items = $this->model->getCitiesDDData();
                break;
            case 'consultant_cities' :
                $items = $this->model->getConsultantCitiesDDData($param);
                break;
            
                
            case 'consultant' :
                $items = $this->model->getConsultantDDData(); 
                break;
            
                
                
        }
         
        return $this->getOptionsHtml($items, $selectedValue);

        
    }
   
    
    private function getOptionsHtml($items, $selectedValue = null) {
        
        $options = '';
        if(is_array($items)) {
            foreach ($items as  $item) {
                if(array_key_exists('value', $item)) {
                    
                    $selected = ($item['value']==$selectedValue) ? ' selected ' : '';
                    
                   
                    $options .= '<option value="'. $item['value'].'" '.$selected.'>'.$item['caption'].'</option>'; 
                    
                    
                } else if (array_key_exists($item['name'])){
                    $selected = ($item['name']==$selectedValue) ? ' selected ' : '';
                    $options.= '<option'. ' value="'. $item['name'] .'" '.$selected.'>'.$item['name'] .'</option>';
                } else {
                    $selected = ($item==$selectedValue) ? ' selected ' : '';
                    $options.= '<option'. ' value="'. $item .'" '.$selected.'>'.$item .'</option>';
                }
                
                
                
            }
        }
       
        return $options;
    }
    
    private function getMultiOptionsHtml($items, $selectedValue = null) {
        $options = '';
        //print_r($selectedValue);
        if(is_array($items)) {
            foreach ($items as  $item) {
                if(array_key_exists('value', $item)) {
                    //print_R($item);
                    $selected = (in_array($item['value'], $selectedValue)) ? ' selected ' : '';
                    $options .= '<option'. ' value="'. $item['value'].'" '.$selected.'>'.$item['caption'].'</option>';
                } 
                
            }
        }
        return $options;
    }
}

?>
