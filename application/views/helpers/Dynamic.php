<?php
class Zend_View_Helper_Dynamic extends Zend_View_Helper_Abstract {
    
    
     public function dynamic($type,$data,$selectedValue=null){
         return $this->$type($data,$selectedValue);
     }
    

	function getCity($city,$selectedValue=null){
	
		  if(!$city){
           return null; 
        }	
		 $htm='';
		     
		 foreach($city as $item) {
			$selected = ($item['city_id'] == $selectedValue) ? ' selected ' :''; 
				   $htm.="<option  value = \"".$item['city_id']."\" $selected>".$item['name']."</option>";   
		 }
		 
		 return $htm;
	}

	
	function getDropdown($optionarray,$selectedValue)
	{
			
			$htm='';
			$flag=0;
		//	$htm = "<select id='activityinfo_type' name='activityinfo_type'>";
			foreach($optionarray as $item=>$val) {
				   $selected = ($val == $selectedValue) ? ' selected ' :''; 
				   $htm.="<option  value = \"".$val."\" $selected>".$val."</option>";  
				   $flag++;
			}
			
			return $htm;
	}
    function getacc($optionarray)
	{
		 if(!$optionarray){
           return ''; 
        }	
        $htm='';
			//$optionarray = array_unique($optionarray);
		//	$htm = "<select id='activityinfo_type' name='activityinfo_type'>";
			foreach($optionarray as $key=>$val) {
				   $htm.="<li  val='".implode('|',  array_unique($val) )."'>".$val['first_name'].' '.$val['last_name']." </li>";  
			}
			
			return $htm;
	}
    public function getParticipants($data){
        if(!$data){
           return ''; 
        }
        $i = 1;
         $htm = '<table class="table table-bordered table-striped"><thead><tr>
                    <th>Sno.</th>
                    <th>Student/Entrepreneur</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>D.O.B</th>
                    <th>M / F</th>
                    <th>Email Id</th>
                    <th>Mobile No</th>
                    <th>Fees Paid</th>
                    <th>Performance</th>
                </tr>
            </thead>
            <tbody>';
 foreach ($data as $key=>$val){
     $fee_paid = isset($val['Fee']) ? $val['Fee'] : '-' ;
            $htm .= '<tr><td>'.$i.'<td>'.$val['category'].'</td><td>'.$val['first_name'].'</td>'.
                '<td>'.$val['last_name'].'</td>'.
                '<td>'.$val['dob'].'</td>'.
                '<td>'.$val['gender'].'</td>'.
                '<td>'.$val['email'].'</td>'.
                '<td>'.$val['phone'].'</td>'.
                '<td>'.$fee_paid .'</td>'.
                '<td>'.$val['performance_remarks'].'</td></tr>';                  
                    
                $i++;
        }
                
            $htm .= '</tbody> </table>';
            return $htm;
    } 
    function getMentees($data){
        if(!$data){
           return ''; 
        }
        $htm = '';
        $i = 1;
        $htm = '<table class="table table-bordered table-striped"><thead><tr>
                    <th>Sno.</th>
                    <th>Category</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>D.O.B</th>
                    <th>M / F</th>
                    <th>Email Id</th>
                    <th>Mobile No</th>
                    <th>mentored_by</th>
                    
                </tr>
            </thead>
            <tbody>';
        foreach ($data as $key=>$val){
            $htm .= '<tr><td>'.$i.'</td><td>'.
                    $val['category'].'</td><td>'.
                    $val['first_name'].'</td>'.
                '<td>'.$val['last_name'].'</td>'.
                '<td>'.$val['dob'].'</td>'.
                '<td>'.$val['gender'].'</td>'.
                '<td>'.$val['email'].'</td>'.
                '<td>'.$val['phone'].'</td>'.
                '<td>'.$val['mentored_by'].'</td></tr>';
            $i++;
                    
                
        }
                
            $htm .= '</tbody> </table>';
            return $htm;
        
    }
    function getMentors($data){
        if(!$data){
           return ''; 
        }
        $htm = '';
        $i = 1;
        $htm = '<table class="table table-bordered table-striped"><thead><tr>
                    <th>Sno.</th>
                    <th>apellation</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>D.O.B</th>
                    <th>M / F</th>
                    <th>Email Id</th>
                    <th>Mobile No</th>
                    <th>mentoring_whom</th>
                    
                </tr>
            </thead>
            <tbody>';
        foreach ($data as $key=>$val){
            $htm .= '<tr><td>'.$i.'</td><td>'.
                    $val['category'].'</td><td>'.
                    $val['first_name'].'</td>'.
                '<td>'.$val['last_name'].'</td>'.
                '<td>'.$val['dob'].'</td>'.
                '<td>'.$val['gender'].'</td>'.
                '<td>'.$val['email'].'</td>'.
                '<td>'.$val['phone'].'</td>'.
                '<td>'.$val['mentoring_whom'].'</td></tr>';
            $i++;
                    
                
        }
                
            $htm .= '</tbody> </table>';
            return $htm;
    }
    
    function getFaculty($data,$selected=NULL){
        $selectedVals = array();
        if(!$data){
           return ''; 
        }
        $htm = '';
        
        if($selected){
           foreach($selected as $index=>$select){
            $selectedVals[] = $select['human_id'];
            } 
            
            foreach ($data as $key=>$val){
                $flag = in_array($val['id'],$selectedVals) ? 'selected' : '' ;
            $htm .= '<option '.$flag.' value="'.$val['id'].'" >'.$val['first_name'].' '.$val['last_name'].' - '.$val['email'].' </option>';  
                 }
        
        }
        else{
            foreach ($data as $key=>$val){                
            $htm .= '<option value="'.$val['id'].'" >'.$val['first_name'].' '.$val['last_name'].' - '.$val['email'].' </option>';  
                 }
        }
        
             
        
            
        
        
            return $htm;
    }
    function getInstituteList($data,$selected=NULL){
        if(!$data){
           return ''; 
        }
        $htm = '';
        foreach ($data as $key=>$val){
            $flag  = $val['id']== $selected ? 'selected' :'' ;
            $htm .= '<option value="'.$val['id'].'"  '.$flag.' >'.$val['name'].' </option>';         
        }
        
            return $htm;
    }
    function getSponserList($data){
        if(!$data){
            return '';
        }
        $htm = '';
     
        $list = explode(',',$data);
        foreach($list as $val){
            $htm.= '<li>'.str_replace(array('(',')',';'), array('amount:','','  Institute:'), $val).'</li>';
            } 
        
        return $htm;
        
        
    }
    function getPartnerList($data){
        if(!$data){
            return '';
        }
        $htm = '';
     
        $list = explode(',',$data);
        foreach($list as $val){
            $htm.= '<li>'.str_replace(array('(',')',';'), array('','','  Institute:'), $val).'</li>';
            } 
        
        return $htm;
        
        
    }
    function getSponsorInput($data){
        
         if(!$data){
            return '<div id= "contributor_sponser" class="controls form-input sponser"><input type="text" name="activitycontributor_sponsors_money[]"  id="activitycontributor_sponsors_money"   placeholder="Rs: xxxx"/><input type="text" name="activitycontributor_sponsors[]"  id="activitycontributor_sponsors"   placeholder="Institute name"/><input type="button" value="+" class="addsponser add"></div>';
        }
        $htm = '';
        $temp = array();
        $list = explode(',',$data);
        foreach($list as $val){
            $val = str_replace(array('(',')'), array('',''), $val);
            $temp = explode(';',$val);
            $htm.= '<div id= "contributor_sponser" class="controls form-input sponser">
          <input type="text" name="activitycontributor_sponsors_money[]"  id="activitycontributor_sponsors_money"   placeholder="Rs: xxxx" value="'.$temp[0].'" />
          <input type="text" name="activitycontributor_sponsors[]"  id="activitycontributor_sponsors"   placeholder="Institute name" value="'.$temp[1].'" /><input type="button" value="X" class="add remove_partner">
                
                </div>' ;
            } 
        $htm.= '<div id= "contributor_sponser" class="controls form-input sponser"><input type="text" name="activitycontributor_sponsors_money[]"  id="activitycontributor_sponsors_money"   placeholder="Rs: xxxx"/><input type="text" name="activitycontributor_sponsors[]"  id="activitycontributor_sponsors"   placeholder="Institute name"/><input type="button" value="+" class="addsponser add"></div>';
        return $htm;
    }
    function getPartnerInput($data){
        
         if(!isset($data)){
            return '<div id="contributor_partner" class="controls form-input"><input type="text" name="activitycontributor_partner[]"  id="activitycontributor_partner"   placeholder="Organisation / Institute name"/><input type="button" value="+" class="addpartner add"></div>';
        }
        $htm = '';
        $list = explode(',',$data);
        foreach($list as $val){
            $val = str_replace(array('(',')'), array('',''), $val);            
            $htm.= '<div id="contributor_partner" class="controls form-input">
             <input type="text" name="activitycontributor_partner[]"  id="activitycontributor_partner"   placeholder="Organisation / Institute name" value="'.$val.'" /><input type="button" value="X" class="add remove_partner"></div>' ;
            } 
            
        $htm.= '<div id="contributor_partner" class="controls form-input"><input type="text" name="activitycontributor_partner[]"  id="activitycontributor_partner"   placeholder="Organisation / Institute name"/><input type="button" value="+" class="addpartner add"></div>';
        return $htm;
    }
    function getVenueDropDown($venues,$selected=NULL){
        if(!$venues){
           return ''; 
        }
        $htm = '';
        foreach ($venues as $key=>$val){
            $flag  = $val['id']== $selected ? 'selected' :'' ;
            $htm .= '<option value="'.$val['id'].'"  '.$flag.' >'.$val['name'].' </option>';         
        }
        
            return $htm;
    }
    

    
    
}

