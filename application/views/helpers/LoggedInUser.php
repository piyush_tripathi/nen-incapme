<?php

class Zend_View_Helper_LoggedInUser
{
    protected $view;
    function setView($view)  { 
        $this->view = $view; 
    } 
    function loggedInUser()
    {
        $auth = Zend_Auth::getInstance(); 
        if($auth->hasIdentity()) 
        {
            $logoutUrl = $this->view->url(array('controller'=>'login','action'=>'logout'));
            $user = $auth->getIdentity(); 
            $username = $this->view->escape(ucfirst($user->first_name) . ' '.ucfirst($user->last_name));
            switch($user->role) {
                case 'ADMIN':
                    $userType = 'Administrator';
                    $link_contact = 'contact/';
                    break;
                case 'FAC':
                    $userType = 'Faculty';
                    $link_contact = 'contact/add';
                    break;
                case 'CON':
                    $userType = 'Consultant';
                    $link_contact = 'contact/add';
                    break; 
            }
            $profile_url = "/user?id=".$user->human_id;
            $string =
            '<ul class="nav btn-group">
            <li><a title="" href="'.$profile_url.'"><i class="icon icon-user"></i> <span class="text">'.$username.'</span></a></li>
            <li><span class="text user-type user-top">'.$userType.'</span></li>
            <li><a title="" href="/'.$link_contact.'"><i class="icon icon-share"></i> <span class="text contact-border">Contact</span></a></li>
            <li><a class="user-logout" href="'.$logoutUrl.'"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
            </ul>';
        } else {
            return '';
        }
        return $string;
    }
}
?>


