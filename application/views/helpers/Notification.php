    <?php

class Zend_View_Helper_Notification extends Zend_View_Helper_Abstract {

    protected $model;

    public function __construct() {
        $this->model = new Application_Model_Common();
    }

    public function notification($identifier, $type='error', $message='Unexpected Error', $clsVisibile = 'hide-block' ) {
        switch ($type) {
            case 'error':
                $clsType = 'alert-error';
                $message = '<strong>Error!! </strong>'.$message;
                break;
            case 'success':
                $clsType = 'alert-success';
                $message = '<strong>Success!! </strong>'.$message;
                break;
            case 'info':
                $clsType = 'alert-info';
                $message = '<strong>Info!! </strong>'.$message;
                break;

            default:
                break;
        }
           return '<div id="'.$identifier.'" class="alert '.$clsVisibile .' '.$clsType.'">
                    <button class="close" data-dismiss="alert">x</button>
                    '.$message.'
                </div>';
    }

}

?>
