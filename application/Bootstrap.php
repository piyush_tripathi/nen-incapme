<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    public function _initRoutes() {

        $auth = Zend_Auth::getInstance(); //fetch an instance of Zend_Auth
        $r = explode('?', $_SERVER['REQUEST_URI']);
        $uri = $r[0];
        $uriparts = explode('/', $uri);
        $u = $uriparts[1]; //
       // echo $u;
        if($u != 'login') {
            if ($auth->hasIdentity()) {
                
            }  else {
                header('Location: /login/');
            }
        }
        $config = new Zend_Config_Ini(APPLICATION_PATH."/configs/application.ini", APPLICATION_ENV);
        $dbconfig = $config->resources->db->params;
        Zend_Registry::set('dbconfig', $dbconfig);
        
        $commonModel = New Application_Model_Common();
        $user = $user = $auth->getIdentity();
        $list = $commonModel->getInstituteForUser($user->human_id, $user->role, null);
        if($user->role == 'FAC') {
            if(isset($_GET['instituteid']) &&  ($_GET['instituteid'] != $list[0]['id'])) {
                header('Location: /invalid/');
            }
        } 
        
        if($user->role == 'CON') {
            $isValidUser = false;
            foreach($list as $l) {
                if(isset($_GET['instituteid']) &&  ($_GET['instituteid'] == $l['id'])) {
                    $isValidUser = true;
                    break;
                }
            }
            if(!$isValidUser && isset($_GET['instituteid'])) {
                header('Location: /invalid/');
            }
            
        }
    }

}

